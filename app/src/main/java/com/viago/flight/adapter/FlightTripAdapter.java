package com.viago.flight.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.viago.R;
import com.viago.flight.model.FlightTripModel;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class FlightTripAdapter extends RecyclerView.Adapter<FlightTripAdapter.MyViewHolder> {

    private List<FlightTripModel> flightTripModelList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView originCityShortName, originCityFullName, destinationCityShortName,destinationCityFullName;

        public MyViewHolder(View view) {
            super(view);
            originCityShortName = (TextView) view.findViewById(R.id.originCityShortName);
            originCityFullName = (TextView) view.findViewById(R.id.originCityFullName);
            destinationCityShortName = (TextView) view.findViewById(R.id.destinationCityShortName);
            destinationCityFullName = (TextView) view.findViewById(R.id.destinationCityFullName);
        }
    }


    public FlightTripAdapter(List<FlightTripModel> moviesList) {
        this.flightTripModelList = moviesList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.flight_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FlightTripModel flightTripModel = flightTripModelList.get(position);
        holder.originCityShortName.setText(flightTripModel.getOriginCityShortName());
        holder.originCityFullName.setText(flightTripModel.getOriginCityFullName());
        holder.destinationCityShortName.setText(flightTripModel.getDestinationCityShortName());
        holder.destinationCityFullName.setText(flightTripModel.getDestinationCityFullName());
    }

    @Override
    public int getItemCount() {
        return flightTripModelList.size();
    }
}