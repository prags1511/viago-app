package com.viago.flight.screen

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import bindView
import com.google.android.material.tabs.TabLayout
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.baseclasses.NavItemClickEvent
import com.viago.extensions.hide
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.notifications.Notification
import com.viago.notifications.NotificationsFragment
import com.viago.notifications.NotificationsUtil
import com.viago.utils.Constants
import com.viago.utils.GeneralUtils
import com.viago.utils.SharedPrefsUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode



class Flight : BaseDrawerActivity() {
    private var menu: Menu? = null
    private val userToken by lazy { UserUtil.getUserToken() }
    private var list = ArrayList<Notification>()
    private var nextUrl :String?=null
    private var oneWayBtn:LinearLayout?=null
//    private val oneWay = findViewById<LinearLayout?>(R.id.onewaybtn)?.findViewById<android.widget.Button?>(R.id.roudedcorner)
//    private val roundTripBtn = findViewById<LinearLayout?>(R.id.roundtripbtn)
//    private val roundTrip = findViewById<LinearLayout?>(R.id.roundtripbtn)?.findViewById<android.widget.Button?>(R.id.roudedcorner)
//    private val multicityBtn = findViewById<LinearLayout?>(R.id.multicitybtn)
//    private val multicity = findViewById<LinearLayout?>(R.id.multicitybtn)?.findViewById<android.widget.Button?>(R.id.roudedcorner)
//    private val roundreturndately = findViewById<LinearLayout?>(R.id.roundreturndately)
//    private val plane_return = findViewById<android.widget.ImageView?>(R.id.plane_return)
//
//    private val oneroundly = findViewById<LinearLayout?>(R.id.oneroundly)
//    private val onerounddately = findViewById<LinearLayout?>(R.id.onerounddately)
//    private val multicityly = findViewById<LinearLayout?>(R.id.multicityly)

    override fun getLayoutRes() = R.layout.activity_flights

    fun clickFlights(view: View?) {
        startActivity(Intent(this, FlightsActivity::class.java))
    }



    override fun onCreated(savedInstanceState: Bundle?) {

       // setContentView(getLayoutRes())
        oneWayBtn = findViewById<LinearLayout?>(R.id.onewaybtn)
    }


    override fun onBackPressed() {
            super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()

        var unreadCount: Int = 0
        list.forEach {
            if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + it.trip+it.created_on, false))
                unreadCount++
        }

        if(unreadCount!=0){
            SharedPrefsUtils.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, unreadCount )
        }else if (notificationMenuCountView != null && NOTIFICATION_COUNT!=-1){
            notificationMenuCountView!!.hide()
        }
        NOTIFICATION_COUNT = SharedPrefsUtils.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, -1)

        if (NOTIFICATION_COUNT > 0) {
            if (notificationMenuCountView != null) {
                if(NOTIFICATION_COUNT>20){
                    notificationMenuCountView!!.text = "20+"
                }else {
                    notificationMenuCountView!!.text = NOTIFICATION_COUNT.toString()
                }
            }
        }

    }

    val onSuccess = { baseListModel: BaseListModel<Notification> ->

        //notificationCountView.hide()
        notificationMenuCountView!!.hide()

        nextUrl = baseListModel.next

        baseListModel.results.forEach {

            list.add(it)

        }

        if (!nextUrl.isNullOrEmpty()) {

            call()
        }else{

            var unreadCount: Int = 0
            list.forEach {
                if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + it.trip+it.created_on, false))
                    unreadCount++
            }
            SharedPrefsUtils.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, unreadCount)

            refreshNotificationCount()
        }
    }



    val onFailure: (String) -> Unit = {
        refreshNotificationCount()
    }


    fun call():Unit{

        NotificationsUtil.getMoreNotifications(userToken, nextUrl!!, onSuccess, onFailure)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        this.menu = menu
        if( notificationMenuCountView==null) {
            notificationView = menu!!.findItem(R.id.action_notifications).getActionView()
        }
        if(notificationMenuCountView==null) {
            notificationMenuCountView = notificationView!!.findViewById<TextView>(R.id.notification_count) as TextView
        }

        list = ArrayList<Notification>()
        NotificationsUtil.getMyNotifications(userToken, onSuccess, onFailure)

        notificationView!!.setOnClickListener { startActivity(Intent(this, NotificationsFragment::class.java)) }

        return true
    }



    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        toolbar.title = null
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_add_person -> {
            GeneralUtils.performInviteAction(this,UserUtil.getUser()?.name)
            true
        }
        R.id.action_notifications -> {
            startActivity(Intent(this, NotificationsFragment::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }



    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onNavItemClick(navItemClickEvent: NavItemClickEvent?) {
        if (supportFragmentManager.backStackEntryCount > 0)
            super.onBackPressed()
        when (navItemClickEvent?.itemId) {
//            R.id.action_create_trip -> viewPager.currentItem = 0
       //     R.id.action_my_trips -> viewPager.currentItem = 1
//            R.id.action_book_now -> viewPager.currentItem = 2
            R.id.action_notifications -> startActivity(Intent(this, NotificationsFragment::class.java))
        }
        EventBus.getDefault().removeStickyEvent(navItemClickEvent)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }
}