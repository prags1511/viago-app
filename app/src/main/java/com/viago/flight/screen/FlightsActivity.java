package com.viago.flight.screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.viago.R;
import com.viago.baseclasses.BaseDrawerActivity;
import com.viago.baseclasses.NavItemClickEvent;
import com.viago.flight.adapter.FlightTripAdapter;
import com.viago.flight.model.FlightTripModel;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class FlightsActivity extends BaseDrawerActivity {
    private LinearLayout oneWayBtn, roundTripBtn, multicityBtn, oneroundly, onerounddately, multicityly, roundreturndately ;
    private Button oneWay, roundTrip, multicity, backbutton;
    private List<FlightTripModel> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private FlightTripAdapter mAdapter;
    private ImageView plane_return;


    private void prepareFlightData() {
        FlightTripModel flightTripModel = new FlightTripModel();
        flightTripModel.setOriginCityShortName("DEL");
        flightTripModel.setOriginCityFullName("Delhi");
        flightTripModel.setDestinationCityShortName("MUM");
        flightTripModel.setDestinationCityFullName("Mumbai");
        movieList.add(flightTripModel);

        flightTripModel = new FlightTripModel();
        flightTripModel.setOriginCityShortName("MUM");
        flightTripModel.setOriginCityFullName("Mumbai");
        flightTripModel.setDestinationCityShortName("DUB");
        flightTripModel.setDestinationCityFullName("Dubai");
        movieList.add(flightTripModel);

        flightTripModel = new FlightTripModel();
        flightTripModel.setOriginCityShortName("DUB");
        flightTripModel.setOriginCityFullName("Dubai");
        flightTripModel.setDestinationCityShortName("DEL");
        flightTripModel.setDestinationCityFullName("Delhi");
        movieList.add(flightTripModel);

        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreated(@Nullable Bundle savedInstanceState) {
        oneWayBtn = findViewById(R.id.onewaybtn);
        oneWay = findViewById(R.id.onewaybtn).findViewById(R.id.roudedcorner);
        roundTripBtn = findViewById(R.id.roundtripbtn);
        roundTrip = findViewById(R.id.roundtripbtn).findViewById(R.id.roudedcorner);
        multicityBtn = findViewById(R.id.multicitybtn);
        multicity = findViewById(R.id.multicitybtn).findViewById(R.id.roudedcorner);
        roundreturndately = findViewById(R.id.roundreturndately);
        plane_return = findViewById(R.id.plane_return);
        backbutton = findViewById(R.id.backbutton);

        oneroundly = findViewById(R.id.oneroundly);
        onerounddately = findViewById(R.id.onerounddately);
        multicityly = findViewById(R.id.multicityly);

        oneWayBtn.setSelected(true);
        oneWay.setTextColor(Color.WHITE);
        // oneroundly, onerounddately, multicityly
        oneWayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneWayBtn.setSelected(true);
                roundTripBtn.setSelected(false);
                multicityBtn.setSelected(false);
                oneWay.setTextColor(Color.WHITE);
                roundTrip.setTextColor(Color.BLACK);
                multicity.setTextColor(Color.BLACK);
                oneroundly.setVisibility(View.VISIBLE);
                onerounddately.setVisibility(View.VISIBLE);
                multicityly.setVisibility(View.GONE);
                roundreturndately.setVisibility(View.GONE);
                plane_return.setVisibility(View.GONE);
            }
        });

        roundTripBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneWayBtn.setSelected(false);
                roundTripBtn.setSelected(true);
                multicityBtn.setSelected(false);
                oneWay.setTextColor(Color.BLACK);
                roundTrip.setTextColor(Color.WHITE);
                multicity.setTextColor(Color.BLACK);
                oneroundly.setVisibility(View.VISIBLE);
                onerounddately.setVisibility(View.VISIBLE);
                multicityly.setVisibility(View.GONE);
                roundreturndately.setVisibility(View.VISIBLE);
                plane_return.setVisibility(View.VISIBLE);
            }
        });

        multicityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oneWayBtn.setSelected(false);
                roundTripBtn.setSelected(false);
                multicityBtn.setSelected(true);
                oneWay.setTextColor(Color.BLACK);
                roundTrip.setTextColor(Color.BLACK);
                multicity.setTextColor(Color.WHITE);
                oneroundly.setVisibility(View.GONE);
                onerounddately.setVisibility(View.GONE);
                multicityly.setVisibility(View.VISIBLE);
            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        mAdapter = new FlightTripAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareFlightData();
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_flights;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        getToolbar().setTitle(null);
        return super.onPrepareOptionsMenu(menu);
    }


}
