package com.viago.flight.model;

public class FlightTripModel {
    private String originCityShortName;
    private String originCityFullName;
    private String destinationCityShortName;
    private String destinationCityFullName;

    public String getOriginCityShortName() {
        return originCityShortName;
    }

    public void setOriginCityShortName(String originCityShortName) {
        this.originCityShortName = originCityShortName;
    }

    public String getOriginCityFullName() {
        return originCityFullName;
    }

    public void setOriginCityFullName(String originCityFullName) {
        this.originCityFullName = originCityFullName;
    }

    public String getDestinationCityShortName() {
        return destinationCityShortName;
    }

    public void setDestinationCityShortName(String destinationCityShortName) {
        this.destinationCityShortName = destinationCityShortName;
    }

    public String getDestinationCityFullName() {
        return destinationCityFullName;
    }

    public void setDestinationCityFullName(String destinationCityFullName) {
        this.destinationCityFullName = destinationCityFullName;
    }

}
