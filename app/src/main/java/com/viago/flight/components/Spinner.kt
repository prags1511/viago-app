package com.mobile.travel

import android.content.Context
import android.util.AttributeSet
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import com.viago.R


class Spinner(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    init {
        inflate(context, R.layout.spinner, this)

        val editText: EditText = findViewById(R.id.edittext)
        val spinner: Spinner = findViewById(R.id.spinner)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.spinner)
        editText.setHint(attributes.getString(R.styleable.spinner_edittext_hint))
        attributes.recycle()

    }
}