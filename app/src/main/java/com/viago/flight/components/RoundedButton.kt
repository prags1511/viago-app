package com.mobile.travel

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.Button
import android.widget.LinearLayout
import com.viago.R

class RoundedButton(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    init {
        inflate(context, R.layout.rounded_button, this)
        val rounded_button: Button = findViewById(R.id.roudedcorner)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.rounded_button)
        rounded_button.width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, attributes.getInteger(R.styleable.rounded_button_rounded_button_width, 110).toFloat(), resources.displayMetrics).toInt()
        rounded_button.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, attributes.getInteger(R.styleable.rounded_button_rounded_button_height, 30).toFloat(), resources.displayMetrics).toInt()
        rounded_button.text = attributes.getString(R.styleable.rounded_button_rounded_button_text)
        rounded_button.background = attributes.getDrawable(R.styleable.rounded_button_rounded_button_background)
        rounded_button.setTextColor(attributes.getInteger(R.styleable.rounded_button_rounded_button_text_color, 0))
        attributes.recycle()

    }
}