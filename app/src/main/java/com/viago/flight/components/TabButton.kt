package com.mobile.travel

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.viago.R

class TabButton(context: Context, attrs: AttributeSet): LinearLayout(context, attrs) {

    init {
        inflate(context, R.layout.tab_button, this)

        val imageView: ImageView = findViewById(R.id.image)
        val textView: TextView = findViewById(R.id.text)

        val attributes = context.obtainStyledAttributes(attrs, R.styleable.tab_button)
        imageView.setImageDrawable(attributes.getDrawable(R.styleable.tab_button_image))
        textView.text = attributes.getString(R.styleable.tab_button_text)
        attributes.recycle()

    }
}