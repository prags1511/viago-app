package com.viago.services;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import androidx.annotation.Nullable;
import android.util.Log;

import com.viago.models.Contact;
import com.viago.retrofitservice.UserService;
import com.viago.utils.ApiUtils;
import com.viago.utils.ContactsGetter;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.os.Process.THREAD_PRIORITY_BACKGROUND;

/**
 * Created by mayank on 29/10/16.
 */

public class DataSyncService extends Service {

    private static final String TAG = "DataSyncService";
    private String apiKey;

    private ServiceHandler mServiceHandler;

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {
        ServiceHandler(Looper looper) {
            super(looper);
        }
        @Override
        public void handleMessage(Message msg) {
            retrieveContacts(msg);
        }
    }

    @Override
    public void onCreate() {
        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "service starting");
        apiKey = intent.getStringExtra("apiKey");


        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        // If we get killed, after returning from here, restart
        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void retrieveContacts(final Message msg){
        new ContactsGetter(this, new ContactsGetter.ContactsLoadListener() {
            @Override
            public void onLoadFinished(ArrayList<Contact> contactsData) {
                if (contactsData != null) {
                    Log.d(TAG, "contacts count: " + contactsData.size());
                    UserService userService = ApiUtils.INSTANCE.getRetrofitInstance().create(UserService.class);
                    Call<JSONObject> postContactsCall = userService.postContacts(apiKey, contactsData);
                    postContactsCall.enqueue(new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            Log.d(TAG, "response");
                            stopSelf(msg.arg1);
                        }

                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {
                            Log.d(TAG, "failure");
                            stopSelf(msg.arg1);
                        }
                    });
                }
            }
        });
    }

}
