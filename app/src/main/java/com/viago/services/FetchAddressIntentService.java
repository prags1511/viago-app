package com.viago.services;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import com.viago.utils.Constants;
import com.viago.utils.SharedPrefsUtils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * An {@link IntentService} subclass for getting location address using latitude & longitude
 */
public class FetchAddressIntentService extends IntentService {

    public static final String PACKAGE_NAME =
            "com.travel";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME +
            ".LOCATION_DATA_EXTRA";

    public FetchAddressIntentService() {
        super("FetchAddressIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String errorMessage = "";

        // Get the location passed to this service through an extra.
        Location location = intent.getParcelableExtra(
                LOCATION_DATA_EXTRA);

        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = "Service not available";
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "Invalid lat long";
        }

        if (addresses != null && addresses.size()  != 0) {
            Address address = addresses.get(0);
            StringBuilder addressFragments = new StringBuilder();

            // build address string
            if(address.getSubLocality()!=null) {
                addressFragments.append(address.getSubLocality()).append(",");
            }
            if(address.getLocality()!=null) {
                addressFragments.append(address.getLocality()).append(",");
            }
            if(address.getAdminArea()!=null) {
                addressFragments.append(address.getAdminArea()).append(",");
            }
            if(address.getCountryName()!=null) {
                addressFragments.append(address.getCountryName());
            }

            // save in shared preferences
            SharedPrefsUtils.INSTANCE.setStringPreference(Constants.KEY_LOCATION, addressFragments.toString());
        }
    }
}
