package com.viago.services;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
import com.viago.login.UserUtil;
import com.viago.models.Gcm;
import com.viago.retrofitservice.DeviceService;
import com.viago.utils.ApiUtils;
import com.viago.utils.Constants;
import com.viago.utils.SharedPrefsUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyFirebaseInstanceIDService {

}

/**
 * Created by mayank on 2/9/16.
 *//*
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Timber.d(refreshedToken);

       // sendRegistrationToServer(getApplicationContext(), refreshedToken);
    }
    public void sendRegistrationToServer(Context context, final String refreshedToken) {
        Timber.d("in send register");

        if (TextUtils.isEmpty(refreshedToken)) {
            Timber.d("empty");
            return;
        }

        if (!UserUtil.INSTANCE.isLoggedIn()) return;

        String userToken = UserUtil.INSTANCE.getUserToken();

        if (TextUtils.isEmpty(userToken)) return;

        DeviceService deviceService = ApiUtils.INSTANCE.getRetrofitInstance().create(DeviceService.class);
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Timber.d("id: " + android_id);
        Gcm gcm = new Gcm(SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_NOTIFICATION_ENABLED, true), android_id, refreshedToken);
        Call<Gcm> registerGcmCall;
        if (SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, true)) {
            registerGcmCall = deviceService.registerGcm(userToken, gcm);
        } else {
            registerGcmCall = deviceService.updateGcm(userToken,
                    SharedPrefsUtils.INSTANCE.getStringPreference(Constants.KEY_GCM_TOKEN),
                    gcm);
        }
        registerGcmCall.enqueue(new Callback<Gcm>() {
            @Override
            public void onResponse(Call<Gcm> call, Response<Gcm> response) {
                if (response.isSuccessful()) {
                    Timber.d("response successful");
                    SharedPrefsUtils.INSTANCE.setStringPreference(Constants.KEY_GCM_TOKEN, refreshedToken);
                    if (SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, true)) {
                        SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, false);
                    }
                    SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, false);
                } else {
                    try {
                        Timber.d("response error: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, true);
                }
            }

            @Override
            public void onFailure(Call<Gcm> call, Throwable t) {
                Timber.d("response failed: " + t.getMessage());
                SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, true);
            }
        });
    }
}*/
