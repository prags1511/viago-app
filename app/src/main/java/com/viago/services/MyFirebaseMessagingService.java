package com.viago.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.viago.R;
import com.viago.activities.MainActivity;
import com.viago.login.UserUtil;
import com.viago.models.Gcm;
import com.viago.retrofitservice.DeviceService;
import com.viago.utils.ApiUtils;
import com.viago.utils.Constants;
import com.viago.utils.SharedPrefsUtils;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by mayank on 2/9/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

       // writeToFile(remoteMessage.getFrom()+"---"+remoteMessage.getData()+"-----"+remoteMessage.getNotification(),this);

        if (remoteMessage.getData().size() > 0) {
              Log.d(TAG, "Message data payload: " + remoteMessage.getData());
          //  Log.d(TAG, "Notification data payload: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getData().get("trip_id"),0);
            SharedPrefsUtils.INSTANCE.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, SharedPrefsUtils.INSTANCE.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, 0) + 1);
        }

        /*else if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Notification data payload: " + remoteMessage.getNotification().getBody());
            sendNotification(remoteMessage.getNotification().getBody(),0);
            SharedPrefsUtils.INSTANCE.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, SharedPrefsUtils.INSTANCE.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, 0) + 1);
        }*/
    }

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        sendRegistrationToServer(getApplicationContext(), refreshedToken);

    }

    public void sendRegistrationToServer(Context context, final String refreshedToken) {
        Timber.d("in send register");

        if (TextUtils.isEmpty(refreshedToken)) {
            Timber.d("empty");
            return;
        }

        if (!UserUtil.INSTANCE.isLoggedIn()) return;

        String userToken = UserUtil.INSTANCE.getUserToken();

        if (TextUtils.isEmpty(userToken)) return;

        DeviceService deviceService = ApiUtils.INSTANCE.getRetrofitInstance().create(DeviceService.class);
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        Timber.d("id: " + android_id);
        Gcm gcm = new Gcm(SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_NOTIFICATION_ENABLED, true), android_id, refreshedToken);
        Call<Gcm> registerGcmCall;
        if (SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, true)) {
            registerGcmCall = deviceService.registerGcm(userToken, gcm);
        } else {
            registerGcmCall = deviceService.updateGcm(userToken,
                    SharedPrefsUtils.INSTANCE.getStringPreference(Constants.KEY_GCM_TOKEN),
                    gcm);
        }
        registerGcmCall.enqueue(new Callback<Gcm>() {
            @Override
            public void onResponse(Call<Gcm> call, Response<Gcm> response) {
                if (response.isSuccessful()) {
                    Timber.d("response successful");
                    SharedPrefsUtils.INSTANCE.setStringPreference(Constants.KEY_GCM_TOKEN, refreshedToken);
                    if (SharedPrefsUtils.INSTANCE.getBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, true)) {
                        SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_IS_FIRST_GCM_TOKEN, false);
                    }
                    SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, false);
                } else {
                    try {
                        Timber.d("response error: " + response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, true);
                }
            }

            @Override
            public void onFailure(Call<Gcm> call, Throwable t) {
                Timber.d("response failed: " + t.getMessage());
                SharedPrefsUtils.INSTANCE.setBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, true);
            }
        });
    }

  /*  @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);

        // sendNotification(intent.getExtras().getString("trip_id"),intent.getExtras().getInt("google.message_id"));
       // SharedPrefsUtils.INSTANCE.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, SharedPrefsUtils.INSTANCE.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, 0) + 1);
    }*/

    private void sendNotification(String tripId,int messageid) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra("trip_id", tripId);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Trip suggestion")
                .setContentText("You got suggestion on one of your trips")
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(messageid /* ID of notification */, notificationBuilder.build());
    }

   /* private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("config.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }*/
}
