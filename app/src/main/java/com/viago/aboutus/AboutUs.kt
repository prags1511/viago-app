package com.viago.aboutus

/**
 * Created by mayank on 8/10/17.
 */
data class AboutUs(
        val image: String?,
        val content: String
)