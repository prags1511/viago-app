package com.viago.aboutus

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.extensions.loadImage
import com.viago.extensions.showToast

/**
 * Created by mayank on 22/8/17.
 */
class AboutUsActivity: BaseDrawerActivity() {

  //  val image by bindView<ImageView>(R.id.image)
    val content by bindView<TextView>(R.id.content)

    override fun getLayoutRes() = R.layout.activity_about_us

    override fun onCreated(savedInstanceState: Bundle?) {
        toolbar.title = ""
       // image.loadImage(R.drawable.office)

        content.text = getString(R.string.about_us);

//        AboutUsUtil.getAboutUs({
//          //  image.loadImage(it.image)
//            content.text = it.content
//        }, {
//            showToast(it)
//        })
    }
}