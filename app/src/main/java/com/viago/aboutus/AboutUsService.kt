package com.viago.aboutus

import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by mayank on 7/9/17.
 */
internal interface AboutUsService {

    @GET("trips/about-us/")
    fun getAboutUs(): Call<AboutUs>
}