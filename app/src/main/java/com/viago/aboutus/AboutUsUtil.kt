package com.viago.aboutus

import com.viago.utils.ApiUtils
import com.viago.utils.getFailureMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mayank on 8/9/17.
 */
object AboutUsUtil {

    private val aboutUsService = ApiUtils.retrofitInstance.create(AboutUsService::class.java)

    fun getAboutUs(onSuccess: (AboutUs) -> Unit, onFailure: (String) -> Unit) {

        aboutUsService.getAboutUs()
                .enqueue(object: Callback<AboutUs> {
                    override fun onResponse(call: Call<AboutUs>?, response: Response<AboutUs>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Cannot get notifications. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<AboutUs>?, t: Throwable?) {
                        onFailure("Cannot get notifications. Please try again")
                    }
                })
    }
}