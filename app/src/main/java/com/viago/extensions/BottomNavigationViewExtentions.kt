package com.viago.extensions

import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
//import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.viewpager.widget.ViewPager
import timber.log.Timber


/**
 * Created by mayank on 29/7/17.
 */
/*fun BottomNavigationView.disableShiftMode() {
    val menuView = getChildAt(0) as BottomNavigationMenuView
    try {
        val shiftingMode = menuView.javaClass.getDeclaredField("mShiftingMode")
        shiftingMode.isAccessible = true
        shiftingMode.setBoolean(menuView, false)
        shiftingMode.isAccessible = false
        for (i in 0 until menuView.childCount) {
            val item = menuView.getChildAt(i) as BottomNavigationItemView

            item.setShiftingMode(false)
            // set once again checked value, so view will be updated

            item.setChecked(item.itemData.isChecked)
        }
    } catch (e: NoSuchFieldException) {
        Timber.e("Unable to get shift mode field", e)
    } catch (e: IllegalAccessException) {
        Timber.e("Unable to change value of shift mode", e)
    }
}*/

/*
fun BottomNavigationView.bindViewPager (viewPager: ViewPager) {

    val menuItemIds = Array(menu.size(), { i -> menu.getItem(i).itemId })

    setOnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            in menuItemIds -> {
                viewPager.currentItem = menuItemIds.indexOf(item.itemId)
                true
            }
            else -> false
        }
    }


    viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
        var viewPagerPosition = 0

        override fun onPageSelected(position: Int) {
            menu.getItem(viewPagerPosition).setChecked(false);
            menu.getItem(position).setChecked(true);
            viewPagerPosition = position
        }
    })
}
*/
