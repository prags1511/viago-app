package com.viago.extensions

import android.view.View

/**
 * Created by mayank on 11/10/16.
 */

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}

fun View.isShowing() = visibility == View.VISIBLE

fun View.isHidden() = visibility == View.GONE

fun View.toggleVisibility() {
    if (isShowing()) hide() else show()
}