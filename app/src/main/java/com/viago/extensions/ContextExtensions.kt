package com.viago.extensions

import android.content.Context
import androidx.annotation.StringRes
import android.util.TypedValue
import android.widget.Toast

/**
 * Created by mayank on 23/8/17.
 */

fun Context.dpToPx(dp: Int): Int = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), resources.displayMetrics).toInt()

fun Context.pxToDp(px: Int): Int = (px / resources.displayMetrics.density).toInt()

fun Context.showToast(text: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, text, duration).show()
}

fun Context.showToast(@StringRes resId: Int, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, resId, duration).show()
}