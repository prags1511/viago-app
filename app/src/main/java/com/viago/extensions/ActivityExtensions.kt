@file:JvmName("KeyboardUtils")
package com.viago.extensions

import android.app.Activity
import android.content.Context
import android.os.Handler
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager


/**
 * Created by mayank on 8/10/16.
 */

fun Activity.closeKeyboard() {
    val view : View? = currentFocus
    view?.let {
        val inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun Activity.openKeyboard() {
    Handler().post {
        val view = currentFocus
        view?.let {
            val inputMethodManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(view, 0)
        }
    }
}

fun AppCompatActivity.openFragment(fragment: Fragment, @IdRes containerViewId: Int = android.R.id.content, addToBackStack: Boolean = true) {
    supportFragmentManager.beginTransaction()
            .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            .add(containerViewId, fragment, fragment.javaClass.simpleName)
            .apply { if (addToBackStack) addToBackStack(fragment.javaClass.simpleName) }
            .commit()
}

