package com.viago.extensions

import androidx.annotation.LayoutRes
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by mayank on 11/8/17.
 */

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, layoutInflater: LayoutInflater = LayoutInflater.from(context)): View {
    return layoutInflater.inflate(layoutRes, this, false)
}