package com.viago.extensions

import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.signature.ObjectKey
import com.viago.R

/**
 * Created by mayank on 29/7/17.
 */

fun ImageView.loadImage(url: Any?, showPlaceHolder: Boolean = true, onDrawableReadyListener: OnDrawableReadyListener? = null) {
    Glide.with(context)
            .load(url)
            .run {
        if (showPlaceHolder) apply(RequestOptions().placeholder(R.drawable.placeholder))

        onDrawableReadyListener?.let {
            listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean) = false

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                    resource?.let { onDrawableReadyListener.onDrawableReady(it) }
                    return false
                }

            })
        }
        into(this@loadImage)
    }
}

interface OnDrawableReadyListener {
    fun onDrawableReady(drawable: Drawable)
}