package com.viago.extensions

import androidx.fragment.app.Fragment

/**
 * Created by mayank on 29/7/17.
 */

fun Fragment.dpToPx(dp: Int): Int = context!!.dpToPx(dp)

fun Fragment.pxToDp(px: Int): Int = context!!.pxToDp(px)
