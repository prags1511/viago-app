package com.viago.extensions

import androidx.annotation.LayoutRes
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View
import android.widget.EditText

/**
 * Created by mayank on 16/8/17.
 */

fun EditText.enableRightDrawablePasswordToggle(@LayoutRes compoundDrawableRightPasswordShown: Int,
                                               @LayoutRes compoundDrawableRightPasswordNotShown: Int,
                                               @LayoutRes compoundDrawableLeft: Int = 0,
                                               @LayoutRes compoundDrawableTop: Int = 0,
                                               @LayoutRes compoundDrawableBottom: Int = 0) {
    setOnTouchListener(OnEditTextPasswordTouchListener(compoundDrawableLeft,
            compoundDrawableTop,
            compoundDrawableRightPasswordShown,
            compoundDrawableRightPasswordNotShown,
            compoundDrawableBottom))
}

private class OnEditTextPasswordTouchListener(
        @LayoutRes private val drawableLeft: Int,
        @LayoutRes private val drawableTop: Int,
        @LayoutRes private val drawableRightPasswordShown: Int,
        @LayoutRes private val drawableRightPasswordNotShown: Int,
        @LayoutRes private val drawableBottom: Int
) : View.OnTouchListener {

    private var isPasswordVisible: Boolean = true

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        val DRAWABLE_RIGHT = 2

        var isHandlingTouch = false

        with(v as EditText) {
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= right - compoundDrawables[DRAWABLE_RIGHT].bounds.width() + paddingRight) {
                    if (isPasswordVisible) {
                        transformationMethod = HideReturnsTransformationMethod.getInstance()
                        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRightPasswordNotShown, drawableBottom)
                        isPasswordVisible = false
                    } else {
                        transformationMethod = PasswordTransformationMethod.getInstance()
                        setCompoundDrawablesWithIntrinsicBounds(drawableLeft, drawableTop, drawableRightPasswordShown, drawableBottom)
                        isPasswordVisible = true
                    }

                    setSelection(text.length)

                    isHandlingTouch = true
                }
            }
        }

        return isHandlingTouch
    }
}
