package com.viago.maputils

import android.content.Context
import android.graphics.Color
import android.util.Log
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.viago.R
import com.viago.maputils.models.GoogleMapsRoute
import com.viago.trips.createtrip.placesearchservices.GoogleMapsService
import com.viago.utils.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.util.concurrent.TimeUnit

/**
 * Created by mayank on 21/8/17.
 */

class MapsRouteUtil(private val context: Context, private val map: GoogleMap) {

    fun showRoute(sourceLatLng: LatLng,
                  destLatLng: LatLng,
                  waypoints: List<LatLng>?,
                  @ColorRes colorRes: Int = R.color.colorPrimaryDark,
                  onRouteValuesObtained: ((distanceKm: String, distanceMile: String, duration: String, bounds: LatLngBounds?) -> Unit)? = null,
                  optimized: Boolean = false,
                  onOptimizedWaypointOrderReceived: ((List<Int>) -> Unit)? = null) {

        val googleMapsService = ApiUtils.retrofitInstance.create(GoogleMapsService::class.java)
        val googleMapsRouteCall: Call<GoogleMapsRoute>
        if (waypoints == null || waypoints.isEmpty()) {
            googleMapsRouteCall = googleMapsService.getRoute(sourceLatLng.latitude.toString() + "," + sourceLatLng.longitude,
                    destLatLng.latitude.toString() + "," + destLatLng.longitude)
        } else {
            googleMapsRouteCall = googleMapsService.getRouteWithWaypoints(sourceLatLng.latitude.toString() + "," + sourceLatLng.longitude,
                    destLatLng.latitude.toString() + "," + destLatLng.longitude,
                    "optimize:$optimized|enc:" + PolylineUtils.encode(waypoints) + ":")
        }

        googleMapsRouteCall.enqueue(object : Callback<GoogleMapsRoute> {
            override fun onResponse(call: Call<GoogleMapsRoute>, response: Response<GoogleMapsRoute>) {
                if (response.isSuccessful) {
                    Timber.d("success")

                    val googleMapsRoute = response.body()
                    val route = googleMapsRoute.routes?.firstOrNull()
                    if (route != null) {
                        val polylineOptions = PolylineOptions().color(ContextCompat.getColor(context, colorRes))
                        polylineOptions.addAll(PolylineUtils.decode(route.overviewPolyline.points))
                        polylineOptions.color(Color.parseColor("#21618c"))
                        polylineOptions.width(10f)
                        map.addPolyline(polylineOptions)
                        Log.d("route*",route.overviewPolyline.toString())
                        onRouteValuesObtained?.let {
                            val distanceInMetres = route?.legs.sumBy { leg -> leg.distance.value }
                            Log.d("distanceInMetres*","hello")
                            val durationInSeconds = route?.legs.sumBy { leg -> leg.duration.value }
                            Log.d("durationInSeconds*",Integer.toString(durationInSeconds))
                            it.invoke(getReadableDistanceKm(distanceInMetres),
                                    getReadableDistanceMile(distanceInMetres),
                                    getReadableTime(durationInSeconds),
                                    LatLngBounds(route.bounds?.southwest?.toLatLng(), route.bounds?.northeast?.toLatLng()))
                        }
                        Log.d("waypoint**",route.waypointOrder.toString())
                        onOptimizedWaypointOrderReceived?.invoke(route.waypointOrder)
                    } else {
                        Timber.d("no route found")
                        onOptimizedWaypointOrderReceived?.invoke(emptyList())
                    }

                } else {
                    try {
                        onOptimizedWaypointOrderReceived?.invoke(emptyList())
                        Timber.d("unsuccessful " + response.errorBody().string())
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<GoogleMapsRoute>, t: Throwable) {
                val polylineOptions = PolylineOptions().color(ContextCompat.getColor(context, colorRes))
                polylineOptions.add(sourceLatLng)
                polylineOptions.addAll(waypoints)
                polylineOptions.add(destLatLng)
                polylineOptions.color(Color.parseColor("#21618c"))
                polylineOptions.width(10f)
                map.addPolyline(polylineOptions)
                val wayList: MutableList<Int> = mutableListOf()
                var i=0;
                if (waypoints != null) {
                    while(i < waypoints.size){
                        wayList.add(i)
                        i++
                    }
                }
                Log.d("polylineOptions*",polylineOptions.points.toString())
                onRouteValuesObtained?.let { null }
Log.d("list* ",wayList.toString())
                onOptimizedWaypointOrderReceived?.invoke(wayList)
              //  onOptimizedWaypointOrderReceived?.invoke(emptyList())
                //Timber.d("failed")
            }
        })
    }

    private fun getReadableDistanceKm(distanceInMetres: Int): String {
        var readableDistance = "%.2f".format(distanceInMetres / 1000.0)
        return readableDistance + " km"
    }

    private fun getReadableDistanceMile(distanceInMetres: Int): String {
        var readableDistance = "%.2f".format(distanceInMetres * 0.000621371192)
        return readableDistance + " miles"
    }

    private fun getReadableTime(durationInSeconds: Int): String {
        var readableTime = ""
        with(TimeUnit.SECONDS) {
            var days = toDays(durationInSeconds.toLong())
            var hoursLong = toHours(durationInSeconds.toLong()) - days * 24
            var minutes = toMinutes(durationInSeconds.toLong()) - (hoursLong + days * 24) * 60

            var hours = ""

            if(days == 0L && hoursLong == 0L){

                if(minutes>30){

                    hours = "1"

                }else {

                    hours = "0.5"

                }

            }

            if (days > 0) readableTime = readableTime + days + if (days == 1L) " Day " else " Days "
            if (hoursLong > 0) readableTime = readableTime + hoursLong + if (hoursLong == 1L) " Hr" else " Hrs"
            if (!hours.isEmpty()) readableTime = readableTime + hours +  " Hr"
            //if (minutes > 0) readableTime = readableTime + minutes + if (minutes == 1L) " Min " else " Mins "
        }

        return readableTime.trim()
    }
}
