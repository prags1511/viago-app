package com.viago.baseclasses

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.github.inflationx.viewpump.ViewPumpContextWrapper

//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper

/**
 * Created by mayank on 20/9/17.
 */
open class BaseActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context?) {
        Log.d("BaseActivity*", newBase.toString())
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!))
    }
}