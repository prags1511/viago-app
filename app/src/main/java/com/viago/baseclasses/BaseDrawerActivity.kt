package com.viago.baseclasses

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.LayoutRes
import com.google.android.material.navigation.NavigationView
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.aboutus.AboutUsActivity
import com.viago.activities.MainActivity
import com.viago.appscreens.SettingsActivity
import com.viago.extensions.hide
import com.viago.extensions.loadImage
import com.viago.extensions.show
import com.viago.login.LoginActivity
import com.viago.login.UserUtil
import com.viago.profile.CoolZoneActivity
import com.viago.profile.MyProfileActivity
import com.viago.support.SupportActivity
import com.viago.utils.*
import com.viago.views.CircularImageView
import org.greenrobot.eventbus.EventBus
import android.os.Build
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey


/**
 * Created by mayank on 28/8/17.
 */
abstract class BaseDrawerActivity : BaseActivity() {

    protected val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val navigationView by bindView<NavigationView>(R.id.navigation_view)
    private val userDetailsContainer by bindView<View>(R.id.user_details_container)
    private val drawerUserImage by bindView<CircularImageView>(R.id.user_image)
    private val drawerUserName by bindView<TextView>(R.id.user_name)
    private val drawerLayout by bindView<DrawerLayout>(R.id.drawer_layout)
    private val actionSettings by bindView<View>(R.id.action_settings)
    private val actionAboutUs by bindView<View>(R.id.action_about_us)
    private val followOnFb by bindView<View>(R.id.follow_on_fb)
    private val followOnTwitter by bindView<View>(R.id.follow_on_twitter)
    private val followOnInsta by bindView<View>(R.id.follow_on_insta)

    protected var notificationMenuCountView: TextView? = null
    protected var notificationView: View? = null

    private lateinit var actionBarDrawerToggle: ActionBarDrawerToggle

    public lateinit var notificationCountView: TextView

    protected val TAKE_PHOTO_REQUEST =102;



    override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
        if (!UserUtil.isLoggedIn()) {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }

        setContentView(R.layout.activity_base)



    drawerLayout.addView(layoutInflater.inflate(getLayoutRes(), drawerLayout, false), 0)

        onCreated(savedInstanceState)

        setSupportActionBar(toolbar)

        userDetailsContainer.setOnClickListener {
            drawerLayout.closeDrawer(GravityCompat.START)
            replaceActivity(MyProfileActivity::class.java)
        }

        actionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0)
        actionBarDrawerToggle.isDrawerIndicatorEnabled = true

        notificationCountView = navigationView.menu.findItem(R.id.action_notifications).actionView.findViewById<TextView>(R.id.notification_count) as TextView

        setNotificationBadgeCount()



        navigationView.setNavigationItemSelectedListener { item: MenuItem ->
            drawerLayout.closeDrawer(GravityCompat.START)

            when (item.itemId) {
                R.id.action_create_trip,
                R.id.action_my_trips,
//                R.id.action_book_now,
                R.id.action_notifications -> {
                    EventBus.getDefault().postSticky(NavItemClickEvent(item.itemId))
                    closeIfNotMainActivity()
                }
                R.id.action_cool_zone -> replaceActivity(CoolZoneActivity::class.java)
                R.id.action_invite -> GeneralUtils.performInviteAction(this,UserUtil.getUser()?.name)
                R.id.action_rate_us -> GeneralUtils.openPlayStoreIntent(this)
                R.id.action_support -> replaceActivity(SupportActivity::class.java)
            }
            true
        }

        actionSettings.setOnClickListener {
            drawerLayout.closeDrawer(GravityCompat.START)
            replaceActivity(SettingsActivity::class.java)
        }

        actionAboutUs.setOnClickListener {
            drawerLayout.closeDrawer(GravityCompat.START)
            replaceActivity(AboutUsActivity::class.java)
        }

        followOnFb.setOnClickListener { ShareUtil.openFacebookPage(this) }
        followOnTwitter.setOnClickListener { ShareUtil.openTwitterPage(this) }
        followOnInsta.setOnClickListener { ShareUtil.openInstagramPage(this) }
    }

    protected fun setNotificationBadgeCount() {
        notificationCountView = navigationView.menu.findItem(R.id.action_notifications).actionView.findViewById<TextView>(R.id.notification_count) as TextView

        if (NOTIFICATION_COUNT <= 0) {
            notificationCountView.hide()
            if (notificationMenuCountView != null) {
                notificationMenuCountView!!.hide()
            }
        } else {
            notificationCountView.show()
            if(NOTIFICATION_COUNT>20){
                notificationCountView.text  = "20+"
            }else {
                notificationCountView.text  = NOTIFICATION_COUNT.toString()
            }
            if (notificationMenuCountView != null) {
                if(NOTIFICATION_COUNT>20){
                    notificationMenuCountView!!.text = "20+"
                }else {
                    notificationMenuCountView!!.text = NOTIFICATION_COUNT.toString()
                }

                notificationMenuCountView!!.show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setUserData()
        refreshNotificationCount()
    }

    protected fun refreshNotificationCount() {
        NOTIFICATION_COUNT = SharedPrefsUtils.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, -1)
        setNotificationBadgeCount()
    }

    protected fun setUserData() {
        UserUtil.getUser()?.let {
            drawerUserName.text = it.name
          //  drawerUserImage.loadImage(it.image)

            var lastProfileCache = System.currentTimeMillis();

            Glide.with(this)
                    .apply {
                        RequestOptions.circleCropTransform()
                        RequestOptions().signature( ObjectKey(lastProfileCache.toString()))
                        RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                        RequestOptions().error(R.drawable.placeholder)


                    }
                    .load(it.image)
                    .into(drawerUserImage)



          //  FileUtils.loadImage(it.image, drawerUserImage)


        }
    }

    private fun replaceActivity(cls: Class<*>) {
        replaceActivity(Intent(this, cls))
    }

    private fun replaceActivity(intent: Intent) {
        startActivity(intent)
        closeIfNotMainActivity()
    }

    private fun closeIfNotMainActivity() {
        if (this !is MainActivity) finish()
    }

    abstract fun onCreated(savedInstanceState: Bundle?)

    @LayoutRes
    abstract fun getLayoutRes(): Int

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        actionBarDrawerToggle.syncState()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == BaseLocationFragment.MY_LOCATION_REQUEST_CODE) {
            supportFragmentManager.fragments
                    .filter { fragment -> fragment is BaseLocationFragment }
                    .forEach { it.onRequestPermissionsResult(requestCode, permissions, grantResults) }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == LocationHelper.REQUEST_LOCATION_SETTINGS) {
            supportFragmentManager.fragments
                    .filter { fragment -> fragment is BaseLocationFragment }
                    .forEach { it.onActivityResult(requestCode, resultCode, data) }
        }
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    companion object {
        var NOTIFICATION_COUNT: Int = 0
    }


    fun checkPermissionForReadExtertalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = BaseDrawerActivity@this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }


    fun requestPermissionForReadExtertalStorage() {
        try {
            ActivityCompat.requestPermissions(this@BaseDrawerActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    102)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}