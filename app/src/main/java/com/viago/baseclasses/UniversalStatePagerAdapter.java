package com.viago.baseclasses;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayank on 10/7/16.
 */
public class UniversalStatePagerAdapter<T extends Fragment> extends FragmentStatePagerAdapter {

    private static final String TAG = UniversalStatePagerAdapter.class.getSimpleName();

    private final List<T> fragmentsList = new ArrayList<>();
    private final List<String> fragmentsTitleList = new ArrayList<>();
    private int nonPresentTitleNum = 0;
    private boolean showTitle = true;

    public UniversalStatePagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public T getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    public void addFrag(@NonNull T fragment) {
        addFrag(fragment, String.valueOf(nonPresentTitleNum++));
    }

    public void addFrag(@NonNull T fragment, @NonNull String title) {
        fragmentsList.add(fragment);
        fragmentsTitleList.add(title);
    }

    public void removeFrag(int pos){
        fragmentsList.remove(pos);
        fragmentsTitleList.remove(pos);
        notifyDataSetChanged();
    }

    public void replaceFrag(T oldFrag, T newFrag){
        Log.d(TAG, "replace fragment");
        int oldFragIndex = fragmentsList.indexOf(oldFrag);
        if (oldFragIndex != -1){
            Log.d(TAG, "replace fragment index");
            fragmentsList.set(oldFragIndex, newFrag);
            notifyDataSetChanged();
        } else {
            Log.d(TAG, "replace fragment not found");

        }
    }

    public void showTitle(boolean showTitle){
        this.showTitle = showTitle;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(fragmentsTitleList.get(position).equals("Google") || fragmentsTitleList.get(position).equals("Foursquare")
                ||fragmentsTitleList.get(position).equals("Yelp")){

            return null;
        }
        return showTitle ? fragmentsTitleList.get(position) : null;
    }
}
