package com.viago.baseclasses

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.os.Handler
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.model.LatLng
import com.viago.extensions.showToast
import com.viago.utils.LocationHelper
import timber.log.Timber

/**
 * Created by mayank on 23/8/17.
 */
abstract class BaseLocationFragment : BaseFragment() {

    private val locationHelper: LocationHelper by lazy { LocationHelper(context) }
    private val progressDialog by lazy { ProgressDialog(context).apply { setMessage("Obtaining current location") } }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == MY_LOCATION_REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            initLocationHelper()
        } else {
            context!!.showToast("Location permission is needed for app to function properly.")
        }
    }

    protected fun requestLatLong() {
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            initLocationHelper()
        } else
            ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), MY_LOCATION_REQUEST_CODE)
    }

    private fun initLocationHelper() {
        locationHelper.build()
        locationHelper.onStart()
        locationHelper.onResume()
        progressDialog.show()
        locationHelper.setOnLocationReceivedListener(onLocationReceivedListener)

        Handler().postDelayed({
            if (progressDialog.isShowing) onLocationReceivedListener.onLocationReceivedFailed()
        }, 10000)

        progressDialog.setOnDismissListener {
            locationHelper.onPause()
            locationHelper.onStop()
        }
    }

    private val onLocationReceivedListener: LocationHelper.OnLocationReceivedListener = object : LocationHelper.OnLocationReceivedListener {
        override fun onLocationReceivedSuccess(latLng: LatLng) {
            Timber.d("location frag: onLocationReceivedSuccess")
            onLocationReceived(latLng, LocationHelper.getAddress(context, latLng))
            progressDialog.dismiss()
        }

        override fun onLocationReceivedFailed() {
            Timber.d("location frag: onLocationReceivedFailed")
            progressDialog.dismiss()
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.tag("LocationHelper").d("onActivityResult")
        if (requestCode == LocationHelper.REQUEST_LOCATION_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                Timber.tag("LocationHelper").d("req")
                requestLatLong()
            } else {
                if (progressDialog.isShowing) progressDialog.dismiss()
            }
        }
    }

    protected abstract fun onLocationReceived(latLng: LatLng, address: Address?)

    override fun onPause() {
        super.onPause()
        locationHelper.onPause()
    }

    override fun onStop() {
        locationHelper.onStop()
        super.onStop()
    }

    companion object {
        val MY_LOCATION_REQUEST_CODE = 200
    }
}