package com.viago.baseclasses

import com.viago.listeners.ItemDragListener
import java.util.*

/**
 * Created by mayank on 5/7/17.
 */

abstract class DraggableEasyRecyclerAdapter<T> : EasyRecyclerViewAdapter<T>(), ItemDragListener {

    override fun onItemMoved(fromPosition: Int, toPosition: Int) {

        if (fromPosition < 0 || toPosition < 0 || fromPosition >= itemCount || toPosition >= itemCount) {
            return
        }

        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(itemsList, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(itemsList, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        notifyItemRangeChanged(minOf(fromPosition, toPosition), Math.abs(fromPosition - toPosition) + 1, true)
    }
}
