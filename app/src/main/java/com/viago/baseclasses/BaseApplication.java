package com.viago.baseclasses;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.viago.BuildConfig;
import com.viago.R;
import com.viago.baseclasses.receivers.ConnectivityReceiver;
import com.viago.utils.SharedPrefsUtils;

import io.branch.referral.Branch;
import io.fabric.sdk.android.Fabric;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import timber.log.Timber;
//import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by mayank on 11/2/17.
 */

public class BaseApplication extends Application {

    private boolean isLeakCanaryActive = false;
    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context) throws NullPointerException{
        BaseApplication application = (BaseApplication) context.getApplicationContext();
        if (!application.isLeakCanaryActive){
            return null;
        }
        return application.refWatcher;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Branch.getAutoInstance(this);

       /* CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/UbuntuRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );*/
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/UbuntuRegular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());

        if (isLeakCanaryActive) {
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            refWatcher = LeakCanary.install(this);
        }

        ConnectivityReceiver.init(this);
        SharedPrefsUtils.INSTANCE.init(this);

        // logging
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
