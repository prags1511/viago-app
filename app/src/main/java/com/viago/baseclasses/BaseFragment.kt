package com.viago.baseclasses

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.leakcanary.RefWatcher

abstract class BaseFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return  inflater.inflate(getLayoutRes(), container, false)
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    protected fun getAppCompatActivity(): AppCompatActivity = activity as AppCompatActivity

    override fun onDestroy() {
        super.onDestroy()
        val refWatcher: RefWatcher? = BaseApplication.getRefWatcher(activity)
        refWatcher?.watch(this)
    }
}
