package com.viago.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import bindView
import bindViews
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.showToast
import com.viago.utils.PinInputHelper

/**
 * Created by mayank on 17/8/17.
 */
internal class ResetPasswordFragment private constructor() : BaseFragment() {

    private val backButton by bindView<Button>(R.id.back_button)
    private val confirm by bindView<Button>(R.id.confirm)

    private val codeDigits by bindView<EditText>(R.id.verification_code)

    private val newPassword by bindView<EditText>(R.id.new_password)
    private val confirmPassword by bindView<EditText>(R.id.confirm_password)

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(context).apply {
        setMessage("Sending password reset mail")
        isIndeterminate = true
    } }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun getLayoutRes(): Int = R.layout.fragment_reset_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        backButton.setOnClickListener { activity!!.onBackPressed() }

       // PinInputHelper(activity, codeDigits)

        confirm.setOnClickListener {
            if (validateForm()) {
                progressDialog.show()
                LoginUtil.resetPassword(arguments!!.getString(EMAIL)!!, newPassword.text.toString(),
                        confirmPassword.text.toString(), codeDigits.text.toString(), {
                    progressDialog.dismiss()
                    activity!!.showToast("Password reset successful")
                    startActivity(Intent(context, MainActivity::class.java))
                    activity!!.finish()
                }, {
                    progressDialog.dismiss()
                    context!!.showToast(it)
                })
            }
        }
    }

    private fun validateForm(): Boolean {
        var valid = true

        if (codeDigits.text.toString().isEmpty() || codeDigits.text.toString().length!=6) {
            activity!!.showToast("Enter valid 6-digit code")
            return false
        }

        arrayOf(newPassword, confirmPassword).forEach { v ->
            if (v.text.isNullOrBlank()) {
                v.error = "Required"
                valid = false
            } else {
                v.error = null
            }
        }

        if (!valid) return false

        if (newPassword.text.length < 6) {
            newPassword.error = "Min length: 6."
            valid = false
        } else {
            newPassword.error = null
        }

        if (!valid) return false

        if (newPassword.text.toString() != confirmPassword.text.toString()) {
            confirmPassword.error = "Passwords do not match"
            valid = false
        } else {
            confirmPassword.error = null
        }

        return valid
    }

    override fun onDestroy() {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        super.onDestroy()
    }

    companion object {
        private val EMAIL: String = ResetPasswordFragment::class.java.simpleName + "email"

        fun newInstance(email: String) : ResetPasswordFragment {
            return ResetPasswordFragment().apply { arguments = Bundle().apply { putString(EMAIL, email) } }
        }
    }
}