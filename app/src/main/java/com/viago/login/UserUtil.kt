package com.viago.login

import com.google.gson.Gson
import com.viago.login.models.User
import com.viago.utils.SharedPrefsUtils

/**
 * Created by mayank on 4/9/17.
 */
public object UserUtil {

    private const val KEY_USER = "key user"
    private const val KEY_USER_TOKEN = "key user token"
    private const val KEY_IS_LOGGED_IN = "key is logged in"
    private val gson by lazy { Gson() }

    private fun saveUser(user: User) {
        SharedPrefsUtils.setStringPreference(KEY_USER, gson.toJson(user))
    }

    fun updateUser(user: User) {
        SharedPrefsUtils.setStringPreference(KEY_USER, gson.toJson(user))
    }

    public fun getUser(): User? =
            gson.fromJson(SharedPrefsUtils.getStringPreference(KEY_USER), User::class.java)

    private fun saveUserToken(key: String) {
        SharedPrefsUtils.setStringPreference(KEY_USER_TOKEN, key)
}

    public fun getUserToken(): String =
            SharedPrefsUtils.getStringPreference(KEY_USER_TOKEN)!!

    public fun login(key: String, user: User) {
        saveUserToken("Token $key")
        saveUser(user)
        SharedPrefsUtils.setBooleanPreference(KEY_IS_LOGGED_IN, true)
    }

    public fun login() {
        var key = "dffsdfg";
       saveUserToken("Token $key")
      //  saveUser(user)
        SharedPrefsUtils.setBooleanPreference(KEY_IS_LOGGED_IN, true)
    }

    public fun logout() {
        SharedPrefsUtils.clear()
    }

    public fun isLoggedIn(): Boolean =
            SharedPrefsUtils.getBooleanPreference(KEY_IS_LOGGED_IN, false)
}