package com.viago.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import bindView
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.baseclasses.BaseActivity
import com.viago.extensions.loadImage
import com.viago.extensions.openFragment
import com.viago.extensions.showToast
import com.viago.login.models.LoginResponse
import com.viago.login.models.User
import timber.log.Timber


/**
 * Created by mayank on 16/8/17.
 */
class LoginwithEmailActivity : BaseActivity(), View.OnClickListener {


//    private val email by bindView<EditText>(R.id.username)
//    private val password by bindView<EditText>(R.id.password)
//    private val login by bindView<View>(R.id.signin)
//    private val backButton by bindView<Button>(R.id.back_button)
//    private val forgotPassword by bindView<View>(R.id.forgot_password)


    private var callbackManager: CallbackManager? = null

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Loading")
            isIndeterminate = true
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        //arrayOf(login, email, forgotPassword,backButton).forEach { view -> view.setOnClickListener(this) }
    }

    override fun onClick(v: View?) {
        when (v) {
//            login -> signIn()
//            forgotPassword -> openFragment(ForgotPasswordFragment())
//            backButton -> finish()
        }
    }


    private fun signIn() {

        if (!validateForm()) return

        progressDialog.show()

      //  LoginUtil.loginWithEmailAndPassword(email.text.toString(), password.text.toString(), onSuccess, onFailure)
    }

    private val onSuccess: (LoginResponse) -> Unit = {
        UserUtil.login(it.key, it.user)
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private val onFailure: (String) -> Unit = {
        showToast(it)
        if (it.equals("E-mail is not verified.")) {
            openFragment(EmailVerificationFragment())
        }
        progressDialog.dismiss()
    }


    private fun validateForm(): Boolean {
        var valid = true

//        if (email.text.isNullOrBlank()) {
//            email.error = "Required."
//            valid = false
//        } else {
//            email.error = null
//        }
//
//        if (password.text.isNullOrBlank()) {
//            password.error = "Required."
//            valid = false
//        } else {
//            password.error = null
//        }
//
//        if (!valid) return false
//
//        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
//            email.error = "Invalid email."
//            valid = false
//        } else {
//            email.error = null
//        }
//
//        if (password.text.length < 6) {
//            password.error = "Min length: 6."
//            valid = false
//        } else {
//            password.error = null
//        }

        return valid
    }

    override fun onDestroy() {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        super.onDestroy()
    }

}