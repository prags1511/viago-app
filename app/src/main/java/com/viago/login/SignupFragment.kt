package com.viago.login

import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.widget.SwitchCompat
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import bindView
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.enableRightDrawablePasswordToggle
import com.viago.extensions.loadImage
import com.viago.extensions.openFragment
import com.viago.extensions.showToast
import com.viago.login.models.RegistrationRequest
import com.viago.views.ButtonCompat
import com.viago.views.MyDatePickerDialog
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import android.text.Editable
import android.text.TextWatcher
import java.text.ParseException
import android.content.Intent




/**
 * Created by mayank on 17/8/17.
 */
class SignupFragment : BaseFragment(), View.OnClickListener {

    private val backButton by bindView<View>(R.id.back_button)
    private val appLogo by bindView<ImageView>(R.id.app_logo)
    private val fullName by bindView<EditText>(R.id.full_name)
    private val email by bindView<EditText>(R.id.email)
    private val country by bindView<ButtonCompat>(R.id.country)
    private val flg_img by bindView<ImageView>(R.id.flg_img)
    private val dob by bindView<EditText>(R.id.dob)
    private val password by bindView<EditText>(R.id.password)
    private val confirmPassword by bindView<EditText>(R.id.confirm_password)
    private val submit by bindView<View>(R.id.signup)
    private val privacyPolicy by bindView<View>(R.id.privacy_policy)
    private val termsAndConditions by bindView<View>(R.id.t_n_c)
    private val genderSwitch by bindView<SwitchCompat>(R.id.gender_switch)

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(context).apply {
            setMessage("Signing up")
            isIndeterminate = true
            setCancelable(false)
        }
    }

    private var selectedDateOfBirth: String = ""

    override fun getLayoutRes(): Int = R.layout.fragment_signup

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        appLogo.loadImage(R.drawable.logo)

        arrayOf(backButton, country, dob, submit, privacyPolicy, termsAndConditions).forEach { v: View -> v.setOnClickListener(this) }

      //  country.text = Country.getCountryByLocale(Locale.getDefault())?.name

        flg_img.setBackgroundResource(Country.getCountryByLocale(Locale.getDefault()).flag)

        arrayOf(password, confirmPassword).forEach { v ->
            v.enableRightDrawablePasswordToggle(
                    R.drawable.ic_visibility,
                    R.drawable.ic_visibility_off,
                    R.color.gray_color
            )
        }
    }

    override fun onClick(v: View) {
        when (v) {
            backButton -> activity!!.onBackPressed()
            country -> showCountryPicker()
            dob -> showDatePicker()
            submit -> createAccount()
           privacyPolicy -> showPrivacy()
           termsAndConditions -> termsandcondition()

          //  dob  -> createAccount()
        }
    }


    private fun showPrivacy(){

        val intent = Intent(activity, PrivacyPolicyActivity::class.java)
        startActivity(intent)
    }


    private fun termsandcondition(){

        val intent = Intent(activity, TermsandConditionActivity::class.java)
        startActivity(intent)
    }

    private fun showCountryPicker() {
        CountryPicker.newInstance("Select country")
                .apply { setListener { name, _, _, flg -> country.text = name;flg_img.setBackgroundResource(flg); dismiss() } }
                .show(childFragmentManager, "COUNTRY_PICKER")
    }

    private fun showDatePicker() {
        Timber.d("show date picker")
        val calendar = Calendar.getInstance()
        //Make sure user is atleast 13 years old
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 13)

        MyDatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    with(Calendar.getInstance().apply {
                        set(Calendar.YEAR, year)
                        set(Calendar.MONTH, month)
                        set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    }.time) {
                        dob.setText(SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(this))
                        selectedDateOfBirth = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(this)
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        ).apply {
            setPermanentTitle("Date of Birth")
            datePicker.maxDate = calendar.timeInMillis
        }.show()
    }

    private fun createAccount() {

        if (!validateForm()) return

        progressDialog.show()

        val registrationRequest = RegistrationRequest(
                fullName.text.toString().capitalize(),
                email.text.toString(),
                password.text.toString(),
                confirmPassword.text.toString(),
                selectedDateOfBirth,
                Country.getCountryByName(country.text.toString()).code,
                if (genderSwitch.isChecked) "F" else "M"
        )

        LoginUtil.register(registrationRequest, {
            getAppCompatActivity().openFragment(EmailVerificationFragment())
            progressDialog.dismiss()
        }, {
            activity!!.showToast(it)
            progressDialog.dismiss()
        })
    }

    private fun validateForm(): Boolean {
        var valid = true

        arrayOf(fullName, email, dob, password, confirmPassword).forEach { v ->
            if (v.text.isNullOrBlank()) {
                v.error = "Required"
                valid = false
            } else {
                v.error = null
            }
        }

        if (!valid) return false

        val sdfrmt = SimpleDateFormat("MM/dd/yyyy")
        sdfrmt.isLenient = false
        try {
            val date = sdfrmt.parse(dob.text.toString())
            selectedDateOfBirth = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(date)
        } catch (e: ParseException) {
            dob.error = "Invalid date"
            return false
        }
        /* Date format is invalid */

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text).matches()) {
            email.error = "Invalid email"
            valid = false
        } else {
            email.error = null
        }

        if (password.text.length < 6) {
            password.error = "Min length: 6."
            valid = false
        } else {
            password.error = null
        }

        if (!valid) return false

        if (password.text.toString() != confirmPassword.text.toString()) {
            confirmPassword.error = "Passwords do not match"
            valid = false
        } else {
            confirmPassword.error = null
        }

        return valid
    }

    override fun onDestroy() {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        super.onDestroy()
    }
}