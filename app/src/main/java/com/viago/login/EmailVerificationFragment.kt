package com.viago.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import bindView
import bindViews
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.showToast
import com.viago.utils.PinInputHelper

/**
 * Created by mayank on 17/8/17.
 */
internal class EmailVerificationFragment : BaseFragment() {

    private val backButton by bindView<Button>(R.id.back_button)
    private val verify by bindView<Button>(R.id.verify)

    private val codeDigits by bindViews<EditText>(R.id.code_digit_1, R.id.code_digit_2, R.id.code_digit_3, R.id.code_digit_4, R.id.code_digit_5, R.id.code_digit_6)

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(context).apply {
        setMessage("Sending password reset mail")
        isIndeterminate = true
    } }

    override fun getLayoutRes(): Int = R.layout.fragment_email_verification

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        backButton.setOnClickListener { activity!!.onBackPressed() }

        PinInputHelper(activity!!, codeDigits)

        verify.setOnClickListener {
            if (validateForm()) {
                progressDialog.show()
                LoginUtil.verifyEmail(codeDigits.joinToString("", transform = { it.text }), {
                    progressDialog.dismiss()
                    with(it.user) {
                        UserUtil.login(
                                it.key,
                                it.user
                        )
                    }
                    activity!!.showToast("Email verification successful")
                    startActivity(Intent(context, MainActivity::class.java))
                    activity!!.finish()
                }, {
                    progressDialog.dismiss()
                    context!!.showToast(it)
                })
            }
        }
    }

    private fun validateForm(): Boolean {
        if (codeDigits.joinToString("", transform = { it.text }).run { isNullOrBlank() && length != 6} ) {
            activity!!.showToast("Enter valid 6-digit code")
            return false
        } else {
            return true
        }
    }
}