package com.viago.login

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.app.ActivityCompat
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import bindView
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.gson.Gson
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.baseclasses.BaseActivity
import com.viago.extensions.loadImage
import com.viago.extensions.openFragment
import com.viago.extensions.showToast
import com.viago.login.models.LoginResponse
import com.viago.login.models.User
import timber.log.Timber


/**
 * Created by mayank on 16/8/17.
 */
class LoginActivity : BaseActivity(), View.OnClickListener {

    private val appLogo by bindView<ImageView>(R.id.app_logo)
    private val email by bindView<EditText>(R.id.email)
    private val password by bindView<EditText>(R.id.password)
    private val login by bindView<View>(R.id.login)
    private val forgotPassword by bindView<View>(R.id.forgot_password)
    private val facebook by bindView<View>(R.id.facebook)
    private val google by bindView<View>(R.id.google)
    private val register by bindView<View>(R.id.register)

    private var callbackManager: CallbackManager? = null

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Loading")
            isIndeterminate = true
        }
    }

    companion object {
        private const val EMAIL = "email"
        private const val PUBLIC_PROFILE = "public_profile"
        private const val RC_GOOGLE_SIGN_IN = 9001
        private const val RC_HINT = 9002

        private var autoManage = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

       /* if(!checkPermissionForReadExtertalStorage()){

            requestPermissionForReadExtertalStorage();
        }*/

        appLogo.loadImage(R.drawable.logo)

        /*  password.enableRightDrawablePasswordToggle(
                  R.drawable.ic_visibility,
                  R.drawable.ic_visibility_off,
                  R.drawable.ic_lock
          )*/

        arrayOf(login, facebook, google, email, forgotPassword, register).forEach { view -> view.setOnClickListener(this) }
    }

    override fun onClick(v: View?) {
        when (v) {
            login -> signIn()
            facebook -> startFacebookLogin()
            google -> startGoogleLogin()
            forgotPassword -> openFragment(ForgotPasswordFragment())
            register -> openFragment(SignupFragment())
        }
    }

    private fun startFacebookLogin() {

        progressDialog.show()

        callbackManager = CallbackManager.Factory.create()
        val loginManager = LoginManager.getInstance()

        loginManager.logInWithReadPermissions(this, listOf(EMAIL, PUBLIC_PROFILE))

        loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onCancel() = progressDialog.dismiss()

            override fun onError(error: FacebookException?) {
                progressDialog.dismiss()
                showToast("Error logging in with Facebook. " + error?.message)
            }

            override fun onSuccess(result: LoginResult) = LoginUtil.loginWithFacebook(result.accessToken.token, onSuccess, onFailure)
        })

    }

    private fun startGoogleLogin() {

        progressDialog.show()
        Timber.d("client id: " + getString(R.string.default_web_client_id))
        val googleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
           //     .requestScopes(Scope(Scopes.PROFILE))
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestServerAuthCode(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        val googleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, autoManage++, { connectionResult ->
                    Timber.w("onConnectionFailed: $connectionResult")
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build()

        startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(googleApiClient), RC_GOOGLE_SIGN_IN)
    }

    private fun signIn() {

      //  if (!validateForm()) return

        progressDialog.show()

        LoginUtil.loginWithEmailAndPassword(email.text.toString(), password.text.toString(), onSuccess, onFailure)
    }

    private val onSuccess: (LoginResponse) -> Unit = {
        UserUtil.login(it.key, it.user)
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private val onFailure: (String) -> Unit = {
        showToast(it)
        if (it.equals("E-mail is not verified.")) {
            openFragment(EmailVerificationFragment())
        }
        progressDialog.dismiss()
    }


   /* private fun onSuccess(){

        UserUtil.login(it.key, it.user)
        progressDialog.dismiss()
        startActivity(Intent(this, MainActivity::class.java))
        finish()

    }*/


    private fun handleGoogleAuth(signInAccount: GoogleSignInAccount?) {
        val serverAuthCode = signInAccount?.serverAuthCode
        if (serverAuthCode != null) {
            val gsonStr = Gson();
            val jsonSignInAccount: String = gsonStr.toJson(signInAccount);
           LoginUtil.loginWithGoogle(jsonSignInAccount, onSuccess, onFailure)

           /* UserUtil.login()
            progressDialog.dismiss()
            startActivity(Intent(this, MainActivity::class.java))
            finish()*/

        } else {
            onFailure("Error signing in")
        }
    }

    private fun validateForm(): Boolean {
        var valid = true

        if (email.text.isNullOrBlank()) {
            email.error = "Required."
            valid = false
        } else {
            email.error = null
        }

        if (password.text.isNullOrBlank()) {
            password.error = "Required."
            valid = false
        } else {
            password.error = null
        }

        if (!valid) return false

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text.toString()).matches()) {
            email.error = "Invalid email."
            valid = false
        } else {
            email.error = null
        }

        if (password.text.length < 6) {
            password.error = "Min length: 6."
            valid = false
        } else {
            password.error = null
        }

        return valid
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager?.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_GOOGLE_SIGN_IN) {
            val googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (googleSignInResult.isSuccess) {
                handleGoogleAuth(googleSignInResult.signInAccount)
            } else {
                progressDialog.dismiss()
                showToast("Google sign in failed")
            }
        }

        if (requestCode == RC_HINT) {
            if (resultCode == RESULT_OK) {
                val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY);
                credential?.let { email.setText(it.id) }
            } else {
                Timber.e("Hint Read: NOT OK");
            }
        }

    }

    fun checkPermissionForReadExtertalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = this@LoginActivity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }


    fun requestPermissionForReadExtertalStorage() {
        try {
            ActivityCompat.requestPermissions(this@LoginActivity, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    101)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

}