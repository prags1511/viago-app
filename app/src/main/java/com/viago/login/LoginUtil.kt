package com.viago.login

import android.util.Log
import com.viago.login.models.*
import com.viago.profile.ProfileUpdateRequest
import com.viago.utils.ApiUtils
import com.viago.utils.getFailureMessage
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mayank on 6/9/17.
 */
internal object LoginUtil {

    private val loginService = ApiUtils.retrofitInstance.create(LoginService::class.java)
    var email: String? = null;

    fun register(registrationRequest: RegistrationRequest, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        loginService
                .register(registrationRequest)
                .enqueue(object: Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            email = registrationRequest.email;
                            onSuccess()
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Signup failed. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onFailure("Signup failed. Please try again")
                    }

                })
    }

    fun loginWithEmailAndPassword(email: String, password: String, onSuccess: (LoginResponse) -> Unit, onFailure: (String) -> Unit) {
        login(loginService.loginWithEmail(LoginEmailRequest(email, password)), onSuccess, onFailure)
    }

    fun loginWithFacebook(token: String, onSuccess: (LoginResponse) -> Unit, onFailure: (String) -> Unit) {
        login(loginService.loginWithFacebook(FacebookLoginRequest(token)), onSuccess, onFailure)
    }

    fun loginWithGoogle(code: String, onSuccess: (LoginResponse) -> Unit, onFailure: (String) -> Unit) {
        login(loginService.loginWithGoogle(GoogleLoginRequest(code)), onSuccess, onFailure)
    }

    private fun login(call: Call<LoginResponse>, onSuccess: (LoginResponse) -> Unit, onFailure: (String) -> Unit) {
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                if (response?.isSuccessful == true) {
                    onSuccess(response.body())
                } else {
                    onFailure(getFailureMessage(response?.errorBody(), "Login failed. Please check your email and password"))
                }
            }

            override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                onFailure("Login failed. Please check your email and password")
            }

        })
    }

    fun sendResetPasswordEmail(email: String, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        loginService.forgotPassword(ForgotPasswordRequest(email))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess()
                        } else{
                            onFailure(getFailureMessage(response?.errorBody(), "Cannot send reset email. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onFailure("Cannot send reset email. Please try again")
                    }
                })
    }

    fun resetPassword(email: String, password: String, password2: String, otp: String, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        loginService.resetPassword(ResetPasswordRequest(email, password, password2, otp))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess()
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Reset password failed. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onFailure("Reset password failed. Please try again")
                    }
                })
    }

    fun verifyEmail(key: String, onSuccess: (LoginResponse) -> Unit, onFailure: (String) -> Unit) {
        loginService.verifyEmail(VerifyEmail(key, email))
                .enqueue(object : Callback<LoginResponse> {
                    override fun onResponse(call: Call<LoginResponse>?, response: Response<LoginResponse>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Error verifying email. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<LoginResponse>?, t: Throwable?) {
                        onFailure("Error verifying email. Please try again")
                    }
                })
    }

    fun getProfile(token: String, onSuccess: (user: User) -> Unit, onFailure: (String) -> Unit) {
        loginService.getProfile(token)
                .enqueue(object : Callback<User> {
                    override fun onResponse(call: Call<User>?, response: Response<User>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Error updating Profile. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<User>?, t: Throwable?) {
                        onFailure("Error updating Profile. Please try again")
                    }
                })
    }

    fun updateProfile(token: String, profileUpdateRequest: ProfileUpdateRequest, onSuccess: (user: User) -> Unit, onFailure: (String) -> Unit) {
        loginService.updateProfile(token, profileUpdateRequest)
                .enqueue(object : Callback<User> {
                    override fun onResponse(call: Call<User>?, response: Response<User>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Error updating Profile. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<User>?, t: Throwable?) {
                        onFailure("Error updating Profile. Please try again")
                    }
                })
    }

    fun updateProfilewithImage(Id:Long, file: MultipartBody.Part?, profileUpdateRequest: ProfileUpdateRequest, onSuccess: (responseBody: User) -> Unit, onFailure: (String) -> Unit) {
        loginService.uploadUserProfile(Id, file!!,profileUpdateRequest)
                .enqueue(object : Callback<User> {
                    override fun onResponse(call: Call<User>?, response: Response<User>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                            Log.e("Profile Image", response.body().toString())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Error updating Profile. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<User>?, t: Throwable?) {
                        onFailure("Error updating Profile. Please try again")
                    }
                })
    }




}