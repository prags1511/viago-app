package com.viago.login.models

/**
 * Created by mayank on 6/9/17.
 */
data class LoginResponse(
        val key: String,
        val user: User
)