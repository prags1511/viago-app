package com.viago.login.models

/**
 * Created by mayank on 6/9/17.
 */
internal data class ForgotPasswordRequest(
        val email: String
)