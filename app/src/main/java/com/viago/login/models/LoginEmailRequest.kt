package com.viago.login.models

/**
 * Created by mayank on 6/9/17.
 */
data class LoginEmailRequest(
        val email: String,
        val password: String
)