package com.viago.login.models

/**
 * Created by mayank on 7/9/17.
 */
data class VerifyEmail(
        val key: String,
        val email: String?
)