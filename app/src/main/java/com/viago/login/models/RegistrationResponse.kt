package com.viago.login.models

/**
 * Created by mayank on 4/9/17.
 */
internal data class RegistrationResponse(
        val email: List<String>?,
        val non_field_errors: List<String>?
)