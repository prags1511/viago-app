package com.viago.login.models

/**
 * Created by mayank on 5/9/17.
 */
data class User(
        val pk: Long,
        val name: String,
        val email: String,
        val country: String,
        val date_of_birth: String,
        val gender: String,
        val is_email_verified: Boolean,
        val trips_count: Long,
        val locations_count: Long,
        val image: String? = null
)