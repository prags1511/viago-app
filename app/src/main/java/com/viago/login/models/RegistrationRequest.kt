package com.viago.login.models

/**
 * Created by mayank on 4/9/17.
 */
internal data class RegistrationRequest(
        val name: String,
        val email: String,
        val password1: String,
        val password2: String,
        val date_of_birth: String,
        val country: String,
        val gender: String
)