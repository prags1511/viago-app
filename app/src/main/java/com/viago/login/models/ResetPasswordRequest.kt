package com.viago.login.models

/**
 * Created by mayank on 6/9/17.
 */
internal data class ResetPasswordRequest(
        val email: String,
        val new_password1: String,
        val new_password2: String,
        val otp: String
)