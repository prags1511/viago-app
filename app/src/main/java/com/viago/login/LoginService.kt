package com.viago.login

import com.viago.login.models.*
import com.viago.profile.ProfileUpdateRequest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*
import okhttp3.RequestBody
import okhttp3.MultipartBody
import io.branch.referral.ServerResponse
import retrofit2.http.POST
import retrofit2.http.Multipart



/**
 * Created by mayank on 4/9/17.
 */
internal interface LoginService {

    @POST("rest-auth/registration/")
    fun register(@Body registrationRequest: RegistrationRequest): Call<ResponseBody>

    @POST("rest-auth/login/")
    fun loginWithEmail(@Body loginEmailRequest: LoginEmailRequest): Call<LoginResponse>

    @POST("rest-auth/socialMobile/facebook/")
    fun loginWithFacebook(@Body facebookLoginRequest: FacebookLoginRequest): Call<LoginResponse>

    @POST("rest-auth/socialMobile/google/")
    fun loginWithGoogle(@Body googleLoginRequest: GoogleLoginRequest): Call<LoginResponse>

    @POST("rest-auth/password/reset/")
    fun forgotPassword(@Body forgotPasswordRequest: ForgotPasswordRequest): Call<ResponseBody>

    @POST("rest-auth/password/reset/confirm/")
    fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest): Call<ResponseBody>

    @POST("rest-auth/registration/verify-email/")
    fun verifyEmail(@Body verifyEmail: VerifyEmail): Call<LoginResponse>

    @GET("rest-auth/user/")
    fun getProfile(@Header("Authorization") token: String): Call<User>

    @PUT("rest-auth/user/")
    fun updateProfile(@Header("Authorization") token: String, @Body profileUpdateRequest: ProfileUpdateRequest): Call<User>

    @Multipart
    @PATCH("/spring/viago/user/update/{id}")
    fun uploadUserProfile(@Path("id") enfantId: Long,
                          @Part file: MultipartBody.Part,
                          @Part("payload") requestBody: ProfileUpdateRequest): Call<User>
}