package com.viago.login

import android.app.ProgressDialog
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.loadImage
import com.viago.extensions.openFragment
import com.viago.extensions.showToast

/**
 * Created by mayank on 17/8/17.
 */
internal class ForgotPasswordFragment: BaseFragment() {

    private val backButton by bindView<Button>(R.id.back_button)
    private val appLogo by bindView<ImageView>(R.id.app_logo)
    private val email by bindView<EditText>(R.id.email)
    private val submit by bindView<Button>(R.id.submit)

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(context).apply {
        setMessage("Sending password reset mail")
        isIndeterminate = true
    } }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    override fun getLayoutRes(): Int = R.layout.fragment_forgot_password

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        appLogo.loadImage(R.drawable.logo)

        backButton.setOnClickListener { activity!!.onBackPressed() }

        submit.setOnClickListener {
            if (isEmailValid()) {
                progressDialog.show()
                email.error = null
                LoginUtil.sendResetPasswordEmail(email.text.toString(), {
                    progressDialog.dismiss()
                    activity!!.showToast("Check your email to reset your password")
                    getAppCompatActivity().openFragment(ResetPasswordFragment.newInstance(email.text.toString()))
                }, {
                    progressDialog.dismiss()
                    activity!!.showToast(it)
                })
            } else {
                with("Enter valid email address") {
                    email.error = this
                    activity!!.showToast(this)
                }
            }
        }
    }

    private fun isEmailValid() = Patterns.EMAIL_ADDRESS.matcher(email.text).matches()


    override fun onDestroy() {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        super.onDestroy()
    }
}