package com.viago.support

import com.viago.utils.ApiUtils
import com.viago.utils.getFailureMessage
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mayank on 8/9/17.
 */
object SupportUtil {

    private val supportService = ApiUtils.retrofitInstance.create(SupportService::class.java)

    fun postSupportDetails(token: String, support: Support, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        supportService.postSupportDetails(token, support)
                .enqueue(object: Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess()
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Contact cannot be established. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onFailure("Contact cannot be established. Please try again")
                    }
                })
    }
}