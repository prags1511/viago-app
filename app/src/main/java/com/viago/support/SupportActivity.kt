package com.viago.support

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.widget.AppCompatRadioButton
import android.util.Patterns
import android.view.View
import android.widget.EditText
import android.widget.RadioGroup
import bindView
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.extensions.showToast
import com.viago.login.UserUtil

/**
 * Created by mayank on 22/8/17.
 */
class SupportActivity: BaseDrawerActivity() {

    private val email by bindView<EditText>(R.id.email)
    private val subject by bindView<EditText>(R.id.subject)
    private val message by bindView<EditText>(R.id.message)
    private val submit by bindView<View>(R.id.submit)
    private val typeRadioGroup by bindView<RadioGroup>(R.id.type_radio_group)

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(this).apply {
        setMessage("Sending message")
        isIndeterminate = true
        setCancelable(false)
    } }

    override fun getLayoutRes() = R.layout.activity_support

    override fun onCreated(savedInstanceState: Bundle?) {
        toolbar.title = ""

        typeRadioGroup.check(R.id.feedback)

        submit.setOnClickListener {
            if (isValid()) {
                progressDialog.show()
                val type = (findViewById<AppCompatRadioButton>(typeRadioGroup.checkedRadioButtonId) as AppCompatRadioButton).text.toString()
                SupportUtil
                        .postSupportDetails(
                                UserUtil.getUserToken(),
                                Support(
                                        email.text.toString(),
                                        subject.text.toString(),
                                        message.text.toString(),
                                        type.toUpperCase()
                                ), {
                            showToast("$type sent")
                            progressDialog.dismiss()
                        }, {
                            showToast(it)
                            progressDialog.dismiss()
                            })

            }
        }
    }

    private fun isValid(): Boolean {
        var valid = true

        arrayOf(email, subject, message).forEach { v ->
            if (v.text.isNullOrBlank()) {
                v.error = "Required"
                valid = false
            } else {
                v.error = null
            }
        }

        if (!valid) return false

        if (!Patterns.EMAIL_ADDRESS.matcher(email.text).matches()) {
            email.error = "Invalid email"
            valid = false
        } else {
            email.error = null
        }

        if (typeRadioGroup.checkedRadioButtonId == -1) {
            showToast("Please select type")
            valid = false
        }

        return valid
    }
}