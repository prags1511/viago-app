package com.viago.support

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

/**
 * Created by mayank on 7/9/17.
 */
internal interface SupportService {

    @POST("trips/contact-us/")
    fun postSupportDetails(@Header("Authorization") token: String, @Body support: Support): Call<ResponseBody>
}