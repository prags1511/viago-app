package com.viago.support

/**
 * Created by mayank on 8/10/17.
 */
data class Support (
        val email: String,
        val subject: String,
        val message: String,
        val type: String
)