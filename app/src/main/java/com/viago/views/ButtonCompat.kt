package com.viago.views

import android.content.Context
import android.util.AttributeSet
import com.viago.R

/**
 * Created by mayank on 25/8/17.
 */

class ButtonCompat : TextViewCompat {

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, R.attr.buttonStyle)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun getAccessibilityClassName() = "Button"
}