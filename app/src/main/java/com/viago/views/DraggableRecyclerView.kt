package com.viago.views

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.ItemTouchHelper
import android.util.AttributeSet

import com.viago.baseclasses.DraggableEasyRecyclerAdapter
import timber.log.Timber

/**
 * Created by mayank on 5/7/17.
 */

class DraggableRecyclerView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyle: Int = 0
): RecyclerView(context, attrs, defStyle) {

    var nonMovablePositions: List<Int>? = null

    init {
        if (!isInEditMode) {
            ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.START or ItemTouchHelper.END) {

                override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                    val adapter = adapter
                    if (adapter is DraggableEasyRecyclerAdapter<*>) {
                        Timber.d("adapter pos: " + viewHolder.adapterPosition + "  " + target.adapterPosition)
                        nonMovablePositions?.let {
                            if (it.contains(viewHolder.adapterPosition) || it.contains(target .adapterPosition)) return false
                        }
                        adapter.onItemMoved(viewHolder.adapterPosition, target.adapterPosition)
                        return true
                    } else {
                        return false
                    }
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) = Unit

                override fun isItemViewSwipeEnabled(): Boolean = false

                override fun getDragDirs(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
                    nonMovablePositions?.let {
                        if (it.contains(viewHolder.adapterPosition)) return 0
                    }
                    return super.getDragDirs(recyclerView, viewHolder)
                }
            }).attachToRecyclerView(this)
        }
    }
}
