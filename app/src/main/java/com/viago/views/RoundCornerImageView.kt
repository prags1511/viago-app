package com.viago.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet

class RoundCornerImageView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatImageView(context, attrs, defStyleAttr) {

    private val path: Path by lazy { Path() }
    private val rect: RectF by lazy { RectF() }


    override fun onDraw(canvas: Canvas) {
        rect.right = width.toFloat()
        rect.bottom = height.toFloat()
        path.addRoundRect(rect, RADIUS, RADIUS, Path.Direction.CW)
        canvas.clipPath(path)
        super.onDraw(canvas)
    }

    companion object {
        private val RADIUS = 0.0f
    }
}