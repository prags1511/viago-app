package com.viago.views

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.widget.LinearLayout
import android.widget.TextView
import com.viago.R


/**
 * Created by mayank on 7/10/16.
 */

class MyDatePickerDialog(
        context: Context, callBack: DatePickerDialog.OnDateSetListener, year: Int, monthOfYear: Int, dayOfMonth: Int
) : DatePickerDialog(context, callBack, year, monthOfYear, dayOfMonth) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var titleId = context.resources.getIdentifier("title_template", "id", "android")
        if (titleId > 0) {
            val dialogTitle = findViewById<LinearLayout>(titleId) as LinearLayout
            dialogTitle.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent))
        }
        titleId = context.resources.getIdentifier("alertTitle", "id", "android")
        if (titleId > 0) {
            val dialogTitle = findViewById<TextView>(titleId) as TextView
            dialogTitle.setTextColor(ContextCompat.getColor(context, android.R.color.white))
        }
    }

    private var myTitle: CharSequence? = null

    fun setPermanentTitle(myTitle: CharSequence) {
        this.myTitle = myTitle
        setTitle(myTitle)
    }
}
