package com.viago.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.util.AttributeSet

class CircularImageView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatImageView(context, attrs, defStyleAttr) {

    private val path: Path by lazy { Path() }

    override fun onDraw(canvas: Canvas) {
        path.addCircle(width/2F, height/2F, Math.min(width, height)/2F, Path.Direction.CW);
        canvas.clipPath(path)
        super.onDraw(canvas)
    }
}