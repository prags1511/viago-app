package com.viago.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import androidx.appcompat.content.res.AppCompatResources
import android.util.AttributeSet
import com.viago.R

/**
 * Created by mayank on 25/8/17.
 */

open class TextViewCompat : androidx.appcompat.widget.AppCompatTextView {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initAttrs(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initAttrs(context, attrs)
    }

    private fun initAttrs(context: Context, attrs: AttributeSet?) {

        if (attrs == null) return

        val attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.TextViewCompat)

        setCompoundDrawablesWithIntrinsicBounds(
                attributeArray.getVectorCompatDrawable(R.styleable.TextViewCompat_drawableLeftCompat),
                attributeArray.getVectorCompatDrawable(R.styleable.TextViewCompat_drawableTopCompat),
                attributeArray.getVectorCompatDrawable(R.styleable.TextViewCompat_drawableRightCompat),
                attributeArray.getVectorCompatDrawable(R.styleable.TextViewCompat_drawableBottomCompat)
        )
        attributeArray.recycle()

    }

    private fun TypedArray.getVectorCompatDrawable(index: Int): Drawable? {
        val resourceId = getResourceId(index, -1)
        return if (resourceId == -1) null else AppCompatResources.getDrawable(context, resourceId)
    }
}
