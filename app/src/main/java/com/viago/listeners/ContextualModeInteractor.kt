package com.viago.listeners

/**
 * Created by a_man on 19-11-2017.
 */

interface ContextualModeInteractor {
    fun enableContextualMode()
    fun isContextualMode(): Boolean
    fun updateSelectedCount(count: Int)
}