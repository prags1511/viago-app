package com.viago.listeners;

/**
 * Created by mayank on 5/7/17.
 */

public interface ItemDragListener {
    void onItemMoved(int fromPosition, int toPosition);
}
