package com.viago.profile

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import bindView
import com.viago.BuildConfig
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.extensions.show
import com.viago.listeners.ContextualModeInteractor
import com.viago.views.SquareImageView
import timber.log.Timber
import java.io.File


/**
 * Created by mayank on 23/8/17.
 */
class MediaAdapter(val mediaType: Int, val selectedSet: HashSet<String>, val interactor: ContextualModeInteractor) : EasyRecyclerViewAdapter<String>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PhotosViewHolder(parent.inflate(R.layout.item_media))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, string: String, position: Int) {
        if (holder is PhotosViewHolder) holder.bind(string)
    }

    inner class PhotosViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image by bindView<SquareImageView>(R.id.image)
        private val playButton by bindView<View>(R.id.play_button)
        private val removeIndicator by bindView<View>(R.id.toBeRemoved)

        init {
            if (mediaType == MediaFragment.VIDEO) {
                playButton.show()
            }
            itemView.setOnClickListener {
                if (interactor.isContextualMode()) {
                    toggleSelection(getItem(adapterPosition), adapterPosition)
                } else {
                    openIntent(itemView.context, getItem(adapterPosition))
                }
            }
            itemView.setOnLongClickListener {
                interactor.enableContextualMode()
                toggleSelection(getItem(adapterPosition), adapterPosition)
                return@setOnLongClickListener true
            }
        }

        fun bind(string: String) {
            image.loadImage(string)
            removeIndicator.visibility = if (selectedSet.contains(string)) View.VISIBLE else View.GONE
        }

        private fun openIntent(context: Context, path: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            Timber.d("path: " + path)
/*            val uri = FileProvider.getUriForFile(context,
                    null,
                    File(path))*/
            val uri: Uri
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                uri = Uri.parse("file://" + path)
            } else {
                uri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID, File(path))
            }
            intent.setDataAndType(uri, if (mediaType == MediaFragment.VIDEO) "video/*" else "image/*")
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            context.startActivity(intent)
        }

        private fun openVideoIntent(context: Context, path: String) {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(Uri.parse(path), "video/*")
            context.startActivity(intent)
        }
    }

    private fun toggleSelection(item: String, pos: Int) {
        if (selectedSet.contains(item)) selectedSet.remove(item) else selectedSet.add(item)
        notifyItemChanged(pos)
        interactor.updateSelectedCount(selectedSet.size)
    }
}