package com.viago.profile;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.viago.R;

import java.util.ArrayList;



public class ImageGridAdapter extends RecyclerView.Adapter<ImageGridAdapter.ViewHolder> {

    private  Context context;
    private  ArrayList<String> imagePathList ;
    private  LayoutInflater layoutInflater;

    public ImageGridAdapter(Context context,  ArrayList<String> imagePathList){
        this.context= context;
        this.imagePathList=imagePathList;
        layoutInflater = LayoutInflater.from(context);

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         View inflatingView = layoutInflater.inflate(R.layout.list_item, parent, false);

        return new ViewHolder(inflatingView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

      String imageBean =imagePathList.get(position);
        Glide.with(context)
                .load("file://" + imageBean)

          .into(  holder.imageView);

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context,CoolZoneImageVieweActivity.class);
                intent.putExtra("position",position);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return imagePathList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder{
       private ImageView imageView;
       private AppCompatCheckBox checkbox;
       private View overLayView ;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.item_img);


          //  iv_folder_thumbnail.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.rec_gray));
        }
    }


}
