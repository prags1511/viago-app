package com.viago.profile

import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.provider.MediaStore
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by mayank on 7/10/17.
 */
object MediaDBUtil {
    private val DATA_IMAGE: String = "IMAGEDATA"
    private val DATA_VIDEO: String = "VIDEODATA"

    public fun getImages(context: Context, startDate: String, endDate: String) = getMedia(context, startDate, endDate, true)

    public fun getVideos(context: Context, startDate: String, endDate: String) = getMedia(context, startDate, endDate, false)

    public fun getImageCount(context: Context, startDate: String, endDate: String): Int = getMedia(context, startDate, endDate, true).size

    public fun getVideoCount(context: Context, startDate: String, endDate: String): Int = getMedia(context, startDate, endDate, false).size

    private fun getMediaFromCursor(cursor: Cursor): List<String> {
        val dataColumnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA)
        val mediaList: ArrayList<String> = ArrayList()

        while (cursor.moveToNext()) {
            mediaList.add(cursor.getString(dataColumnIndex))
        }

        cursor.close()

        return mediaList
    }

    private fun getMedia(context: Context, startDate: String, endDate: String, isImage: Boolean): ArrayList<String> {
        val prefString: String? = SharedPrefsUtils.getStringPreference(if (isImage) DATA_IMAGE else DATA_VIDEO)
        val mediaPaths: ArrayList<String> = ArrayList<String>()
        if (prefString != null) {
            val gson: Gson = Gson()
            val type = object : TypeToken<ArrayList<String>>() {

            }.getType()
            mediaPaths.addAll(gson.fromJson(prefString, type))
        }
        if (mediaPaths.isEmpty() || Calendar.getInstance().timeInMillis < (convertDateStringInMillis(endDate) + 86400000)) {
            val projection = arrayOf(MediaStore.MediaColumns.DATA)
            val uri: Uri
            val dateTakenColumn: String
            val cameraColumn: String = MediaStore.Images.Media.BUCKET_DISPLAY_NAME

            if (isImage) {
                uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                dateTakenColumn = MediaStore.Images.ImageColumns.DATE_TAKEN
            } else {
                uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                dateTakenColumn = MediaStore.Video.VideoColumns.DATE_TAKEN
            }

            val orderBy = dateTakenColumn + " DESC"
            val selection = "$cameraColumn=? and $dateTakenColumn>=? and $dateTakenColumn<=?"
            val selectionArgs = arrayOf("Camera", convertDateStringInMillis(startDate).toString(), (convertDateStringInMillis(endDate) + 86400000).toString())

            val cursor = context.contentResolver.query(uri,
                    projection,
                    selection,
                    selectionArgs,
                    orderBy)

            val dataColumnIndex = cursor?.getColumnIndex(MediaStore.MediaColumns.DATA)

            if (cursor != null) {
                while (cursor.moveToNext()) {
                    val dataString = cursor.getString(dataColumnIndex!!)
                    if (!mediaPaths.contains(dataString))
                        mediaPaths.add(dataString)
                }
            }

            saveMedia(mediaPaths, isImage)

            cursor?.close()
        }
        return mediaPaths
    }

    fun saveMedia(mediaPaths: ArrayList<String>, isImage: Boolean) {
        val gson: Gson = Gson()
        SharedPrefsUtils.setStringPreference(if (isImage) DATA_IMAGE else DATA_VIDEO, gson.toJson(mediaPaths))
    }

    fun convertDateStringInMillis(dateString: String): Long {
        val simpleDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        val date = simpleDateFormat.parse(dateString)
        return date.time
    }

    public fun saveStartTimeOf(tripId: Long) {
        SharedPrefsUtils.setStringPreference(Constants.KEY_SAVED_TRIP + tripId, convertMillisToDateString(Calendar.getInstance().time))
    }

    fun convertMillisToDateString(timeInMillis: Long): String {
        return convertMillisToDateString(Date().apply { time = timeInMillis })
    }

    fun convertMillisToDateString(time: Date): String {
        val simpleDateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        return simpleDateFormat.format(time)
    }
}