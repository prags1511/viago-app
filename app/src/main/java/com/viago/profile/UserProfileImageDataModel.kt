package com.viago.profile

data class UserProfileImageDataModel(
        val url: String,
        val status: String

)

