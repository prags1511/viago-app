package com.viago.profile

/**
 * Created by mayank on 8/10/17.
 */
data class ProfileUpdateRequest(
        val country: String,
        val gender: String,
        val date_of_birth: String

)