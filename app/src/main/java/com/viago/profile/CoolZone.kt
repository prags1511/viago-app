package com.viago.profile

/**
 * Created by mayank on 22/8/17.
 */
data class CoolZone(
        val name: String,
        val imageUrl: String?,
        val startDate: String,
        val endDate: String,
        val imageCount: Int,
        val videoCount: Int
)