package com.viago.profile

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bindView
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.extensions.dpToPx
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.trips.MyTrip
import com.viago.trips.TripsUtil
import com.viago.utils.SpacesItemDecoration
import timber.log.Timber
import java.util.*
import kotlin.concurrent.timer


/**
 * Created by mayank on 22/8/17.
 */
class CoolZoneActivity : BaseDrawerActivity(), CoolZoneInterface {
    override fun getLayoutRes() = R.layout.activity_cool_zone

    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    private var nextUrl: String? = null
    private val userToken by lazy { UserUtil.getUserToken() }
    override var coolZone: CoolZone? = null
    private var tripMediaFragment: TripMediaFragment? = null
    private var coolZoneAdapter :ImageGridAdapter?=null;
    private var manager : GridLayoutManager?=null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        this.tripMediaFragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreated(savedInstanceState: Bundle?) {
        toolbar.title = "Cool Zone"

        manager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)

        recyclerView.run {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            layoutManager = manager
            addItemDecoration(SpacesItemDecoration(dpToPx(0)))

            if (arePermissionsGranted()) {
                imagelist= fetchImageList();
                coolZoneAdapter = ImageGridAdapter(this@CoolZoneActivity,imagelist)
            } else {
                requestPermissions()
            }


            adapter = coolZoneAdapter

//            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
//                override fun onLoadMore(page: Int, totalItemsCount: Int) {
//                    nextUrl?.let { TripsUtil.getMoreMyTrips(userToken, it, onSuccess, onFailure) }
//                }
//            })
        }

      //  getSavedTrips()
    }

    private val onSuccess = { baseListModel: BaseListModel<MyTrip> ->
       /* coolZoneAdapter.addItemsAtBottom(baseListModel.results.filter {
            SharedPrefsUtils.contains(Constants.KEY_SAVED_TRIP + it.id)
        }.mapNotNull { myTrip: MyTrip ->
            val coolZone: CoolZone?
            val startDate = SharedPrefsUtils.getStringPreference(Constants.KEY_SAVED_TRIP + myTrip.id)
            if (startDate != null && myTrip.duration != null) {
                val endDate = MediaDBUtil.convertMillisToDateString(MediaDBUtil.convertDateStringInMillis(startDate) + (2 * convertReadableTimeToMillis(myTrip.duration)))
                val imageCount = MediaDBUtil.getImageCount(this, startDate, endDate)
                val videoCount = MediaDBUtil.getVideoCount(this, startDate, endDate)
                coolZone = CoolZone(myTrip.title, myTrip.image, startDate, endDate, imageCount, videoCount)
            } else {
                coolZone = null
            }
            coolZone
        })
        nextUrl = baseListModel.next
        if (nextUrl.isNullOrEmpty()) recyclerView.clearOnScrollListeners()*/
    }

    private val onFailure: (String) -> Unit = {
        showToast(it)
    }


    private fun convertReadableTimeToMillis(readableTime: String): Long {
        var timeInMillis: Long = 1
        val readableTimeParts = readableTime.split(" ")

        for (i in 0 until readableTimeParts.size step 2) {
            val unit = readableTimeParts[i + 1].toLowerCase()
            val value = readableTimeParts[i]

            if (unit.contains("day")) {
                timeInMillis += value.toInt() * 86400000
            } else if (unit.contains("hr")) {
                timeInMillis += value.toInt() * 3600000
            } else if (unit.contains("min")) {
                timeInMillis += value.toInt() * 60000
            }
        }

        return timeInMillis
    }

    private fun getSavedTrips() {
        if (arePermissionsGranted()) {
            TripsUtil.getMyTrips(userToken, onSuccess, onFailure)
        } else {
            requestPermissions()
        }
    }

    private fun arePermissionsGranted(): Boolean {
        return ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        /* if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                 Manifest.permission.WRITE_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(this,
                 Manifest.permission.READ_EXTERNAL_STORAGE)) {

             //TODO open Settings
         } else {*/
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_PERMISSIONS)
//        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_PERMISSIONS) {
            if (grantResults.all { it == PackageManager.PERMISSION_GRANTED }) {
                recyclerView.run {
                    layoutManager = LinearLayoutManager(context)
                    setHasFixedSize(true)
                    layoutManager = manager
                    addItemDecoration(SpacesItemDecoration(dpToPx(0)))

                    if (arePermissionsGranted()) {
                        imagelist= fetchImageList();
                        coolZoneAdapter = ImageGridAdapter(this@CoolZoneActivity,imagelist)
                    } else {
                        requestPermissions()
                    }


                    adapter = coolZoneAdapter

//            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
//                override fun onLoadMore(page: Int, totalItemsCount: Int) {
//                    nextUrl?.let { TripsUtil.getMoreMyTrips(userToken, it, onSuccess, onFailure) }
//                }
//            })
                }
            } else {
                showToast("The app was not  allowed to read or write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG)
            }
        }
    }

    companion object {
        private const val REQUEST_PERMISSIONS: Int = 9131
        lateinit var imagelist: ArrayList<String>
    }

    fun fetchImageList(): ArrayList<String> {

        val imageArrayList = ArrayList<String>()
        val int_position = 0
        val uri: Uri
        val cursor: Cursor?
        val column_index_data: Int
        val column_index_folder_name: Int

        var absolutePathOfImage: String? = null
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI

        val projection = arrayOf(MediaStore.MediaColumns.DATA, MediaStore.Images.Media.BUCKET_DISPLAY_NAME)

        val orderBy = MediaStore.Images.Media.DATE_TAKEN

        cursor = applicationContext.contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, "$orderBy DESC")

        column_index_data = cursor!!.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
        if (cursor != null) {
            Timber.d("ImageUtils: ${cursor.count}" )
            Timber.d("column_index_data: $column_index_data")
        }
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data)
            //            Log.e("Column", absolutePathOfImage);
            //            ImageBean imageInfo = new ImageBean();
            //            imageInfo.setImagePath(absolutePathOfImage);
            //            imageInfo.setChecked(false);
            imageArrayList.add(absolutePathOfImage)
        }
        return imageArrayList

    }
}