package com.viago.profile

import android.content.Context
import android.os.Bundle
import androidx.annotation.IntDef
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.listeners.ContextualModeInteractor
import com.viago.utils.SpacesItemDecoration
import java.util.ArrayList

/**
 * Created by mayank on 23/8/17.
 */
class MediaFragment private constructor() : BaseFragment() {

    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    private lateinit var coolZoneInterface: CoolZoneInterface
    private lateinit var interactor: ContextualModeInteractor
    private lateinit var mediaAdapter: MediaAdapter
    private var mediaType: Int = -1

    override fun getLayoutRes() = R.layout.recycler_view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        //@MediaType val mediaType: Int = arguments.getInt(MEDIA_TYPE)
        mediaType = arguments!!.getInt(MEDIA_TYPE)

        mediaAdapter = MediaAdapter(mediaType, HashSet<String>(), interactor)

        recyclerView.run {
            layoutManager = GridLayoutManager(context, 3)
            setHasFixedSize(true)
            addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            adapter = mediaAdapter
        }

        coolZoneInterface.coolZone?.let {
            mediaAdapter.addItemsAtBottom(
                    if (mediaType == IMAGE) MediaDBUtil.getImages(context!!, it.startDate, it.endDate)
                    else MediaDBUtil.getVideos(context!!, it.startDate, it.endDate))
        }
    }

    fun addMedia(mPaths: List<String>) {
        var modified: Boolean = false
        for (item in mPaths) {
            if (!mediaAdapter.itemsList.contains(item)) {
                mediaAdapter.addItemAtBottom(item)
                modified = true
            }
        }
        if (modified) {
            saveMedia(mediaAdapter.itemsList)
        }
    }

    fun removeSelectedMedia() {
        for (item in mediaAdapter.selectedSet) {
            mediaAdapter.findAndRemoveItem(item)
        }
        interactor.updateSelectedCount(0)
        saveMedia(mediaAdapter.itemsList)
    }

    fun contextualModeDisabled() {
        mediaAdapter.selectedSet.clear()
        mediaAdapter.notifyDataSetChanged()
    }

    private fun saveMedia(itemsList: List<String>) {
        MediaDBUtil.saveMedia(itemsList as ArrayList<String>, mediaType == 0)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        coolZoneInterface = context as CoolZoneInterface
    }


    companion object {

        const val IMAGE = 0
        const val VIDEO = 1

        private const val MEDIA_TYPE = "Media Type"

        @IntDef(IMAGE, VIDEO)
        @Retention(AnnotationRetention.SOURCE)
        annotation class MediaType

        public fun newInstance(@MediaType mediaType: Int, interactor: ContextualModeInteractor): MediaFragment {
            val mediaFragment: MediaFragment = MediaFragment().apply { arguments = Bundle().apply { putInt(MEDIA_TYPE, mediaType) } }
            mediaFragment.interactor = interactor
            return mediaFragment
        }
    }
}