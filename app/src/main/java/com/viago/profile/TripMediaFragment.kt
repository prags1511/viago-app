package com.viago.profile

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.Toolbar
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.baseclasses.UniversalStatePagerAdapter
import net.alhazmy13.mediapicker.Image.ImagePicker
import net.alhazmy13.mediapicker.Video.VideoPicker
import android.app.Activity.RESULT_OK
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.viago.listeners.ContextualModeInteractor


/**
 * Created by mayank on 23/8/17.
 */
class TripMediaFragment : BaseFragment(), ContextualModeInteractor {
    private val contextualLayout by bindView<RelativeLayout>(R.id.contextualLayout)
    private val selectedCount by bindView<TextView>(R.id.selectedCount)
    private val selectedDelete by bindView<ImageView>(R.id.selectedDelete)
    private val viewPager by bindView<ViewPager>(R.id.view_pager)
    private val tabs by bindView<TabLayout>(R.id.tabs)
    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val fab by bindView<FloatingActionButton>(R.id.fab)

    private lateinit var coolZoneInterface: CoolZoneInterface
    private lateinit var mediaImageFragment: MediaFragment
    private lateinit var mediaVideoFragment: MediaFragment

    override fun getLayoutRes() = R.layout.fragment_trip_media

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(toolbar) {
            setNavigationIcon(com.viago.R.drawable.ic_chevron_left_white_24dp)
            title = coolZoneInterface.coolZone?.name
            getAppCompatActivity().setSupportActionBar(this)
            setNavigationOnClickListener { if (isContextualMode()) disableContextualMode() else activity!!.onBackPressed() }
        }

        mediaImageFragment = MediaFragment.newInstance(MediaFragment.IMAGE, this)
        mediaVideoFragment = MediaFragment.newInstance(MediaFragment.VIDEO, this)

        viewPager.run {
            adapter = UniversalStatePagerAdapter<Fragment>(childFragmentManager).apply {
                addFrag(mediaImageFragment, "Photos")
                addFrag(mediaVideoFragment, "Videos")
            }
            tabs.setupWithViewPager(this)
        }

        viewPager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
            override fun onPageSelected(position: Int) {
                disableContextualMode()
            }
        })

        fab.setOnClickListener {
            if (viewPager.currentItem == 0) {
                ImagePicker.Builder(activity).mode(ImagePicker.Mode.GALLERY).allowMultipleImages(true).build()
            } else {
                VideoPicker.Builder(activity).mode(VideoPicker.Mode.GALLERY).build()
            }
        }
        selectedDelete.setOnClickListener {
            TripMediaDeleteConfirmation().let {
                it.onOkayClicked = {
                    if (viewPager.currentItem == 0) mediaImageFragment.removeSelectedMedia() else mediaVideoFragment.removeSelectedMedia()
                }
                it.onCancelClicked = {
                }
                it.show(childFragmentManager, TripMediaDeleteConfirmation::class.java.simpleName)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == VideoPicker.VIDEO_PICKER_REQUEST_CODE) {
                val mPaths: List<String> = data?.getSerializableExtra(VideoPicker.EXTRA_VIDEO_PATH) as List<String>
                if (!mPaths.isEmpty() && viewPager.currentItem == 1) {
                    mediaVideoFragment.addMedia(mPaths)
                }
            } else if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE) {
                val mPaths: List<String> = data?.getSerializableExtra(ImagePicker.EXTRA_IMAGE_PATH) as List<String>
                if (!mPaths.isEmpty() && viewPager.currentItem == 0) {
                    mediaImageFragment.addMedia(mPaths)
                }
            }
        }
    }

    override fun enableContextualMode() {
        getAppCompatActivity().getSupportActionBar()?.setDisplayShowTitleEnabled(false)
        contextualLayout.visibility = View.VISIBLE
    }

    override fun isContextualMode(): Boolean = contextualLayout.visibility == View.VISIBLE

    override fun updateSelectedCount(count: Int) {
        if (count > 0) {
            selectedCount.setText("%d selected".format(count))
        } else {
            disableContextualMode()
        }
    }

    private fun disableContextualMode() {
        contextualLayout.visibility = View.GONE
        getAppCompatActivity().getSupportActionBar()?.setDisplayShowTitleEnabled(true)
        if(viewPager.currentItem==0) mediaImageFragment.contextualModeDisabled() else mediaVideoFragment.contextualModeDisabled()
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        coolZoneInterface = context as CoolZoneInterface
    }

}