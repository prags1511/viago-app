package com.viago.profile

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.views.RoundCornerImageView

/**
 * Created by mayank on 22/8/17.
 */
class CoolZoneAdapter(inline val onItemCLick: (CoolZone) -> Unit) : EasyRecyclerViewAdapter<CoolZone>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CoolZoneViewHolder(parent.inflate(R.layout.item_cool_zone))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, coolZone: CoolZone, position: Int) {
        if (holder is CoolZoneViewHolder) holder.bind(coolZone)
    }

    inner class CoolZoneViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val image by bindView<RoundCornerImageView>(R.id.image)
        //private val share by bindView<ImageView>(R.id.share)
        private val name by bindView<TextView>(R.id.name)
        private val date by bindView<TextView>(R.id.date)
        private val numberOfImages by bindView<TextView>(R.id.number_of_images)
        private val numberOfVideos by bindView<TextView>(R.id.number_of_videos)

        init {
            itemView.setOnClickListener { onItemCLick(getItem(adapterPosition)) }

//            share.setOnClickListener {
//                //Todo
//            }
        }

        internal fun bind(coolZone: CoolZone) {
            image.loadImage(coolZone.imageUrl)
            name.text = coolZone.name
            date.text = coolZone.startDate
            numberOfImages.text = coolZone.imageCount.toString()
            numberOfVideos.text = coolZone.videoCount.toString()
        }
    }
}