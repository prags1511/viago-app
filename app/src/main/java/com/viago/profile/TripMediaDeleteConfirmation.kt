package com.viago.profile

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import android.widget.TextView
import com.viago.R
import com.viago.baseclasses.BaseDialog

/**
 * Created by a_man on 19-11-2017.
 */
class TripMediaDeleteConfirmation : BaseDialog() {
    var onOkayClicked: (() -> Unit)? = null
    var onCancelClicked: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_approve_suggestions, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            val title: TextView = findViewById<TextView>(R.id.title) as TextView
            val message: TextView = findViewById<TextView>(R.id.message) as TextView
            val cancel: Button = findViewById<Button>(R.id.update_current) as Button
            val okay: Button = findViewById<Button>(R.id.create_new) as Button

            title.setText(getString(R.string.trip_media_delete_title))
            message.setText(getString(R.string.trip_media_delete_message))
            okay.setText("Okay")
            cancel.setText("Cancel")

            okay.setOnClickListener {
                onOkayClicked?.invoke()
                dismiss()
            }
            cancel.setOnClickListener {
                onCancelClicked?.invoke()
                dismiss()
            }
        }

        return dialog
    }
}