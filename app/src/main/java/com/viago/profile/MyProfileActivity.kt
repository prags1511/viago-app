package com.viago.profile

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import android.provider.MediaStore
//import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import bindView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.bumptech.glide.signature.ObjectKey
import com.google.gson.Gson
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker
import com.viago.BuildConfig
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.activities.OptionActivity
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.baseclasses.BaseLocationFragment
import com.viago.extensions.closeKeyboard
import com.viago.extensions.loadImage
import com.viago.extensions.showToast
import com.viago.login.LoginActivity
import com.viago.login.LoginUtil
import com.viago.login.UserUtil
import com.viago.profile.Utils.*
import com.viago.trips.TripsUtil
import com.viago.utils.*
import com.viago.utils.FileUtils.*
import com.viago.views.CircularImageView
import com.viago.views.MyDatePickerDialog
import okhttp3.*
import timber.log.Timber
import java.io.*
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by mayank on 22/8/17.
 */
class MyProfileActivity : BaseDrawerActivity(), View.OnClickListener {

    private val image by bindView<CircularImageView>(R.id.image)
    private val name by bindView<TextView>(R.id.name)
    private val email by bindView<TextView>(R.id.email)
    private val dob by bindView<TextView>(R.id.dob)
    private val gender by bindView<TextView>(R.id.gender)
    private val country by bindView<TextView>(R.id.country)
    private val totalTrips by bindView<TextView>(R.id.total_trips)
    private val placesVisited by bindView<TextView>(R.id.places_visited)
    private val savedTrips by bindView<TextView>(R.id.saved_trips)
    private val sharedTrips by bindView<TextView>(R.id.shared_trips)
    private val camerapicture by bindView<ImageView>(R.id.camerapicture)

    //private val bottomNavigationView by bindView<BottomNavigationView>(R.id.bottom_navigation)
    private lateinit var menuItemEdit: MenuItem
    private lateinit var menuItemDone: MenuItem
    private var photoPath: String? = ""
    private var isEditingProfile = false
    private var isEditingProfileImage = false
    private var selectedDateOfBirth: String = ""
    private var mCurrentPhotoPath: String? = "";
    private lateinit var photoURI: Uri ;
    private lateinit var path : String;
    private lateinit var fileUri: Uri
    private val ACTIVITY_RESULT_CODE_REQUEST_CAMERA: Int = 103
    private val ACTIVITY_RESULT_CODE_SELECT_FILE: Int = 102
    private val PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: Int = 105

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Getting Profile")
            isIndeterminate = true
            setCancelable(false)
        }
    }

    private var isProfileObtained = false
    private var imageUrl: String? = null;
    override fun getLayoutRes() = R.layout.fragment_my_profile

    override fun onCreated(savedInstanceState: Bundle?) {
        toolbar.title = ""


        checkPermission(this@MyProfileActivity)

        camerapicture.setOnClickListener(View.OnClickListener {
            if (isEditingProfile && checkPermission(this@MyProfileActivity)) {
                alert()

            }
        })

        getProfile()


        arrayOf(dob, gender, country).forEach { it.setOnClickListener(this) }

//        bottomNavigationView.setSelectedItemId(R.id.action_profile);
//
//        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
//            when (item.itemId) {
//                R.id.action_home -> {
//                    startActivity(Intent(this,MainActivity::class.java))
//                    finish()
//                }
//                R.id.action_profile -> {
//                    startActivity(Intent(this,MyProfileActivity::class.java))
//
//                }
//                R.id.action_invite -> {
//                    GeneralUtils.performInviteAction(this)
//                }
//            }
//            false
//        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_edit -> {
                if (isProfileObtained) {
                    isEditingProfile = true
                    menuItemEdit.setVisible(false)
                    menuItemDone.setVisible(true)

                    name.isEnabled = false
                } else {
                    getProfile()
                }
            }
            R.id.action_done -> {
                //updateProfile()
                if(isEditingProfileImage){
                    isEditingProfileImage = false
                    profileToServer()
                }else {

                    updateProfile()

                }

            }
            R.id.action_logout -> {
                UserUtil.logout()
                startActivity(Intent(this, LoginActivity::class.java))
                finishAffinity()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun alert(){

        val colors = arrayOf("Camera", "Gallery")

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Select Option")
        builder.setItems(colors) { dialog, which ->

            if(which==0){
                camera()

            }

            if(which==1){

                gallery()
            }


        }
        builder.show()
    }


    fun gallery(){
        if(checkPermission(this@MyProfileActivity)) {

            if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.M) {

                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, ""), ACTIVITY_RESULT_CODE_SELECT_FILE)

            }else {

                var intent=  Intent(Intent.ACTION_PICK);
       // Sets the type as image/*. This ensures only components of type image are selected
       intent.setType("image/*");
       //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
                var mimeTypes = arrayOf("image/jpeg", "image/png") ;
       intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
       // Launching the Intent
       startActivityForResult(intent,ACTIVITY_RESULT_CODE_SELECT_FILE);
            }
        }
    }

    fun camera() {
        if (checkPermissionForCamera()) {
            if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.M) {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, ACTIVITY_RESULT_CODE_REQUEST_CAMERA)

            } else {

                var takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                try {
                    var photoFile = createImageFile(this);
                    path = photoFile.getAbsolutePath();
                    photoURI = FileProvider.getUriForFile(this,
                            BuildConfig.APPLICATION_ID + ".provider",
                            photoFile);

                } catch (ex: Exception) {
                    Log.e("TakePicture", ex.message);
                }
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
               // takePictureIntent.setClipData(ClipData.newRawUri("", photoURI));
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, ACTIVITY_RESULT_CODE_REQUEST_CAMERA);

            }


        } else {

            requestPermissionCamera()
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ACTIVITY_RESULT_CODE_REQUEST_CAMERA -> {
                    onCaptureImageResult(data)
                }
                ACTIVITY_RESULT_CODE_SELECT_FILE -> {
                    onSelectFromGalleryResult(data)
                }


            }
        }
    }

    private fun onSelectFromGalleryResult(data: Intent?) {
        isEditingProfileImage = true

        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.M) {
            var bm: Bitmap? = null
            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(applicationContext.contentResolver, data.data)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            image.setImageBitmap(bm)
            if (bm != null) {

                photoPath = saveImage(bm)
            }
        }else {

            var selectedImage = data!!.getData();
            var filePathColumn =  arrayOf(MediaStore.Images.Media.DATA )
            // Get the cursor
            var cursor = getContentResolver().query(selectedImage!!, filePathColumn, null, null, null);
            // Move to first row
            cursor?.moveToFirst();
            //Get the column index of MediaStore.Images.Media.DATA
            var columnIndex = cursor?.getColumnIndex(filePathColumn[0]);
            //Gets the String value in the column
            var imgDecodableString = cursor?.getString(columnIndex!!);
            cursor?.close();
            // Set the Image in ImageView after decoding the String
           // image.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

          //  var bitmap = BitmapFactory.decodeFile(imgDecodableString)

           // val bitmap = BitmapFactory.decodeStream(contentResolver.openInputStream(selectedImage))

          //  var imageBitmap =  getBitmap(imgDecodableString, bitmap)

            var lastProfileCache = System.currentTimeMillis();



            Glide.with(this)

                    .apply {
                        RequestOptions.circleCropTransform()
                        RequestOptions().signature( ObjectKey(lastProfileCache.toString()))
                        RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                        RequestOptions().error(R.drawable.placeholder)


                    }

                    .load(imgDecodableString)
                    .into(image)


          //  image.loadImage(imgDecodableString)

            photoPath = imgDecodableString


        }



    }



    private fun onCaptureImageResult(data: Intent?) {
        isEditingProfileImage = true
        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.M){

            Log.e("photo path1:", "" + data + "")
            if (data == null) return
            val thumbnail = data.extras!!.get("data") as Bitmap

            var tempUri = getImageUri(getApplicationContext(), thumbnail);
            photoPath = getRealPathFromURI(this,tempUri);

            //   iv_image.setImageBitmap(thumbnail)
            if (thumbnail != null) {
                image.setImageBitmap(thumbnail)
            }
            //    photoPath = saveImage(thumbnail)
            Log.e("photo path1:", photoPath)
            // image.loadImage(photoPath)

        }else {

         // var source = BitmapFactory.decodeFile(path, provideCompressionBitmapFactoryOptions());

           // var imageBitmap =  getBitmap(path, source)

            var lastProfileCache = System.currentTimeMillis();

            Glide.with(this)
                    .apply {
                        RequestOptions.circleCropTransform()
                        RequestOptions().signature( ObjectKey(lastProfileCache.toString()))
                        RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE)
                                .skipMemoryCache(true)
                        RequestOptions().error(R.drawable.placeholder)


                    }
                    .load(path)
                    .into(image)


         //   image.loadImage(path)
            //image.setImageBitmap(imageBitmap);
            photoPath = path
        }
    }





    private fun getProfile() {
        progressDialog.setMessage("Getting Profile...")
        progressDialog.show()
        sharedTrips.text = SharedPrefsUtils.getIntegerPreference(Constants.KEY_COUNT_SHARE, 0).toString()
        savedTrips.text = TripsUtil.getSavedTrips().size.toString()
        LoginUtil.getProfile(UserUtil.getUserToken(), {
            progressDialog.dismiss()
            isProfileObtained = true

            UserUtil.updateUser(it)

         //   image.loadImage(it.image)

            Glide.with(this)
                    .apply {
                        RequestOptions()
                                .error(R.drawable.placeholder)
                    }

                    .load(it.image)

                    .into(image)

          // var bitmap = FileUtils.getBitmapFromURL(it.image);

         //   image.setImageBitmap(bitmap)

          //  loadImage(it.image, image)

            photoPath = it.image
            name.text = it.name
            email.text = it.email
            if (!it.date_of_birth.isNullOrBlank()) {
                dob.text = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(it.date_of_birth))
                selectedDateOfBirth = it.date_of_birth
            }
            gender.text = if (it.gender == "M") "Male" else "Female"
            country.text = it.country
            totalTrips.text = it.trips_count.toString()
            placesVisited.text = it.locations_count.toString()

        }, {
            showToast(it)
            progressDialog.dismiss()
        })


    }


    /* if(!checkPermissionForCamera()){

                requestPermissionCamera();
            }else {

                camera()

            }*/

    override fun onClick(v: View?) {
        if (!isEditingProfile) return

        when (v) {
            dob -> showDatePicker()
            gender -> showGenderDialog()
            country -> showCountryPicker()
        }
    }

    private fun showGenderDialog() {
        AlertDialog.Builder(this)
                .setSingleChoiceItems(arrayOf("Male", "Female"), if (gender.text == "Male") 0 else 1) { d: DialogInterface, which: Int ->
                    if (which == 0) gender.text = "Male"
                    else if (which == 1) gender.text = "Female"
                    d.dismiss()
                }
                .setTitle("Gender")
                .show()
    }

    private fun showDatePicker() {
        Timber.d("show date picker")
        val calendar = Calendar.getInstance()
        //Make sure user is atleast 13 years old
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 13)

        MyDatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    with(Calendar.getInstance().apply {
                        set(Calendar.YEAR, year)
                        set(Calendar.MONTH, month)
                        set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    }.time) {
                        dob.text = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()).format(this)
                        selectedDateOfBirth = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(this)
                    }
                },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        ).apply {
            setPermanentTitle("Date of Birth")
            datePicker.maxDate = calendar.timeInMillis;
        }.show()
    }

    private fun showCountryPicker() {
        CountryPicker.newInstance("Select country")
                .apply { setListener { name, _, _, _ -> country.text = name; dismiss() } }
                .show(supportFragmentManager, "COUNTRY_PICKER")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_profile_menu, menu)
        menuItemEdit = menu!!.findItem(R.id.action_edit)
        menuItemDone = menu.findItem(R.id.action_done)
        return true
    }




    private fun updateProfile() {
        if (!validateForm()) return

        progressDialog.setMessage("Updating Profile...")
        progressDialog.show()

        if (Country.getCountryByName(country.text.toString()) == null) {
            country.error = "give a valid country"
            return
        }

        val registrationRequest = ProfileUpdateRequest(
//                name.text.toString().capitalize(),
                country.text.toString(),
                if ("female" == gender.text.toString().toLowerCase()) "F" else "M",
                selectedDateOfBirth

        )





        LoginUtil.updateProfile(UserUtil.getUserToken(), registrationRequest, {
            UserUtil.updateUser(it)
            setUserData()
            progressDialog.dismiss()
            showToast("Profile updated")
            isEditingProfile = false
            menuItemDone.setVisible(false)
            menuItemEdit.setVisible(true)
        }, {
            showToast(it)
            progressDialog.dismiss()
        })
    }



    fun createFile(realPath: String): File {
        return File(realPath)
    }

    fun createRequestBody(file: File): RequestBody {
        val MEDIA_TYPE_IMAGE: MediaType = MediaType.parse("image/*")!!
        return RequestBody.create(MEDIA_TYPE_IMAGE, file)
    }

    fun createPart(file: File, requestBody: RequestBody): MultipartBody.Part {
        return MultipartBody.Part.createFormData("image", file.name, requestBody)
    }



    private fun profileToServer( ) {

        if (!validateForm()) return

        progressDialog.setMessage("Updating Profile...")
        progressDialog.show()

        if (Country.getCountryByName(country.text.toString()) == null) {
            country.error = "give a valid country"
            return
        }

        val registrationRequest = ProfileUpdateRequest(
//                name.text.toString().capitalize(),
                country.text.toString(),
                if ("female" == gender.text.toString().toLowerCase()) "F" else "M",
                selectedDateOfBirth

        )

        Glide
                .with(this)
                .asBitmap()
                .load(photoPath)
                .into(object : SimpleTarget<Bitmap>(93, 93) {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>) {

                        //    var resizeFile = resizeAndCompressImageBeforeSend(this,photoPath, "profileImage"+ System.currentTimeMillis()+".png");

                       // var fileImage = createFile(photoPath!!)

                        //  photoPath = fileImage.absolutePath

                        // if(resizeFile!=null && !resizeFile.isEmpty()){

                        // fileImage = createFile(resizeFile)

                        // }

                        var fileImage = getFileFromBitmap(this@MyProfileActivity, resource);


                        var createRequestBody = createRequestBody(fileImage)

                        var multipart = createPart(fileImage,createRequestBody)

                        var id = 0L;

                        UserUtil.getUser()?.let {

                            id = it.pk

                        }



                        LoginUtil.updateProfilewithImage(id, multipart,registrationRequest, {

                            //  var gson = Gson()
                            // var mMineUserEntity = gson?.fromJson(it.string(), UserProfileImageDataModel::class.java)


                            UserUtil.updateUser(it)
                            //   image.loadImage(mMineUserEntity.url)
                            setUserData()
                            // image.loadImage(it.image)
                            progressDialog.dismiss()
                            showToast("Profile updated")
                            isEditingProfile = false
                            menuItemDone.setVisible(false)
                            menuItemEdit.setVisible(true)
                        }, {
                            showToast(it)
                            progressDialog.dismiss()
                            menuItemDone.setVisible(false)
                            menuItemEdit.setVisible(true)
                        })



                    }

                })







 }




    private fun validateForm(): Boolean {
        var valid = true

        closeKeyboard()

        arrayOf(name, email, country, dob, gender).forEach { v ->
            if (v.text.isNullOrBlank()) {
                v.error = "Required"
                valid = false
            } else {
                v.error = null
            }
        }

        return valid
    }

//    private fun processCapturedPhoto() {
//
//
////        val cursor = contentResolver.query(Uri.parse(mCurrentPhotoPath),
////                Array(1) {android.provider.MediaStore.Images.ImageColumns.DATA},
////                null, null, null)
////
////
////
////       // Toast.makeText(this@MyProfileActivity,""+mCurrentPhotoPath,Toast.LENGTH_LONG).show()
////        if(cursor!=null) {
////            cursor.moveToFirst()
////            photoPath = cursor.getString(0)
////            cursor.close()
////        }
//
//     //   val selectedImage = imageReturnedIntent.getData()
//      //  imageview.setImageURI(selectedImage)
//
//        image.loadImage(photoPath)
//
//    }







    fun getPath(uri: Uri?): String? {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or show user feedback
            return null
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = managedQuery(uri, projection, null, null, null)
        if (cursor != null) {
            val column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val path = cursor.getString(column_index)
            cursor.close()
            return path
        }
        // this is our fallback here
        return uri.path
    }



    private fun saveImage(bitmap: Bitmap): String {

        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)

        // create a directory if it doesn't already exist
        val photoDirectory = File(getExternalStorageDirectory().absolutePath + "/cameraphoto/")
        if (!photoDirectory.exists()) {
            photoDirectory.mkdirs()
        }

        val destination = File(photoDirectory, "captured_photo"  + ".jpg")
        sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(destination)))
        Log.e("photo path:", destination.absolutePath)
        var outFile: OutputStream? = null
        try {
            outFile =  FileOutputStream(destination);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
            outFile.flush();
            outFile.close();
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (destination != null) {

            return destination.absolutePath
        }

        return "";
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
         if (resultCode == Activity.RESULT_OK
                && requestCode == TAKE_PHOTO_REQUEST) {
           // processCapturedPhoto()

             val selectedImage = data!!.getData()
             val photoPath = PathUtil.getPath(this@MyProfileActivity, selectedImage)

             image.loadImage(photoPath)
        }
    }*/




    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ACTIVITY_RESULT_CODE_REQUEST_CAMERA) {
            camera()
        } else if (requestCode == PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {

            if (imageUrl != null) {
                Glide.with(this)
                        .load(Uri.parse(imageUrl))
                        .into(image)
                image.loadImage(imageUrl)
            }
        }
    }


    fun checkPermissionForCamera(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val result = this@MyProfileActivity.checkSelfPermission(Manifest.permission.CAMERA)
            return result == PackageManager.PERMISSION_GRANTED
        }
        return false
    }


    fun requestPermissionCamera() {
        try {
            ActivityCompat.requestPermissions(this@MyProfileActivity, arrayOf(Manifest.permission.CAMERA),
                    ACTIVITY_RESULT_CODE_REQUEST_CAMERA)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private fun checkPermission(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(context as Activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    val alertBuilder = AlertDialog.Builder(context)
                    alertBuilder.setCancelable(true)
                    alertBuilder.setTitle("Permission Needed")
                    alertBuilder.setMessage("External storage permission is needed to proceed.")
                    alertBuilder.setPositiveButton(android.R.string.yes) { _, _ ->
                        ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 102)
                    }
                    val alert = alertBuilder.create()
                    alert.show()
                } else {
                    ActivityCompat.requestPermissions(context, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 102)
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }



}