package com.viago.appscreens

import android.os.Bundle
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity

/**
 * Created by mayank on 22/8/17.
 */
class SettingsActivity: BaseDrawerActivity() {

    override fun getLayoutRes() = R.layout.activity_settings

    override fun onCreated(savedInstanceState: Bundle?) {
        toolbar.title = ""
    }
}