package com.viago.activities

import android.animation.Animator
import android.animation.Animator.AnimatorListener
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageView
import bindView
import com.viago.R
import com.viago.baseclasses.BaseActivity
import com.viago.extensions.loadImage
import com.viago.login.LoginActivity
import com.viago.login.UserUtil
import com.viago.trips.suggesttrip.SuggestTripActivity
import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar
import io.branch.referral.Branch
import timber.log.Timber


class SplashScreenActivity : BaseActivity() {

    private val appLogo by bindView<ImageView>(R.id.app_logo)
   // private val background by bindView<ImageView>(R.id.background)

    private val ANIMATION_DURATION = 1200L
    private val ANIMATION_DELAY = 1000L
    private val POST_ANIMATION_DELAY = 1000L
    private var isHandlingDeepLink: Boolean = false
    private val mDilatingDotsProgressBar by bindView<DilatingDotsProgressBar>(R.id.progress)
    lateinit var data: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("onCreate")
        setContentView(R.layout.activity_splash_screen)

        appLogo.loadImage(R.drawable.logo_new, false)
      //  background.loadImage(R.drawable.background_overley, false)

        mDilatingDotsProgressBar.showNow();
        if(this.intent.data != null)
            data = this.intent.data!!
        else
            data = Uri.EMPTY
// stop animation and hide
        // mDilatingDotsProgressBar.hideNow();

        /*https://github.com/JustZak/DilatingDotsProgressBar*/

        //appLogo.alpha = 0F
//Log.d("splash*",this.intent.data.toString())

        Handler().postDelayed({
            Timber.d("in handler")

                Handler().postDelayed({
                    val branch = Branch.getInstance()
                    branch.initSession({ branchUniversalObject, linkProperties, error ->
                        if (error == null) {
                            Timber.d("error is null")
                        } else {
                            Timber.d("error is not null")
                        }
                        if (branchUniversalObject == null) {
                            Timber.d("branchUniversalObject is null")
                        } else {
                            Timber.d("branchUniversalObject is not null")
                        }
                        if (error == null && branchUniversalObject != null) {
                            isHandlingDeepLink = true
                            if (!UserUtil.isLoggedIn()) {
                                startLoginActivity()
                            } else {
                                // This code will execute when your app is opened from a Branch deep link, which
                                // means that you can route to a custom activity depending on what they clicked.
                                // In this example, we'll just print out the data from the link that was clicked.

                                Timber.i("referring Branch Universal Object: $branchUniversalObject")
                                Timber.i("trip_id" + branchUniversalObject.metadata["trip_id"])
                                // check if the item is contained in the metadata
                                if (branchUniversalObject.metadata.containsKey("trip_id")) {
                                    SuggestTripActivity.makeIntent(this, branchUniversalObject.metadata.get("trip_id")?.toLong() ?: -1, {
                                        startActivity(it)
                                        finish()
                                    }, {
                                        ifLoggedInOpenMainActivityElseOpenLoginActivity()
                                    })
                                }else {
                                    ifLoggedInOpenMainActivityElseOpenLoginActivity()
                                }
                            }
                        }else {
                            ifLoggedInOpenMainActivityElseOpenLoginActivity()
                        }
                    }, data, this)
                }, POST_ANIMATION_DELAY)


            /* AnimatorSet().apply {
                 playTogether(
                         ObjectAnimator.ofFloat(appLogo, "alpha", 0F, 1F).setDuration(ANIMATION_DURATION),
                         ObjectAnimator.ofFloat(appLogo, "scaleX", 0.0f, 1.0f).setDuration(ANIMATION_DURATION),
                         ObjectAnimator.ofFloat(appLogo, "scaleY", 0.0f, 1.0f).setDuration(ANIMATION_DURATION)
                 )
                 addListener(object : AnimatorListener {
                     override fun onAnimationRepeat(animation: Animator?) {}

                     override fun onAnimationEnd(animation: Animator?) {

                     }

                     override fun onAnimationCancel(animation: Animator?) {}

                     override fun onAnimationStart(animation: Animator?) {}
                 })
                 start()
             }*/
        }, ANIMATION_DELAY)
    }

    private fun ifLoggedInOpenMainActivityElseOpenLoginActivity() {
//        Log.e("CHECK", UserUtil.getUserToken())
        if (UserUtil.isLoggedIn()) startMainActivity() else startLoginActivity()
    }

    private fun startLoginActivity() {
        startActivityFinishingThis(Intent(this, LoginActivity::class.java))
    }

    private fun startMainActivity() {
        startActivityFinishingThis(Intent(this, MainActivity::class.java))
    }

    private fun startActivityFinishingThis(intent: Intent) {
        startActivity(intent)
        finish()
    }


    override fun onStart() {
        Timber.d("onStart")
        super.onStart()

    }

    public override fun onNewIntent(intent: Intent) {
        Timber.d("onNewIntent")
        this.intent = intent
    }

}