package com.viago.activities

//import com.google.android.material.bottomnavigation.BottomNavigationView
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import bindView
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.baseclasses.BaseFragment
import com.viago.baseclasses.NavItemClickEvent
import com.viago.baseclasses.UniversalStatePagerAdapter
import com.viago.extensions.hide
import com.viago.extensions.showToast
import com.viago.flight.screen.Flight
import com.viago.flight.screen.FlightsActivity
import com.viago.listeners.ContextualModeInteractor
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.notifications.Notification
import com.viago.notifications.NotificationsFragment
import com.viago.notifications.NotificationsUtil
import com.viago.services.MyFirebaseMessagingService
import com.viago.trips.MyTrip
import com.viago.trips.TripsUtil
import com.viago.trips.createtrip.CreateTripActivity
import com.viago.trips.createtrip.search.SearchQueryFragment
import com.viago.trips.mytrips.MyTripsFragment
import com.viago.trips.mytrips.TripDeleteConfirmationDialogFragment
import com.viago.trips.tripdetails.TripDetailsActivity
import com.viago.utils.Constants
import com.viago.utils.GeneralUtils
import com.viago.utils.SharedPrefsUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode


class MainActivity : BaseDrawerActivity(), ContextualModeInteractor {
    private val tabs by bindView<TabLayout>(R.id.tabs)
    private val viewPager by bindView<ViewPager>(R.id.main_view_pager)
    private var menu: Menu? = null
    private var myTripFragment: MyTripsFragment? = null
    private val userToken by lazy { UserUtil.getUserToken() }
    private var list = ArrayList<Notification>()
    private var nextUrl :String?=null
    private val dialogNotify: ProgressDialog by lazy {
        ProgressDialog(this).apply {
            setMessage("Deleting selected trip(s)")
            isIndeterminate = true
            setCancelable(false)
        }
    }

  //  private val bottomNavigationView by bindView<BottomNavigationView>(R.id.bottom_navigation)
    override fun getLayoutRes() = R.layout.activity_main

    fun clickFlights(view: View?) {
        startActivity(Intent(this, FlightsActivity::class.java))
    }


    override fun onCreated(savedInstanceState: Bundle?) {
        if (this.intent.hasExtra("trip_id")) {
            val trip_id = this.intent.getStringExtra("trip_id")
            loadTripDetails(trip_id)
        }


        SharedPrefsUtils.removePreference(Constants.KEY_TEMP_LOCATIONS)

        val savedTempLocationsString = SharedPrefsUtils.getStringPreference(Constants.KEY_TEMP_LOCATIONS)

        myTripFragment = MyTripsFragment.newInstance(this@MainActivity)
        with(viewPager) {
            adapter = UniversalStatePagerAdapter<BaseFragment>(supportFragmentManager).apply {
                addFrag(SearchQueryFragment.newInstance(null, object : SearchQueryFragment.Companion.PlaceSearchListener {
                    override fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String, isCurrentLocation:Boolean) {
                        //activity.onBackPressed()
                        //

                        startActivity(CreateTripActivity.getIntent(
                                context,
                                destination,
                                latLng,
                                keyword,isCurrentLocation)
                        )
                      //  finish()
                    }
                },false), getString(R.string.create_trip))

                addFrag(myTripFragment!!, getString(R.string.my_trips))

//                addFrag(BookFragment(), getString(R.string.book_now))
            }

            offscreenPageLimit = 3

            tabs.setupWithViewPager(this)
            currentItem = 0
        }

        Thread(Runnable {
            if (SharedPrefsUtils.getBooleanPreference(Constants.KEY_GCM_TOKEN_UPDATE_FAILED, true)) {
                MyFirebaseMessagingService().sendRegistrationToServer(this@MainActivity, FirebaseInstanceId.getInstance().token)
            }
        }).start()

//        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
//            when (item.itemId) {
//                R.id.action_home -> {
//                }
//                R.id.action_profile -> {
//                    startActivity(Intent(this,MyProfileActivity::class.java))
//                    finish()
//                }
//                R.id.action_invite -> {
//                    GeneralUtils.performInviteAction(this)
//                }
//            }
//            false
//        }
    }

    private fun loadTripDetails(trip_id: String?) {
        dialogNotify.setMessage("Loading trip details")
        dialogNotify.show()
        TripsUtil.getTripDetails(UserUtil.getUserToken(), trip_id!!.toLong(), {
            startActivity(TripDetailsActivity.getIntent(this, it, true))
            dialogNotify.dismiss()
        }, {
            dialogNotify.dismiss()
            Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show()
        })
    }

    override fun onBackPressed() {
        if (isContextualMode())
            disableContextualMode()
        else if (viewPager.currentItem != 0)
            viewPager.postDelayed({ viewPager.setCurrentItem(0, true) }, 100)
        else
            super.onBackPressed()
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onResume() {
        super.onResume()

         var unreadCount: Int = 0
            list.forEach {
                if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + it.trip+it.created_on, false))
                    unreadCount++
            }

        if(unreadCount!=0){
            SharedPrefsUtils.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, unreadCount )
        }else if (notificationMenuCountView != null && NOTIFICATION_COUNT!=-1){
            notificationMenuCountView!!.hide()
        }
        NOTIFICATION_COUNT = SharedPrefsUtils.getIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, -1)

        if (NOTIFICATION_COUNT > 0) {
            if (notificationMenuCountView != null) {
                if(NOTIFICATION_COUNT>20){
                    notificationMenuCountView!!.text = "20+"
                }else {
                    notificationMenuCountView!!.text = NOTIFICATION_COUNT.toString()
                }
            }
        }

    }




    val onSuccess = { baseListModel: BaseListModel<Notification> ->

        //notificationCountView.hide()
      //  notificationMenuCountView!!.hide()

         nextUrl = baseListModel.next

            baseListModel.results.forEach {

                list.add(it)

            }

        if (!nextUrl.isNullOrEmpty()) {

            call()
        }else{

            var unreadCount: Int = 0
            list.forEach {
                if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + it.trip+it.created_on, false))
                    unreadCount++
            }
            SharedPrefsUtils.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, unreadCount)

            refreshNotificationCount()
        }

       /* if (baseListModel.count == 0) {
            recyclerView.hide()
            noNotificationsFoundContainer.show()
        }*/
    }



    val onFailure: (String) -> Unit = {
        refreshNotificationCount()
    }


    fun call():Unit{

        NotificationsUtil.getMoreNotifications(userToken, nextUrl!!, onSuccess, onFailure)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        this.menu = menu
        if( notificationMenuCountView==null) {
            notificationView = menu!!.findItem(R.id.action_notifications).getActionView()
        }
        if(notificationMenuCountView==null) {
            notificationMenuCountView = notificationView!!.findViewById<TextView>(R.id.notification_count) as TextView
        }

        list = ArrayList<Notification>()
        NotificationsUtil.getMyNotifications(userToken, onSuccess, onFailure)

        if (NOTIFICATION_COUNT > 0) {
            if (notificationMenuCountView != null) {
                if(NOTIFICATION_COUNT>20){
                    notificationMenuCountView!!.text = "20+"
                }else {
                    notificationMenuCountView!!.text = NOTIFICATION_COUNT.toString()
                }
            }
        }

       /* if (NOTIFICATION_COUNT > 0) {
            if (notificationMenuCountView != null) {
                notificationMenuCountView!!.setText(NOTIFICATION_COUNT.toString())
                notificationMenuCountView!!.hide()
                notificationMenuCountView!!.show()
            }
        }
*/
//startActivity(Intent(this, MainActivity::class.java))
        notificationView!!.setOnClickListener { startActivity(Intent(this, NotificationsFragment::class.java)) }

        return true
    }



    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        toolbar.title = null
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_add_person -> {
            GeneralUtils.performInviteAction(this,UserUtil.getUser()?.name)
            true
        }
        R.id.action_notifications -> {
            startActivity(Intent(this, NotificationsFragment::class.java))
            true
        }
        R.id.action_delete_trip -> {
            deleteTrip(myTripFragment!!.getSelectedTrips())
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    private fun deleteTrip(selectedTrips: List<MyTrip>) {
        TripDeleteConfirmationDialogFragment().let {
            it.onOkayClicked = {
                disableContextualMode()
                dialogNotify.setMessage("Deleting selected trip(s)")
                dialogNotify.show()
                var deletedCount: Int = 0
                for (trip in selectedTrips) {
                    TripsUtil.deleteTrip(UserUtil.getUserToken(), trip.id, {
                        deletedCount++
                        if (deletedCount == selectedTrips.size) {
                            dialogNotify.dismiss()
                            showToast("$deletedCount trips deleted!")
                            myTripFragment!!.refresh()
                        }
                    }, {
                        dialogNotify.dismiss()
                        showToast("Something went wrong. deleted $deletedCount trips")
                        if (deletedCount > 0)
                            myTripFragment!!.refresh()
                    })
                }
            }
            it.onCancelClicked = {
                disableContextualMode()
            }
            it.show(supportFragmentManager, TripDeleteConfirmationDialogFragment::class.java.simpleName)
        }
    }

    override fun enableContextualMode() {
        enterContextualMode(true)
    }

    override fun isContextualMode(): Boolean =
            menu!!.findItem(R.id.action_delete_trip) != null && menu!!.findItem(R.id.action_delete_trip).isVisible

    override fun updateSelectedCount(count: Int) {
        if (count > 0) {
            //selectedCount.setText("%d selected".format(count))
        } else {
            disableContextualMode()
        }
    }

    private fun disableContextualMode() {
        enterContextualMode(false)
        myTripFragment!!.contextualModeDisabled()
    }

    private fun enterContextualMode(enter: Boolean) {
        menu!!.findItem(R.id.action_add_person).isVisible = !enter
        menu!!.findItem(R.id.action_notifications).isVisible = !enter
        menu!!.findItem(R.id.action_delete_trip).isVisible = enter
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onNavItemClick(navItemClickEvent: NavItemClickEvent?) {
        if (supportFragmentManager.backStackEntryCount > 0)
            super.onBackPressed()
        when (navItemClickEvent?.itemId) {
            R.id.action_create_trip -> viewPager.currentItem = 0
            R.id.action_my_trips -> viewPager.currentItem = 1
//            R.id.action_book_now -> viewPager.currentItem = 2
            R.id.action_notifications -> startActivity(Intent(this, NotificationsFragment::class.java))
        }
        EventBus.getDefault().removeStickyEvent(navItemClickEvent)
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }
}