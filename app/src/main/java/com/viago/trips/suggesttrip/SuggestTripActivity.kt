package com.viago.trips.suggesttrip

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.extensions.openFragment
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.trips.Location
import com.viago.trips.MyTrip
import com.viago.trips.TripsUtil
import com.viago.trips.createtrip.CreateTripActivity
import com.viago.trips.createtrip.SourceDestSelectionFragment
import com.viago.trips.createtrip.TripRearrangeFragment
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.interfaces.TripInfo
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils

class SuggestTripActivity : CreateTripActivity(), SuggestionsTripInfo {

    @SuppressLint("SupportAnnotationUsage", "WrongConstant")
    @TripActionType.Companion.ActionType
    override var actionType: Int = TripActionType.SUGGESTING

    lateinit var tripDetails: MyTrip

    override val originalTripLocations: ArrayList<Location> = ArrayList()
    override var tripId: Long = -1
    override var suggestionId: Long? = null

    override var categories: List<Int> = ArrayList()
    override var title: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {

        tripDetails = intent.getParcelableExtra(TRIP_DETAILS)
        tripId = tripDetails.id
        selectedLocations.clear()
        originalTripLocations.clear()
        title = tripDetails.title
        categories = tripDetails.categories!!.map { it.id }



        super.onCreate(savedInstanceState)


        if (!intent.getBooleanExtra(ALLOW_LOCATIONS, false)) {
            source = tripDetails.locations!!.first()
            destination = tripDetails.locations!!.last()
//            (tripDetails.locations as ArrayList).let {
//                it.removeAt(0)
//                it.removeAt(it.size - 1)
//            }
            originalTripLocations.addAll(tripDetails.locations!!)
            selectedLocations.addAll(tripDetails.locations!!)
            openFragment(TripRearrangeFragment(), android.R.id.content, false)
        }
        else{

            selectedLocations.forEach {
                val location = it
                if(location.provider==3L){
                    selectedLocations.remove(location)

                }



            }
//            originalTripLocations.addAll(tripDetails.locations!!)
//            selectedLocations.addAll(tripDetails.locations!!)
           /* (selectedLocations).let {
                                it.removeAt(0)
                it.removeAt(it.size - 1)
            }*/
        }
        onSelectedPlacesUpdated()
    }

    private fun showTapTarget() {


    }

    companion object {

        const val TRIP_DETAILS = "trip details"
        public const val ALLOW_LOCATIONS = "allow locations"

        fun getIntent(context: Context, placeName: String, latLng: LatLng, placeType: String, tripInfo: TripInfo, tripDetail: MyTrip): Intent {
            val intent = Intent(context, SuggestTripActivity::class.java)
            intent.putExtra(PLACE_NAME, placeName)
            intent.putExtra(LAT_LNG, latLng)
            intent.putExtra(PLACE_TYPE, placeType)
            intent.putExtra(TRIP_DETAILS, tripDetail)
            intent.putExtra(ALLOW_LOCATIONS, true)

            SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson((tripInfo as SuggestTripActivity)?.tripDetails.locations, object : TypeToken<ArrayList<Location>>() {
            }.type))

            return intent
        }

        fun makeIntent(context: Context, tripId: Long, onSuccess: (Intent) -> Unit, onFailure: () -> Unit) {
            val intent = Intent(context, SuggestTripActivity::class.java)
            val progressDialog: ProgressDialog by lazy {
                ProgressDialog(context).apply {
                    setMessage("Obtaining Trip details")
                    isIndeterminate = true
                    setCancelable(false)
                }
            }
            try {
                if (progressDialog != null && !progressDialog.isShowing) {
 //                   progressDialog.show()
                }
            }catch (e:Exception){

            }
            TripsUtil.getTripDetails(UserUtil.getUserToken(), tripId, {
                intent.putExtra(PLACE_NAME, it.searched_location)
                intent.putExtra(LAT_LNG, LatLng(it.searched_location_latitude, it.searched_location_longitude))
                intent.putExtra(PLACE_TYPE, it.searched_location_type)
                intent.putExtra(TRIP_DETAILS, it)
                progressDialog.dismiss()
                onSuccess(intent)
            }, {
                progressDialog.dismiss()
                context.showToast(it)
                onFailure()
            })
        }
    }

}