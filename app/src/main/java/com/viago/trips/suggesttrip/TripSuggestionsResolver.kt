package com.viago.trips.suggesttrip

import android.util.Log
import com.viago.trips.Location
import com.viago.trips.annotations.LocationStatus

/**
 * Created by mayank on 25/9/17.
 */
class TripSuggestionsResolver {

    fun getResolvedSuggestions(originalTripLocations: List<Location>, selectedLocations: MutableList<Location>): MutableList<Location> {
        val addedLocations = selectedLocations - originalTripLocations
        val removedLocations = originalTripLocations - selectedLocations

        selectedLocations.forEach {
            it.status = LocationStatus.NONE
            if (addedLocations.contains(it)) {
                it.status = LocationStatus.ADDED
            }
        }

        removedLocations.forEach {
            val index = originalTripLocations.indexOf(it)
            if (index != -1) {
                it.status = LocationStatus.REMOVED
                selectedLocations.add(index, it)
            }
        }

        return selectedLocations
    }
}