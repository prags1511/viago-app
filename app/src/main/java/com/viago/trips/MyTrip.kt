package com.viago.trips

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by mayank on 8/9/17.
 */
data class MyTrip(
        val owner: Long,
        val id: Long,
        val image: String?,
        val title: String,
        val duration: String?,
        val locations_count: Int,
        val searched_location_type: String,
        val searched_location_latitude: Double,
        val searched_location_longitude: Double,
        val suggestions_count: Long,
        var selected: Boolean = false,
        val distance: String?,
        val searched_location: String?,
        val categories: List<Category>?,
        val locations: List<Location>?
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readString(),
            parcel.readString()!!,
            parcel.readString(),
            parcel.readInt(),
            parcel.readString()!!,
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readLong(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Category),
            parcel.createTypedArrayList(Location)) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(owner)
        parcel.writeLong(id)
        parcel.writeString(image)
        parcel.writeString(title)
        parcel.writeString(duration)
        parcel.writeInt(locations_count)
        parcel.writeString(searched_location_type)
        parcel.writeDouble(searched_location_latitude)
        parcel.writeDouble(searched_location_longitude)
        parcel.writeLong(suggestions_count)
        parcel.writeByte(if (selected) 1 else 0)
        parcel.writeString(distance)
        parcel.writeString(searched_location)
        parcel.writeTypedList(categories)
        parcel.writeTypedList(locations)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MyTrip> {
        override fun createFromParcel(parcel: Parcel): MyTrip {
            return MyTrip(parcel)
        }

        override fun newArray(size: Int): Array<MyTrip?> {
            return arrayOfNulls(size)
        }
    }
}