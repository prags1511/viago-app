package com.viago.trips

import com.viago.models.BaseListModel
import com.viago.trips.models.*
import com.viago.trips.models.suggestions.TripPostSuggestionRequest
import com.viago.trips.models.suggestions.TripSuggestion
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by mayank on 7/9/17.
 */
internal interface TripsService {

    @GET("trips/categories")
    fun getTripCategories(@Header("Authorization") token: String): Call<List<Category>>

    @POST("trips/")
    fun createTrip(@Header("Authorization") token: String, @Body createTripRequest: CreateTripRequest): Call<MyTrip>

   // @PUT("trips/{tripId}/")
    @POST("trips/{tripId}/")
    fun updateTrip(@Header("Authorization") token: String, @Path("tripId") tripId: Long, @Body createTripRequest: CreateTripRequest): Call<CreateTripRequest>

   // @PUT("trips/suggestion-detail/{suggestionId}/")
    @POST("trips/suggestion-detail/{suggestionId}/")
    fun declineSuggestion(@Header("Authorization") token: String, @Path("suggestionId") suggestionId: Long, @Body suggestionDeclineStatus: SuggestionDeclineStatus = SuggestionDeclineStatus()): Call<ResponseBody>

//    @GET("trips/")
//    fun getTrips(@Header("Authorization") token: String): Call<BaseListModel<MyTrip>>
    @GET("trips/")
    fun getTrips(@Header("Authorization") token: String): Call<BaseListModel<MyTrip>>

    @GET()
    fun getMoreTrips(@Header("Authorization") token: String, @Url nextUrl: String): Call<BaseListModel<MyTrip>>

    @GET("trips/{tripId}/")
    fun getTrip(@Header("Authorization") token: String, @Path("tripId") tripId: Long): Call<MyTrip>

    // @GDELETE("trips/{tripId}/")
    @GET("trips/delete/{tripId}/")
    fun deleteTrip(@Header("Authorization") token: String, @Path("tripId") tripId: Long): Call<ResponseBody>

    @POST("trips/suggestion/{tripId}")
    fun postSuggestions(@Header("Authorization") token: String, @Path("tripId") tripId: Long, @Body tripPostSuggestionRequest: TripPostSuggestionRequest): Call<ResponseBody>

    @GET("trips/suggestion/{tripId}")
    fun getTripSuggestions(@Header("Authorization") token: String, @Path("tripId") tripId: Long): Call<BaseListModel<TripSuggestion>>

    @GET()
    fun getMoreSuggestions(@Header("Authorization") token: String, @Url nextUrl: String): Call<BaseListModel<TripSuggestion>>

    @GET("trips/suggestion-detail/{suggestionId}/")
    fun getSuggestionDetails(@Header("Authorization") token: String, @Path("suggestionId") suggestionId: Long): Call<SuggestionDetails>

    @GET("trips/keywords/")
    fun getKeywords(): Call<List<Keyword>>
}