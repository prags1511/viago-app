package com.viago.trips.edittrip

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.extensions.openFragment
import com.viago.trips.Location
import com.viago.trips.createtrip.CreateTripActivity
import com.viago.trips.createtrip.TripRearrangeFragment
import com.viago.trips.createtrip.search.SearchQueryFragment
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.interfaces.TripInfo
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils

class EditTripActivity : CreateTripActivity(), SuggestionsTripInfo {

    @SuppressLint("SupportAnnotationUsage", "WrongConstant")
    @TripActionType.Companion.ActionType
    override var actionType: Int = TripActionType.EDITING_TRIP


    override val originalTripLocations: ArrayList<Location> = ArrayList()
    override var tripId: Long = -1
    override var suggestionId: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        actionType = intent.getIntExtra(TRIP_ACTION_TYPE, TripActionType.EDITING_TRIP)

        title = intent.getStringExtra(TRIP_TITLE)
        categories = intent.getIntegerArrayListExtra(TRIP_CATEGORIES)
        source = intent.getParcelableExtra(TRIP_SOURCE)
        destination = intent.getParcelableExtra(TRIP_DESTINATION)

        if (intent.hasExtra(TRIP_ORIGINAL_LOCATIONS)) {
            originalTripLocations.clear()
            originalTripLocations.addAll(intent.getParcelableArrayListExtra(TRIP_ORIGINAL_LOCATIONS))
        }
        if (intent.hasExtra(TRIP_NEW_LOCATIONS)) {
            selectedLocations.clear()
            selectedLocations.addAll(intent.getParcelableArrayListExtra(TRIP_NEW_LOCATIONS))
        }
        if (intent.hasExtra(TRIP_ID))
            tripId = intent.getLongExtra(TRIP_ID, -1)
        if (intent.hasExtra(SUGGESTION_ID))
        suggestionId = intent.getLongExtra(SUGGESTION_ID, -1)


        if (suggestionId == -1L) suggestionId = null

        onSelectedPlacesUpdated()
        //onSelectedPlacesUpdated()
        if (!intent.getBooleanExtra(ALLOW_LOCATIONS, false))
            openFragment(TripRearrangeFragment(), android.R.id.content, false)
    }

    companion object {

        private const val TRIP_TITLE = "trip title"
        private const val TRIP_CATEGORIES = "trip categories"
        private const val TRIP_ACTION_TYPE = "trip action type"
        private const val TRIP_SOURCE = "trip source"
        private const val TRIP_DESTINATION = "trip desination"
        public const val TRIP_ORIGINAL_LOCATIONS = "trip original locations"
        public const val TRIP_NEW_LOCATIONS = "trip new locations"
        private const val TRIP_ID = "trip id"
        private const val SUGGESTION_ID = "suggestion id"
        public const val ALLOW_LOCATIONS = "allow locations"

        fun getIntent(context: Context, placeName: String, latLng: LatLng, placeType: String, tripInfo: TripInfo, tripId: Long, suggestionId: Long? = null): Intent {
            val intent = Intent(context, EditTripActivity::class.java)
            intent.putExtra(PLACE_NAME, placeName)
            intent.putExtra(LAT_LNG, latLng)

            intent.putExtra(ALLOW_LOCATIONS, true)

            intent.putExtra(LAT_LNG, latLng)
            intent.putExtra(PLACE_TYPE, placeType)
            intent.putExtra(TRIP_TITLE, tripInfo.title)
            intent.putExtra(TRIP_CATEGORIES, tripInfo.categories as ArrayList<Int>)

            SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(tripInfo.selectedLocations, object : TypeToken<ArrayList<Location>>() {
            }.type))

            intent.putExtra(TRIP_SOURCE, tripInfo.source)
            intent.putExtra(TRIP_DESTINATION, tripInfo.destination)
            intent.putExtra(TRIP_ID, tripId)
            intent.putExtra(SUGGESTION_ID, suggestionId)

            return intent
        }

        fun getIntent(context: Context,
                      placeName: String,
                      latLng: LatLng,
                      title: String,
                      placeType: String,
                      categories: ArrayList<Int>,
                      @TripActionType.Companion.ActionType tripActionType: Int,
                      source: Location?,
                      destination: Location?,
                      originalLocations: ArrayList<Location>,
                      newLocations: ArrayList<Location>,
                      tripId: Long,
                      suggestionId: Long? = null
        ): Intent {
            val intent = Intent(context, EditTripActivity::class.java)
            intent.putExtra(PLACE_NAME, placeName)
            intent.putExtra(LAT_LNG, latLng)
            intent.putExtra(PLACE_TYPE, placeType)
            intent.putExtra(TRIP_TITLE, title)
            intent.putExtra(TRIP_CATEGORIES, categories)
            intent.putExtra(TRIP_ACTION_TYPE, tripActionType)
            intent.putExtra(TRIP_SOURCE, source)
            intent.putExtra(TRIP_DESTINATION, destination)
            intent.putExtra(TRIP_ORIGINAL_LOCATIONS, originalLocations)
            intent.putExtra(TRIP_NEW_LOCATIONS, newLocations)
            intent.putExtra(TRIP_ID, tripId)
            intent.putExtra(SUGGESTION_ID, suggestionId)
            return intent
        }
    }

}