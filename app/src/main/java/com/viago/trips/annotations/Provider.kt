package com.viago.trips.annotations

import androidx.annotation.IntDef
import androidx.annotation.LongDef

/**
 * Created by mayank on 18/9/17.
 */
const val GOOGLE = 0L
const val FOURSQUARE = 1L
const val YELP = 2L
const val NONE = 3L

@LongDef(GOOGLE, FOURSQUARE, YELP, NONE)
@Retention(AnnotationRetention.SOURCE)
annotation class PlaceSearchProvider
