package com.viago.trips.annotations

import androidx.annotation.IntDef
import androidx.annotation.LongDef

/**
 * Created by mayank on 18/9/17.
 */
object LocationStatus {
    const val ADDED = 1
    const val NONE = 2
    const val REMOVED = 3

    @LongDef(ADDED.toLong(), NONE.toLong(), REMOVED.toLong())
    @Retention(AnnotationRetention.SOURCE)
    annotation class Status
}