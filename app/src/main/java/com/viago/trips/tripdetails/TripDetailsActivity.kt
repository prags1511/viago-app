package com.viago.trips.tripdetails

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import bindView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.viago.R
import com.viago.baseclasses.UniversalStatePagerAdapter
import com.viago.extensions.dpToPx
import com.viago.extensions.show
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.maputils.MapsRouteUtil
import com.viago.profile.MediaDBUtil
import com.viago.trips.Location
import com.viago.trips.MyTrip
import com.viago.trips.MyTripUpdatedEvent
import com.viago.trips.TripsUtil
import com.viago.trips.createtrip.BaseTripActivity
import com.viago.trips.createtrip.BaseTripMyTripActivity
import com.viago.trips.edittrip.EditTripActivity
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.mytrips.TripDeleteConfirmationDialogFragment
import com.viago.utils.GoogleMapsUtil
import com.viago.utils.LocationHelper
import com.viago.utils.ShareUtil
import com.viago.utils.marker.IconGenerator
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber

/**
 * Created by mayank on 23/8/17.
 */
class TripDetailsActivity : BaseTripMyTripActivity(), SuggestionsTripInfo, View.OnClickListener {

    override var categories: List<Int> = ArrayList()
    override var title: String = ""

    override var duration: String = ""
    override var distance: String = ""
    override var source: Location? = null
    override var destination: Location? = null

    private var tripLocations: List<Location>? = null
    override var tripId: Long = -1
    override var suggestionId: Long? = null

    override val originalTripLocations: ArrayList<Location> = ArrayList()

    private val tripDetailsActionContainer by bindView<View>(R.id.trip_details_action_container)
    private val startTrip by bindView<View>(R.id.action_start_trip)
    private val shareTrip by bindView<View>(R.id.action_share_trip)
    private val editTrip by bindView<View>(R.id.action_edit_trip)

    private val tripSummaryView by bindView<RelativeLayout>(R.id.trip_summary)
    private val tripLocationsText by bindView<TextView>(R.id.trip_locations_text)
    private val durationAndDistance by bindView<TextView>(R.id.trip_locations_duration_and_distance)
    private val distanceUnitSwitch by bindView<TextView>(R.id.kmmileswitch)
    private var tripDuration: String = ""
    private var tripDistance: String = ""

    private val colorPrimaryDark by lazy { ContextCompat.getColor(this, R.color.colorPrimaryDark) }
    private val colorAccent by lazy { ContextCompat.getColor(this, R.color.colorAccent) }


    @TripActionType.Companion.ActionType
    override var actionType: Int = TripActionType.VIEWING_SUGGESTIONS

    private val colorBlue by lazy { ContextCompat.getColor(this, R.color.pin_skyblue) }

    private val tripLocationsFragment by lazy { TripLocationsFragment() }

    override fun getPagerAdapter(): UniversalStatePagerAdapter<Fragment> {
        return UniversalStatePagerAdapter<Fragment>(supportFragmentManager).apply {
            addFrag(tripLocationsFragment, "Locations")
            addFrag(TripSuggestionsFragment(), "Suggestions")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        getMenuInflater().inflate(R.menu.activity_trip_details_menu, menu);
        return true;
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Timber.d("onCreate")

        EventBus.getDefault().register(this)

        tripDetailsActionContainer.show()

        arrayOf(startTrip, shareTrip, editTrip).forEach { it.setOnClickListener(this) }

        tripId = intent.getLongExtra(TRIP_ID, -1)
        if (intent.hasExtra(TRIP_DETAIL)) {
            val details: MyTrip = intent.getParcelableExtra(TRIP_DETAIL)
            if (details != null && !details.locations!!.isEmpty()) {
                Handler().postDelayed({ setTripDetails(details) }, 200)
                if (intent.hasExtra(SHOW_SUGGESTIONS) && intent.getBooleanExtra(SHOW_SUGGESTIONS, false))
                    viewPager.postDelayed({ viewPager.setCurrentItem(1, true) }, 100)
            } else {
                getTripDetails()
            }
        } else {
            getTripDetails()
        }



        distanceUnitSwitch.setOnClickListener {
            if (tripDistance != null && tripDistance.contains(":")) {
                val arr = tripDistance.split(":")
                if (distanceUnitSwitch.text.contains("Miles")) {
                    durationAndDistance.text = tripDuration + " | " + arr[1]
                    distanceUnitSwitch.text = "(View in Kms)"
                } else {
                    durationAndDistance.text = tripDuration + " | " + arr[0]
                    distanceUnitSwitch.text = "(View in Miles)"
                }
            }
        }

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                arrayOf(startTrip, shareTrip, editTrip).forEach { it.visibility = if (position == 0) View.VISIBLE else View.GONE }
            }

        })
    }

    private fun setTripDetails(details: MyTrip) {
        source = details.locations!!.first()
        destination = details.locations.last()
        (details.locations as ArrayList).let {
            it.removeAt(0)
            it.removeAt(it.size - 1)
        }
        title = details.title
        toolbar.title = title
        categories = details.categories!!.map { it.id }
        originalTripLocations.addAll(details.locations)
        selectedLocations.addAll(details.locations)

        tripLocationsText.text = "Trip locations (${selectedLocations.size})"
        tripDuration = details.duration!!
        tripDistance = details.distance!!
        durationAndDistance.text = "$tripDuration | ${tripDistance.split(":")[0]}"
        tripSummaryView.visibility = View.VISIBLE

        setMapMarkers()
        onSelectedPlacesUpdated()
    }

    private fun getTripDetails() {
        TripsUtil.getTripDetails(UserUtil.getUserToken(), tripId, {
            setTripDetails(it)
        }, {
            showToast(it)
        })
    }

    override fun setMapMarkers() {
        googleMap?.run {
            clear()
            source?.let { destination?.let { it1 -> plotSourceAndDestinationMarkers(this, it, it1) } }
            plotMarkers(this, originalTripLocations)

            if (originalTripLocations.size > 2) {
                val latLngList = originalTripLocations.map { place -> LatLng(place.latitude, place.longitude) }

                if (source != null && destination != null) {
                    MapsRouteUtil(this@TripDetailsActivity, this).showRoute(LatLng(source!!.latitude, source!!.longitude),
                            LatLng(destination!!.latitude, destination!!.longitude),
                            latLngList
                    )
                } else {
                    MapsRouteUtil(this@TripDetailsActivity, this).showRoute(latLngList.first(),
                            latLngList.last(),
                            latLngList.subList(1, latLngList.lastIndex)
                    )
                }
            }

            moveCamera(CameraUpdateFactory.scrollBy(0F, dpToPx(55).toFloat()))
        }
    }

    private fun plotSourceAndDestinationMarkers(googleMap: GoogleMap, source: Location, destination: Location) {


        if (source == destination) {
            val markerOptions: MarkerOptions = MarkerOptions().position(LatLng(source.latitude, source.longitude)).title(source.title)
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
            googleMap.addMarker(markerOptions)
        } else {

            val iconGenerator = IconGenerator(this)
            iconGenerator.setColor(colorPrimaryDark)

            // Creating starting icon
            val startingMarkerOption = MarkerOptions().position(LatLng(source.latitude, source.longitude)).title(source.title)
            val startingIcon = iconGenerator.makeIcon("S")
            startingMarkerOption.icon(BitmapDescriptorFactory.fromBitmap(startingIcon))
            googleMap.addMarker(startingMarkerOption)

            // creating destination icon
            val destinationMarkerOption = MarkerOptions().position(LatLng(destination.latitude, destination.longitude)).title(destination.title)
            val destinationIcon = iconGenerator.makeIcon("E")
            destinationMarkerOption.icon(BitmapDescriptorFactory.fromBitmap(destinationIcon))
            googleMap.addMarker(destinationMarkerOption)
        }
    }

    /*
    private fun plotSourceAnDestinationMarkers(googleMap: GoogleMap, source: Location, destination: Location) {

        var isEqual = false

        if ( arrayOf(source, destination).size>0 && arrayOf(source, destination)[0].title.equals(arrayOf(source, destination)[arrayOf(source, destination).lastIndex].title)) {

            isEqual = true
        }

        val iconGenerator = IconGenerator(this)

        arrayOf(source, destination).forEachIndexed { index, location ->
            val markerText: String
            val markerColor: Int
            if (index == 0) {
                markerColor = colorPrimaryDark
                markerText = "S"
            } else {
                markerColor = colorPrimaryDark
                markerText = "E"
            }
            iconGenerator.setColor(markerColor)
            val icon = iconGenerator.makeIcon(markerText)

            val markerOptions = MarkerOptions().position(LatLng(location.latitude, location.longitude)).title(location.title)
            if (index == 0) {
                if (isEqual) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                }

            } else if (index ==  arrayOf(source, destination).lastIndex) {
                if (isEqual) {
                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                } else {
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                }
            }else {

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
            }

            googleMap.addMarker(markerOptions).tag = index

        }

    }*/

    private fun plotMarkers(googleMap: GoogleMap, locations: List<Location>) {

        val iconGenerator = IconGenerator(this)

        var builder =  LatLngBounds.Builder();

        val locationLength: Int = locations.size;

        for (index in 0 until locationLength) {
            val curLoc: Location = locations[index]
            if (curLoc == source || curLoc == destination) continue

            val markerText = (index + 1).toString()
            val markerColor = colorBlue
            iconGenerator.setColor(markerColor)
            val icon = iconGenerator.makeIcon(markerText)

            builder.include(LatLng(curLoc.latitude, curLoc.longitude))

            val markerOptions = MarkerOptions().position(LatLng(curLoc.latitude, curLoc.longitude)).title(curLoc.title)
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
            googleMap.addMarker(markerOptions).tag = index + 1

        }

//        locations.forEachIndexed { index, location ->
//            val markerText = (index + 1).toString()
//            val markerColor = colorBlue
//            iconGenerator.setColor(markerColor)
//            val icon = iconGenerator.makeIcon(markerText)
//
//            builder.include(LatLng(location.latitude, location.longitude));
//
//            location.run {
//                val markerOptions = MarkerOptions().position(LatLng(latitude, longitude)).title(title)
//                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
//                googleMap.addMarker(markerOptions).tag = index + 1
//            }
//        }

        if(locations.size>0) {

            var bounds = builder.build();

             val width = getResources().getDisplayMetrics().widthPixels;
             val height = getResources().getDisplayMetrics().heightPixels;
              val padding = (width * 0.30)


            var cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding.toInt());

            googleMap.moveCamera(cu);

        }
    }

    override val selectedLocations: ArrayList<Location> = ArrayList()

    override fun onSelectedPlacesUpdated() {
        tripLocationsFragment.onLocationsObtained(originalTripLocations)
    }

    override fun onClick(v: View?) {
        when (v) {
            editTrip -> {
                startActivity(EditTripActivity
                        .getIntent(this,
                                placeName,
                                latLng,
                                title,
                                keyword,
                                categories as ArrayList<Int>,
                                TripActionType.EDITING_TRIP,
                                source,
                                destination,
                                originalTripLocations,
                                selectedLocations,
                                tripId))

            }
            startTrip -> {
                var toastMsg: String = ""
                if (GoogleMapsUtil().openGoogleMapsDirection(this, originalTripLocations
                        .apply {
                            source?.let { add(0, it) }; destination?.let { add(it) }
                        }
                        .map {
                            "${it.latitude},${it.longitude}"
                        })) {
                    MediaDBUtil.saveStartTimeOf(tripId)
                    toastMsg = "You can make this trip available offline!";
                } else {
                    toastMsg = "Something went wrong with the trip, try again!";
                }
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show()
            }
            shareTrip -> {
                ShareUtil.shareTrip(this, title, tripId)
            }
        }
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onTripUpdated(myTripUpdatedEvent: MyTripUpdatedEvent?) {
        myTripUpdatedEvent?.createTripRequest?.let {
            source = it.locations.first()
            destination = it.locations.last()
            (it.locations as ArrayList).let {
                it.removeAt(0)
                it.removeAt(it.size - 1)
            }
            title = it.title
            categories = it.categories
            originalTripLocations.clear()
            selectedLocations.clear()
            originalTripLocations.addAll(it.locations)
            selectedLocations.addAll(it.locations)

            tripLocationsText.text = "Trip locations (${selectedLocations.size})"
            tripDuration = myTripUpdatedEvent.createTripRequest.duration!!
            tripDistance = myTripUpdatedEvent.createTripRequest.distance!!
            durationAndDistance.text = "$tripDuration | ${tripDistance.split(":")[0]}"

            setMapMarkers()
            onSelectedPlacesUpdated()
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    companion object {

        const val TRIP_ID = "trip id"
        const val TRIP_DETAIL = "trip detail"
        const val SHOW_SUGGESTIONS = "show suggestions"

        fun getIntent(context: Context, placeName: String, latLng: LatLng, placeType: String, tripId: Long): Intent {
            val intent = Intent(context, TripDetailsActivity::class.java)
            intent.putExtra(PLACE_NAME, placeName)
            intent.putExtra(LAT_LNG, latLng)
            intent.putExtra(PLACE_TYPE, placeType)
            intent.putExtra(TRIP_ID, tripId)
            return intent
        }

        fun getIntent(context: Context, tripDetail: MyTrip, showSuggestions: Boolean): Intent {
            val intent = Intent(context, TripDetailsActivity::class.java)
            intent.putExtra(PLACE_NAME, tripDetail.title)
            intent.putExtra(LAT_LNG, LatLng(tripDetail.searched_location_latitude, tripDetail.searched_location_longitude))
            intent.putExtra(PLACE_TYPE, tripDetail.searched_location_type)
            intent.putExtra(TRIP_ID, tripDetail.id)
            intent.putExtra(TRIP_DETAIL, tripDetail)
            intent.putExtra(SHOW_SUGGESTIONS, showSuggestions)
            return intent
        }
    }

    fun clickLocation(_position: Int) {


        googleMap?.run {
            val iconGenerator = IconGenerator(this@TripDetailsActivity)

            var isEqual = false

            if ( tripLocationsFragment.tripLocationAdapter.itemsList.size>0 &&
                    tripLocationsFragment.tripLocationAdapter.itemsList[0].title.equals(tripLocationsFragment.tripLocationAdapter.itemsList
                            [tripLocationsFragment.tripLocationAdapter.itemsList.lastIndex].title)) {

                isEqual = true
            }

           // clear()

            tripLocationsFragment.tripLocationAdapter.itemsList.forEachIndexed { index, location ->
//                val markerOptions = MarkerOptions()
//                        .position(LatLng(location.latitude, location.longitude))
//                        .title(location.title)
//                        .snippet(location.address)

                val markerText: String
                val markerColor: Int

                val markerOptions = MarkerOptions().position(LatLng(location.latitude, location.longitude))
                        .title(location.title)
                        .snippet(location.address)

                if (index == 0) {
                    markerColor = colorPrimaryDark
                    markerText = "S"
                } else if (index == tripLocationsFragment.tripLocationAdapter.itemsList.size-1) {
                    markerColor = colorPrimaryDark
                    markerText = "E"
                } else {
                    markerColor = colorBlue
                    markerText = index.toString()
                }
                iconGenerator.setColor(markerColor)
                val icon = iconGenerator.makeIcon(markerText)

                if (index == 0) {
                    if (isEqual) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    }

                } else if (index == tripLocationsFragment.tripLocationAdapter.itemsList.size - 1) {
                    if (isEqual) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    }
                } else {

                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                }
//                val markerOptions = MarkerOptions()
//                        .position(LatLng(location.latitude, location.longitude))
//                        .title(location.title)
//                        .snippet(location.address)
//                        .icon(BitmapDescriptorFactory.fromBitmap(icon))
//                if (selectedLocations.any { selectedLocation -> selectedLocation.latitude == location.latitude && selectedLocation.longitude == location.longitude || selectedLocation.address.equals(location.address) }) {
//                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
//                } else {
//
//                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(huePrimaryDark))
//
//
//                }


                addMarker(markerOptions).tag = index
//                if(_position==0 || _position==selectedLocations.size-1){
//                    addMarker(markerOptions).showInfoWindow()
//                }
 //               else
                if(index ==_position) {

                    addMarker(markerOptions).showInfoWindow()
//                    val update=CameraUpdateFactory.newLatLng(LatLng(location.latitude,location.longitude))
//                    animateCamera(update)

                }

            }

//            if(isCurrentLocation && selectedLocations.size!=0) {
//
//                val markerOptions = MarkerOptions()
//                        .position(LatLng(latLng.latitude, latLng.longitude))
//                        .title( LocationHelper.getAddress(applicationContext, latLng)?.locality)
//                        .snippet(LocationHelper.getAddress(applicationContext, latLng)?.getAddressLine(0))
//
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(currentLocationColor))
//                addMarker(markerOptions).tag = -1
//
//
//
//            }


        }

    }

}