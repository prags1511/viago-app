package com.viago.trips.tripdetails

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.trips.models.suggestions.TripSuggestion
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import com.viago.views.CircularImageView

/**
 * Created by mayank on 23/8/17.
 */
internal class SuggestionAdapter(val onItemClicked: (suggestionId: Long) -> Unit) : EasyRecyclerViewAdapter<TripSuggestion>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return SuggestionViewHolder(parent.inflate(R.layout.item_suggestion))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, tripSuggestion: TripSuggestion, position: Int) {
        if (holder is SuggestionViewHolder) holder.bind(tripSuggestion)
    }

    inner class SuggestionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image by bindView<CircularImageView>(R.id.image)
        private val title by bindView<TextView>(R.id.title)
        private val description by bindView<TextView>(R.id.description)

        init {
            itemView.setOnClickListener {
                val id: Long = getItem(adapterPosition).id
                SharedPrefsUtils.setBooleanPreference(Constants.KEY_SUGGESTION_READ + id.toString(), true)
                onItemClicked(id)
                notifyItemChanged(adapterPosition)
            }
        }

        fun bind(tripSuggestion: TripSuggestion) {
            image.loadImage(tripSuggestion.image)
            title.text = tripSuggestion.suggested_by.name
            val updatedLocationCount = tripSuggestion.locations_count
            description.text = "$updatedLocationCount ${if (updatedLocationCount == 1) "Location" else "Location"} updated"
            description.setCompoundDrawablesWithIntrinsicBounds(if (SharedPrefsUtils.getBooleanPreference(Constants.KEY_SUGGESTION_READ + tripSuggestion.id, false)) 0 else R.drawable.unread_indicater,
                    0, 0, 0)
        }
    }
}