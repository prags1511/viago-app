package com.viago.trips.tripdetails

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import com.viago.R
import com.viago.baseclasses.BaseDialog

/**
 * Created by mayank on 1/8/17.
 */
class ApproveSuggestionsDialogFragment : BaseDialog() {

    var onUpdateCurrentClicked: (() -> Unit)? = null
    var onCreateNewClicked: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_approve_suggestions, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            findViewById<Button>(R.id.update_current).setOnClickListener {
                onUpdateCurrentClicked?.invoke()
                dismiss()
            }
            findViewById<Button>(R.id.create_new).setOnClickListener {
                onCreateNewClicked?.invoke()
                dismiss()
            }
        }

        return dialog
    }
}