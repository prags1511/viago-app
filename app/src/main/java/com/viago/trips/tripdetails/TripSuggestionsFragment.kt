package com.viago.trips.tripdetails

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.extensions.hide
import com.viago.extensions.show
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.trips.Location
import com.viago.trips.TripsUtil
import com.viago.trips.edittrip.EditTripActivity
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.interfaces.TripInfo
import com.viago.trips.models.suggestions.TripSuggestion
import com.viago.utils.EndlessRecyclerViewScrollListener
import com.viago.utils.SpacesItemDecoration
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

/**
 * Created by mayank on 23/8/17.
 */
class TripSuggestionsFragment : BaseFragment() {

    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    private val noSuggestionsFoundContainer by bindView<View>(R.id.no_suggestions_found_container)

    private val userToken by lazy { UserUtil.getUserToken() }
    private var nextUrl: String? = null
    private lateinit var suggestionsTripInfo: SuggestionsTripInfo
    private lateinit var tripInfo: TripInfo

    private lateinit var suggestionAdapter: SuggestionAdapter
    private lateinit var tripActionType: TripActionType

    private val progressDialog: ProgressDialog by lazy { ProgressDialog(context).apply {
        setMessage("Getting suggestions...")
        isIndeterminate = true
        setCancelable(false)
    } }

    override fun getLayoutRes() = R.layout.fragment_trip_suggestions

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        EventBus.getDefault().register(this)

        suggestionAdapter = SuggestionAdapter { suggestionId: Long ->
            progressDialog.show()
            suggestionsTripInfo.suggestionId = suggestionId
            TripsUtil.getSuggestionDetails(userToken, suggestionId, {
                progressDialog.dismiss()

                tripActionType.actionType = TripActionType.VIEWING_SUGGESTIONS

                val newLocations = it.locations as ArrayList<Location>
                newLocations.removeAt(0)
                newLocations.removeAt(newLocations.lastIndex)

                context!!.startActivity(EditTripActivity
                        .getIntent(context!!,
                                tripInfo.placeName,
                                tripInfo.latLng,
                                tripInfo.title,
                                tripInfo.keyword,
                                tripInfo.categories as ArrayList<Int>,
                                TripActionType.VIEWING_SUGGESTIONS,
                                tripInfo.source,
                                tripInfo.destination,
                                tripInfo.selectedLocations,
                                newLocations,
                                suggestionsTripInfo.tripId,
                                suggestionId))
//                getAppCompatActivity().openFragment(TripRearrangeFragment())
            }, {
                progressDialog.dismiss()
                context!!.showToast(it)
            })
        }

        val onSuccess = { baseListModel: BaseListModel<TripSuggestion> ->
            suggestionAdapter.addItemsAtBottom(baseListModel.results)
            nextUrl = baseListModel.next
            if (nextUrl.isNullOrEmpty()) recyclerView.clearOnScrollListeners()
            if (baseListModel.count == 0) {
                recyclerView.hide()
                noSuggestionsFoundContainer.show()
            }
        }

        val onFailure: (String) -> Unit = {
            context!!.showToast(it)
        }


        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            //addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            adapter = suggestionAdapter
//            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
//                override fun onLoadMore(page: Int, totalItemsCount: Int) {
//                    nextUrl?.let { TripsUtil.getMoreTripSuggestions(userToken, it, onSuccess, onFailure) }
//                }
//            })
        }

        TripsUtil.getTripSuggestions(userToken, suggestionsTripInfo.tripId, onSuccess, onFailure)
    }

    @Subscribe
    fun onSuggestionActionTaken(suggestionActionEvent: SuggestionActionEvent) {
        if (suggestionActionEvent.suggestionId == -1L) return
        suggestionAdapter.removeItemAt(suggestionAdapter.itemsList.indexOfFirst { it.id == suggestionActionEvent.suggestionId })
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
        suggestionsTripInfo = context as SuggestionsTripInfo
        tripActionType = context as TripActionType
    }
}