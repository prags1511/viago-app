package com.viago.trips.tripdetails

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.trips.Location
import com.viago.trips.createtrip.CreateTripActivity
import com.viago.trips.createtrip.finalize.TripLocationAdapter
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripInfo
import com.viago.utils.Scrollable
import com.viago.utils.SpacesItemDecoration

/**
 * Created by mayank on 23/8/17.
 */
class TripLocationsFragment : BaseFragment(), Scrollable {


    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    public val tripLocationAdapter by lazy { TripLocationAdapter(object : LocationClickCallBack {
        override fun clickLocation(position: Int) {

            (activity as TripDetailsActivity).clickLocation(position)

        }
    })}

    private lateinit var suggestionsTripInfo: SuggestionsTripInfo
    private lateinit var tripInfo: TripInfo

    override fun getLayoutRes() = R.layout.fragment_trip_locations

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            getRecycledViewPool().setMaxRecycledViews(0, 0);
          //  addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            adapter = tripLocationAdapter

        }


        tripLocationAdapter.addItemOnTop(tripInfo.source)
        tripLocationAdapter.addItemsAtBottom(suggestionsTripInfo.originalTripLocations)
        tripLocationAdapter.addItemAtBottom(tripInfo.destination)
    }

    fun onLocationsObtained(locations: List<Location>) {
        tripLocationAdapter.clear()
        tripLocationAdapter.addItemOnTop(tripInfo?.source)
        tripLocationAdapter.addItemsAtBottom(locations)
        tripLocationAdapter.addItemAtBottom(tripInfo.destination)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
        suggestionsTripInfo = context as SuggestionsTripInfo
    }

    override fun scrollTo(position: Int): Boolean {
        if (position < 0 || position > tripLocationAdapter.itemCount) {
            return false
        } else {
            recyclerView.scrollToPosition(position)
            return true
        }
    }

    override fun setBackGroundItem(position: Int): Unit {
        tripLocationAdapter?.setBackgroundItem(position)

    }
}