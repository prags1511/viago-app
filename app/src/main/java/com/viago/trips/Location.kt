package com.viago.trips

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.trips.annotations.*
import com.viago.trips.models.Place

/**
 * Created by mayank on 8/9/17.
 */
data class Location(
        val image: String?,
        val title: String,
        val address: String,
        val latitude: Double,
        val longitude: Double,
        val meta: String? = null,
        @SuppressLint("SupportAnnotationUsage") @LocationStatus.Status var status: Int = LocationStatus.NONE!!,
        @PlaceSearchProvider internal var provider: Long = NONE,
        val distance: Float=0.0f

) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString()!!,
            parcel.readString()!!,
            parcel.readDouble(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readLong() ,
            parcel.readFloat()) {
    }

    fun toPlace(gson:Gson) = Place(title, address, LatLng(latitude, longitude), image, extraData = gson.fromJson(meta.takeIf { meta?.take(1) == "{" }, object : TypeToken<Place.PlaceExtraData>() {
    }.type), provider = provider)

    fun getProviderInitial(): String = when (provider) {
        GOOGLE -> "G"
        FOURSQUARE -> "F"
        YELP -> "Y"
        else -> ""
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
        parcel.writeString(title)
        parcel.writeString(address)
        parcel.writeDouble(latitude)
        parcel.writeDouble(longitude)
        parcel.writeString(meta)
        parcel.writeInt(status)
        parcel.writeLong(provider)
        parcel.writeFloat(distance)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }

}