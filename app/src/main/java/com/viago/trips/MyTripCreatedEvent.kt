package com.viago.trips

/**
 * Created by mayank on 8/9/17.
 */
class MyTripCreatedEvent(
        val myTrip: MyTrip,
        val start: Boolean = false,
        val share: Boolean = false,
        val commaSeparatedLatLngList: List<String>? = null
)