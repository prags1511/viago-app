package com.viago.trips.models

import android.os.Parcel
import android.os.Parcelable
import android.text.TextUtils
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.trips.Location
import com.viago.trips.annotations.*
import io.branch.referral.util.ProductCategory


/**
 * Created by mayank on 3/8/17.
 */
data class Place(internal val name: String?,
                 internal val address: String?,
                 internal val latLng: LatLng,
                 internal var imageUrl: String?,
                 internal val rating: Float = -1F,
                 internal val noOfRatings: Long? = -1,
                 internal val moreImages: List<String?>? = null,
                 internal val placeId: String? = null,
                 internal val extraData: PlaceExtraData? = null,
                 @PlaceSearchProvider internal var provider: Long = NONE
) : Parcelable {

    internal var isAdded = false

    internal var isAlreadyAdded = false

    internal var isNeedToHide = false

    internal var distance = 0.0f

    fun updateImage(imgUrl: String) {
        imageUrl = imgUrl
    }

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readParcelable(LatLng::class.java.classLoader)!!,
            parcel.readString(),
            parcel.readFloat(),
            parcel.readValue(Long::class.java.classLoader) as? Long,
            parcel.createStringArrayList(),
            parcel.readString(),
            parcel.readParcelable(PlaceExtraData::class.java.classLoader),
            parcel.readLong()) {
        isAdded = parcel.readByte() != 0.toByte()
    }

    fun toLocation(gson: Gson): Location = Location(imageUrl, if (TextUtils.isEmpty(name)) "" else name!!, if (TextUtils.isEmpty(address)) "" else address!!, latLng.latitude, latLng.longitude, gson.toJson(extraData, object : TypeToken<PlaceExtraData>() {
    }.type), provider = provider)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other?.javaClass != javaClass) return false

        other as Place

        if (latLng != other.latLng) return false

        return true
    }

    override fun hashCode(): Int {
        return latLng.hashCode()
    }

    fun getProviderInitial(): String = when (provider) {
        GOOGLE -> "G"
        FOURSQUARE -> "F"
        YELP -> "Y"
        else -> ""
    }

    data class PlaceExtraData(internal val hours: String? = null,
                              internal var open: Boolean = false,
                              internal val contact: String? = null,
                              internal val category: String? = null,
                              internal val socialLinks: PlaceSocial? = null) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readByte() != 0.toByte(),
                parcel.readString(),
                parcel.readString(),
                parcel.readParcelable(PlaceSocial::class.java.classLoader)) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
            parcel.writeString(hours)
            parcel.writeByte(if (open) 1 else 0)
            parcel.writeString(contact)
            parcel.writeString(category)
            parcel.writeParcelable(socialLinks, flags)
        }

        override fun describeContents(): Int {
            return 0
        }

        companion object CREATOR : Parcelable.Creator<PlaceExtraData> {
            override fun createFromParcel(parcel: Parcel): PlaceExtraData {
                return PlaceExtraData(parcel)
            }

            override fun newArray(size: Int): Array<PlaceExtraData?> {
                return arrayOfNulls(size)
            }
        }
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeParcelable(latLng, flags)
        parcel.writeString(imageUrl)
        parcel.writeFloat(rating)
        parcel.writeValue(noOfRatings)
        parcel.writeStringList(moreImages)
        parcel.writeString(placeId)
        parcel.writeParcelable(extraData, flags)
        parcel.writeLong(provider)
        parcel.writeByte(if (isAdded) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Place> {
        override fun createFromParcel(parcel: Parcel): Place {
            return Place(parcel)
        }

        override fun newArray(size: Int): Array<Place?> {
            return arrayOfNulls(size)
        }
    }

}