package com.viago.trips.models

/**
 * Created by mayank on 8/10/17.
 */
data class SuggestionDeclineStatus(
        val status: Int = 3
)