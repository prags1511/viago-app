package com.viago.trips.models

import com.viago.trips.Location

/**
 * Created by mayank on 8/9/17.
 */
data class CreateTripRequest(
        val searched_location_type: String,
        val title: String,
        val distance: String?,
        val duration: String?,
        val searched_location: String,
        val searched_location_latitude: Double,
        val searched_location_longitude: Double,
        val categories: List<Int>,
        val locations: List<Location>,
        val suggestion_id: Long? = null,
        val id: Long? = null
)