package com.viago.trips.models.suggestions

import com.viago.trips.Location

/**
 * Created by mayank on 25/9/17.
 */
class TripPostSuggestionRequest(
        val locations: List<Location>
)