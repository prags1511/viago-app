package com.viago.trips.models

import com.viago.trips.Location
import com.viago.trips.models.suggestions.SuggestedBy

/**
 * Created by mayank on 26/9/17.
 */
data class SuggestionDetails(
        val id: Long,
        val suggested_by: SuggestedBy,
        val locations: List<Location>
)