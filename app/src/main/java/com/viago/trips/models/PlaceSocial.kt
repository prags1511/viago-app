package com.viago.trips.models

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by a_man on 05-11-2017.
 */

data class PlaceSocial(internal val website: String? = null, internal val facebook: String? = null, internal val twitter: String? = null) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(website)
        parcel.writeString(facebook)
        parcel.writeString(twitter)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlaceSocial> {
        override fun createFromParcel(parcel: Parcel): PlaceSocial {
            return PlaceSocial(parcel)
        }

        override fun newArray(size: Int): Array<PlaceSocial?> {
            return arrayOfNulls(size)
        }
    }
}