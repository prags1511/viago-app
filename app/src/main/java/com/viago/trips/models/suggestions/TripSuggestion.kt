package com.viago.trips.models.suggestions

/**
 * Created by mayank on 26/9/17.
 */
class TripSuggestion(
    val id: Long,
    val suggested_by: SuggestedBy,
    val locations_count: Int,
    val image: String
)