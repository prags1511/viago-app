package com.viago.trips.models

/**
 * Created by mayank on 9/10/17.
 */
data class Keyword (
        val title: String
)