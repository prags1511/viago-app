
package com.viago.trips.models.foursquare;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flags {

    @SerializedName("outsideRadius")
    @Expose
    private Boolean outsideRadius;

    public Boolean getOutsideRadius() {
        return outsideRadius;
    }

    public void setOutsideRadius(Boolean outsideRadius) {
        this.outsideRadius = outsideRadius;
    }

}
