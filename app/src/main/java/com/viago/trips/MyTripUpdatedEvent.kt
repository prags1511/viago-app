package com.viago.trips

import com.viago.trips.models.CreateTripRequest

/**
 * Created by mayank on 8/9/17.
 */
class MyTripUpdatedEvent(
        val createTripRequest: CreateTripRequest,
        val isEdited: Boolean
)