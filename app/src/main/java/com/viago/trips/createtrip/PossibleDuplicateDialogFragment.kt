package com.viago.trips.createtrip

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import com.viago.R
import com.viago.baseclasses.BaseDialog

/**
 * Created by mayank on 1/8/17.
 */
class PossibleDuplicateDialogFragment : BaseDialog() {

    var dontAddToTripClicked: (() -> Unit)? = null
    var addToTripClicked: (() -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_possible_duplicate, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            findViewById<Button>(R.id.dont_add_to_trip).setOnClickListener {
                dontAddToTripClicked?.invoke()
                dismiss()
            }
            findViewById<Button>(R.id.add_to_trip).setOnClickListener {
                addToTripClicked?.invoke()
                dismiss()
            }
        }

        return dialog
    }
}