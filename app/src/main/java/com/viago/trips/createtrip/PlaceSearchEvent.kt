package com.viago.trips.createtrip

import com.google.android.gms.maps.model.LatLng

/**
 * Created by mayank on 23/9/17.
 */
class PlaceSearchEvent (
    val destination: String,
    val latLng: LatLng,
    val keyword: String
)