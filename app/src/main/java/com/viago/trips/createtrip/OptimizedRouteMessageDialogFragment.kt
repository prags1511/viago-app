package com.viago.trips.createtrip

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import com.viago.R
import com.viago.baseclasses.BaseDialog
import com.viago.extensions.hide

/**
 * Created by a_man on 27-10-2017.
 */

class OptimizedRouteMessageDialogFragment : BaseDialog() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_approve_suggestions, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            val title: TextView = findViewById<TextView>(R.id.title) as TextView
            val message: TextView = findViewById<TextView>(R.id.message) as TextView
            title.setText("Optimising trip")
            message.setText("Creating optimized route to minimize travel time.")
            findViewById<LinearLayout>(R.id.button_container).hide()
        }

        var handler: Handler = Handler()
        try {
            handler.postDelayed(Runnable { dismiss() }, 3000)
        }catch(e:Exception){

        }
        return dialog
    }
}