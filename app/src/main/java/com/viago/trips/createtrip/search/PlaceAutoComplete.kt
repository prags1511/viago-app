package com.viago.trips.createtrip.search

/**
 * Created by mayank on 22/9/17.
 */
data class PlaceAutoComplete(
        val primaryText: String?,
        val secondaryText: String?,
        val placeId: String?
)