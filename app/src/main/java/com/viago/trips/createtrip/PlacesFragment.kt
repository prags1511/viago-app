package com.viago.trips.createtrip

import android.content.Context
import android.os.Bundle
import android.os.Handler

import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import bindView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.extensions.hide
import com.viago.extensions.show
import com.viago.trips.Location
import com.viago.trips.annotations.FOURSQUARE
import com.viago.trips.annotations.GOOGLE
import com.viago.trips.annotations.PlaceSearchProvider
import com.viago.trips.annotations.YELP
import com.viago.trips.createtrip.placesearchutils.FoursquareNearbyPlacesUtil
import com.viago.trips.createtrip.placesearchutils.GoogleNearbyPlacesUtil
import com.viago.trips.createtrip.placesearchutils.PlaceUtil
import com.viago.trips.createtrip.placesearchutils.YelpNearbyPlacesUtil
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.interfaces.PlacesSearchResultCallback
import com.viago.trips.interfaces.TripInfo
import com.viago.trips.models.Place
import com.viago.utils.LocationHelper
import com.viago.utils.Scrollable
import com.viago.utils.SpacesItemDecoration
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by mayank on 2/8/17.
 */
class PlacesFragment : BaseFragment(), Scrollable {


    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    private val noResultsView by bindView<View>(R.id.no_results_view)
    val loadingMoreResults: ProgressBar by bindView(R.id.loadingMoreResults)
    val btnLoadMore by bindView<Button>(R.id.btnLoadMore)

    private val placesAdapter by lazy { PlacesAdapter(object : LocationClickCallBack {
        override fun clickLocation(position: Int) {

            (activity as CreateTripActivity).clickLocation(position)

        }
    })}
    private val placeUtil by lazy<PlaceUtil> {
     //   GoogleNearbyPlacesUtil.pageToken=""
        when (placeSearchProvider) {
            GOOGLE -> GoogleNearbyPlacesUtil(context!!)
            FOURSQUARE -> FoursquareNearbyPlacesUtil()
            YELP -> YelpNearbyPlacesUtil(context!!)
            else -> GoogleNearbyPlacesUtil(context!!)
        }
    }

    private lateinit var tripInfo: TripInfo

    private lateinit var placeArraylistTemp: ArrayList<Place>

    private lateinit var placesSearchResultCallback: PlacesSearchResultCallback

    private val gson = Gson()

    private val lastVisibleItemPosition: Int
        get() = (recyclerView.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()

//    private val delhiLatLong = LatLng(28.7041, 77.1025)
//    private val singaporeLatLong = LatLng(1.3521, 103.8198)

    @PlaceSearchProvider
    var placeSearchProvider = GOOGLE

    override fun getLayoutRes(): Int = R.layout.fragment_places

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        placeSearchProvider = arguments!!.getLong(PLACE_SEARCH_PROVIDER)

        recyclerView.run {
            layoutManager = LinearLayoutManager(context)
            adapter = placesAdapter
        //    setPadding(dpToPx(12), paddingTop, dpToPx(12), paddingBottom)
           // addItemDecoration(SpacesItemDecoration(dpToPx(4)))
         //   setPadding(dpToPx(12), paddingTop, dpToPx(12), paddingBottom)
        //    addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView!!, newState)

                    val totalItemCount = this@PlacesFragment.recyclerView!!.layoutManager!!.itemCount
                    if (totalItemCount == lastVisibleItemPosition + 1) {
                        when (placeSearchProvider) {
                            GOOGLE -> {
                              //  Toast.makeText(context,"Click"+ GoogleNearbyPlacesUtil.pageToken,Toast.LENGTH_LONG).show()
                                if(!GoogleNearbyPlacesUtil.pageToken.equals("stop")) {
                                    btnLoadMore.visibility=View.VISIBLE
                                    btnLoadMore.setOnClickListener(View.OnClickListener {
                                        btnLoadMore.visibility=View.GONE
                                        loadingMoreResults.visibility=View.VISIBLE
                                        placeUtil.findNearbyPlaces(tripInfo.latLng.latitude, tripInfo.latLng.longitude, tripInfo.keyword, placeSearchCallback)

                                    })
                                    }else{
                                    loadingMoreResults.visibility=View.GONE
                                    btnLoadMore.visibility=View.GONE
                                }
                            }


                        }
                    }else{
                        btnLoadMore.visibility=View.GONE
                    }
                }
            })
        }

        searchPlaces()
    }



    fun addLocation(it: Location) {
        val place = it.toPlace(gson)
        place.isAdded = true
        placesAdapter.addItemOnTop(place)
    }

    private fun searchPlaces() {
        noResultsView.hide()
        recyclerView.show()

        placeUtil.findNearbyPlaces(tripInfo.latLng.latitude, tripInfo.latLng.longitude, tripInfo.keyword, placeSearchCallback)
    }

    private val placeSearchCallback by lazy {
        object : PlacesSearchCallback {
            override fun onSearchFailed() {}

            override fun onSearchSuccess(places: List<Place>) {
                if (places.isEmpty() && recyclerView.adapter!!.itemCount==0) {
                    noResultsView.show()
                    recyclerView.hide()
                    return
                }
              var list = ArrayList<Location>()
                if (!tripInfo.selectedLocations.isEmpty()) {
                    places.forEach {
                        val place = it


                        tripInfo.selectedLocations.forEach {

                          //  var placemodel = it.toPlace(gson)


                               /*  if (place?.latLng.latitude.equals( it.latitude)) {
                                     place.isAdded = true
                                     place.provider = FOURSQUARE
                                     //  return
                                 }*/

                            if (place.name.equals(it.title,ignoreCase = true) && place.address.equals(it.address,ignoreCase = true)) {
                                place.isAdded = true
                               //place.provider = FOURSQUARE
                                place.isAlreadyAdded = true
                                list.add(it)
                                //  return

                            }


                        }




                        if (tripInfo?.source!=null && place.name.equals(tripInfo?.source!!.title,ignoreCase = true) && place.address.equals(tripInfo?.source!!.address,ignoreCase = true)) {
                            place.isAdded = true
                            //place.provider = FOURSQUARE
                            place.isAlreadyAdded = true
                            tripInfo.selectedLocations.add(0,tripInfo.source!!)


                            //  return

                        }
                        if (tripInfo?.source!=null  &&
                                place.name.equals(tripInfo.destination!!.title,ignoreCase = true) && place.address.equals(tripInfo.destination!!.address,ignoreCase = true)) {
                            place.isAdded = true
                            //place.provider = FOURSQUARE
                            place.isAlreadyAdded = true
                            tripInfo.selectedLocations.add(tripInfo.destination!!)
                            //  return

                        }
                        //if (tripInfo.selectedLocations.contains(it.toLocation(Gson()))) it.isAdded = true
                    }
                }




                placeArraylistTemp = ArrayList<Place>()

                places.forEach {
                    val place = it

                    val locationselected: android.location.Location = android.location.Location("Loc a")
                    val locationsearched: android.location.Location = android.location.Location("Loc b")
                    locationselected.latitude = tripInfo.latLng.latitude
                    locationselected.longitude = tripInfo.latLng.longitude

                    locationsearched.latitude = place.latLng.latitude
                    locationsearched.longitude = place.latLng.longitude

                    var distance = locationselected.distanceTo(locationsearched)

                    place.distance = distance
                    placeArraylistTemp.add(place)

                }



//                 placeArraylistTemp.sortBy { it.distance }


                // var listItem = ArrayList<Place>();

                tripInfo.selectedLocations.forEach {

                    var location = it

                    var isAdded = false

                    list.forEach {

                        if(location.address.equals(it.address,ignoreCase = true)){

                            isAdded = true


                        }else {


                        }

                    }


                   // if(location.provider!=3L && !isAdded && placeArraylistTemp.size>0 && placeArraylistTemp[0].provider == location.provider ){

                     if(location.provider!=3L && !isAdded && placeArraylistTemp.size>0 ){

                    var locationItem = location.toPlace(gson)
                        locationItem.isAlreadyAdded = true

                        locationItem.isNeedToHide = true

                        placeArraylistTemp.add(0,locationItem)

                    }


                }

               /* placeArraylistTemp.forEach {

                    val place = it


                }*/

                placesAdapter.addItemsAtBottom(placeArraylistTemp as ArrayList<Place>)

                placesSearchResultCallback.onPlacesSearchResultObtained(placeSearchProvider, placeArraylistTemp)
            }

        }
    }

    fun researchPlaces() {
        placesAdapter.clear()
        searchPlaces()

    }

    fun setSelectedLocations(selectedLocations: ArrayList<Location>) {
        Handler().post({
            tripInfo.selectedLocations.clear()
            tripInfo.selectedLocations.addAll(selectedLocations)
            updatePlaces()
        })
    }

    override fun scrollTo(position: Int): Boolean {
        if (position < 0 || position > placesAdapter.itemCount) {
            return false
        } else {
            recyclerView.scrollToPosition(position)
            return true
        }
    }

    override fun setBackGroundItem(position: Int): Unit {

        placesAdapter?.setBackgroundItem(position)

    }

    fun updatePlaces() {
//        if(!tripInfo.selectedLocations.contains(tripInfo.source!!)){
//            tripInfo.selectedLocations.add(0,tripInfo.source!!)
//            tripInfo.selectedLocations.add(tripInfo.destination!!)
//        }
        placesAdapter.itemsList.forEachIndexed {
            index, place ->
            var location = place
            val isPlaceInSelectedPlaces = tripInfo.selectedLocations.contains(place.toLocation(Gson()))
            if (place.isAdded && !isPlaceInSelectedPlaces) {
                placesAdapter.updateItem(index, place.apply { place.isAdded = false }, true)
            } else if (!place.isAdded && isPlaceInSelectedPlaces) {
                placesAdapter.updateItem(index, place.apply { place.isAdded = true }, true)
            }
            if (!place.isAdded && place.name.equals(tripInfo.source!!.title) && place.address.equals(tripInfo.source!!.address)){
                placesAdapter.updateItem(index, place.apply { place.isAdded = true }, true)
                tripInfo.selectedLocations.add(0,tripInfo.source!!)
            }
            if (!place.isAdded && place.name.equals(tripInfo.destination!!.title) && place.address.equals(tripInfo.destination!!.address)){
                placesAdapter.updateItem(index, place.apply { place.isAdded = true }, true)
                if(!tripInfo.destination!!.title.equals(tripInfo.source!!.title))
                tripInfo.selectedLocations.add(tripInfo.destination!!)
            }
        }
        if(placesAdapter.itemsList !=null && placesAdapter.itemsList.size>0)
        (tripInfo as CreateTripActivity).onPlacesUpdated()

    }

    fun unAddPlace(place: Place) {
        val index = placesAdapter.itemsList.indexOf(place)
        if (index != -1) {
            placesAdapter.updateItem(index, place.apply { isAdded = false }, true)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
//        if(tripInfo.source != null && !tripInfo.selectedLocations.contains(tripInfo.source!!)){
//            tripInfo.selectedLocations.add(0,tripInfo.source!!)
//            tripInfo.selectedLocations.add(tripInfo.destination!!)
//        }
        placesSearchResultCallback = context as PlacesSearchResultCallback
    }

    internal companion object {

        private const val PLACE_SEARCH_PROVIDER = "PlaceSearchProvider"

        fun newInstance(@PlaceSearchProvider placeSearchProvider: Long): PlacesFragment {
            val placesFragment = PlacesFragment()
            val args = Bundle()
            args.putLong(PLACE_SEARCH_PROVIDER, placeSearchProvider)
            placesFragment.arguments = args
            return placesFragment
        }
    }

    override fun onDetach() {
        super.onDetach()
       // GoogleNearbyPlacesUtil.pageToken=""
    }

}
