package com.viago.trips.createtrip

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Button
import com.viago.R
import com.viago.baseclasses.BaseDialog
import com.viago.baseclasses.NavItemClickEvent
import org.greenrobot.eventbus.EventBus

/**
 * Created by mayank on 1/8/17.
 */
class TripCreatedDialogFragment: BaseDialog() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_trip_created, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            findViewById<Button>(R.id.no_button).setOnClickListener {
                EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_my_trips))
                dismiss()
                activity!!.finish()
            }
            findViewById<Button>(R.id.yes_button).setOnClickListener {
//                EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_book_now))
                dismiss()
                activity!!.finish()
            }
        }

        return dialog
    }
}