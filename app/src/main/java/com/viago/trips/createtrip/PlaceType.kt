package com.viago.trips.createtrip

/**
 * https://developers.google.com/places/supported_types
 */

object PlaceType {
    internal const val ACCOUNTING = "accounting"
    internal const val AIRPORT = "airport"
    internal const val AMUSEMENT_PARK = "amusement_park"
    internal const val AQUARIUM = "aquarium"
    internal const val ART_GALLERY = "art_gallery"
    internal const val ATM = "atm"
    internal const val BAKERY = "bakery"
    internal const val BANK = "bank"
    internal const val BAR = "bar"
    internal const val BEAUTY_SALON = "beauty_salon"
    internal const val BICYCLE_STORE = "bicycle_store"
    internal const val BOOK_STORE = "book_store"
    internal const val BOWLING_ALLEY = "bowling_alley"
    internal const val BUS_STATION = "bus_station"
    internal const val CAFE = "cafe"
    internal const val CAMPGROUND = "campground"
    internal const val CAR_DEALER = "car_dealer"
    internal const val CAR_RENTAL = "car_rental"
    internal const val CAR_REPAIR = "car_repair"
    internal const val CAR_WASH = "car_wash"
    internal const val CASINO = "casino"
    internal const val CEMETERY = "cemetery"
    internal const val CHURCH = "church"
    internal const val CITY_HALL = "city_hall"
    internal const val CLOTHING_STORE = "clothing_store"
    internal const val CONVENIENCE_STORE = "convenience_store"
    internal const val COURTHOUSE = "courthouse"
    internal const val DENTIST = "dentist"
    internal const val DEPARTMENT_STORE = "department_store"
    internal const val DOCTOR = "doctor"
    internal const val ELECTRICIAN = "electrician"
    internal const val ELECTRONICS_STORE = "electronics_store"
    internal const val EMBASSY = "embassy"
    internal const val FIRE_STATION = "fire_station"
    internal const val FLORIST = "florist"
    internal const val FUNERAL_HOME = "funeral_home"
    internal const val FURNITURE_STORE = "furniture_store"
    internal const val GAS_STATION = "gas_station"
    internal const val GYM = "gym"
    internal const val HAIR_CARE = "hair_care"
    internal const val HARDWARE_STORE = "hardware_store"
    internal const val HINDU_TEMPLE = "hindu_temple"
    internal const val HOME_GOODS_STORE = "home_goods_store"
    internal const val HOSPITAL = "hospital"
    internal const val INSURANCE_AGENCY = "insurance_agency"
    internal const val JEWELRY_STORE = "jewelry_store"
    internal const val LAUNDRY = "laundry"
    internal const val LAWYER = "lawyer"
    internal const val LIBRARY = "library"
    internal const val LIQUOR_STORE = "liquor_store"
    internal const val LOCAL_GOVERNMENT_OFFICE = "local_government_office"
    internal const val LOCKSMITH = "locksmith"
    internal const val LODGING = "lodging"
    internal const val MEAL_DELIVERY = "meal_delivery"
    internal const val MEAL_TAKEAWAY = "meal_takeaway"
    internal const val MOSQUE = "mosque"
    internal const val MOVIE_RENTAL = "movie_rental"
    internal const val MOVIE_THEATER = "movie_theater"
    internal const val MOVING_COMPANY = "moving_company"
    internal const val MUSEUM = "museum"
    internal const val NIGHT_CLUB = "night_club"
    internal const val PAINTER = "painter"
    internal const val PARK = "park"
    internal const val PARKING = "parking"
    internal const val PET_STORE = "pet_store"
    internal const val PHARMACY = "pharmacy"
    internal const val PHYSIOTHERAPIST = "physiotherapist"
    internal const val PLUMBER = "plumber"
    internal const val POLICE = "police"
    internal const val POST_OFFICE = "post_office"
    internal const val REAL_ESTATE_AGENCY = "real_estate_agency"
    internal const val RESTAURANT = "restaurant"
    internal const val ROOFING_CONTRACTOR = "roofing_contractor"
    internal const val RV_PARK = "rv_park"
    internal const val SCHOOL = "school"
    internal const val SHOE_STORE = "shoe_store"
    internal const val SHOPPING_MALL = "shopping_mall"
    internal const val SPA = "spa"
    internal const val STADIUM = "stadium"
    internal const val STORAGE = "storage"
    internal const val STORE = "store"
    internal const val SUBWAY_STATION = "subway_station"
    internal const val SYNAGOGUE = "synagogue"
    internal const val TAXI_STAND = "taxi_stand"
    internal const val TRAIN_STATION = "train_station"
    internal const val TRANSIT_STATION = "transit_station"
    internal const val TRAVEL_AGENCY = "travel_agency"
    internal const val UNIVERSITY = "university"
    internal const val VETERINARY_CARE = "veterinary_care"
    internal const val ZOO = "zoo"

    val list by lazy { listOf(
            ACCOUNTING, AIRPORT, AMUSEMENT_PARK, AQUARIUM, ART_GALLERY, ATM,
            BAKERY, BANK, BAR, BEAUTY_SALON, BICYCLE_STORE, BOOK_STORE, BOWLING_ALLEY, BUS_STATION,
            CAFE, CAMPGROUND, CAR_DEALER, CAR_RENTAL, CAR_REPAIR, CAR_WASH, CASINO, CEMETERY, CHURCH, CITY_HALL, CLOTHING_STORE, CONVENIENCE_STORE, COURTHOUSE,
            DENTIST, DEPARTMENT_STORE, DOCTOR,
            ELECTRICIAN, ELECTRONICS_STORE, EMBASSY,
            FIRE_STATION, FLORIST, FUNERAL_HOME, FURNITURE_STORE,
            GAS_STATION, GYM,
            HAIR_CARE, HARDWARE_STORE, HINDU_TEMPLE, HOME_GOODS_STORE, HOSPITAL,
            INSURANCE_AGENCY,
            JEWELRY_STORE,
            LAUNDRY, LAWYER, LIBRARY, LIQUOR_STORE, LOCAL_GOVERNMENT_OFFICE, LOCKSMITH, LODGING,
            MEAL_DELIVERY, MEAL_TAKEAWAY, MOSQUE, MOVIE_RENTAL, MOVIE_THEATER, MOVING_COMPANY, MUSEUM,
            NIGHT_CLUB,
            PAINTER, PARK, PARKING, PET_STORE, PHARMACY, PHYSIOTHERAPIST, PLUMBER, POLICE, POST_OFFICE,
            REAL_ESTATE_AGENCY, RESTAURANT, ROOFING_CONTRACTOR, RV_PARK,
            SCHOOL, SHOE_STORE, SHOPPING_MALL, SPA, STADIUM, STORAGE, STORE, SUBWAY_STATION, SYNAGOGUE,
            TAXI_STAND, TRAIN_STATION, TRANSIT_STATION, TRAVEL_AGENCY,
            UNIVERSITY,
            VETERINARY_CARE,
            ZOO
    ).map { string -> string.replace("_", " ").capitalize() } }
}