package com.viago.trips.createtrip.search

import android.Manifest
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.location.Address
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.annotation.LayoutRes
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import bindView
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence


import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.libraries.places.compat.*

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.baseclasses.BaseLocationFragment
import com.viago.extensions.closeKeyboard
import com.viago.extensions.hide
import com.viago.extensions.openKeyboard
import com.viago.extensions.show
import com.viago.trips.TripsUtil
import com.viago.trips.createtrip.PlaceType
import com.viago.trips.createtrip.PlaceTypesAdapter
import com.viago.trips.createtrip.placesearchutils.GoogleNearbyPlacesUtil
import com.viago.trips.interfaces.TripInfo
import com.viago.utils.Constants
import com.viago.utils.LocationHelper
import com.viago.utils.SharedPrefsUtils
import com.viago.views.EditTextCompat
import org.jetbrains.annotations.Nullable


/**
 * Created by mayank on 20/9/17.
 */
class SearchQueryFragment public constructor() : BaseLocationFragment() {

    private val destination by bindView<EditTextCompat>(R.id.destination)
    private val myLocation by bindView<View>(R.id.my_location)
    private val searchLocation by bindView<View>(R.id.search_location)
    private val searchType by bindView<View>(R.id.search_type)
    private val placeTypeText by bindView<EditTextCompat>(R.id.search_type_text)
    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)
    private val poweredByGoogle by bindView<View>(R.id.powered_by_google)
    private val search by bindView<View>(R.id.search)
    private val my_keyword by bindView<ImageView>(R.id.my_keyword)
    private lateinit var placeLatLng: LatLng
    private var isDestinationSelected: Boolean = false
    protected val toolbar by bindView<Toolbar>(R.id.toolbar)
    protected val searchmap by bindView<View>(R.id.searchmap)
    protected var isShowToolBar:Boolean = true


    private val autocompleteFilter = AutocompleteFilter.Builder()
            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
            //.setCountry("IN")
            .build()
    private val geoDataClient by lazy { Places.getGeoDataClient(context!!, null) }

    private lateinit var placeSearchListener: PlaceSearchListener
    private var tripInfo: TripInfo? = null
    private var isCurrentLocation: Boolean = false


    @LayoutRes
    override fun getLayoutRes(): Int = R.layout.fragment_search_query

    private var keywords: List<String> = ArrayList()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
        val placesAutoCompleteAdapter = PlaceAutoCompleteRecyclerAdapter({ place: PlaceAutoComplete? ->
            place?.let {
                geoDataClient.getPlaceById(place.placeId)
                        .addOnSuccessListener { placeBufferResponse: PlaceBufferResponse? ->
                            placeBufferResponse?.let {
                                it.firstOrNull()?.let {
                                    setDestination(it.latLng, it.name?.toString())
                                }
                                it.release()
                            }
                        }
            }
        })


        SharedPrefsUtils.setBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,false);

        val placeTypesAdapter = PlaceTypesAdapter { string -> run { placeTypeText.setText(string); recyclerView.hide() } }

        TripsUtil.getKeywords({
            keywords = it.map { it.title }
            placeTypesAdapter.addItemsAtBottom(keywords)
        }, {
            keywords = PlaceType.list
            placeTypesAdapter.addItemsAtBottom(keywords)
        })

        recyclerView.run {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }

        arrayOf(searchLocation, destination).forEach {
            it.setOnClickListener {
               // poweredByGoogle.show()
                if (destination.text.isNullOrBlank()) recyclerView.hide() else recyclerView.show()
                recyclerView.adapter = placesAutoCompleteAdapter
            }
        }

        arrayOf(destination, placeTypeText).forEach { it.setOnFocusChangeListener { v, hasFocus -> if (hasFocus) v.performClick() } }

        myLocation.setOnClickListener { requestLatLong() }

       // toolbar.title = "Search"

        arrayOf(searchType, placeTypeText).forEach {
            it.setOnClickListener {
               // poweredByGoogle.hide()
                recyclerView.show()
                recyclerView.adapter = placeTypesAdapter
            }
        }

        destination.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                isDestinationSelected = false


                if (s.isNullOrBlank()) {
                    placesAutoCompleteAdapter.clear()
                    recyclerView.hide()
                } else {
                    if(SharedPrefsUtils.getBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,false)){

                        isCurrentLocation = false
                        SharedPrefsUtils.setBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,false);


                    }
                    recyclerView.show()
                    showPlaceAutoCompletePredictions(s.toString(), placesAutoCompleteAdapter)
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        })

        placeTypeText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.isNullOrBlank()) {
                    placesAutoCompleteAdapter.clear()
                } else {
                    // Filtering the list based on the search keyword
                    val filteredList: List<String> = keywords.filter { it.toLowerCase().contains(s.toString().toLowerCase()) }
                    placeTypesAdapter.itemsList = filteredList;
//                    placeTypesAdapter.addItemsAtBottom( filteredList )
                    showPlaceAutoCompletePredictions(s.toString(), placesAutoCompleteAdapter)
                }
                placeTypesAdapter.notifyDataSetChanged();
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        placeTypeText.setOnEditorActionListener { v, actionId, event ->
            if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                my_keyword.performClick()
            }
            false
        }
        my_keyword.setOnClickListener {
            GoogleNearbyPlacesUtil.pageToken=""
            SharedPrefsUtils.setBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,false);
            postSearchEvent()

        }

        destination.requestFocus()
        activity!!.openKeyboard()

        if (tripInfo != null) {
            setDestination(tripInfo?.latLng!!,LocationHelper.getAddress(getAppCompatActivity(),tripInfo?.latLng!!)?.locality)
            var keyword: String = tripInfo?.keyword!!
            keyword = keyword.capitalize()
            if (keyword.contains("_")) {
                val keywordSplit = keyword.split("_")
                keyword = ""
                for (key in keywordSplit)
                    keyword += key.capitalize() + " "
                keyword = keyword.trim()
            }
            placeTypeText.setText(keyword)
        }

        if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_TAP_TARGET_SEARCH_LOCATION, false)) {
            showTapTargetView()
            SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_SEARCH_LOCATION, true)
        }

        if(isShowToolBar) {
            toolbar.visibility = View.VISIBLE
            searchmap.visibility = View.GONE
        }else {
            toolbar.visibility = View.GONE
            searchmap.visibility = View.VISIBLE
        }

    }


    override fun onResume() {
        super.onResume()

        if (SharedPrefsUtils.getStringPreference(Constants.KEY_TEMP_LOCATIONS)!=null &&
                !SharedPrefsUtils.getStringPreference(Constants.KEY_TEMP_LOCATIONS).equals("[]")) {

            destination.isEnabled = false
            myLocation.isEnabled = false
            myLocation.setBackgroundColor(Color.parseColor("#f5f5f5"))
            destination.setBackgroundColor(Color.parseColor("#f5f5f5"))


        }else {

            destination.isEnabled = true
            myLocation.isEnabled = true
            myLocation.setBackgroundColor(Color.parseColor("#ffffff"))
            destination.setBackgroundColor(Color.parseColor("#ffffff"))
        }


    }
    private fun showTapTargetView() {
//        TapTargetSequence(activity)
//                .target(TapTarget.forView(myLocation, "Current location", "Click here to select your current location"))
//                .start()
    }

    private fun showPlaceAutoCompletePredictions(s: String, placesAutoCompleteAdapter: PlaceAutoCompleteRecyclerAdapter) {
        getAutoCompletePredictions(s, {
            placesAutoCompleteAdapter.clear()
            placesAutoCompleteAdapter.addItemsAtBottom(it)
        })
    }


    private val autocompletePredictionsCache = HashMap<String, List<PlaceAutoComplete>>()


    private fun getAutoCompletePredictions(s: String, onResultObtained: (result: List<PlaceAutoComplete>) -> Unit) {

        if (autocompletePredictionsCache.contains(s)) {

            onResultObtained(autocompletePredictionsCache[s] ?: ArrayList())

        } else {

            val task = geoDataClient.getAutocompletePredictions(s, null, autocompleteFilter)

            task.addOnSuccessListener {
                autocompletePredictionBufferResponse: AutocompletePredictionBufferResponse? ->

                autocompletePredictionBufferResponse?.let {
                    val result = it.map { PlaceAutoComplete(it.getPrimaryText(null)?.toString(), it.getSecondaryText(null)?.toString(), it.placeId) }
                    autocompletePredictionsCache.put(s, result)
                    onResultObtained(result)
                    it.release()
                }
            }

            task.addOnFailureListener{
                it.message
                }
        }
    }

    private fun postSearchEvent() {
        if (isValid()) {
            activity!!.closeKeyboard()
            placeSearchListener.onPlaceSearchEvent(
                    destination.text.toString(),
                    placeLatLng,
                    placeTypeText.text.toString().trim().toLowerCase().replace(" ", "_"),isCurrentLocation
            )
        }
    }

    private fun isValid(): Boolean {
        var isValid = true

        if (placeTypeText.text.isNullOrBlank()) {
            placeTypeText.error = "Please enter a keyword"
            isValid = false
        } else {
            placeTypeText.error = null
        }

        if (!isDestinationSelected) {
            destination.error = "Please select a valid destination"
            isValid = false
        } else {
            destination.error = null
        }

        if (destination.text.isNullOrBlank()) {
            destination.error = "Please select destination"
            isValid = false
        } else {
            destination.error = null
        }

        return isValid
    }

    override fun onLocationReceived(latLng: LatLng, address: Address?) {

        isCurrentLocation = true

        SharedPrefsUtils.setBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,false);

        setDestination(latLng, address?.locality)
    }

    private fun setDestination(latLng: LatLng, name: String?) {
        destination.setText(name)
        placeLatLng = latLng
        isDestinationSelected = true
        placeTypeText.requestFocus()
    }

    companion object {

        interface PlaceSearchListener {
            fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String,isCurrentLocation: Boolean)
        }

        fun newInstance(tripInfo: TripInfo?, placeSearchListener: PlaceSearchListener,isShowToolbar:Boolean): SearchQueryFragment {
            val searchQueryFragment = SearchQueryFragment()
            searchQueryFragment.placeSearchListener = placeSearchListener
            searchQueryFragment.tripInfo = tripInfo
            searchQueryFragment.isShowToolBar = isShowToolbar
            return searchQueryFragment
        }

        /*  val sydney = LatLng(28.65381, 77.22897)
        val opera = LatLng(28.663649, 77.452271)
        val opera1 = LatLng(28.628454, 77.376945)*/

        /*private val BOUNDS_GREATER_SYDNEY = LatLngBounds(
                LatLng(-34.041458, 150.790100), LatLng(-33.682247, 151.383362))*/

        private val BOUNDS_GREATER_DELHI_REGION = LatLngBounds(
                LatLng(28.628454, 77.376945), LatLng(28.65381, 77.22897))
    }


}