package com.viago.trips.createtrip.placesearchutils

import com.google.android.gms.maps.model.LatLng
import com.viago.trips.annotations.FOURSQUARE
import com.viago.trips.createtrip.PlacesSearchCallback
import com.viago.trips.createtrip.placesearchservices.FoursquareService
import com.viago.trips.interfaces.PlaceInfo
import com.viago.trips.models.Place
import com.viago.trips.models.PlaceSocial
import com.viago.trips.models.foursquare.FoursquarePlaces
import com.viago.trips.models.foursquare.Item
import com.viago.utils.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.net.URLEncoder
import java.util.*

/**
 * Created by mayank on 4/7/17.
 */

internal class FoursquareNearbyPlacesUtil : Callback<FoursquarePlaces>, PlaceUtil {
    override fun pleaceDetail(placeId: String, placesSearchCallback: PlaceInfo) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var placesSearchCallback: PlacesSearchCallback
    val foursquareService by lazy { ApiUtils.retrofitInstance.create(FoursquareService::class.java) }

    override fun findNearbyPlaces(latitude: Double, longitude: Double, placeType: String, placesSearchCallback: PlacesSearchCallback, radius: Int) {
        this.placesSearchCallback = placesSearchCallback

        val foursquarePlacesCall = foursquareService.getNearbyPlaces(latitude.toString() + "," + longitude, URLEncoder.encode(placeType.replace("_", " "), "UTF-8"), radius)
        foursquarePlacesCall.enqueue(this)
    }

    override fun getMaxRadius(): Int = MAX_RADIUS

    override fun onResponse(call: Call<FoursquarePlaces>, response: Response<FoursquarePlaces>) {
        if (response.isSuccessful) {
            Timber.d("search finished")
            val foursquarePlaces = response.body()
            val items: List<Item> = foursquarePlaces.response.groups[0].items
            val placeList = ArrayList<Place>()
            for (i in 0 until items.size) {
                val venue = items[i].venue
                val photo = venue.photos.groups.firstOrNull()?.items?.firstOrNull()
                val place = Place(venue.name,
                        venue.location.formattedAddress.joinToString(),
                        LatLng(venue.location.lat, venue.location.lng),
                        if (venue.categories.size > 0) venue.categories[0].icon.prefix+"bg_64"+venue.categories[0].icon.suffix else null,
                        (venue.rating ?: -2F) / 2,
                        venue.ratingSignals,
                        null,
                        venue.id,
                        Place.PlaceExtraData(if (venue.hours == null) null else venue.hours.status,
                                if (venue.hours == null) false else venue.hours.isOpen,
                                venue.contact.phone,
                                if (venue.categories.size > 0) venue.categories[0].name else null,
                                PlaceSocial(venue.url, null, venue.contact.twitter))
                        ,
                        FOURSQUARE)
                if( venue.location.formattedAddress!=null &&  venue.location.formattedAddress.size>1) {
                    placeList.add(place)
                }
            }
            placesSearchCallback.onSearchSuccess(placeList)
        } else {
            Timber.d("search error")
            placesSearchCallback.onSearchFailed()
        }
    }

    override fun onFailure(call: Call<FoursquarePlaces>, t: Throwable) {
        Timber.d("search failed")
        placesSearchCallback.onSearchFailed()
    }

    companion object {
        private const val MAX_RADIUS = 50000
    }
}
