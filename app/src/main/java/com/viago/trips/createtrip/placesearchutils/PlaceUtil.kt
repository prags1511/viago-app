package com.viago.trips.createtrip.placesearchutils

import com.viago.trips.createtrip.PlacesSearchCallback
import com.viago.trips.interfaces.PlaceInfo

/**
 * Created by mayank on 2/8/17.
 */
interface PlaceUtil {
    fun findNearbyPlaces(latitude: Double, longitude: Double, placeType: String, placesSearchCallback: PlacesSearchCallback, radius: Int = getMaxRadius())
    fun getMaxRadius(): Int
    fun pleaceDetail( placeId: String,placesSearchCallback: PlaceInfo)
}