package com.viago.trips.createtrip

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.location.Address
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import bindView
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.compat.AutocompleteFilter
import com.google.android.libraries.places.compat.ui.PlaceAutocomplete
import com.viago.R
import com.viago.baseclasses.BaseLocationFragment
import com.viago.trips.Location
import com.viago.trips.annotations.NONE
import com.viago.trips.interfaces.TripInfo
import timber.log.Timber


/**
 * Created by a_man on 27-10-2017.
 */

class SourceDestSelectionFragment private constructor() : BaseLocationFragment() {
    private val startLocation by bindView<TextView>(R.id.start_location)
    private val endLocation by bindView<TextView>(R.id.end_location)
    private val myLocation by bindView<ImageView>(R.id.my_location)
    private val checkbox by bindView<CheckBox>(R.id.checkbox)
    private val planmytrip by bindView<Button>(R.id.planmytrip)
    //private val close by bindView<View>(R.id.close)
    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)

    private var locType = START_LOC
    private var startPlace: Location? = null
    private var endPlace: Location? = null
    private lateinit var tripInfo: TripInfo
    private lateinit var _adapter: SourceDestSuggestionAdapter

    private lateinit var endLocationsSelectedListener: EndLocationsSelectedListener

    override fun getLayoutRes() = R.layout.fragment_source_dest_selection

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        startLocation.setText(tripInfo.source?.title)
        endLocation.setText(tripInfo.destination?.title)
        startPlace=tripInfo.source
        endPlace=tripInfo.destination
        startLocation.setOnClickListener { openPlaceAutoComplete(START_LOC) }
        endLocation.setOnClickListener { openPlaceAutoComplete(END_LOC) }
        myLocation.setOnClickListener { requestLatLong() }
        //close.setOnClickListener { activity.onBackPressed() }
        checkbox.setOnCheckedChangeListener { _, isChecked ->
            setDest(if (isChecked) startPlace else null)
        }
        toolbar.run {
            title = "Select Points"
            setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
            setNavigationOnClickListener { activity!!.onBackPressed() }
        }
        planmytrip.setOnClickListener { view ->
            openTripRearrangeFragment()
        }

        _adapter = SourceDestSuggestionAdapter({
            setOrigin(it)
        }, {
            setDest(it)
        })
        recyclerView.run {
            layoutManager = LinearLayoutManager(context)
            adapter =_adapter.apply { addItemsAtBottom(tripInfo.selectedLocations) }

          //  addItemDecoration(SpacesItemDecoration(dpToPx(4)))
        }
    }

    private fun openPlaceAutoComplete(locType: Int) {
        this.locType = locType
        try {
            val autocompleteFilter = AutocompleteFilter.Builder()
                    .setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE)
                    .build()
            val intent = PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(autocompleteFilter)
                    .build(activity)
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE)
        } catch (e: GooglePlayServicesRepairableException) {
            GooglePlayServicesUtil.showErrorDialogFragment(e.connectionStatusCode, activity, null, PLACE_AUTOCOMPLETE_REQUEST_CODE, null)
        } catch (e: GooglePlayServicesNotAvailableException) {
            Toast.makeText(context, "Google Play Services is not available.", Toast.LENGTH_LONG).show()
        }

    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(context, data)
                //TODO add place image GeoDataClient
                if (locType == START_LOC) {
                    place.run { setOrigin(Location(null, name.toString(), address.toString(), latLng.latitude, latLng.longitude,null)) }
                } else {
                    place.run { setDest(Location(null, name.toString(), address.toString(), latLng.latitude, latLng.longitude,null)) }
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                val status = PlaceAutocomplete.getStatus(context, data)
                // TODO: Handle the error.
                Timber.d(status.statusMessage)

            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private fun openTripRearrangeFragment() {
        val origin = startPlace
        val dest = endPlace
        if (origin != null && dest != null) {
            tripInfo.source = origin
            tripInfo.destination = dest
            tripInfo.selectedLocations.run {
                remove(origin)
                remove(dest)
            }
            endLocationsSelectedListener.onLocationsSelected()
        }else{
            //Toast.makeText(context, "Please select the start and end location", Toast.LENGTH_LONG).show()
            showCreateDialog(context)
        }
    }

    private fun showCreateDialog(context: Context?) {
        /*val dialogTitle = "Please select the start and end location"
        val positiveButtonTitle = "Ok"

        //val builder = AlertDialog.Builder(context)


        val builder = AlertDialog.Builder(ContextThemeWrapper(context,android.R.style.Theme_Dialog))


        builder.setTitle(dialogTitle)

        builder.setPositiveButton(positiveButtonTitle) { dialog, _ ->
            dialog.dismiss()
        }


        val alert = builder.create()
        alert.show()*/

        val builder = AlertDialog.Builder(context)

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_fragment_select_from_to, null)
        builder.setView(view)

        val dialog = builder.create()

        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)

        with(view) {
            findViewById<Button>(R.id.dialog_dismiss).setOnClickListener {
                dialog.dismiss()
            }
        }
        dialog.show()
    }

    override fun onLocationReceived(latLng: LatLng, address: Address?) {
        address?.run {
            val addressString = getAddressLine(0) + ", " + locality + ", " + adminArea + ", " + countryName
            setOrigin(Location(null, locality, addressString, latLng.latitude, latLng.longitude))
        }
    }

    private fun setOrigin(location: Location) {
        startLocation.text = location.title
        startPlace = location
        location?.provider = NONE
        if (checkbox.isChecked) setDest(location)
        _adapter.setSelected(startPlace,endPlace)
        _adapter.notifyDataSetChanged()
       // else openTripRearrangeFragment()
    }

    private fun setDest(location: Location?) {
        Log.d("tag**",location?.title)
        endLocation.text = location?.title
        location?.provider = NONE
        endPlace = location
        _adapter.setSelected(startPlace,endPlace)
        _adapter.notifyDataSetChanged()
       // openTripRearrangeFragment()
    }

    companion object {
        private const val PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
        private const val START_LOC = 0
        private const val END_LOC = 1

        interface EndLocationsSelectedListener {
            fun onLocationsSelected()
        }

        fun newInstance(endLocationsSelectedListener: EndLocationsSelectedListener): SourceDestSelectionFragment {
            val sourceDestSelectionFragment = SourceDestSelectionFragment()
            sourceDestSelectionFragment.endLocationsSelectedListener = endLocationsSelectedListener
            return sourceDestSelectionFragment
        }

    }
}