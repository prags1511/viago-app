package com.viago.trips.createtrip.search

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import bindView
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate

/**
 * Created by mayank on 22/9/17.
 */
class PlaceAutoCompleteRecyclerAdapter(val onPlaceSelected: (place: PlaceAutoComplete?) -> Unit) : EasyRecyclerViewAdapter<PlaceAutoComplete>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = parent.inflate(android.R.layout.simple_expandable_list_item_2)
        return PlaceAutoCompleteViewHolder(view)
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder?, placeAutoComplete: PlaceAutoComplete, position: Int) {
        if (holder is PlaceAutoCompleteViewHolder) holder.bind(placeAutoComplete)
    }

    inner class PlaceAutoCompleteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text1 by bindView<TextView>(android.R.id.text1)
        val text2 by bindView<TextView>(android.R.id.text2)

        init {
            itemView.setOnClickListener { onPlaceSelected(getItem(adapterPosition)) }
        }

        fun bind(placeAutoComplete: PlaceAutoComplete) {
            text1.text = placeAutoComplete.primaryText
            text2.text = placeAutoComplete.secondaryText
        }
    }

}