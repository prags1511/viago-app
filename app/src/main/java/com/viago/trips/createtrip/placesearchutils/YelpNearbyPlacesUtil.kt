package com.viago.trips.createtrip.placesearchutils

import android.content.Context
import com.google.android.gms.maps.model.LatLng
import com.viago.trips.annotations.YELP
import com.viago.trips.createtrip.PlacesSearchCallback
import com.viago.trips.createtrip.placesearchservices.YelpService
import com.viago.trips.interfaces.PlaceInfo
import com.viago.trips.models.Place
import com.viago.trips.models.PlaceSocial
import com.viago.trips.models.yelp.Business
import com.viago.trips.models.yelp.YelpAuth
import com.viago.trips.models.yelp.YelpPlaces
import com.viago.utils.ApiUtils
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by mayank on 4/7/17.
 */

class YelpNearbyPlacesUtil(private val context: Context) : Callback<YelpPlaces>, PlaceUtil {
    override fun pleaceDetail(placeId: String, placesSearchCallback: PlaceInfo) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var placesSearchCallback: PlacesSearchCallback

    private val yelpService: YelpService by lazy { ApiUtils.retrofitInstance.create(YelpService::class.java) }

    override fun findNearbyPlaces(latitude: Double, longitude: Double, placeType: String, placesSearchCallback: PlacesSearchCallback, radius: Int) {
        this.placesSearchCallback = placesSearchCallback

        yelpService.getNearbyPlaces("Bearer " + "EttXjAGj_dQDVHPlUIhJbV8oHprDR_wMeBP4bqjdd0_4iFqLT4vIqMkpEHH_-hAH24XTpMIeyWCt27t72t6qCtu0hYaUP_DiXePu08FfEfzHyENCsszREVZGfKrlXHYx",
                placeType,
                latitude,
                longitude,
                radius)
                .enqueue(this)


       // this.placesSearchCallback = placesSearchCallback



        /* val expiryDateInSeconds: Long = SharedPrefsUtils.getLongPreference(Constants.KEY_YELP_EXPIRY, -1)
         val currentTimeInMillis: Long = Calendar.getInstance(TimeZone.getDefault()).timeInMillis

        if (expiryDateInSeconds == -1L || expiryDateInSeconds < currentTimeInMillis / 1000) {
            val yelpAuthResponseCall = yelpService.authenticate()
            yelpAuthResponseCall.enqueue(object : Callback<YelpAuth> {
                override fun onResponse(call: Call<YelpAuth>, response: Response<YelpAuth>) {
                    if (response.isSuccessful) {
                        Timber.d("auth success")
                        val yelpAuth = response.body()
                        SharedPrefsUtils.setStringPreference(Constants.KEY_YELP_ACCESS_TOKEN, yelpAuth.accessToken)
                        SharedPrefsUtils.setLongPreference(Constants.KEY_YELP_EXPIRY, currentTimeInMillis + yelpAuth.expiresIn - 100000)
                        init(SharedPrefsUtils.getStringPreference(Constants.KEY_YELP_ACCESS_TOKEN) ?: "", latitude, longitude, placeType, radius)
                    } else {
                        try {
                            Timber.d("auth error" + response.errorBody().string())
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        placesSearchCallback.onSearchFailed()
                    }
                }

                override fun onFailure(call: Call<YelpAuth>, t: Throwable) {
                    Timber.d("auth failure")
                    placesSearchCallback.onSearchFailed()
                }
            })
        } else {
            init(SharedPrefsUtils.getStringPreference(Constants.KEY_YELP_ACCESS_TOKEN) ?: "", latitude, longitude, placeType, radius)
        }*/
    }

    override fun getMaxRadius(): Int = MAX_RADIUS

    private fun init(authToken: String, latitude: Double, longitude: Double, placeType: String, radius: Int) {
        yelpService.getNearbyPlaces("Bearer " + "GhiuG1l7ZBwawE8MrW-pJe_ll-kiDRJoyqHdNhRo94RibqyuQBF6FAfj-e96qUp8tK3tuy4s7cGdxjUm8ZwCZTS9_loIhw4SXOqG8mL4WSsHuTLsyNJED7JEzbeqWnYx",
                placeType,
                latitude,
                longitude,
                radius)
                .enqueue(this)
    }

    override fun onResponse(call: Call<YelpPlaces>, response: Response<YelpPlaces>) {
        if (response.isSuccessful) {
            Timber.d("search finished")
            val yelpPlaces = response.body()
            val businesses: List<Business> = yelpPlaces.businesses
            val placeList = ArrayList<Place>()
            for (i in 0 until businesses.size) {
                val business = businesses[i]
                if(business?.coordinates.latitude!=null) {
                    val place = Place(business.name,
                            business.location.displayAddress.joinToString(),
                            LatLng(business?.coordinates.latitude, business?.coordinates.longitude),
                            business?.imageUrl,
                            business?.rating,
                            business?.reviewCount,
                            null,
                            null,
                            Place.PlaceExtraData("",
                                    !business.isClosed,
                                    business.phone,
                                    if (business.categories.size > 0) business.categories.get(0).title else "",
                                    PlaceSocial(business.url, null, null)),
                            YELP)


                    placeList.add(place)
                }
            }
            placesSearchCallback.onSearchSuccess(placeList)
        } else {
            Timber.d("search error")
            placesSearchCallback.onSearchFailed()
        }
    }

    override fun onFailure(call: Call<YelpPlaces>, t: Throwable) {
        Timber.d("search failed: " + t.cause)
        placesSearchCallback.onSearchFailed()
    }

    companion object {
        private const val MAX_RADIUS = 40000
    }
}
