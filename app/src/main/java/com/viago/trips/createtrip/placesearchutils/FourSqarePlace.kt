package com.viago.trips.createtrip.placesearchutils

import com.google.android.gms.maps.model.LatLng
import com.viago.trips.annotations.FOURSQUARE
import com.viago.trips.createtrip.PlacesSearchCallback
import com.viago.trips.createtrip.placesearchservices.FoursquareService
import com.viago.trips.interfaces.PlaceInfo
import com.viago.trips.models.Place
import com.viago.trips.models.PlaceSocial
import com.viago.trips.models.foursquare.FoursquarePlaces
import com.viago.trips.models.foursquare.Item__
import com.viago.trips.models.foursquare.Venue
import com.viago.utils.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

/**
 * Created by thinksysuser on 5/9/18.
 */



internal class FourSqarePlace : Callback<FoursquarePlaces>, PlaceUtil {


    private lateinit var placesSearchCallback: PlaceInfo
    val foursquareService by lazy { ApiUtils.retrofitInstance.create(FoursquareService::class.java) }

    override fun pleaceDetail(placeId: String,placesSearchCallback: PlaceInfo) {
        this.placesSearchCallback = placesSearchCallback
        val foursquarePlacesCall = foursquareService.getPlaceInfo(placeId)
        foursquarePlacesCall.enqueue(this)
    }

    override fun findNearbyPlaces(latitude: Double, longitude: Double, placeType: String, placesSearchCallback: PlacesSearchCallback, radius: Int) {
        //  this.placesSearchCallback = placesSearchCallback

        //  val foursquarePlacesCall = foursquareService.getNearbyPlaces(latitude.toString() + "," + longitude, URLEncoder.encode(placeType.replace("_", " "), "UTF-8"), radius)
        // foursquarePlacesCall.enqueue(this)
    }

    override fun getMaxRadius(): Int = MAX_RADIUS

    override fun onResponse(call: Call<FoursquarePlaces>, response: Response<FoursquarePlaces>) {
        if (response.isSuccessful) {
            Timber.d("search finished")
            val venue = response.body().response.venue
            val photo = getImage(venue)
            val place = Place(venue.name,
                    venue.location.formattedAddress.joinToString(),
                    LatLng(venue.location.lat, venue.location.lng),
                    photo?.let { it.prefix + it.width * minOf(it.height, 200) / it.height + "x" + minOf(it.height, 200) + it.suffix },
                    (venue.rating ?: -2F) / 2,
                    venue.ratingSignals,
                    null,
                    venue.id,
                    Place.PlaceExtraData(if (venue.hours == null) null else venue.hours.status,
                            if (venue.hours == null) false else venue.hours.isOpen,
                            venue.contact.phone,
                            if (venue.categories.size > 0) venue.categories[0].name else null,
                            PlaceSocial(venue.url, null, venue.contact.twitter))
                    ,
                    FOURSQUARE)

            Timber.d("search result",place)
            placesSearchCallback.onSearchSuccess(place)
        } else {
            Timber.d("search error")
            placesSearchCallback.onSearchFailed()
        }
    }

    override fun onFailure(call: Call<FoursquarePlaces>, t: Throwable) {
        Timber.d("search failed")
        placesSearchCallback.onSearchFailed()
    }

    companion object {
        private const val MAX_RADIUS = 50000
    }

    fun getImage(venue: Venue): Item__? {

        if(venue.photos.groups!=null && venue.photos.groups.size>0)
        {

            try{
                return venue.photos.groups[0]?.items?.firstOrNull()!!

            }catch (e:Exception){

                return null

            }

        }

        return null

    }
}