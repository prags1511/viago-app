package com.viago.trips.createtrip

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.trips.Location

/**
 * Created by a_man on 27-10-2017.
 */

class SourceDestSuggestionAdapter(val onOriginClicked : (location: Location) -> Unit, val onDestinationClicked : (location: Location) -> Unit) : EasyRecyclerViewAdapter<Location>() {

    private var _startLocation:Location?=null;
    private var _endLocation:Location?=null;

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            LocationsViewHolder(parent.inflate(R.layout.item_trip_location_suggestion))


    override fun onBindItemView(holder: RecyclerView.ViewHolder, location: Location, position: Int) {
        (holder as? LocationsViewHolder)?.bind(location,position)
    }

    private inner class LocationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textno by bindView<TextView>(R.id.textno)
        private val placeName by bindView<TextView>(R.id.place_name)
        private val address by bindView<TextView>(R.id.address)
        private val index by bindView<TextView>(R.id.index)
        private val origin by bindView<TextView>(R.id.origin);
        private val destination by bindView<TextView>(R.id.destination);
        private val mainlayout by bindView<View>(R.id.mainlayout);

        init {
            origin.setOnClickListener{ onOriginClicked(getItem(adapterPosition)) }
            destination.setOnClickListener { onDestinationClicked(getItem(adapterPosition)) }
        }

        fun bind(location: Location,position: Int) {
           // image.loadImage(location.image)
            textno.text = ""+(position+1)+")"
            placeName.text = location.title
            address.text = location.address

            origin.setBackgroundColor(Color.parseColor("#B6B6B6"))
            destination.setBackgroundColor(Color.parseColor("#00000000"))
            destination.setTextColor(Color.parseColor("#757575"))



            if(_startLocation!=null && location.title == _startLocation!!.title){

                    origin.setBackgroundColor(Color.parseColor("#2c7d98"))

                }

            if(_endLocation!=null && location.title == _endLocation!!.title)
                {

                    destination.setBackgroundColor(Color.parseColor("#2c7d98"))
                    destination.setTextColor(Color.parseColor("#ffffff"))

                }


               // mainlayout.setBackgroundColor(Color.parseColor("#D5D5D5"))



            else {
                mainlayout.setBackgroundColor(Color.parseColor("#F7F7F7"))
            }
//797777
            //EDEDED
           // index.text = (position+1).toString()
        }
    }

    public fun setSelected( startlocation:Location?, endlocation:Location?){

        _startLocation = startlocation

        _endLocation = endlocation

    }
}
