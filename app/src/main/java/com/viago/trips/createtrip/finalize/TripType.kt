package com.viago.trips.createtrip.finalize

/**
 * Created by mayank on 21/8/17.
 */
data class TripType(
        val type: String,
        var isSelected: Boolean
)