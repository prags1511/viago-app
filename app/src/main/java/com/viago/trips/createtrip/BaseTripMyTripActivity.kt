package com.viago.trips.createtrip

import android.os.Bundle
import androidx.annotation.ColorRes
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayout
import androidx.fragment.app.Fragment
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.widget.Toolbar
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import bindView
import biz.laenger.android.vpbs.BottomSheetUtils
import biz.laenger.android.vpbs.ViewPagerBottomSheetBehavior
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.viago.R
import com.viago.baseclasses.BaseActivity
import com.viago.baseclasses.UniversalStatePagerAdapter
import com.viago.extensions.dpToPx
import com.viago.extensions.hide
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.interfaces.TripInfo
import com.viago.utils.Scrollable

/**
 * Created by mayank on 23/8/17.
 */
abstract class BaseTripMyTripActivity : BaseActivity(), OnMapReadyCallback, TripInfo, TripActionType {

    protected val universalStatePagerAdapter by lazy { getPagerAdapter() }
    protected val viewPager by bindView<ViewPager>(R.id.view_pager)
    protected val tabLayout by bindView<TabLayout>(R.id.tab_layout)
    protected val bottomSheet by bindView<LinearLayout>(R.id.bottom_sheet)
    protected val bottomSheetBehavior: ViewPagerBottomSheetBehavior<LinearLayout> by lazy { ViewPagerBottomSheetBehavior.from(bottomSheet) }

    protected val createTrip by bindView<TextView>(R.id.create_trip)
    protected val createTripLayout by bindView<View>(R.id.create_trip_layout)
    protected var googleMap: GoogleMap? = null
    protected val toolbar by bindView<Toolbar>(R.id.toolbar)

    override lateinit var latLng: LatLng
    override lateinit var placeName: String
    override lateinit var keyword: String
    var isCurrentLocation: Boolean = false

    protected val hueAccent by lazy { getHue(R.color.colorAccent) }
    protected val currentLocationColor by lazy { getHue(R.color.md_yellow_800) }

    protected val huePrimaryDark by lazy { getHue(R.color.pin_skyblue) }

    private fun getHue(@ColorRes colorRes: Int): Float {
        val hueColor = FloatArray(3)
        ColorUtils.colorToHSL(ContextCompat.getColor(this, colorRes), hueColor)
        return hueColor[0]
    }

    protected abstract fun getPagerAdapter(): UniversalStatePagerAdapter<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_trip_and_trip_details)

        placeName = intent.getStringExtra(PLACE_NAME)
        keyword = intent.getStringExtra(PLACE_TYPE)
        latLng = intent.getParcelableExtra(LAT_LNG)
        isCurrentLocation =  intent.getBooleanExtra(IS_CURRENT_LOCATION,false)

        with(toolbar) {
            setSupportActionBar(toolbar)
            setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
            title = placeName
            setNavigationOnClickListener { onBackPressed() }
        }


        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        viewPager.run {
            adapter = universalStatePagerAdapter
            offscreenPageLimit = universalStatePagerAdapter.count
            tabLayout.setupWithViewPager(this)
            BottomSheetUtils.setupViewPager(this)
        }


        tabLayout.post {
            bottomSheetBehavior.peekHeight = 3 * tabLayout.height
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        }

        createTripLayout.hide()

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        moveMapToSelectedLocation()
        setMapMarkers()

        googleMap.setOnMapClickListener {

            (universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.setBackGroundItem(-1)

        }

        googleMap.setOnMarkerClickListener { marker ->


            val status : Boolean

            if(marker.tag as Int !=-1){

                status = (universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.scrollTo(marker.tag as Int) ?: false

                (universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.setBackGroundItem(marker.tag as Int)

            }else {

                status = true;

                (universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.setBackGroundItem(-1)

            }

            marker.showInfoWindow()

            //(universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.scrollTo(marker.tag as Int) ?: false

            status
        }

        val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        this, R.raw.map))
        if (!success)
        {
            // Log.e(TAG, "Style parsing failed.")
        }
    }

    private fun moveMapToSelectedLocation() {
        googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latLng.latitude - .2, latLng.longitude), ZOOM_LEVEL))
        googleMap?.moveCamera(CameraUpdateFactory.scrollBy(0F, dpToPx(40).toFloat()))
    }

    abstract fun setMapMarkers()

    override fun onSearchValuesChanged() {
        toolbar.title = placeName
        moveMapToSelectedLocation()
    }

    protected companion object {
        const val PLACE_NAME = "place name"
        const val PLACE_TYPE = "place type"
        const val LAT_LNG = "lat long"
        const val IS_CURRENT_LOCATION = "currentlocation"

        const val ZOOM_LEVEL = 8.5F
    }


}