package com.viago.trips.createtrip

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.hide
import com.viago.extensions.loadImage
import com.viago.extensions.show
import com.viago.trips.models.Place
import org.greenrobot.eventbus.EventBus
import android.content.Intent
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import android.widget.*
import android.animation.AnimatorListenerAdapter
import android.opengl.ETC1.getHeight
import androidx.core.view.ViewCompat.animate
import android.R.attr.translationY
import android.animation.Animator
import android.graphics.Color
import androidx.cardview.widget.CardView
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import com.viago.trips.annotations.FOURSQUARE
import com.viago.trips.createtrip.placesearchutils.FourSqarePlace
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.interfaces.PlaceInfo
import io.branch.referral.InstallListener.setListener


/**
 * Created by mayank on 5/7/17.
 */

class PlacesAdapter(val _locationClickCallBack: LocationClickCallBack) : EasyRecyclerViewAdapter<Place>() {

    var _position : Int =-1

    companion object {
        var locationClickCallBack:LocationClickCallBack? = null
    }

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_place, parent, false)
        locationClickCallBack = _locationClickCallBack
        return PlaceViewHolder(itemView)
    }



    override fun onBindItemView(holder: RecyclerView.ViewHolder, place: Place, position: Int) {
        if (holder is PlaceViewHolder) holder.bind(place,_position,position)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        super.onBindViewHolder(holder!!, position!!)
        if (holder is PlaceViewHolder) holder.update(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return super.getItemViewType(position)
    }


   inner class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image: ImageView by bindView(R.id.image)
        private val placeName: TextView by bindView(R.id.place_name)
        private val address: TextView by bindView(R.id.address)
        private val rating: TextView by bindView(R.id.rating)
        private val ratingBar: RatingBar by bindView(R.id.rating_bar)
        private val ratingCount: TextView by bindView(R.id.rating_count)
        private val add: Button by bindView(R.id.add)
        private val toggleDetail: ImageView by bindView(R.id.toggleDetail)
        private val addbottom: Button by bindView(R.id.addbottom)

        private var detailToggle = false
        private val detailContainer: RelativeLayout by bindView(R.id.detailContainer)
        private val mainlayout: RelativeLayout by bindView(R.id.mainlayout)
        private val mainlayouttop: CardView by bindView(R.id.mainlayouttop)
        private val hoursHeading: TextView by bindView(R.id.hoursHeading)
        private val hoursDetail: TextView by bindView(R.id.hoursDetail)
        private val contactHeading: TextView by bindView(R.id.phoneHeading)
        private val contactDetail: TextView by bindView(R.id.phoneDetail)
        private val priceGroupCategoryHeading: TextView by bindView(R.id.priceGroupCategoryHeading)
        private val priceGroupCategory: TextView by bindView(R.id.priceGroupCategory)
        private val website: TextView by bindView(R.id.website)
        private val facebook: TextView by bindView(R.id.facebook)
        private val twitter: TextView by bindView(R.id.twitter)

       private fun setMargins(view: View, left: Int, top: Int, right: Int, bottom: Int) {
           if (view.layoutParams is ViewGroup.MarginLayoutParams) {
               val p = view.layoutParams as ViewGroup.MarginLayoutParams
               p.setMargins(left, top, right, bottom)
               view.requestLayout()
           }
       }

        internal fun bind(place: Place,selectedPosition: Int,position: Int) {

            if(place.isNeedToHide){

                Log.e(" need ",""+position)
              //  setMargins(mainlayouttop,0,0,0,0)

                mainlayout.visibility = View.GONE

            }else {

                Log.e("No need ",""+position)


              //  setMargins(mainlayouttop,0,0,0,14)
                mainlayout.visibility = View.VISIBLE

            }


            if(selectedPosition == position){

                mainlayout.setBackgroundColor(Color.parseColor("#e9e9e9"))


            }else {

                mainlayout.setBackgroundColor(Color.WHITE)



            }

            mainlayout.setOnClickListener {

                locationClickCallBack?.clickLocation(position)

                mainlayout.setBackgroundColor(Color.parseColor("#e9e9e9"))

                setBackgroundItem(position)



            }

           // if(place.provider != FOURSQUARE) {
                image.loadImage(place.imageUrl)
           // }
            placeName.text = place.name
            address.text = if (TextUtils.isEmpty(place.address)) "" else place.address
            rating.text = place.rating.toString()
            ratingBar.rating = place.rating
            val noOfRatings: Long = place.noOfRatings ?: 0
            if (noOfRatings > 0) {
                ratingCount.text = "($noOfRatings)"
            } else {
                ratingCount.text = ""
            }

            if (place.rating < 0) {
                ratingBar.hide()
                rating.hide()
                ratingCount.text = "No rating found"
            } else {
                ratingBar.show()
                rating.show()
            }


            setAddButton(place)

            add.setOnClickListener {
                place.isAdded = !place.isAdded
                setAddButton(place)
                EventBus.getDefault().post(place)
            }

            addbottom.setOnClickListener {
                place.isAdded = !place.isAdded
                setAddButton(place)
                EventBus.getDefault().post(place)
            }

            toggleDetail.setOnClickListener {
                if (detailToggle) {
                    detailContainer.visibility = View.GONE
                    detailToggle = false
                    toggleDetail.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.ic_arrow_down_24dp))
                    add.visibility = View.VISIBLE
                } else {
                    if(place.provider == FOURSQUARE) {
                        val placeSearchCallback by lazy {
                            object : PlaceInfo {
                                override fun onSearchFailed() {}

                                override fun onSearchSuccess(place: Place) {

                                    image.loadImage(place.imageUrl)

                                    if (place != null && place.extraData != null && place.extraData!!.hours == null) {
                                        hoursDetail.visibility = View.GONE
                                        hoursHeading.visibility = View.GONE
                                    } else {
                                        hoursHeading.visibility = View.VISIBLE
                                        hoursDetail.visibility = View.VISIBLE
                                        if (place != null && place.extraData != null && place.extraData.open != null && place.extraData.hours!!.length == 0) {
                                            hoursDetail.text = if (place.extraData!!.open) "Open now" else "Closed now"
                                        } else if (place.extraData != null && place.extraData.hours != null) {
                                            hoursDetail.text = place.extraData!!.hours
                                        }
                                    }

                                    if (place.extraData != null && place.extraData.contact != null && !TextUtils.isEmpty(place.extraData!!.contact)) {
                                        contactDetail.visibility = View.VISIBLE
                                        contactHeading.visibility = View.VISIBLE
                                        contactDetail.text = place.extraData!!.contact
                                    } else {
                                        contactDetail.visibility = View.GONE
                                        contactHeading.visibility = View.GONE
                                    }

                                    if (place.extraData != null && place.extraData.category != null && !TextUtils.isEmpty(place.extraData!!.category)) {
                                        priceGroupCategory.visibility = View.VISIBLE
                                        priceGroupCategoryHeading.visibility = View.VISIBLE
                                        if (place.extraData!!.category.equals("point_of_interest")) {

                                            priceGroupCategory.text = "tourist place"


                                        } else {

                                            priceGroupCategory.text = place.extraData!!.category


                                        }
                                    } else {
                                        priceGroupCategory.visibility = View.GONE
                                        priceGroupCategoryHeading.visibility = View.GONE
                                    }

                                    website.visibility = if (place.extraData != null && place.extraData.socialLinks != null
                                            && place.extraData.socialLinks?.website != null) View.VISIBLE else View.GONE
                                    facebook.visibility = if (place.extraData != null && place.extraData.socialLinks != null &&
                                            place.extraData.socialLinks?.facebook != null) View.VISIBLE else View.GONE
                                    twitter.visibility = if (place.extraData != null && place.extraData.socialLinks != null &&
                                            place.extraData.socialLinks?.twitter != null) View.VISIBLE else View.GONE

                                    contactDetail.setOnClickListener {
                                        if (place.extraData != null && place.extraData.contact != null) {
                                            itemView.context.startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", place.extraData.contact, null)))
                                        }
                                    }
                                    website.setOnClickListener {
                                        if (place.extraData != null && place.extraData.socialLinks != null && place.extraData.socialLinks?.website != null) {
                                            var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.website);
                                            itemView.context.startActivity(intent);
                                            //    itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.website)))
                                        } else {
                                            Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                    facebook.setOnClickListener {
                                        if (place.extraData != null && place.extraData.socialLinks?.facebook != null) {
                                            var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.facebook);
                                            itemView.context.startActivity(intent);
                                            // itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.facebook)))
                                        } else {
                                            Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                    twitter.setOnClickListener {
                                        if (place.extraData != null && place.extraData.socialLinks?.twitter != null) {
                                            var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.twitter);
                                            itemView.context.startActivity(intent);
                                            //itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.twitter)))
                                        } else {
                                            Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                                        }
                                    }

                                }

                            }
                        }
                       // FourSqarePlace().pleaceDetail("4fbfa89ee4b0fddeca5b5cef", placeSearchCallback)
                        FourSqarePlace().pleaceDetail(place.placeId!!, placeSearchCallback)
                    }
                    add.visibility = View.GONE
                    detailContainer.visibility = View.VISIBLE
                    detailToggle = true
                    toggleDetail.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.ic_arrow_up_24dp))
                }
            }

            if(place.provider != FOURSQUARE) {

                if (place != null && place.extraData != null && place.extraData!!.hours == null) {
                    hoursDetail.visibility = View.GONE
                    hoursHeading.visibility = View.GONE
                } else {
                    hoursHeading.visibility = View.VISIBLE
                    hoursDetail.visibility = View.VISIBLE
                    if (place != null && place.extraData != null && place.extraData.open != null && place.extraData.hours!!.length == 0) {
                        hoursDetail.text = if (place.extraData!!.open) "Open now" else "Closed now"
                    } else if (place.extraData != null && place.extraData.hours != null) {
                        hoursDetail.text = place.extraData!!.hours
                    }
                }

                if (place.extraData != null && place.extraData.contact != null && !TextUtils.isEmpty(place.extraData!!.contact)) {
                    contactDetail.visibility = View.VISIBLE
                    contactHeading.visibility = View.VISIBLE
                    contactDetail.text = place.extraData!!.contact
                } else {
                    contactDetail.visibility = View.GONE
                    contactHeading.visibility = View.GONE
                }

                if (place.extraData != null && place.extraData.category != null && !TextUtils.isEmpty(place.extraData!!.category)) {
                    priceGroupCategory.visibility = View.VISIBLE
                    priceGroupCategoryHeading.visibility = View.VISIBLE
                    if (place.extraData!!.category.equals("point_of_interest")) {

                        priceGroupCategory.text = "tourist place"


                    } else {

                        priceGroupCategory.text = place.extraData!!.category


                    }
                } else {
                    priceGroupCategory.visibility = View.GONE
                    priceGroupCategoryHeading.visibility = View.GONE
                }

                website.visibility = if (place.extraData != null && place.extraData.socialLinks != null
                        && place.extraData.socialLinks?.website != null) View.VISIBLE else View.GONE
                facebook.visibility = if (place.extraData != null && place.extraData.socialLinks != null &&
                        place.extraData.socialLinks?.facebook != null) View.VISIBLE else View.GONE
                twitter.visibility = if (place.extraData != null && place.extraData.socialLinks != null &&
                        place.extraData.socialLinks?.twitter != null) View.VISIBLE else View.GONE

                contactDetail.setOnClickListener {
                    if (place.extraData != null && place.extraData.contact != null) {
                        itemView.context.startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", place.extraData.contact, null)))
                    }
                }
                website.setOnClickListener {
                    if (place.extraData != null && place.extraData.socialLinks != null && place.extraData.socialLinks?.website != null) {
                        var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.website);
                        itemView.context.startActivity(intent);
                        //    itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.website)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }
                facebook.setOnClickListener {
                    if (place.extraData != null && place.extraData.socialLinks?.facebook != null) {
                        var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.facebook);
                        itemView.context.startActivity(intent);
                        // itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.facebook)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }
                twitter.setOnClickListener {
                    if (place.extraData != null && place.extraData.socialLinks?.twitter != null) {
                        var intent = WebsiteActivity.newIntent(itemView.context, place.extraData.socialLinks.twitter);
                        itemView.context.startActivity(intent);
                        //itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(place.extraData.socialLinks.twitter)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }
            }

        }

        fun update(place: Place) {
            setAddButton(place)
        }




        private fun setAddButton(place: Place) {

            if(place.isAlreadyAdded) {

                add.text = "Already Added"
                add.isEnabled = false
                add.setBackgroundResource(R.drawable.bg_accent_rounded_corner_4dp)
                addbottom.text = "Already Added"
                addbottom.isEnabled = false
                add.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9.89f);
                addbottom.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12.0f);
                addbottom.setBackgroundResource(R.drawable.bg_accent_rounded_corner_4dp)
              /*  add.setLayoutParams(RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT))
                addbottom.setLayoutParams(RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT))*/
              //

            } else if (place.isAdded) {
                add.text = "Added"
                add.isEnabled = true
                add.setBackgroundResource(R.drawable.bg_accent_rounded_corner_4dp)
                add.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10.5f);
                addbottom.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12.0f);
                addbottom.text = "Added"
                addbottom.isEnabled = true
                addbottom.setBackgroundResource(R.drawable.bg_accent_rounded_corner_4dp)
            } else {
                add.isEnabled = true
                add.text = "+ Add"
                add.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10.5f);
                addbottom.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12.0f);
                add.setBackgroundResource(R.drawable.bg_primary_rounded_corner_4dp)
                addbottom.isEnabled = true
                addbottom.text = "+ Add"
                addbottom.setBackgroundResource(R.drawable.bg_primary_rounded_corner_4dp)
            }

        }
    }

    public fun setBackgroundItem(position: Int) {

        _position = position

        notifyDataSetChanged()

    }
}
