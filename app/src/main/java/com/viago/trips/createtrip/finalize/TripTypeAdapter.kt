package com.viago.trips.createtrip.finalize

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.trips.Category

/**
 * Created by mayank on 20/8/17.
 */
class TripTypeAdapter(context: Context) : EasyRecyclerViewAdapter<Category>() {

    val colorWhite = ContextCompat.getColor(context, android.R.color.white)
    val colorPrimaryText = ContextCompat.getColor(context, R.color.primary_text)

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TripTypeViewHolder(parent.inflate(R.layout.item_trip_type))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, category: Category, position: Int) {
        if (holder is TripTypeViewHolder) holder.bind(category)
    }

    internal fun getSelectedCategoriesId(): List<Int> =
            itemsList.filter { it.isSelected }.map { it.id }

    inner class TripTypeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        private val type by bindView<LinearLayout>(R.id.type)
        private val title by bindView<TextView>(R.id.title)
        private val icon by bindView<ImageView>(R.id.icon)



        init {
            type.setOnClickListener {

//                itemsList?.forEachIndexed {index, category ->
//
//                    if(index!=adapterPosition){
//
//                        getItem(index).run {
//                            isSelected = false
//                            bind(category)
//                        }
//
//                    }
//
//                }
                getItem(adapterPosition).run {
                    isSelected = !isSelected
                    bind(this)
                }
            }



        }

        fun bind(category: Category) {
            title.text = category.title

            icon.loadImage(category.image)

            if (category.isSelected) {
                type.setBackgroundResource(R.drawable.bg_white_rounded_corner_24dp)
                title.setTextColor(colorPrimaryText)
                icon.setColorFilter(colorPrimaryText)
            } else {
                type.setBackgroundResource(R.drawable.bg_accent_rounded_corner_24dp)
                title.setTextColor(colorWhite)
                icon.setColorFilter(colorWhite)

            }
        }
    }
}