package com.viago.trips.createtrip;

import com.viago.trips.models.Place;

import java.util.List;

/**
 * Created by mayank on 4/7/17.
 */

public interface PlacesSearchCallback {

    void onSearchFailed();
    void onSearchSuccess(List<Place> places);
}
