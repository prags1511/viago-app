package com.viago.trips.createtrip

import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Address
import android.os.Bundle
import android.os.Handler
import androidx.annotation.ColorRes
import androidx.annotation.LayoutRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import android.view.View
import bindView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.viago.R
import com.viago.R.attr.colorAccent
import com.viago.R.color.colorPrimaryDark
import com.viago.baseclasses.BaseLocationFragment
import com.viago.extensions.openFragment
import com.viago.trips.Location
import com.viago.trips.createtrip.search.SearchQueryFragment
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.marker.IconGenerator
import org.greenrobot.eventbus.EventBus
import com.viago.R.color.colorPrimary
import com.arsy.maps_library.MapRipple
import com.google.android.gms.maps.model.*
import com.viago.trips.createtrip.placesearchutils.GoogleNearbyPlacesUtil
import com.google.android.gms.maps.model.LatLng




class CreateTripFragment : BaseLocationFragment(), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    lateinit var   mapRipple: MapRipple
    private val searchLocation by bindView<View>(R.id.search_location)
    protected val hueAccent by lazy { getHue(R.color.colorAccent) }
    lateinit var mCircle: Circle;

    @LayoutRes
    override fun getLayoutRes(): Int = R.layout.fragment_create_trip

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
       // GoogleNearbyPlacesUtil.pageToken=""
        requestLatLong()

        searchLocation.setOnClickListener {

            GoogleNearbyPlacesUtil.pageToken=""
            SharedPrefsUtils.removePreference(Constants.KEY_TEMP_LOCATIONS)

            getAppCompatActivity().openFragment(SearchQueryFragment.newInstance(null, object : SearchQueryFragment.Companion.PlaceSearchListener {
                override fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String,isCurrentLocation:Boolean) {
                    //activity.onBackPressed()
                   //

                    startActivity(CreateTripActivity.getIntent(
                            context!!,
                            destination,
                            latLng,
                            keyword,isCurrentLocation)
                    )
                }
            },true))
        }
    }

    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.run {
            uiSettings.setAllGesturesEnabled(false)
        }
        this.googleMap = googleMap
        val success = googleMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(
                        getAppCompatActivity(), R.raw.map))
        if (!success)
        {
            // Log.e(TAG, "Style parsing failed.")
        }
    }

    @SuppressLint("MissingPermission")
    override fun onLocationReceived(latLng: LatLng, address: Address?) {
        googleMap?.run {
            isMyLocationEnabled = false
            uiSettings.isMyLocationButtonEnabled = false
            moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12F))
            setMyLocationEnabled(true);
           // drawMarkerWithCircle(latLng)

//         addCircle( CircleOptions()
//                    .center(latLng)
//                    .radius(208.0)
//                    .strokeColor(Color.parseColor("#F9666C"))
//                    .fillColor(Color.parseColor("#F9666C")));



            //  plotMarkers(latLng)

//            val accuracyCircleOptions = CircleOptions()
//                    .center(LatLng(latLng.latitude,  latLng.longitude))
//
//                    .fillColor(colorPrimary)
//                    .strokeColor(colorPrimaryDark)
//                    .strokeWidth(2.0f)
//                    .radius(50.0)
//
//           addCircle(accuracyCircleOptions)


          /*  val markerOptions = MarkerOptions().position(LatLng(latLng.latitude, latLng.longitude))
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
            addMarker(markerOptions)*/



        }

//        Handler().postDelayed({
//
//            if(latLng!=null) {
//
//                rippleEffect(latLng)
//            }
//
//        }, 1000)




    }


    private fun updateMarkerWithCircle(position: LatLng) {
        mCircle.setCenter(position)

    }

    private fun drawMarkerWithCircle(position: LatLng) {
        val radiusInMeters = 100.0
        val strokeColor = -0x10000 //red outline
        val shadeColor = 0x44ff0000 //opaque red fill

        val circleOptions = CircleOptions().center(position).radius(radiusInMeters).strokeWidth(12f)
        mCircle = googleMap!!.addCircle(circleOptions)

    }


    private fun plotMarkers(latLng: LatLng) {

       /* val iconGenerator = IconGenerator(context)

            val markerText: String
            val markerColor: Int

            markerColor = colorPrimaryDark
            markerText = "E"

            iconGenerator.setColor(markerColor)
            val icon = iconGenerator.makeIcon(markerText)



            val markerOptions = MarkerOptions().position(LatLng(latLng.latitude, latLng.longitude)).title("hi")
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
            this.googleMap?.addMarker(markerOptions)*/


      /*  val accuracyCircleOptions = CircleOptions()
                .center(LatLng(latLng.latitude,  latLng.longitude))

                .fillColor(colorPrimary)
                .strokeColor(colorPrimaryDark)
                .strokeWidth(2.0f)
                .radius(50.0)

        this.googleMap?.addCircle(accuracyCircleOptions)
*/



    }

    private fun getHue(@ColorRes colorRes: Int): Float {
        val hueColor = FloatArray(3)
        ColorUtils.colorToHSL(ContextCompat.getColor(getAppCompatActivity(), colorRes), hueColor)
        return hueColor[0]
    }

   /* override fun onResume() {
        super.onResume()

        if (!mapRipple.isAnimationRunning()) {
            mapRipple.startRippleMapAnimation();
        }
    }
*/
    override fun onStop() {
        super.onStop()

      /*  if (mapRipple.isAnimationRunning()) {
            mapRipple.stopRippleMapAnimation();
        }*/
    }



    fun rippleEffect(latLng: LatLng){

        mapRipple = MapRipple( googleMap, latLng, context)

        mapRipple.withNumberOfRipples(2)
        mapRipple.withFillColor(Color.parseColor("#F9666C"))
        mapRipple.withStrokeColor(Color.BLACK)
        mapRipple.withStrokewidth(10)
        mapRipple.withDistance(1500.0)
        mapRipple.withRippleDuration(3000)
        mapRipple.withTransparency(0.74f);
        mapRipple.withDurationBetweenTwoRipples(400)

        if (!mapRipple.isAnimationRunning()) {
            mapRipple.startRippleMapAnimation();
        }


    }
}