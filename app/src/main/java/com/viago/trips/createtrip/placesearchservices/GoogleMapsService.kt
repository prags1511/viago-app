package com.viago.trips.createtrip.placesearchservices

import com.viago.maputils.models.GoogleMapsRoute
import com.viago.trips.models.googlemaps.GoogleMapsPlace
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by mayank on 4/7/17.
 */

interface GoogleMapsService {

    @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyCFUfb7H85Mkea-o0doF8uCun0EoWmGTKw&language=en")
    fun getNearbyPlaces(@Query("location") commaSeparatedLatLong: String, @Query("keyword") keyword: String,@Query("pagetoken") pagetoken: String,@Query("rankby") rankby: String): Call<GoogleMapsPlace>

    @GET("https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyCFUfb7H85Mkea-o0doF8uCun0EoWmGTKw")
    fun getRoute(
            @Query("origin") commaSeparatedSourceLatLong: String,
            @Query("destination") commaSeparatedDestLatLong: String
    ): Call<GoogleMapsRoute>

    @GET("https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyCFUfb7H85Mkea-o0doF8uCun0EoWmGTKw")
    fun getRouteWithWaypoints(
            @Query("origin") commaSeparatedSourceLatLong: String,
            @Query("destination") commaSeparatedDestLatLong: String,
            @Query("waypoints") waypoints: String
    ): Call<GoogleMapsRoute>
}
