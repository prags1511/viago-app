package com.viago.trips.createtrip.placesearchservices

import com.viago.trips.models.foursquare.FoursquarePlaces

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by mayank on 4/7/17.
 */

interface FoursquareService {

    @GET("https://api.foursquare.com/v2/venues/explore?client_id=X15M4CJXIQ3O22UWLYJEILEMUHYGEETWJ4GT1S4K5SIGK4X0&client_secret=0BB4ENBI2Z3UEZTJQHBOCAZQAQLMCJMRYMEVU4BMEIXWLSMI&v=20170704&limit=60&venuePhotos=1")
    fun getNearbyPlaces(@Query("ll") commaSeparatedLatLong: String, @Query("query") category: String, @Query("radius") radius: Int): Call<FoursquarePlaces>

    @GET("https://api.foursquare.com/v2/venues/{VENUE_ID}?client_id=X15M4CJXIQ3O22UWLYJEILEMUHYGEETWJ4GT1S4K5SIGK4X0&client_secret=0BB4ENBI2Z3UEZTJQHBOCAZQAQLMCJMRYMEVU4BMEIXWLSMI&v=20180905")
    fun getPlaceInfo( @Path("VENUE_ID") placeId: String): Call<FoursquarePlaces>
}
