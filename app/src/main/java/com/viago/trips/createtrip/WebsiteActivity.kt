package com.viago.trips.createtrip

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.webkit.WebView
import android.webkit.WebViewClient
import bindView
import com.viago.R
import android.webkit.WebChromeClient
import android.widget.ProgressBar
import com.viago.R.color.colorPrimaryDark
import com.viago.extensions.hide
import com.viago.extensions.show


open class WebsiteActivity : AppCompatActivity() {

        protected val webView by bindView<WebView>(R.id.websiteurl)

        protected val toolbar by bindView<Toolbar>(R.id.toolbar)


        protected val progressBar by bindView<ProgressBar>(R.id.progressBar)



        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_website)

            with(toolbar) {
                setSupportActionBar(toolbar)
                setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
                title = "Viago"
                setNavigationOnClickListener { onBackPressed() }
            }

            progressBar.getProgressDrawable().setColorFilter(
                   Color.parseColor("#23647a") , android.graphics.PorterDuff.Mode.SRC_IN);


            val webUrl = intent.getStringExtra(WEB_URL)

            webView.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                    view?.loadUrl(url);
                    return true
                }
            } )


            webView.setWebChromeClient(object : WebChromeClient() {
                override fun onProgressChanged(view: WebView, progress: Int) {
                    progressBar.setProgress(progress)
                    if (progress == 100) {
                        progressBar.hide()

                    } else {
                        progressBar.show()

                    }
                }
            })

            webView.clearCache(true);
            webView.clearHistory();
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webView.loadUrl(webUrl)


        }

        companion object {

  private val WEB_URL = "weburl"

        fun newIntent(context: Context, url: String?): Intent {
        val intent = Intent(context, WebsiteActivity::class.java)
        intent.putExtra(WEB_URL, url)
        return intent
        }
        }
        }



/*open class Website : AppCompatActivity() {

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val userId = intent.getStringExtra(INTENT_USER_ID)
        ?: throw IllegalStateException("field $INTENT_USER_ID missing in Intent")
        }

        companion object {

private val INTENT_USER_ID = "user_id"

        fun newIntent(context: Context, user: User): Intent {
        val intent = Intent(context, UserDetailActivity::class.java)
        intent.putExtra(INTENT_USER_ID, user.id)
        return intent
        }
        }
        }*/
/*
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.viago.R;

public class Website extends Activity {


    WebView browser;

    public static Intent newIntent(final Context context, final String webUrl) {
        final Intent intent = new Intent(context, Website.class);
        intent.putExtra("weburl", webUrl);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find the WebView by name in the main.xml of step 2
        browser=(WebView)findViewById(R.id.websiteurl);

        // Enable javascript
//        browser.getSettings().setJavaScriptEnabled(true);

        // Set WebView client
        browser.setWebChromeClient(new WebChromeClient());

        browser.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        // Load the webpage
        browser.loadUrl("http://google.com/");
    }


}
*/
