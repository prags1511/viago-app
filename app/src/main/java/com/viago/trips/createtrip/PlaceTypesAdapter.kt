package com.viago.trips.createtrip

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate

/**
 * Created by mayank on 26/8/17.
 */
class PlaceTypesAdapter(val onItemClick: (String) -> Unit): EasyRecyclerViewAdapter<String>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PlaceTypeViewHolder(parent.inflate(R.layout.item_place_type))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, string: String, position: Int) {
        if (holder is PlaceTypeViewHolder) holder.bind(string)
    }

    inner class PlaceTypeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val text by bindView<TextView>(R.id.text)

        init {
            itemView.setOnClickListener { onItemClick(getItem(adapterPosition)) }
        }

        fun bind(string: String) {
            text.text = string
        }
    }
}