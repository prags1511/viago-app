package com.viago.trips.createtrip.placesearchutils

import android.content.Context
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import com.viago.R
import com.viago.trips.annotations.GOOGLE
import com.viago.trips.createtrip.PlacesSearchCallback
import com.viago.trips.createtrip.placesearchservices.GoogleMapsService
import com.viago.trips.interfaces.PlaceInfo
import com.viago.trips.models.Place
import com.viago.trips.models.googlemaps.GoogleMapsPlace
import com.viago.trips.models.googlemaps.Result
import com.viago.utils.ApiUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*

/**
 * Created by mayank on 4/7/17.
 */

class GoogleNearbyPlacesUtil(private val context: Context) : PlaceUtil, Callback<GoogleMapsPlace> {
    override fun pleaceDetail(placeId: String, placesSearchCallback: PlaceInfo) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private lateinit var placesSearchCallback: PlacesSearchCallback
    val googleMapsService by lazy { ApiUtils.retrofitInstance.create(GoogleMapsService::class.java) }
    val key: String by lazy { context.getString(R.string.geo_api_key) }

    override fun findNearbyPlaces(latitude: Double, longitude: Double, keyword: String, placesSearchCallback: PlacesSearchCallback, radius: Int) {
        this.placesSearchCallback = placesSearchCallback

        val key = keyword

        val googleMapsPlacesCall = googleMapsService.getNearbyPlaces(latitude.toString() + "," + longitude, keyword.replace("_"," "), pageToken,"distance")
        googleMapsPlacesCall.enqueue(this)
    }

    override fun getMaxRadius(): Int = MAX_RADIUS

    override fun onResponse(call: Call<GoogleMapsPlace>, response: Response<GoogleMapsPlace>) {
        if (response.isSuccessful) {
            Timber.d("search finished")
            val googleMapsPlace = response.body()
            pageToken = if(googleMapsPlace.nextPageToken!=null)
                googleMapsPlace.nextPageToken
            else{
                "stop"
            }
          //  Toast.makeText(context,"Google "+GoogleNearbyPlacesUtil.pageToken, Toast.LENGTH_LONG).show()

            val results: List<Result> = googleMapsPlace.results
            val placeList = ArrayList<Place>()
            for (result in results) {
                val photo = result.photos?.get(0)
                val location = result.geometry.location
                val price = if (result.priceLevel != null) result.priceLevel.toString() else null
                val cat = if (result.types.size > 0) result.types.get(0) else null
                val place = Place(result.name,
                        result.vicinity,
                        LatLng(location.lat, location.lng),
                        photo?.let { "https://maps.googleapis.com/maps/api/place/photo?key=$key&photoreference=${photo.photoReference}&maxheight=${minOf(photo.height, 200)}" },
                        result.rating ?: -1F,
                        -1,
                        null,
                        null,
                        Place.PlaceExtraData(if (result.openingHours == null) null else "",
                                if (result.openingHours == null && result.openingHours.openNow == null ) false else result.openingHours.openNow,
                                null,
                                if (price != null) (price + " : ") else "" + if (cat != null) cat else "",
                                null)
                        ,
                        GOOGLE)
                placeList.add(place)
            }
            placesSearchCallback.onSearchSuccess(placeList)
        } else {
            Timber.d("search error")
            placesSearchCallback.onSearchFailed()
        }
    }

    override fun onFailure(call: Call<GoogleMapsPlace>, t: Throwable) {
        Timber.d("search failed")
        placesSearchCallback.onSearchFailed()
    }

    companion object {
        private const val MAX_RADIUS = 50000
        public var pageToken=""
    }
}
