package com.viago.trips.createtrip.finalize

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.baseclasses.NavItemClickEvent
import com.viago.extensions.dpToPx
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.profile.MediaDBUtil
import com.viago.trips.MyTrip
import com.viago.trips.MyTripCreatedEvent
import com.viago.trips.TripsUtil
import com.viago.trips.createtrip.CreateTripActivity
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.interfaces.TripInfo
import com.viago.trips.tripdetails.TripDetailsActivity
import com.viago.utils.SpacesItemDecoration
import org.greenrobot.eventbus.EventBus

/**
 * Created by mayank on 1/8/17.
 */
class CreateTripFinalizeFragment : BaseFragment() {

    private val share by bindView<View>(R.id.share)
    private val save by bindView<View>(R.id.save)
    private val start by bindView<View>(R.id.start)
    private val TripLocationsRecyclerView by bindView<RecyclerView>(R.id.trip_locations_recycler_view)
    private val tripTypeRecyclerView by bindView<RecyclerView>(R.id.trip_type_recycler_view)
    private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val tripName by bindView<EditText>(R.id.trip_name)
    private val travelingToText by bindView<TextView>(R.id.traveling_to_text)
    private val tripDistance by bindView<TextView>(R.id.trip_distance)
    private val tripDuration by bindView<TextView>(R.id.trip_duration)
    private val editLocation by bindView<View>(R.id.edit_location)
    private val distanceUnitSwitch by bindView<TextView>(R.id.kmmileswitch)

    private lateinit var tripInfo: TripInfo

    private val tripTypeAdapter by lazy {
        TripTypeAdapter(context!!).apply {
            TripsUtil.getCategories(userToken, { categories ->
                CreateTripFinalizeFragment.selectedCategoryIds?.let {
                    categories.forEachIndexed { index, category ->
                        if (it.contains(category.id)) {
                            category.isSelected = true
                        }
                    }
                }
                addItemsAtBottom(categories)
            }, { context!!.showToast(it) })
        }
    }

    private val userToken by lazy { UserUtil.getUserToken() }
    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(context).apply {
            setMessage("Creating Trip")
            isIndeterminate = true
            setCancelable(false)
        }
    }


    override fun getLayoutRes(): Int = R.layout.fragment_create_trip_finalize

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        travelingToText.text = "Trip Locations (${tripInfo.selectedLocations.size})"

        tripTypeRecyclerView.isNestedScrollingEnabled = false
        val arr = tripInfo.distance.split(":")
        tripDistance.text = " | " +arr[0]
        tripDuration.text = tripInfo.duration

        save.setOnClickListener { save() }
        share.setOnClickListener { saveAndShare() }
        start.setOnClickListener { saveAndStart() }

        toolbar.run {
            title = "Create Trip"
            setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
            setNavigationOnClickListener {
                CreateTripFinalizeFragment.tripName = tripName.text.toString()
                CreateTripFinalizeFragment.selectedCategoryIds = tripTypeAdapter.getSelectedCategoriesId()
                activity!!.onBackPressed()
            }
        }

        TripLocationsRecyclerView.run {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = TripLocationAdapter((object : LocationClickCallBack {
                override fun clickLocation(position: Int) {

                    (activity as CreateTripActivity).clickLocation(position)

                }
            })).apply {
                addItemOnTop(tripInfo.source)
                addItemsAtBottom(tripInfo.selectedLocations)
                addItemAtBottom(tripInfo.destination)
            }
            //addItemDecoration(SpacesItemDecoration(dpToPx(4)))
        }

        tripTypeRecyclerView.run {
            layoutManager = GridLayoutManager(context, 4)
            setHasFixedSize(true)
            adapter = tripTypeAdapter
         //   addItemDecoration(SpacesItemDecoration(dpToPx(4)))
        }

        TripLocationsRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

        editLocation.setOnClickListener {
            CreateTripFinalizeFragment.tripName = tripName.text.toString()
            CreateTripFinalizeFragment.selectedCategoryIds = tripTypeAdapter.getSelectedCategoriesId()
            activity!!.onBackPressed()
        }

        distanceUnitSwitch.setOnClickListener {
           /* val arr = tripInfo.distance.split(":")
            if (distanceUnitSwitch.text.contains("Miles")) {
                durationAndDistance.text = tripDuration + " | " + arr[1]
                distanceUnitSwitch.text = "(View in Kms)"
            } else {
                durationAndDistance.text = tripDuration + " | " + arr[0]
                distanceUnitSwitch.text = "(View in Miles)"
            }
*/

           // val arr = tripInfo.distance.split(":")
            if (distanceUnitSwitch.text.contains("Miles")) {
                tripDistance.text = " | " +arr[1]
                distanceUnitSwitch.setText("(View in Kms)");
            } else {
                tripDistance.text = " | " +arr[0]
                distanceUnitSwitch.setText("(View in Miles)");
            }
        }

        CreateTripFinalizeFragment.tripName?.let { tripName.setText(it) }
    }

    private fun saveAndShare() {
        saveTrip {
            EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_my_trips))
            EventBus.getDefault().postSticky(MyTripCreatedEvent(it, share = true))
            activity!!.finish()
        }
    }

    private fun saveAndStart() {
        saveTrip {
            EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_my_trips))
            EventBus.getDefault().postSticky(MyTripCreatedEvent(it, start = true, commaSeparatedLatLngList = tripInfo.selectedLocations.map { "${it.latitude},${it.longitude}" }))
            activity!!.finish()
        }
    }

    private fun save() {
        saveTrip {

            EventBus.getDefault().postSticky(MyTripCreatedEvent(it))
//            TripCreatedDialogFragment().show(childFragmentManager, TripCreatedDialogFragment::class.java.simpleName)

            EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_my_trips))

            activity!!.finish()

        }
    }

    private fun saveTrip(onSaved: (MyTrip) -> Unit) {
        if (isValid()) {
            progressDialog.show()
            tripInfo.categories = tripTypeAdapter.getSelectedCategoriesId()
            tripInfo.title = tripName.text.toString()
            TripsUtil
                    .createTrip(
                            userToken,
                            TripsUtil.makeCreateTripRequestObject(tripInfo), {
                        progressDialog.dismiss()
                        CreateTripFinalizeFragment.tripName = null
                        CreateTripFinalizeFragment.selectedCategoryIds = null
                        onSaved(it)
                    }, {
                        progressDialog.dismiss()
                        context!!.showToast(it)
                    })
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
        Log.e("CHECK", "inattach")
    }

    private fun isValid(): Boolean {
        if (tripName.text.isNullOrBlank()) {
            tripName.error = "Required."
            return false
        } else {
            tripName.error = null
            return true
        }
    }

    companion object {
        var tripName: String? = null
        var selectedCategoryIds: List<Int>? = null
    }
}