package com.viago.trips.createtrip.placesearchservices

import com.viago.trips.models.yelp.YelpAuth
import com.viago.trips.models.yelp.YelpPlaces
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by mayank on 4/7/17.
 */

interface YelpService {

   /* @FormUrlEncoded
    @POST("https://api.yelp.com/oauth2/token")
    fun authenticate(@Field("grant_type") grantType: String = "client_credentials",
                     @Field("client_id") clientId: String = "EJFzSYA8Oq0TSRH-cjGOoQ",
                     @Field("client_secret") clientSecret: String = "GhiuG1l7ZBwawE8MrW-pJe_ll-kiDRJoyqHdNhRo94RibqyuQBF6FAfj-e96qUp8tK3tuy4s7cGdxjUm8ZwCZTS9_loIhw4SXOqG8mL4WSsHuTLsyNJED7JEzbeqWnYx"): Call<YelpAuth>

    @GET("https://api.yelp.com/v3/businesses/search?limit=50")
    fun getNearbyPlaces(@Header("Authorization") auth: String,
                        @Query("term") category: String,
                        @Query("latitude") latitude: Double,
                        @Query("longitude") longitude: Double,
                        @Query("radius") radius: Int): Call<YelpPlaces>*/


    @FormUrlEncoded
    @POST("https://api.yelp.com/oauth2/token")
    fun authenticate(@Field("grant_type") grantType: String = "client_credentials",
                     @Field("client_id") clientId: String = "EJFzSYA8Oq0TSRH-cjGOoQ",
                     @Field("client_secret") clientSecret: String = "GhiuG1l7ZBwawE8MrW-pJe_ll-kiDRJoyqHdNhRo94RibqyuQBF6FAfj-e96qUp8tK3tuy4s7cGdxjUm8ZwCZTS9_loIhw4SXOqG8mL4WSsHuTLsyNJED7JEzbeqWnYx"): Call<YelpAuth>

    @GET("https://api.yelp.com/v3/businesses/search?limit=50")
    fun getNearbyPlaces(@Header("Authorization") auth: String,
                        @Query("term") category: String,
                        @Query("latitude") latitude: Double,
                        @Query("longitude") longitude: Double,
                        @Query("radius") radius: Int): Call<YelpPlaces>

}
