package com.viago.trips.createtrip.finalize

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.widget.*
import bindView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.hide
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.extensions.show
import com.viago.trips.Location
import com.viago.trips.createtrip.PlacesAdapter
import com.viago.trips.createtrip.WebsiteActivity
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.models.Place

/**
 * Created by mayank on 20/8/17.
 */
class TripLocationAdapter(val _locationClickCallBack: LocationClickCallBack) : EasyRecyclerViewAdapter<Location>() {
    val gson: Gson = Gson()
    var _position: Int = -1
    var isEqual : Boolean = false

    companion object {
        var locationClickCallBack: LocationClickCallBack? = null
    }

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if ( itemsList.size>0 &&  itemsList[0]!=null && itemsList[0].title!=null && itemsList[itemsList.lastIndex].title!=null
                && itemsList[0].title.equals(itemsList[itemsList.lastIndex].title)) {

            isEqual = true
        }
        TripLocationAdapter.locationClickCallBack = _locationClickCallBack
        return TripLocationViewHolder(parent.inflate(R.layout.item_trip_location))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, location: Location?, position: Int) {
        if (holder is TripLocationViewHolder) location?.let { holder.bind(it, _position, position) }
    }

    inner class TripLocationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val textno by bindView<TextView>(R.id.image)
        private val placeName by bindView<TextView>(R.id.place_name)
        private val address by bindView<TextView>(R.id.address)
        private val mainlayout: LinearLayout by bindView(R.id.mainlayout)
        private val index by bindView<TextView>(R.id.index)
        private val toggleDetail: ImageView by bindView(R.id.toggleDetail)
        private var detailToggle = false
        private val detailContainer: RelativeLayout by bindView(R.id.detailContainer)
        private val hoursHeading: TextView by bindView(R.id.hoursHeading)
        private val hoursDetail: TextView by bindView(R.id.hoursDetail)
        private val contactHeading: TextView by bindView(R.id.phoneHeading)
        private val contactDetail: TextView by bindView(R.id.phoneDetail)
        private val priceGroupCategoryHeading: TextView by bindView(R.id.priceGroupCategoryHeading)
        private val priceGroupCategory: TextView by bindView(R.id.priceGroupCategory)
        private val website: TextView by bindView(R.id.website)
        private val facebook: TextView by bindView(R.id.facebook)
        private val twitter: TextView by bindView(R.id.twitter)
        private val lineextra by bindView<View>(R.id.lineextra)
        private val upperline by bindView<View>(R.id.upperline)
        private val linebelow by bindView<View>(R.id.linebelow)
        private val locationimg by bindView<ImageView>(R.id.locationimg)
        private var extraData: Place.PlaceExtraData? = null
        private val samelocation by bindView<ImageView>(R.id.samelocation)

        internal fun bind(location: Location, selectedPosition: Int, position: Int) {

            if (selectedPosition == position) {
                mainlayout.setBackgroundColor(Color.parseColor("#e9e9e9"))

            } else {
                mainlayout.setBackgroundColor(Color.WHITE)
            }

            mainlayout.setOnClickListener {

                TripLocationAdapter.locationClickCallBack?.clickLocation(position)

                mainlayout.setBackgroundColor(Color.parseColor("#e9e9e9"))

                setBackgroundItem(position)

            }
//            image.loadImage(location.image)
            placeName.text = location.title
            address.text = location.address


            if ((position == 0 || position == (itemsList.size - 1))) {

                if (isEqual) {
                    samelocation.visibility = View.VISIBLE
                } else {
                    samelocation.visibility = View.INVISIBLE
                }

                if (position == 0) {
                    placeName.text = "Trip Starting Location"
                    placeName.setTextColor(Color.parseColor("#4BD79D"))
                    upperline.visibility = View.INVISIBLE
                    linebelow.visibility = View.VISIBLE
                    locationimg.setImageDrawable(itemView.context.getResources().getDrawable(R.drawable.tripstart));
                } else {
                    locationimg.setImageDrawable(itemView.context.getResources().getDrawable(R.drawable.tripend));
                    upperline.visibility = View.VISIBLE
                    linebelow.visibility = View.INVISIBLE
                    placeName.text = "Trip Ending Location"
                    placeName.setTextColor(Color.parseColor("#F9666C"))
                }

                textno.hide()
            } else {
                locationimg.setImageDrawable(itemView.context.getResources().getDrawable(R.drawable.trip_between));
                textno.show()
                textno.text = position.toString() + ")"

            }


            toggleDetail.setOnClickListener {
                if (detailToggle) {
                    detailContainer.visibility = View.GONE
                    detailToggle = false
                    lineextra.visibility = View.GONE
                    toggleDetail.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.drawable.ic_arrow_down_24dp))
                } else {
                    detailContainer.visibility = View.VISIBLE
                    detailToggle = true
                    toggleDetail.setImageDrawable(ContextCompat.getDrawable
                    (itemView.context, R.drawable.ic_arrow_up_24dp))
                    lineextra.visibility = View.VISIBLE
                }
            }

            if (!TextUtils.isEmpty(location.meta)) {
                var meta = location.meta as String
                if(meta?.take(1)=="{"){
                extraData = gson.fromJson(location.meta, object : TypeToken<Place.PlaceExtraData>() {
                }.type)
                toggleDetail.visibility = View.VISIBLE

                if (extraData!!.hours == null) {
                    hoursDetail.visibility = View.GONE
                    hoursHeading.visibility = View.GONE
                } else {
                    hoursHeading.visibility = View.VISIBLE
                    hoursDetail.visibility = View.VISIBLE
                    if (extraData!!.hours!!.length == 0) {
                        hoursDetail.text = if (extraData!!.open) "Open now" else "Closed now"
                    } else {
                        hoursDetail.text = extraData!!.hours
                    }
                }

                if (!TextUtils.isEmpty(extraData!!.contact)) {
                    contactDetail.visibility = View.VISIBLE
                    contactHeading.visibility = View.VISIBLE
                    contactDetail.text = extraData!!.contact
                } else {
                    contactDetail.visibility = View.GONE
                    contactHeading.visibility = View.GONE
                }

                if (!TextUtils.isEmpty(extraData!!.category)) {
                    priceGroupCategory.visibility = View.VISIBLE
                    priceGroupCategoryHeading.visibility = View.VISIBLE
                    priceGroupCategory.text = extraData!!.category
                } else {
                    priceGroupCategory.visibility = View.GONE
                    priceGroupCategoryHeading.visibility = View.GONE
                }

                website.visibility = if (extraData!!.socialLinks?.website != null) View.VISIBLE else View.GONE
                facebook.visibility = if (extraData!!.socialLinks?.facebook != null) View.VISIBLE else View.GONE
                twitter.visibility = if (extraData!!.socialLinks?.twitter != null) View.VISIBLE else View.GONE

                contactDetail.setOnClickListener {
                    if (extraData!!.contact != null) {
                        itemView.context.startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", extraData!!.contact, null)))
                    }
                }
                website.setOnClickListener {
                    if (extraData!!.socialLinks?.website != null) {

                        var intent = WebsiteActivity.newIntent(itemView.context, extraData!!.socialLinks!!.website);
                        itemView.context.startActivity(intent);

                        // itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(extraData!!.socialLinks!!.website)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }
                facebook.setOnClickListener {
                    if (extraData!!.socialLinks?.facebook != null) {

                        var intent = WebsiteActivity.newIntent(itemView.context, extraData!!.socialLinks!!.facebook);
                        itemView.context.startActivity(intent);

                        // itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(extraData!!.socialLinks!!.facebook)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }
                twitter.setOnClickListener {
                    if (extraData!!.socialLinks?.twitter != null) {

                        var intent = WebsiteActivity.newIntent(itemView.context, extraData!!.socialLinks!!.twitter);
                        itemView.context.startActivity(intent);

                        //  itemView.context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(extraData!!.socialLinks!!.twitter)))
                    } else {
                        Toast.makeText(itemView.context, "Not available", Toast.LENGTH_SHORT).show()
                    }
                }

            } else {
                toggleDetail.visibility = View.GONE
            }
        }
        }
    }

    public fun setBackgroundItem(position: Int) {

        _position = position

        notifyDataSetChanged()

    }

}