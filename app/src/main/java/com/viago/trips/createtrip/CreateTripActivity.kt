package com.viago.trips.createtrip

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.SparseArray
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.baseclasses.UniversalStatePagerAdapter
import com.viago.extensions.hide
import com.viago.extensions.openFragment
import com.viago.extensions.show
import com.viago.trips.Location
import com.viago.trips.annotations.FOURSQUARE
import com.viago.trips.annotations.GOOGLE
import com.viago.trips.annotations.PlaceSearchProvider
import com.viago.trips.annotations.YELP
import com.viago.trips.createtrip.placesearchservices.FoursquareService
import com.viago.trips.createtrip.placesearchutils.FourSqarePlace
import com.viago.trips.createtrip.search.SearchQueryFragment
import com.viago.trips.edittrip.EditTripActivity
import com.viago.trips.interfaces.PlaceInfo
import com.viago.trips.interfaces.PlacesSearchResultCallback
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.models.Place
import com.viago.trips.suggesttrip.SuggestTripActivity
import com.viago.utils.Constants
import com.viago.utils.FileUtils
import com.viago.utils.LocationHelper
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.marker.IconGenerator
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import timber.log.Timber

open class CreateTripActivity : BaseTripActivity(), OnMapReadyCallback, PlacesSearchResultCallback
{


    override val selectedLocations by lazy { ArrayList<com.viago.trips.Location>() }
    private var alreadyselectedLocations =  ArrayList<com.viago.trips.Location>()
    private val locationsSearchResult by lazy { SparseArray<List<com.viago.trips.Location>>(universalStatePagerAdapter.count) }
    private val locationsSearchResultTemp by lazy { SparseArray<List<com.viago.trips.Location>>(universalStatePagerAdapter.count) }
    override var duration: String = ""
    override var distance: String = ""
    protected val colorAccentupdated by lazy { ContextCompat.getColor(this, R.color.colorAccent) }
    private val colorBlue by lazy { ContextCompat.getColor(this, R.color.pin_skyblue) }
    override var source: com.viago.trips.Location? = null
    override var destination: com.viago.trips.Location? = null
    private  val TRIP_SOURCE = "trip source"
    private  val TRIP_DESTINATION = "trip desination"
    private val possibleDuplicatesDialogFragment by lazy { PossibleDuplicateDialogFragment() }
    private val colorPrimaryDark by lazy { ContextCompat.getColor(this, R.color.colorPrimaryDark) }
    val iconGenerator by lazy{ IconGenerator(this@CreateTripActivity) }

    override var categories: List<Int> = ArrayList()
    override var title: String = ""

    var clickItem: Int = -1

    var searchedclicked: Boolean = false
    var isSourceExits: Boolean = false
    var isDestinationExits: Boolean = false

    @SuppressLint("SupportAnnotationUsage")
    @TripActionType.Companion.ActionType
    override var actionType: Int = TripActionType.CREATING

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        EventBus.getDefault().register(this)


        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                setMarkers(position)
            }
        })

        createTrip.setCompoundDrawablesWithIntrinsicBounds(null, null,
                AppCompatResources.getDrawable(this, R.drawable.ic_chevron_right_white_24dp), null)

        createTripLayout.setOnClickListener {
            if (areSourceAndDestinationAvailable()){
                if(selectedLocations.contains(source!!)){
                    selectedLocations.remove(source!!)
                    selectedLocations.remove(destination!!)
                }
                openFragment(TripRearrangeFragment())
            }
            else
            openFragment(SourceDestSelectionFragment.newInstance(object : SourceDestSelectionFragment.Companion.EndLocationsSelectedListener {
                override fun onLocationsSelected() {
                    openFragment(TripRearrangeFragment())
                }
            }))
        }


        val savedTempLocationsString = SharedPrefsUtils.getStringPreference(Constants.KEY_TEMP_LOCATIONS)
        if (savedTempLocationsString != null) {
            val savedTempLocations: ArrayList<Location> = Gson().fromJson(savedTempLocationsString, object : TypeToken<ArrayList<Location>>() {
            }.type)
//            savedTempLocations.forEach {
//                when (it.getProviderInitial()) {
//                    "G" -> (universalStatePagerAdapter.getItem(0) as PlacesFragment).addLocation(it)
//                    "F" -> (universalStatePagerAdapter.getItem(1) as PlacesFragment).addLocation(it)
//                    "Y" -> (universalStatePagerAdapter.getItem(2) as PlacesFragment).addLocation(it)
//                }
//            }
            selectedLocations.addAll(savedTempLocations)
            alreadyselectedLocations.addAll(savedTempLocations)

            savedTempLocations.forEach {
                val location = it
                if(location.provider==3L){
                    selectedLocations.remove(location)
                    alreadyselectedLocations.remove(location)


                    if(source==null){
                        source = location
                    }else if(destination==null) {
                       destination = location
                    }
                }

            }

            if (actionType == com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING){
                savedTempLocations.forEachIndexed { index, location ->

                    if(index==0 || index == savedTempLocations.lastIndex )
                        selectedLocations.remove(location)
                        alreadyselectedLocations.remove(location)

                        if(source==null){
                            source = location
                        }else {
                            destination = location
                        }
                }

            }

            if (intent.hasExtra(TRIP_SOURCE)) {
                source = intent.getParcelableExtra(TRIP_SOURCE)
                destination = intent.getParcelableExtra(TRIP_DESTINATION)
            }

        //    source=savedTempLocations.get(0)
         //    destination=savedTempLocations.get(savedTempLocations.size-1)
          /*  if(alreadyselectedLocations.contains(source!!)){
                alreadyselectedLocations.remove(source!!)
                alreadyselectedLocations.remove(destination!!)
            }*/
            Handler().post({ onSelectedPlacesUpdated() })
            SharedPrefsUtils.removePreference(Constants.KEY_TEMP_LOCATIONS)
        }

    }

    private fun showTapTargetMenu() {
        /*var handler = Handler()
        handler.postDelayed({
            TapTargetSequence(this).target(TapTarget.forToolbarMenuItem(toolbar, R.id.action_search, "", "Click here for additional keyword search")).start()
            SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_SEARCH_NEW_LOCATION, true)
        }, 300)*/

        if( com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP== actionType ) {
           TapTargetSequence(this).target(TapTarget.forToolbarMenuItem(toolbar, R.id.action_search, "", "Click here for additional keyword search")).start()
           SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_SEARCH_NEW_LOCATION, true)
        }
       // TapTargetSequence(this).target(TapTarget.forToolbarMenuItem(toolbar, R.id.action_search, "Re search", "Click here for additional keyword search")).start()



    }

    override fun onBackPressed() {

        if(searchedclicked) {

            searchedclicked = false

            if (supportFragmentManager.backStackEntryCount == 0 && source != null && destination != null) {
                if(selectedLocations.contains(source!!)){
                    selectedLocations.remove(source!!)
                    selectedLocations.remove(destination!!)
                }
                val saveLocations: ArrayList<Location> = ArrayList()
                saveLocations.add(source!!)
                saveLocations.addAll(selectedLocations)
                saveLocations.add(destination!!)
                SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(saveLocations, object : TypeToken<ArrayList<Location>>() {
                }.type))
            }
            if(actionType == com.viago.trips.interfaces.TripActionType.Companion.CREATING ){

              if( source != null && destination != null){

                  selectedLocations.add(source!!)
                  selectedLocations.add(destination!!)

              }
                SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(selectedLocations, object : TypeToken<ArrayList<Location>>() {
                }.type))

                openFragment(SearchQueryFragment.newInstance(this, object : SearchQueryFragment.Companion.PlaceSearchListener {

                    override fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String,isCurrentLocation:Boolean) {
                        //activity.onBackPressed()

                        startActivity(CreateTripActivity.getIntent(
                                this@CreateTripActivity,
                                destination,
                                latLng,
                                keyword,isCurrentLocation)
                        )

                        finish()
                    }

                },true))
                return
            }
           else if (actionType == com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING || actionType == com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP) {

                openFragment(SearchQueryFragment.newInstance(this, object : SearchQueryFragment.Companion.PlaceSearchListener {

                    override fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String,isCurrentLocation:Boolean) {
                        //activity.onBackPressed()
                        var intent: Intent? = null
                        if (this@CreateTripActivity is EditTripActivity) {
                            if (selectedLocations != null && source!! != null)
                                if (!selectedLocations.contains(source!!)) {
                                    selectedLocations.add(0, source!!)
                                    selectedLocations.add(this@CreateTripActivity.destination!!)
                                    setSelectedPlacesView()
                                }
                            val editTripActivity: EditTripActivity = this@CreateTripActivity as EditTripActivity
                            intent = EditTripActivity.getIntent(this@CreateTripActivity, destination, latLng, keyword, this@CreateTripActivity, editTripActivity.tripId, editTripActivity.suggestionId)
                        } else if (this@CreateTripActivity is SuggestTripActivity) {
                            val suggestTripActivity: SuggestTripActivity = this@CreateTripActivity as SuggestTripActivity
                            (suggestTripActivity.tripDetails.locations as ArrayList).clear()
                            (suggestTripActivity.tripDetails.locations as ArrayList).add(source!!)
                            (suggestTripActivity.tripDetails.locations as ArrayList).addAll(selectedLocations)
                            (suggestTripActivity.tripDetails.locations as ArrayList).add(this@CreateTripActivity.destination!!)
                            intent = SuggestTripActivity.getIntent(this@CreateTripActivity, destination, latLng, keyword, this@CreateTripActivity, suggestTripActivity.tripDetails)
                        }
                        if (intent != null) {
                            startActivity(intent)
                            finish()
                        }
                    }

                },true))
                return
            }

        }else {
            try {
//                if (selectedLocations != null && source!! != null)
//                    if (!selectedLocations.contains(source!!)) {
//                        selectedLocations.add(0, source!!)
//                        selectedLocations.add(destination!!)
//                        setSelectedPlacesView()
//                    }
            }catch (e:KotlinNullPointerException){
                e.printStackTrace()
            }
            SharedPrefsUtils.setBooleanPreference(Constants.CREATETRIPACTIVITY_BACK,true);
        }

       // startActivity(Intent(this, MainActivity::class.java))
       // finish()

        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_create_trip_menu, menu)
        if (menu!!.findItem(R.id.action_search) != null && !SharedPrefsUtils.getBooleanPreference(Constants.KEY_TAP_TARGET_SEARCH_NEW_LOCATION, false)) {
            Handler().postDelayed({ showTapTargetMenu() }, 300)
        }

        if (FileUtils.isAddLocationClicked) {
            FileUtils.isAddLocationClicked = false
            Handler().postDelayed({ showTapTargetMenu() }, 1000)
        }

       var search =  menu!!.findItem(R.id.action_search)
       search.isVisible = isShowSearch()

       // if ( !SharedPrefsUtils.getBooleanPreference(Constants.KEY_TAP_TARGET_MULTI_SEARCH, false)) {
           /* TapTargetSequence(this@CreateTripActivity)
                    .target(TapTarget.forToolbarOverflow(toolbar, "Location viewing options", "Click here for detailed information about trip locations")
                            .targetRadius(59)
                            .outerCircleAlpha(0.83f))
                    .start()
            SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_MULTI_SEARCH, true)*/
        //}

        return super.onCreateOptionsMenu(menu)
    }


    private fun  isShowSearch(): Boolean{

        if(com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP== actionType || supportFragmentManager.backStackEntryCount == 0 && source != null && destination != null){

            return true
        }
            return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == R.id.action_search) {
            searchedclicked = true
            onBackPressed()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    public fun areSourceAndDestinationAvailable(): Boolean = (source != null && destination != null)

    private fun getTitleFor(@PlaceSearchProvider provider: Long): String = when (provider) {
        GOOGLE -> "Google"
        FOURSQUARE -> "Foursquare"
        YELP -> "Yelp"
        else -> ""
    }

    override fun getPagerAdapter(): UniversalStatePagerAdapter<Fragment> {

        return UniversalStatePagerAdapter<Fragment>(supportFragmentManager).apply {
            addFrag(PlacesFragment.newInstance(GOOGLE), getTitleFor(GOOGLE))
            addFrag(PlacesFragment.newInstance(FOURSQUARE), getTitleFor(FOURSQUARE))
            addFrag(PlacesFragment.newInstance(YELP), getTitleFor(YELP))
        }
    }

    override fun setMapMarkers() = setMarkers()

    private fun setMarkers(position: Int = viewPager.currentItem) {
        googleMap?.run {

            clear()

            clickItem = position

           /* locationsSearchResult[position]?.forEachIndexed { index, location ->

                val locationselected: android.location.Location = android.location.Location("Loc a")
                val locationsearched: android.location.Location = android.location.Location("Loc b")
                locationselected.latitude = latLng.latitude
                locationselected.longitude = latLng.longitude

                locationsearched.latitude = location.latitude
                locationsearched.longitude = location.longitude

                var distance = locationselected.distanceTo(locationsearched)

               // var locationtemp = Location(location.image, location.title, location.address, location.latitude, location.longitude,
               //        location.meta, location.status, location.provider,distance)


             //   locationsSearchResultTemp.put(position,locationtemp)

                Log.e("distance",""+distance)

               // val results = FloatArray(1)

               // val distance = coords1.toLocation().distanceTo(coords2.toLocation())

              //  latLng.



            }
*/

           var count = 0

            if(alreadyselectedLocations.size==0 && selectedLocations.size >0 &&(actionType == com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING || actionType == com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP) ){

                selectedLocations.forEach {
                    val place = it

                    alreadyselectedLocations.add(place)

                }

            }

            locationsSearchResult[position]?.forEachIndexed { index, location ->
                val markerOptions = MarkerOptions()
                        .position(LatLng(location.latitude, location.longitude))
                        .title(location.title)
                        .snippet(location.address)

                var status = false
                var number = 0

                for ((_index, _selectedLocation) in alreadyselectedLocations.withIndex()) {

                    if(_selectedLocation.latitude == location.latitude && _selectedLocation.longitude == location.longitude
                            || (_selectedLocation.address.equals(location.address)
                                    &&  _selectedLocation.title.equals(location.title))){

                        status = true

                        number = _index+1

                        break;
                    }

                }



               // Log.e("hiiiii","inner");

                if (status) {
                  //  ++ count
                    iconGenerator.setColor(colorBlue)
                    val icon = iconGenerator.makeIcon(number.toString())
                    // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                 //   markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                    Log.e("hiiiii",count.toString());
                } else if (selectedLocations.any { selectedLocation -> selectedLocation.latitude == location.latitude
                                && selectedLocation.longitude == location.longitude
                                || (selectedLocation.address.equals(location.address) &&
                                selectedLocation.title.equals(location.title)
                               )}) {


                    if(source!=null && destination!=null){


                        if(location.latitude == source?.latitude
                                && location.longitude == source?.longitude
                                || (location.address.equals(source?.address) &&
                                        location.title.equals(source?.title)
                                        ))
                        {

                            iconGenerator.setColor(colorPrimaryDark)
                            val icon = iconGenerator.makeIcon("S")

                            isSourceExits = true

                            markerOptions.icon(  BitmapDescriptorFactory.fromBitmap(icon))
                            // addMarker(markerOptions).tag = index

                        }

                        else if(location.latitude == destination?.latitude
                                && location.longitude == destination?.longitude
                                || (location.address.equals(destination?.address) &&
                                        location.title.equals(destination?.title)
                                        ))
                        {

                            iconGenerator.setColor(colorPrimaryDark)
                            val icon = iconGenerator.makeIcon("E")
                            markerOptions.icon(   BitmapDescriptorFactory.fromBitmap(icon))
                            isDestinationExits = true
                            // addMarker(markerOptions).tag = index

                        }else {
                            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                            //  addMarker(markerOptions).tag = index
                        }

                    }else {
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                        // addMarker(markerOptions).tag = index
                    }
                }

                else {
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(huePrimaryDark))

                }

                addMarker(markerOptions).tag = index


                Log.e("locationame",location.address+"== index "+index)
            }


            if(source!=null && !isSourceExits) {

                val markerOptions = MarkerOptions()
                        .position(LatLng(source!!.latitude, source!!.longitude))
                        .title(source!!.title)
                        .snippet(source!!.address)

                iconGenerator.setColor(colorPrimaryDark)
                val icon = iconGenerator.makeIcon("S")

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                addMarker(markerOptions).tag = -1
            }


            if(destination!=null && !isDestinationExits) {
            val markerOptions = MarkerOptions()
                    .position(LatLng(destination!!.latitude, destination!!.longitude))
                    .title(destination!!.title)
                    .snippet(destination!!.address)

            iconGenerator.setColor(colorPrimaryDark)
            val icon = iconGenerator.makeIcon("E")

            markerOptions.icon(   BitmapDescriptorFactory.fromBitmap(icon))
            addMarker(markerOptions).tag = -1
        }

            if(isCurrentLocation && locationsSearchResult[position]?.size!=0) {

               // isCurrentLocation = false

                val markerOptions = MarkerOptions()
                        .position(LatLng(latLng.latitude, latLng.longitude))
                        .title( LocationHelper.getAddress(applicationContext, latLng)?.locality)
                        .snippet(LocationHelper.getAddress(applicationContext, latLng)?.getAddressLine(0))

                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(currentLocationColor))
                addMarker(markerOptions).tag = -1

            }
        }
    }

    @Subscribe
    fun changeSelectedPlacesInList(place: Place?) {
        if (place?.provider == FOURSQUARE && place.placeId != null){
            val placesSearchCallback by lazy {
                object : PlaceInfo {
                    override fun onSearchFailed() {
                        return changeSelectedPlacesInListHelper(place)
                    }

                    override fun onSearchSuccess(places: Place) {
                        place.imageUrl = places.imageUrl
                       return changeSelectedPlacesInListHelper(place)
                    }
                }
            }

            FourSqarePlace().pleaceDetail(place.placeId, placesSearchCallback)
        } else {
            return changeSelectedPlacesInListHelper(place)
        }
    }



    private fun changeSelectedPlacesInListHelper(place: Place?) {
        val gson: Gson = Gson()
        place?.let {
            val locationA: android.location.Location = android.location.Location("Loc a")
            val locationB: android.location.Location = android.location.Location("Loc b")
            locationA.latitude = it.latLng.latitude
            locationA.longitude = it.latLng.longitude
            if (it.isAdded) {
                if (selectedLocations.any {
                    locationB.latitude = it.latitude
                    locationB.longitude = it.longitude
                    locationB.distanceTo(locationA).run { this > 0 && this < 100 }
                }) {
                    Timber.d("add place exists")
                    possibleDuplicatesDialogFragment.addToTripClicked = {
                        selectedLocations.add(it.toLocation(gson))
                        updateOnPlaceChanged(it.provider)
                    }
                    possibleDuplicatesDialogFragment.dontAddToTripClicked = {
                        (universalStatePagerAdapter.getItem(it.provider.toInt()) as? PlacesFragment)?.unAddPlace(it)
                    }
                    possibleDuplicatesDialogFragment.show(supportFragmentManager, PossibleDuplicateDialogFragment::class.java.simpleName)
                } else {
                    Timber.d("add place exists not")
                    selectedLocations.add(it.toLocation(gson))
                    updateOnPlaceChanged(it.provider)
                }
            } else {
                selectedLocations.remove(it.toLocation(gson))
                updateOnPlaceChanged(it.provider)
            }
        }
    }

    private fun updateOnPlaceChanged(@PlaceSearchProvider provider: Long) {
        setSelectedPlacesView()
        setMarkers()
        setTabTextCounters(provider)
    }

    public fun setSelectedPlacesView() {
        if (selectedLocations.size > 0) {
            createTripLayout.show()
            createTrip.text = "Continue (${selectedLocations.size})"
        } else {
            createTripLayout.hide()
        }
    }

    private fun setTabTextCounters(provider: Long) {
        val selectedLocationsCount = selectedLocations.count {
            location: Location -> location.provider == provider
        }

      /*  val selectedLocationsCount = selectedLocations.count {
            location: Location -> location.provider == provider
        }*/

       /* var selectedLocationsCount = 0

        selectedLocations.forEach {

            val location = it

            if(provider==0L && location.image!=null && location.image!!.contains("maps.googleapis.com")){
                ++selectedLocationsCount
            }else  if(provider==1L && location.image!=null && location.image!!.contains("igx.4sqi.net") ){
                ++selectedLocationsCount
            }else  if(provider==2L && location.image!=null && location.image!!.contains("s3-media3.fl.yelpcdn.com") ){
                ++selectedLocationsCount
            }


        }*/

        if(provider.toInt()==0){

            googleCount!!.setText("GOOGLE"+"("+selectedLocationsCount+")")
        }
        else if(provider.toInt()==1){

            foursqareCount!!.setText("FOURSQUARE"+"("+selectedLocationsCount+")")

        }
        else if(provider.toInt()==2) {

            yelpCount!!.setText("YELP" + "(" + selectedLocationsCount + ")")

        }


        tabLayout.getTabAt(provider.toInt())?.run {
            setText("${getTitleFor(provider)} ($selectedLocationsCount)")
        }
    }

    override fun onPlacesSearchResultObtained(provider: Long, places: List<Place>) {
        if(locationsSearchResult[provider.toInt()]!=null) {
            (locationsSearchResult[provider.toInt()] as ArrayList).addAll(places.map { it.toLocation(Gson()) })
        }else{
            locationsSearchResult.put(provider.toInt(),places.map { it.toLocation(Gson()) })
        }
        if ((universalStatePagerAdapter.getItem(viewPager.currentItem) as PlacesFragment).placeSearchProvider== GOOGLE){
            (universalStatePagerAdapter.getItem(viewPager.currentItem) as PlacesFragment).loadingMoreResults.visibility= View.GONE
        }
        //locationsSearchResult.put(provider.toInt(), places.map { it.toLocation(Gson()) })
        setMarkers()
    }

    override fun onSelectedPlacesUpdated() {
        setSelectedPlacesView()
        setMarkers()
        for (i in 0 until tabLayout.tabCount) setTabTextCounters(i.toLong())

        for (i in 0 until universalStatePagerAdapter.count) {
            (universalStatePagerAdapter.getItem(i) as PlacesFragment).updatePlaces()
        }
    }

    public fun onPlacesUpdated() {
        setSelectedPlacesView()
        setMarkers()
        for (i in 0 until tabLayout.tabCount) setTabTextCounters(i.toLong())


    }

    override fun onSearchValuesChanged() {
        super.onSearchValuesChanged()
        locationsSearchResult.clear()
        setMapMarkers()
        for (i in 0 until universalStatePagerAdapter.count) {
            (universalStatePagerAdapter.getItem(i) as PlacesFragment).researchPlaces()
        }
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this)
        super.onDestroy()
    }

    companion object {

        fun getIntent(context: Context, placeName: String, latLng: LatLng, placeType: String,isCurrentLocation:Boolean): Intent {
            val intent = Intent(context, CreateTripActivity::class.java)
            intent.putExtra(PLACE_NAME, placeName)
            intent.putExtra(LAT_LNG, latLng)
            intent.putExtra(PLACE_TYPE, placeType)
            intent.putExtra(IS_CURRENT_LOCATION, isCurrentLocation)

            return intent
        }

//        fun getIntent(context: Context, tripInfo: TripInfo): Intent {
//            val intent = Intent(context, CreateTripActivity::class.java)
//            intent.putExtra(PLACE_NAME, tripInfo.placeName)
//            intent.putExtra(LAT_LNG, tripInfo.latLng)
//
//            var keyword: String = tripInfo.keyword
//            keyword = keyword.capitalize()
//            if (keyword.contains("_")) {
//                val keywordSplit = keyword.split("_")
//                keyword = ""
//                for (key in keywordSplit)
//                    keyword += key.capitalize() + " "
//                keyword = keyword.trim()
//            }
//            intent.putExtra(PLACE_TYPE, keyword)
//            intent.putParcelableArrayListExtra(SELECTED_LOCATIONS, tripInfo.selectedLocations)
//            return intent
//        }
    }



    fun clickLocation(_position: Int) {


        googleMap?.run {

            clear()

            var count = 0

            if(alreadyselectedLocations.size==0 && selectedLocations.size >0 &&
                    (actionType == com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING ||
                            actionType == com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP) ){

                selectedLocations.forEach {
                    val place = it

                    alreadyselectedLocations.add(place)

                }

            }

            locationsSearchResult[clickItem]?.forEachIndexed { index, location ->
                val markerOptions = MarkerOptions()
                        .position(LatLng(location.latitude, location.longitude))
                        .title(location.title)
                        .snippet(location.address)


                var status = false
                var number = 0

                for ((_index, _selectedLocation) in alreadyselectedLocations.withIndex()) {

                    if(_selectedLocation.latitude == location.latitude && _selectedLocation.longitude == location.longitude
                            || (_selectedLocation.address.equals(location.address)
                                    &&  _selectedLocation.title.equals(location.title))){

                        status = true

                        number = _index+1

                        break;
                    }

                }



                // Log.e("hiiiii","inner");

                if (status) {
                    iconGenerator.setColor(colorBlue)
                    val icon = iconGenerator.makeIcon(number.toString())
                    // markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    //   markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))

                } else if (selectedLocations.any { selectedLocation -> selectedLocation.latitude == location.latitude
                                && selectedLocation.longitude == location.longitude
                                || (selectedLocation.address.equals(location.address) &&
                                selectedLocation.title.equals(location.title)
                                )}) {


                    if(source!=null && destination!=null){


                        if(location.latitude == source?.latitude
                                        && location.longitude == source?.longitude
                                        || (location.address.equals(source?.address) &&
                                        location.title.equals(source?.title)
                                        ))
                        {

                            iconGenerator.setColor(colorPrimaryDark)
                            val icon = iconGenerator.makeIcon("S")

                            isSourceExits = true

                            markerOptions.icon(  BitmapDescriptorFactory.fromBitmap(icon))
                            // addMarker(markerOptions).tag = index

                        }

                        else if(location.latitude == destination?.latitude
                                        && location.longitude == destination?.longitude
                                        || (location.address.equals(destination?.address) &&
                                        location.title.equals(destination?.title)
                                        ))
                        {

                            iconGenerator.setColor(colorPrimaryDark)
                            val icon = iconGenerator.makeIcon("E")
                            markerOptions.icon(   BitmapDescriptorFactory.fromBitmap(icon))
                            isDestinationExits = true
                            // addMarker(markerOptions).tag = index

                        }else {
                                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                            //  addMarker(markerOptions).tag = index
                        }

                    }else {
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(hueAccent))
                        // addMarker(markerOptions).tag = index
                    }
                }

                else {
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(huePrimaryDark))
                }

                addMarker(markerOptions).tag = index

                if(index ==_position) {

                    addMarker(markerOptions).showInfoWindow()

                }

                Log.e("locationame",location.address+"== index "+index)
            }


            if(source!=null && !isSourceExits) {

                val markerOptions = MarkerOptions()
                        .position(LatLng(source!!.latitude, source!!.longitude))
                        .title(source!!.title)
                        .snippet(source!!.address)

                iconGenerator.setColor(colorPrimaryDark)
                val icon = iconGenerator.makeIcon("S")

                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                addMarker(markerOptions).tag = -1
            }


            if(destination!=null && !isDestinationExits) {
                val markerOptions = MarkerOptions()
                        .position(LatLng(destination!!.latitude, destination!!.longitude))
                        .title(destination!!.title)
                        .snippet(destination!!.address)

                iconGenerator.setColor(colorPrimaryDark)
                val icon = iconGenerator.makeIcon("E")

                markerOptions.icon(   BitmapDescriptorFactory.fromBitmap(icon))
                addMarker(markerOptions).tag = -1
            }

            if(isCurrentLocation && locationsSearchResult[clickItem]?.size!=0) {

                val markerOptions = MarkerOptions()
                        .position(LatLng(latLng.latitude, latLng.longitude))
                        .title( LocationHelper.getAddress(applicationContext, latLng)?.locality)
                        .snippet(LocationHelper.getAddress(applicationContext, latLng)?.getAddressLine(0))

                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(currentLocationColor))
                addMarker(markerOptions).tag = -1

            }


        }

    }


}