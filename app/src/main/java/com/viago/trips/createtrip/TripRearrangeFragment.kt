package com.viago.trips.createtrip

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.activities.MainActivity
import com.viago.baseclasses.BaseFragment
import com.viago.baseclasses.NavItemClickEvent
import com.viago.extensions.hide
import com.viago.extensions.openFragment
import com.viago.extensions.show
import com.viago.extensions.showToast
import com.viago.login.LoginActivity
import com.viago.login.UserUtil
import com.viago.maputils.MapsRouteUtil
import com.viago.trips.Location
import com.viago.trips.MyTripCreatedEvent
import com.viago.trips.MyTripUpdatedEvent
import com.viago.trips.TripsUtil
import com.viago.trips.annotations.LocationStatus
import com.viago.trips.createtrip.finalize.CreateTripFinalizeFragment
import com.viago.trips.createtrip.placesearchutils.GoogleNearbyPlacesUtil
import com.viago.trips.createtrip.search.SearchQueryFragment
import com.viago.trips.edittrip.AddLocationDialog
import com.viago.trips.edittrip.EditTripActivity
import com.viago.trips.edittrip.TripOptimizeConfirmationDialogFragment
import com.viago.trips.edittrip.TripRearrangeMessageDialogFragment
import com.viago.trips.interfaces.LocationClickCallBack
import com.viago.trips.interfaces.SuggestionsTripInfo
import com.viago.trips.interfaces.TripActionType
import com.viago.trips.interfaces.TripActionType.Companion.CREATING
import com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP
import com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING
import com.viago.trips.interfaces.TripActionType.Companion.VIEWING_SUGGESTIONS
import com.viago.trips.interfaces.TripInfo
import com.viago.trips.models.suggestions.TripPostSuggestionRequest
import com.viago.trips.suggesttrip.SuggestTripActivity
import com.viago.trips.suggesttrip.TripSuggestionsResolver
import com.viago.trips.tripdetails.ApproveSuggestionsDialogFragment
import com.viago.trips.tripdetails.SuggestionActionEvent
import com.viago.utils.Constants
import com.viago.utils.FileUtils.isAddLocationClicked
import com.viago.utils.Scrollable
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.marker.IconGenerator
import com.viago.views.ButtonCompat
import com.viago.views.DraggableRecyclerView
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by mayank on 19/8/17.
 */
class TripRearrangeFragment : BaseFragment(), OnMapReadyCallback, Scrollable {
    private lateinit var bottomSheet: LinearLayout
    private lateinit var draggableRecyclerView: DraggableRecyclerView
    private lateinit var tripLocationsText: TextView
    private lateinit var save: Button
    private lateinit var suggestionsActionContainer: View
    private lateinit var decline: Button
    private lateinit var approve: Button
    private lateinit var toolbar: Toolbar
    private lateinit var addLocation: ButtonCompat
    private lateinit var durationAndDistance: TextView
    private lateinit var distanceUnitSwitch: TextView
    private lateinit var optimize: ImageView

    private var googleMap: GoogleMap? = null
    private lateinit var selectedLocations: ArrayList<Location>
    public lateinit var optimizedOrder: ArrayList<Location>
    private var allLocations: ArrayList<Location> = ArrayList()

    public lateinit var tripInfo: TripInfo
    private lateinit var tripActionType: TripActionType
    private var suggestionsTripInfo: SuggestionsTripInfo? = null
    private var tripDuration: String = ""
    protected val hueAccent by lazy { getHue(R.color.colorAccent) }
    protected val currentLocationColor by lazy { getHue(R.color.md_yellow_800) }

    protected val huePrimaryDark by lazy { getHue(R.color.colorPrimaryDark) }
    private lateinit var draggableTripLocationAdapter: DraggableTripLocationAdapter
    /*private val listPopupWindow by lazy { ListPopupWindow(context).apply {
        anchorView = toolbar.view
    } }*/
    private fun getHue(@ColorRes colorRes: Int): Float {
        val hueColor = FloatArray(3)
        ColorUtils.colorToHSL(ContextCompat.getColor(activity!!, colorRes), hueColor)
        return hueColor[0]
    }

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(context).apply {
            setMessage("Suggesting Trip")
            isIndeterminate = true
            setCancelable(false)
        }
    }

    private val colorPrimaryDark by lazy { ContextCompat.getColor(context!!, R.color.colorPrimaryDark) }
    private val colorBlue by lazy { ContextCompat.getColor(context!!, R.color.pin_skyblue) }
    private val colorGreen by lazy { ContextCompat.getColor(context!!, R.color.green) }
    private val colorRed by lazy { ContextCompat.getColor(context!!,  R.color.colorAccent) }

    override fun getLayoutRes(): Int = R.layout.fragment_trip_rearrange

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        bottomSheet = getView()?.findViewById<LinearLayout>(R.id.bottom_sheet) as LinearLayout

        draggableRecyclerView = getView()?.findViewById<DraggableRecyclerView>(R.id.draggable_recycler_view) as DraggableRecyclerView

        draggableRecyclerView.getRecycledViewPool().setMaxRecycledViews(0, 0);

        tripLocationsText = getView()?.findViewById<TextView>(R.id.trip_locations_text) as TextView

        save = getView()?.findViewById<Button>(com.viago.R.id.save) as Button

        suggestionsActionContainer = getView()?.findViewById<View>(com.viago.R.id.suggestions_action_container) as View

        decline = getView()?.findViewById<Button>(com.viago.R.id.decline) as Button

        approve = getView()?.findViewById<Button>(com.viago.R.id.approve) as Button

        toolbar = getView()?.findViewById<Toolbar>(com.viago.R.id.toolbar) as Toolbar

        addLocation = getView()?.findViewById<ButtonCompat>(com.viago.R.id.add_location) as ButtonCompat

        durationAndDistance = getView()?.findViewById<TextView>(com.viago.R.id.trip_locations_duration_and_distance) as TextView

        distanceUnitSwitch = getView()?.findViewById<TextView>(com.viago.R.id.kmmileswitch) as TextView

        optimize = getView()?.findViewById<ImageView>(com.viago.R.id.optimize) as ImageView

        SharedPrefsUtils.setBooleanPreference(Constants.KEY_FIRST_OPTIMISE, true)



        setHasOptionsMenu(true)

        val mapFragment = childFragmentManager.findFragmentById(R.id.mapp) as SupportMapFragment
        mapFragment.getMapAsync(this)
        if (tripInfo.selectedLocations.contains(tripInfo.source!!)) {
            tripInfo.selectedLocations.remove(tripInfo.source!!)
            tripInfo.selectedLocations.remove(tripInfo.destination!!)
        }
        selectedLocations = tripInfo.selectedLocations
        optimizedOrder = ArrayList<Location>()
        optimizedOrder.addAll(tripInfo.selectedLocations)

        when (tripActionType.actionType) {
            VIEWING_SUGGESTIONS -> {
                addLocation.hide()
                toolbar.title = "Trip Suggestion"
                save.hide()
                suggestionsActionContainer.show()
                draggableTripLocationAdapter = DraggableTripLocationAdapter(object : LocationClickCallBack {
                    override fun clickLocation(position: Int) {
                        clickLocationOnTripRearrangement(position)

                    }
                }, this, true)
                decline.setOnClickListener {
                    progressDialog.setMessage("Declining")
                    progressDialog.show()
                    Handler().postDelayed({
                        progressDialog.dismiss()
                        activity!!.finish()
                    }, 800)
//                    suggestionsTripInfo?.suggestionId?.let { id ->
//                        TripsUtil.declineSuggestion(userToken, id, {
//                            //context.showToast("Declined")
//                            progressDialog.dismiss()
//                            //EventBus.getDefault().postSticky(SuggestionActionEvent(id))
//                            activity.finish()
//                        }, {
//                            context.showToast(it)
//                            progressDialog.dismiss()
//                        })
//                    }
                    //activity.onBackPressed()
                }
                approve.setOnClickListener {
                    ApproveSuggestionsDialogFragment().let {
                        it.onUpdateCurrentClicked = {
                            progressDialog.setMessage("Saving")
                            progressDialog.show()
                            updateSelectedPlaces()
                            suggestionsTripInfo?.run {
                                tripId.let { tripId ->
                                    updateTrip(tripId, originalTripLocations)
                                }
                            }
                        }
                        it.onCreateNewClicked = {
                            updateSelectedPlaces()
                            getAppCompatActivity().openFragment(CreateTripFinalizeFragment())
//                            progressDialog.setMessage("Saving")
//                            progressDialog.show()
//                            createTrip()
                        }
                        it.show(childFragmentManager, ApproveSuggestionsDialogFragment::class.java.simpleName)
                    }
                }
            }
            EDITING_TRIP -> {
                toolbar.title = "Edit Trip"
                save.show()
                save.text = "Save"
                suggestionsActionContainer.hide()
                optimize.visibility = View.VISIBLE
                optimize.setOnClickListener { optimizeTrip() }
                draggableTripLocationAdapter = DraggableTripLocationAdapter(object : LocationClickCallBack {
                    override fun clickLocation(position: Int) {


                        clickLocationOnTripRearrangement(position)

                    }
                }, this, onEditEndLocationClicked = {
                    getAppCompatActivity().openFragment(SourceDestSelectionFragment.newInstance(object : SourceDestSelectionFragment.Companion.EndLocationsSelectedListener {
                        override fun onLocationsSelected() {
                            activity!!.onBackPressed()
                            with(draggableTripLocationAdapter) {
                                updateItem(0, tripInfo.source)
                                updateItem(itemsListSize - 1, tripInfo.destination)
                            }
                        }
                    }))
                })
                save.setOnClickListener {
                    updateSelectedPlaces()
                    progressDialog.setMessage("Saving")
                    progressDialog.show()
                    suggestionsTripInfo?.run {
                        tripId.let { tripId ->
                            updateTrip(tripId, originalTripLocations)
                        }
                    }
                }
            }
            CREATING -> {
                toolbar.title = "Create Trip"
                save.show()
                optimize.visibility = View.VISIBLE
                optimize.setOnClickListener { optimizeTrip() }
                suggestionsActionContainer.hide()
                draggableTripLocationAdapter = DraggableTripLocationAdapter(object : LocationClickCallBack {
                    override fun clickLocation(position: Int) {

                        clickLocationOnTripRearrangement(position)

                    }
                }, this, onEditEndLocationClicked = {
                    activity!!.onBackPressed()
                })
                save.setOnClickListener {
                    updateSelectedPlaces()
                    getAppCompatActivity().openFragment(CreateTripFinalizeFragment())
                }
            }
            SUGGESTING -> {
                toolbar.title = "Suggestion"
                optimize.visibility = View.VISIBLE
                optimize.setOnClickListener { refreshSuggestion() }
                save.hide()
                suggestionsActionContainer.show()
                approve.text = "Suggest"
                decline.text = "Save"
                draggableTripLocationAdapter = DraggableTripLocationAdapter(object : LocationClickCallBack {
                    override fun clickLocation(position: Int) {

                        clickLocationOnTripRearrangement(position)

                    }
                }, this)
                approve.setOnClickListener {
                    onSuggestTrip()
                }
                decline.setOnClickListener {
                    updateSelectedPlaces()
                    getAppCompatActivity().openFragment(CreateTripFinalizeFragment())
                }
            }
        }

        draggableRecyclerView.run {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = draggableTripLocationAdapter.apply {
                addItemOnTop(tripInfo.source)
                addItemsAtBottom(selectedLocations)
                addItemAtBottom(tripInfo.destination)
            }
            //   addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            nonMovablePositions = listOf(0, selectedLocations.size + 1)
            tripLocationsText.text = "Trip locations (${draggableTripLocationAdapter.itemsList.size - 2})"
        }


        allLocations.addAll(draggableTripLocationAdapter.itemsList)

        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)

        bottomSheet.post {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.peekHeight = bottomSheet.height / 2
        }

        toolbar.run {
            if (tripActionType.actionType == VIEWING_SUGGESTIONS) {
                overflowIcon = ContextCompat.getDrawable(context, R.drawable.ic_action_info)
                getAppCompatActivity().setSupportActionBar(this)
            }
            setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
            setNavigationOnClickListener { activity!!.onBackPressed() }
        }

        addLocation.setOnClickListener {

            if(isLocationMoved) {

                AddLocationDialog().let {
                    it.onOkayClicked = {

                        addLocation()
                    }
                    it.onCancelClicked = {


                    }
                    it.show(childFragmentManager, AddLocationDialog::class.java.simpleName)
                }

            }else {

                addLocation()

            }

        }



        distanceUnitSwitch.setOnClickListener {
            val arr = tripInfo.distance.split(":")
            if (distanceUnitSwitch.text.contains("Miles")) {
                durationAndDistance.text = tripDuration + " | " + arr[1]
                distanceUnitSwitch.text = "(View in Kms)"
            } else {
                durationAndDistance.text = tripDuration + " | " + arr[0]
                distanceUnitSwitch.text = "(View in Miles)"
            }
        }

        draggableTripLocationAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                super.onItemRangeRemoved(positionStart, itemCount)

                if (draggableTripLocationAdapter.itemsListSize == 0) {
                    tripLocationsText.text = "Trip locations"
                } else {
                    //-2 because count doesn't comprise of start and end locations
                    tripLocationsText.text = "Trip locations (${draggableTripLocationAdapter.itemsList.size - 2})"
                    setMapMarkers()
                }
            }

            override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
                super.onItemRangeMoved(fromPosition, toPosition, itemCount)
                isLocationMoved = true
                //check if this trip is being rearranged for first time.
                if (SharedPrefsUtils.getBooleanPreference(Constants.KEY_FIRST_OPTIMISE, true)) {
                    TripRearrangeMessageDialogFragment().let {
                        it.onOkayClicked = {
                            SharedPrefsUtils.setBooleanPreference(Constants.KEY_FIRST_OPTIMISE, false)
                            optimize.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_refresh_black_24dp))
                            setMapMarkers()
                        }
                        it.onCancelClicked = {
                            SharedPrefsUtils.setBooleanPreference(Constants.KEY_FIRST_OPTIMISE, false)
                            Collections.swap(draggableTripLocationAdapter.itemsList, fromPosition, toPosition)
                            draggableTripLocationAdapter.notifyItemChanged(fromPosition)
                            draggableTripLocationAdapter.notifyItemChanged(toPosition)
                        }
                        //@TODO Test this code
                        SharedPrefsUtils.setBooleanPreference(Constants.KEY_FIRST_OPTIMISE, false)
                        it.show(childFragmentManager, TripRearrangeMessageDialogFragment::class.java.simpleName)

                    }
                } else {
                    optimize.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_refresh_black_24dp))
                    setMapMarkers()
                }
            }
        })

        if (tripActionType.actionType == VIEWING_SUGGESTIONS && !SharedPrefsUtils.getBooleanPreference(Constants.KEY_TAP_TARGET_VIEW_SUGGESTION, false)) {
            showToolbarTapTarget()
            SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_VIEW_SUGGESTION, true)
        }

        if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_TAP_TARGET_ADD_LOCATION, false)) {
            showViewTapTarget()
            //////////////
            SharedPrefsUtils.setBooleanPreference(Constants.KEY_TAP_TARGET_ADD_LOCATION, true)
        }

    }

    private fun addLocation(){

        isAddLocationClicked = true

        if (tripActionType.actionType == com.viago.trips.interfaces.TripActionType.Companion.CREATING) {
            GoogleNearbyPlacesUtil.pageToken = ""

            //   Toast.makeText(context,"Trip rearrenge c"+ GoogleNearbyPlacesUtil.pageToken, Toast.LENGTH_LONG).show()

//                tripInfo.selectedLocations.add(0,tripInfo.source!!)
//                tripInfo.selectedLocations.add(tripInfo.destination!!)
            /* if(activity.supportFragmentManager.backStackEntryCount>1) {
                 activity.onBackPressed()
                 activity.onBackPressed()
             }else{
                 activity.onBackPressed()
             }*/

            var locations = ArrayList<Location>()


            var selectedlocation = tripInfo.selectedLocations
            selectedlocation.forEach {
                val location = it
                locations.add(location)

            }
            var source = tripInfo?.source
            if (source != null) {
                locations.add(source)
            }

            var destination = tripInfo?.destination
            if (destination != null) {
                locations.add(destination)
            }

            SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(locations, object : TypeToken<ArrayList<Location>>() {
            }.type))

            val intent = activity!!.intent
            intent.putExtra(EditTripActivity.TRIP_NEW_LOCATIONS, tripInfo.selectedLocations)
            intent.putExtra(EditTripActivity.ALLOW_LOCATIONS, true)//SuggestTripActivity.getIntent(context, tripInfo.placeName, tripInfo.latLng, "", tripInfo, suggestTripActivity.tripDetails)
            startActivity(intent)
            activity!!.finish()

        } else if (tripActionType.actionType == com.viago.trips.interfaces.TripActionType.Companion.SUGGESTING) {
            GoogleNearbyPlacesUtil.pageToken = ""
            //   Toast.makeText(context,"Trip rearrenge s"+ GoogleNearbyPlacesUtil.pageToken, Toast.LENGTH_LONG).show()
            if (activity!!.supportFragmentManager.backStackEntryCount > 1) {
                activity!!.onBackPressed()
                activity!!.onBackPressed()
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.onBackPressed()
            } else {

///*
//                    var locations = ArrayList<Location>()
//
//
//                    var selectedlocation = tripInfo.selectedLocations
//                    selectedlocation.forEach {
//                        val location = it
//                        locations.add(location)
//
//                    }
//                    var source = tripInfo?.source
//                    if (source != null) {
//                        locations.add(source)
//                    }
//
//                    var destination = tripInfo?.destination
//                    if (destination != null) {
//                        locations.add(destination)
//                    }
//
//                    SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(locations, object : TypeToken<ArrayList<Location>>() {
//                    }.type))*/

//                    ((activity as SuggestTripActivity).tripDetails.locations as ArrayList).clear()
//                    ((activity as SuggestTripActivity).tripDetails.locations as ArrayList).addAll(tripInfo.selectedLocations)
                val saveLocations: ArrayList<Location> = ArrayList()
                saveLocations.add(tripInfo.source!!)
                saveLocations.addAll(tripInfo.selectedLocations)
                saveLocations.add(tripInfo.destination!!)
                SharedPrefsUtils.setStringPreference(Constants.KEY_TEMP_LOCATIONS, Gson().toJson(saveLocations, object : TypeToken<ArrayList<Location>>() {
                }.type))
                val intent = activity!!.intent
                intent.putExtra(SuggestTripActivity.TRIP_DETAILS, (activity as SuggestTripActivity).tripDetails)
                intent.putExtra(SuggestTripActivity.ALLOW_LOCATIONS, true)//SuggestTripActivity.getIntent(context, tripInfo.placeName, tripInfo.latLng, "", tripInfo, suggestTripActivity.tripDetails)
                startActivity(intent)
                activity!!.finish()
            }
        } else if (tripActionType.actionType == com.viago.trips.interfaces.TripActionType.Companion.EDITING_TRIP) {
            GoogleNearbyPlacesUtil.pageToken = ""
            //  Toast.makeText(context,"Trip rearrenge e"+ GoogleNearbyPlacesUtil.pageToken, Toast.LENGTH_LONG).show()

            if (activity!!.supportFragmentManager.backStackEntryCount > 1) {
                activity!!.onBackPressed()
                activity!!.onBackPressed()
            } else if (activity!!.supportFragmentManager.backStackEntryCount > 0) {
                activity!!.onBackPressed()
            } else {
//                    tripInfo.selectedLocations.add(0,tripInfo.source!!)
//                   tripInfo.selectedLocations.add(tripInfo.destination!!)
                val intent = activity!!.intent
                intent.putExtra(EditTripActivity.TRIP_NEW_LOCATIONS, tripInfo.selectedLocations)
                intent.putExtra(EditTripActivity.ALLOW_LOCATIONS, true)//SuggestTripActivity.getIntent(context, tripInfo.placeName, tripInfo.latLng, "", tripInfo, suggestTripActivity.tripDetails)
                startActivity(intent)
                activity!!.finish()
            }
        } else {
//                tripInfo.selectedLocations.add(0,tripInfo.source!!)
//                tripInfo.selectedLocations.add(tripInfo.destination!!)
            getAppCompatActivity().openFragment(SearchQueryFragment.newInstance(tripInfo, object : SearchQueryFragment.Companion.PlaceSearchListener {

                override fun onPlaceSearchEvent(destination: String, latLng: LatLng, keyword: String, isCurrentLocation: Boolean) {
                    //activity.onBackPressed()
                    activity!!.finish()
                    var intent: Intent? = null
                    if (activity is EditTripActivity) {
                        val editTripActivity: EditTripActivity = activity as EditTripActivity
                        intent = EditTripActivity.getIntent(context!!, destination, latLng, keyword, tripInfo, editTripActivity.tripId, editTripActivity.suggestionId)
                    } else if (activity is SuggestTripActivity) {
                        val suggestTripActivity: SuggestTripActivity = activity as SuggestTripActivity
                        intent = SuggestTripActivity.getIntent(context!!, destination, latLng, keyword, tripInfo, suggestTripActivity.tripDetails)
                    }
                    if (intent != null)
                        startActivity(intent)
                }

            },true))
        }
    }

    private fun showViewTapTarget() {
        TapTargetSequence(activity)
                .target(TapTarget.forView(addLocation, "Add location", "Click here to add more location to the trip")
                        .targetRadius(59)
                        .outerCircleAlpha(0.83f)
                        )
                        .start()
    }

    /*.targetRadius(75)
                        .outerCircleAlpha(0.05f)
                        .targetCircleColor(R.color.colorPrimaryapp)
                        .titleTextColor(R.color.colorPrimaryapp)
                        .descriptionTextColor(R.color.colorPrimaryapp)*/
    private fun showToolbarTapTarget() {
        TapTargetSequence(activity)
                .target(TapTarget.forToolbarOverflow(toolbar, "Location viewing options", "Click here for detailed information about trip locations")
                        .targetRadius(59)
                        .outerCircleAlpha(0.83f))
                .start()
    }

    private fun refreshSuggestion() {
        activity!!.onBackPressed()
        SuggestTripActivity.makeIntent(context!!, suggestionsTripInfo!!.tripId, {
            startActivity(it)
            //activity.finish()
        }, {
            if (UserUtil.isLoggedIn()) startMainActivity() else startLoginActivity()
        })
    }

    private fun startLoginActivity() {
        startActivityFinishingThis(Intent(context, LoginActivity::class.java))
    }

    private fun startMainActivity() {
        startActivityFinishingThis(Intent(context, MainActivity::class.java))
    }

    private fun startActivityFinishingThis(intent: Intent) {
        startActivity(intent)
        //activity.finish()
    }

    private fun optimizeTrip() {
        if (!optimizedOrder.isEmpty()) {
            TripOptimizeConfirmationDialogFragment().let {
                it.onOkayClicked = {
                    draggableTripLocationAdapter.run {
                        clear()
                        addItemOnTop(tripInfo.source)
                        addItemsAtBottom(optimizedOrder)
                        addItemAtBottom(tripInfo.destination)
                    }
                    optimize.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_refresh_blue_24dp))
                    setMapMarkers()
                }
                it.onCancelClicked = {
                }
                it.show(childFragmentManager, TripOptimizeConfirmationDialogFragment::class.java.simpleName)
            }
        }
    }

    private fun createTrip() {
        TripsUtil.createTrip(userToken, TripsUtil.makeCreateTripRequestObject(tripInfo, suggestionsTripInfo?.suggestionId), {
            progressDialog.dismiss()
            context!!.showToast("New trip created")
            EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_my_trips))
            EventBus.getDefault().postSticky(SuggestionActionEvent(suggestionsTripInfo?.suggestionId
                    ?: -1))
            EventBus.getDefault().postSticky(MyTripCreatedEvent(it, share = true))
            activity!!.finish()
        }, onFailure)
    }

    private fun updateTrip(tripId: Long, originalTripLocations: ArrayList<Location>) {
        TripsUtil.updateTrip(userToken, tripId, TripsUtil.makeCreateTripRequestObject(tripInfo, suggestionsTripInfo?.suggestionId), {
            progressDialog.dismiss()
            originalTripLocations.clear()
            originalTripLocations.addAll(tripInfo.selectedLocations)
            tripInfo.selectedLocations.remove(tripInfo.source)
            tripInfo.selectedLocations.remove(tripInfo.destination)
            //EventBus.getDefault().postSticky(SuggestionActionEvent(suggestionsTripInfo?.suggestionId ?: -1))
            EventBus.getDefault().postSticky(MyTripUpdatedEvent(it, suggestionsTripInfo?.suggestionId == null))
            context!!.showToast("Trip updated")
            activity!!.finish()
        }, onFailure)
    }

    private val userToken by lazy { UserUtil.getUserToken() }
    private fun onSuggestTrip() {
        //STATUS_ADDED = 1
        //STATUS_NO_CHANGE = 2
        //STATUS_REMOVED = 3
        suggestionsTripInfo?.let {
            progressDialog.show()
            updateSelectedPlaces()

            var copyValue = (suggestionsTripInfo as SuggestTripActivity)?.tripDetails?.locations


            var orignalLocations = ArrayList<Location>()

            for (item in copyValue!!) {

                orignalLocations.add(item)

            }

            if (orignalLocations.contains(tripInfo.source!!)) {
                orignalLocations.remove(tripInfo.source!!)
                orignalLocations.remove(tripInfo.destination!!)
            }

            val suggestedLocations = TripSuggestionsResolver().getResolvedSuggestions(orignalLocations, tripInfo.selectedLocations)
            tripInfo.source?.let {
                it.status = LocationStatus.NONE
                suggestedLocations.add(0, it)
            }
            tripInfo.destination?.let {
                it.status = LocationStatus.NONE
                suggestedLocations.add(it)
            }

            TripsUtil.postSuggestions(userToken, it.tripId, TripPostSuggestionRequest(suggestedLocations), {
                progressDialog.dismiss()
                context!!.showToast("Suggested")
                activity!!.finish()
            }, onFailure)
        }
    }


    val onFailure: (String) -> Unit = {
        progressDialog.dismiss()
        context!!.showToast(it)
    }

    private fun updateSelectedPlaces() {
        tripInfo.selectedLocations.run {
            clear()
            addAll(draggableTripLocationAdapter.itemsList)
            remove(tripInfo.source)
            remove(tripInfo.destination)
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (tripActionType.actionType == VIEWING_SUGGESTIONS) {
            //if (menu is MenuBuilder) menu.setOptionalIconsVisible(true)
            inflater.inflate(R.menu.suggestion_info, menu)
            if (menu.findItem(R.id.action_search) != null)
                menu.findItem(R.id.action_search).isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean = when (item!!.itemId) {
        R.id.action_trip_original -> {

            decline.visibility = View.GONE
            approve.visibility = View.GONE

            googleMap?.run {
                clear()

                var copyValue = suggestionsTripInfo?.originalTripLocations?.clone() as ArrayList<Location>;

                // orignalLocations.add(0,allLocations.get(0))

                //  orignalLocations.add(allLocations.get(allLocations.size-1))

                // clear()

                var orignalLocations = ArrayList<Location>()

                for (item in copyValue) {

                    orignalLocations.add(item)

                }

                orignalLocations.add(0, allLocations.get(0))

                orignalLocations.add(allLocations.get(allLocations.size - 1))

                orignalLocations?.let {
                    if (it.isNotEmpty()) {
                        plotMarkers(this, it)
                        showRoute(this, it, R.color.pin_skyblue)

                        draggableTripLocationAdapter.clear()
                        draggableTripLocationAdapter.addItemsAtBottom(it)
                    }
                }

                /* suggestionsTripInfo?.originalTripLocations?.let {
                     if (it.isNotEmpty()) {
                         plotMarkers(this, it)
                         showRoute(this, it, R.color.colorPrimaryDark)

                         draggableTripLocationAdapter.clear()
                         draggableTripLocationAdapter.addItemsAtBottom(it)
                     }
                 }*/
            }
            true
        }
        R.id.action_trip_suggested -> {

            decline.visibility = View.VISIBLE
            approve.visibility = View.VISIBLE

            googleMap?.run {
                clear()
                allLocations.let {
                    if (it.isNotEmpty()) {
                        plotMarkers(this, it)

                        var arraylist = ArrayList<Location>()

                        it?.forEachIndexed { index, location ->

                            if(location.status!=LocationStatus.REMOVED){

                                arraylist.add(location)
                            }

                        }

                        showRoute(this, arraylist,R.color.pin_skyblue)

                        draggableTripLocationAdapter.clear()
                        draggableTripLocationAdapter.addItemsAtBottom(it)
                    }
                }
            }
            true
        }
        R.id.action_location_removed -> {
            true
        }

        R.id.action_location_added -> {
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            googleMap.setOnMapClickListener {

                setBackGroundItem(-1)

            }

            googleMap.setOnMarkerClickListener { marker ->


                val status: Boolean

                if (marker.tag as Int != -1) {

                    status = scrollTo(marker.tag as Int) ?: false

                    setBackGroundItem(marker.tag as Int)

                } else {

                    status = true;

                    setBackGroundItem(-1)

                }

                marker.showInfoWindow()

                //(universalStatePagerAdapter.getItem(viewPager.currentItem) as? Scrollable)?.scrollTo(marker.tag as Int) ?: false

                status
            }

            val success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            activity, R.raw.map))
            if (!success) {
                // Log.e(TAG, "Style parsing failed.")
            }
        } catch (e: Resources.NotFoundException) {
            // Log.e(TAG, "Can't find style. Error: ", e)
        }

        (activity as? CreateTripActivity)?.latLng?.let {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude - .2, it.longitude), 16F))
        }

        draggableRecyclerView.post { setMapMarkers() }
    }

    public fun setMapMarkers() {
        googleMap?.run {
            clear()
            @ColorRes val colorRes: Int
            if (tripActionType.actionType == VIEWING_SUGGESTIONS) {
                colorRes = R.color.green
            } else {
                colorRes = R.color.colorPrimaryDark
            }

            /* draggableTripLocationAdapter.itemsList.let {
                 if (it.isNotEmpty()) {
                    showRoute(this, it, colorRes)
                 }
             }
             if (tripActionType.actionType == SUGGESTING)
             suggestionsTripInfo?.originalTripLocations?.let {
                 if (it.isNotEmpty()) {
                     plotMarkers(this, it)
                    showRoute(this, it, R.color.colorPrimaryDark)
                 }
             }*/

            draggableTripLocationAdapter.itemsList?.let {
                if (it.isNotEmpty()) {

                    clear()

                    Log.e("placepoint4", "clear")

                    if (tripActionType.actionType != com.viago.trips.interfaces.TripActionType.Companion.CREATING) {

                        plotMarkers(this, it)

                    }

                    var arraylist = ArrayList<Location>()

                    draggableTripLocationAdapter.itemsList?.forEachIndexed { index, location ->

                        if(location.status!=LocationStatus.REMOVED){

                            arraylist.add(location)
                        }

                    }

                    showRoute(this, arraylist, R.color.pin_skyblue)

                } else {

                    draggableTripLocationAdapter.itemsList.let {
                        if (it.isNotEmpty()) {
                            showRoute(this, it, R.color.pin_skyblue)
                            Log.e("placepoint5", "clear")
                        }
                    }

                }
            }


        }
    }

    private fun plotMarkers(googleMap: GoogleMap, locations: List<Location>) {

      //  val marker = LayoutInflater.from(context).inflate(R.layout.marker, null)

	//	val locationTxt =  marker.findViewById(R.id.location_no) as  TextView

        var isEqual = false

        if ( locations.size>0 && locations[0].title.equals(locations[locations.lastIndex].title)) {

            isEqual = true
        }

        if (context != null) {

           val iconGenerator = IconGenerator(context)

            var builder = LatLngBounds.Builder();

            for (i in 0 until locations.size) {

                Log.e("placepoint2", "" + locations[i] + "   " + i)

                val markerText: String
                val markerColor: Int
                if (i == 0) {
                      markerColor = colorPrimaryDark
                      markerText = "S"
                } else if (i == locations.lastIndex) {
                      markerColor = colorPrimaryDark
                      markerText = "E"
                } else if (locations[i].status == LocationStatus.ADDED) {
                      markerColor = colorGreen
                      markerText = i.toString()

                } else if (locations[i].status == LocationStatus.REMOVED) {
                     markerColor = colorRed
                     markerText = i.toString()

                } else {
                      markerColor = colorBlue
                      markerText = i.toString()

                }

                iconGenerator.setColor(markerColor)
                val icon = iconGenerator.makeIcon(markerText)

                builder.include(LatLng(locations[i].latitude, locations[i].longitude));

                locations[i].run {
                    val markerOptions = MarkerOptions().position(LatLng(latitude, longitude)).title(title)


                    if (i == 0) {
                        if (isEqual) {
                           markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                        } else {
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                        }

                    } else if (i == locations.lastIndex) {
                        if (isEqual) {
                            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                        } else {
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))                        }
                    } else {

                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    }

                    googleMap.addMarker(markerOptions).tag = i

                    Log.e("placepoint3",title+"   "+i )
                }
            }

            if (locations.size > 0) {

                var bounds = builder.build();

                val width = getResources().getDisplayMetrics().widthPixels;
                val height = getResources().getDisplayMetrics().heightPixels;
                val padding = (width * 0.30)


                var cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding.toInt());

                googleMap.moveCamera(cu);

            }

        }
    }


    private var isLocationMoved: Boolean = false
    private var areLocationsOptimized = false

    private fun showRoute(googleMap: GoogleMap, locations: List<Location>, colorRes: Int) {
        val showOptimized = (tripActionType.actionType == CREATING || tripActionType.actionType == EDITING_TRIP || tripActionType.actionType == SUGGESTING) && !isLocationMoved

        if (showOptimized) {
            if(!getAppCompatActivity().isFinishing)
            OptimizedRouteMessageDialogFragment().show(childFragmentManager, OptimizedRouteMessageDialogFragment::class.java.simpleName)
        }

        val latLngList = locations.map { location -> LatLng(location.latitude, location.longitude) }

        if (!latLngList.isEmpty()) {
            MapsRouteUtil(context!!, googleMap).showRoute(latLngList.first(),
                    latLngList.last(),
                    latLngList.subList(1, latLngList.lastIndex),
                    colorRes, { distanceKm: String, distanceMile: String, duration: String, bounds: LatLngBounds? ->
                tripDuration = duration
                tripInfo.distance = distanceKm + ":" + distanceMile
                tripInfo.duration = duration
                durationAndDistance.text = if (tripDuration != null && tripInfo.distance != null) "$tripDuration | $distanceKm" else ""
                /* bounds?.let {

                         googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds,  0))


 //                googleMap.moveCamera(CameraUpdateFactory.scrollBy(0F, bottomSheet.height / 2.5F))
                 }*/
                plotMarkers(googleMap, draggableTripLocationAdapter.itemsList)
            }, showOptimized, if (showOptimized && !areLocationsOptimized) onOptimizedWaypointOrderReceived else null)

            if (showOptimized && !areLocationsOptimized) {
                draggableTripLocationAdapter.clear()
                draggableTripLocationAdapter.showLoading()
            }
        }
    }

    private val onOptimizedWaypointOrderReceived: ((List<Int>) -> Unit) = {
        if (it.isEmpty()) {
            updateAdapterWithNewOrder()
        } else {
            val reorganizedSelectedLocations = ArrayList<Location>()
            it.forEach { reorganizedSelectedLocations.add(tripInfo.selectedLocations[it]) }
            tripInfo.selectedLocations.clear()
            tripInfo.selectedLocations.addAll(reorganizedSelectedLocations)
            areLocationsOptimized = true
            updateAdapterWithNewOrder()
        }

    }

    private fun updateAdapterWithNewOrder() {
        selectedLocations = tripInfo.selectedLocations
        draggableTripLocationAdapter.run {
            clear()
            hideLoading()
            addItemOnTop(tripInfo.source)
            addItemsAtBottom(tripInfo.selectedLocations)
            addItemAtBottom(tripInfo.destination)
        }
        optimizedOrder = ArrayList<Location>()
        optimizedOrder.addAll(tripInfo.selectedLocations)
        tripLocationsText.text = "Trip locations (${draggableTripLocationAdapter.itemsList.size - 2})"

        googleMap?.let { plotMarkers(it, draggableTripLocationAdapter.itemsList) }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        tripInfo = context as TripInfo
        tripActionType = context as TripActionType
        suggestionsTripInfo = context as? SuggestionsTripInfo
    }

    override fun onDestroy() {
        //updateSelectedPlaces()
        tripInfo.onSelectedPlacesUpdated()
        super.onDestroy()
    }

    override fun scrollTo(position: Int): Boolean {
        if (position < 0 || position > draggableTripLocationAdapter.itemCount) {
            return false
        } else {
            draggableRecyclerView.scrollToPosition(position)
            return true
        }
    }

    override fun setBackGroundItem(position: Int): Unit {

        draggableTripLocationAdapter?.setBackgroundItem(position)

    }

    fun clickLocationOnTripRearrangement(_position: Int) {

        googleMap?.run {

            // clear()
            val iconGenerator = IconGenerator(context)

            var isEqual = false

            if ( draggableTripLocationAdapter.itemsList.size>0 &&
                    draggableTripLocationAdapter.itemsList[0].title.equals(draggableTripLocationAdapter.itemsList
                            [draggableTripLocationAdapter.itemsList.lastIndex].title)) {

                isEqual = true
            }
            var builder = LatLngBounds.Builder();

            draggableTripLocationAdapter.itemsList?.forEachIndexed { index, location ->
                val markerText: String
                val markerColor: Int
                val markerOptions = MarkerOptions().position(LatLng(location.latitude, location.longitude))
                        .title(location.title)
                        .snippet(location.address)

                if (index == 0) {
                    markerColor = colorPrimaryDark
                    markerText = "S"
                } else if (index == draggableTripLocationAdapter.itemsList.size - 1) {
                    markerColor = colorPrimaryDark
                    markerText = "E"
                } else if (location.status == LocationStatus.ADDED) {
                    markerColor = colorGreen
                    markerText = index.toString()
                } else if (location.status == LocationStatus.REMOVED) {
                    markerColor = colorRed
                    markerText = index.toString()
                } else {
                    markerColor = colorBlue
                    markerText = index.toString()
                }

                iconGenerator.setColor(markerColor)
                val icon = iconGenerator.makeIcon(markerText)


                if (index == 0) {
                    if (isEqual) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    }

                } else if (index == draggableTripLocationAdapter.itemsList.size - 1) {
                    if (isEqual) {
                        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.same_location))
                    } else {
                        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                    }
                } else {

                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
                }





            /*    val markerOptions = MarkerOptions()
                        .position(LatLng(location.latitude, location.longitude))
                        .title(location.title)

                        .icon(BitmapDescriptorFactory.fromBitmap(icon))
*/

                addMarker(markerOptions).tag = index

                if (index == _position) {

                    addMarker(markerOptions).showInfoWindow()
                    val update = CameraUpdateFactory.newLatLng(LatLng(location.latitude, location.longitude))
                    animateCamera(update)


                }

            }

//            if(isCurrentLocation && selectedLocations?.size!=0) {
//
//                val markerOptions = MarkerOptions()
//                        .position(LatLng(latLng.latitude, latLng.longitude))
//                        .title( LocationHelper.getAddress(applicationContext, latLng)?.locality)
//                        .snippet(LocationHelper.getAddress(applicationContext, latLng)?.getAddressLine(0))
//
//                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(currentLocationColor))
//                addMarker(markerOptions).tag = -1
//
//
//
//            }


        }

    }
}