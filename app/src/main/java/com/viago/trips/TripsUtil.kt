package com.viago.trips

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.models.BaseListModel
import com.viago.trips.annotations.LocationStatus
import com.viago.trips.interfaces.TripInfo
import com.viago.trips.models.CreateTripRequest
import com.viago.trips.models.Keyword
import com.viago.trips.models.SuggestionDetails
import com.viago.trips.models.suggestions.TripPostSuggestionRequest
import com.viago.trips.models.suggestions.TripSuggestion
import com.viago.utils.ApiUtils
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.getFailureMessage
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import com.viago.baseclasses.receivers.ConnectivityReceiver.isConnected
import android.net.NetworkInfo
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.util.Log
import com.viago.R
import com.viago.baseclasses.receivers.ConnectivityReceiver


/**
 * Created by mayank on 8/9/17.
 */
object TripsUtil {
    private val tripsService = ApiUtils.retrofitInstance.create(TripsService::class.java)

    fun getCategories(token: String, onSuccess: (List<Category>) -> Unit, onFailure: (String) -> Unit) {


       // onSuccess("[{"id":1,"title":"Family"},{"id":2,"title":"Friends"},{"id":3,"title":"Adventure"},{"id":4,"title":"Culture"}]")


        var familyCategory = Category(1, "Family", R.drawable.family,false);

        var friendsCategory = Category(2, "Friends",R.drawable.friends,false);

        var adventureCategory = Category(3, "Adventure",R.drawable.adventure,false);

        var spiritual = Category(4, "Spiritual",R.drawable.spiritual,false);

        var  culture= Category(5, "Culture",R.drawable.culture,false);

        var romantic = Category(6, "Romantic",R.drawable.romantic,false);

        var nature = Category(7, "Nature",R.drawable.nature,false);

        var leisure = Category(8, "Leisure",R.drawable.leisure,false);

        var categories = listOf<Category>(familyCategory,friendsCategory,adventureCategory, spiritual,
                culture,romantic,nature,leisure)

        onSuccess(categories)


       /* tripsService
                .getTripCategories(token)
                .enqueue(object : Callback<List<Category>> {
                    override fun onResponse(call: Call<List<Category>>?, response: Response<List<Category>>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Cannot obtain categories. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<List<Category>>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot obtain categories. Please try again")
                        }
                    }
                })*/
    }

    fun createTrip(token: String, createTripRequest: CreateTripRequest, onSuccess: (MyTrip) -> Unit, onFailure: (String) -> Unit) {
        tripsService.createTrip(token, createTripRequest)
                .enqueue(object : Callback<MyTrip> {
                    override fun onResponse(call: Call<MyTrip>?, response: Response<MyTrip>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Cannot save trip. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<MyTrip>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot save trip. Please try again")
                        }
                    }
                })
    }

    fun updateTrip(token: String, tripId: Long, createTripRequest: CreateTripRequest, onSuccess: (CreateTripRequest) -> Unit, onFailure: (String) -> Unit) {
        tripsService.updateTrip(token, tripId, createTripRequest)
                .enqueue(object : Callback<CreateTripRequest> {
                    override fun onResponse(call: Call<CreateTripRequest>?, response: Response<CreateTripRequest>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Cannot save trip. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<CreateTripRequest>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot save trip. Please try again")
                        }
                    }
                })
    }

    fun declineSuggestion(token: String, suggestionId: Long, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        tripsService.declineSuggestion(token, suggestionId)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess()
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Cannot decline suggestion. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot decline suggestion. Please try again")
                        }
                    }
                })
    }

    fun getMyTrips(token: String, onSuccess: (BaseListModel<MyTrip>) -> Unit, onFailure: (String) -> Unit) {
        getTrips(tripsService.getTrips(token), onSuccess, onFailure);
    }

    fun getMoreMyTrips(token: String, nextUrl: String, onSuccess: (BaseListModel<MyTrip>) -> Unit, onFailure: (String) -> Unit) {
        getTrips(tripsService.getMoreTrips(token, nextUrl), onSuccess, onFailure)
    }

    private fun getTrips(getTripCall: Call<BaseListModel<MyTrip>>, onSuccess: (BaseListModel<MyTrip>) -> Unit, onFailure: (String) -> Unit) {
        getTripCall
                .enqueue(object : Callback<BaseListModel<MyTrip>> {
                    override fun onResponse(call: Call<BaseListModel<MyTrip>>?, response: Response<BaseListModel<MyTrip>>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                Log.d("myTag01","Cannot obtain trips. Please try again")
                              //  onFailure(getFailureMessage(response?.errorBody(), "Cannot obtain trips. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<BaseListModel<MyTrip>>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot obtain trips. Please try again")
                        }
                    }
                })
    }

    fun getTripDetails(token: String, tripId: Long, onSuccess: (MyTrip) -> Unit, onFailure: (String) -> Unit) {
        tripsService
                .getTrip(token, tripId)
                .enqueue(object : Callback<MyTrip> {
                    override fun onResponse(call: Call<MyTrip>?, response: Response<MyTrip>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Cannot obtain trip details. Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<MyTrip>?, t: Throwable?) {
                        if (ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot obtain trip details. Please try again")
                        }
                    }
                })
    }

    fun deleteTrip(token: String, tripId: Long, onSuccess: () -> Unit, onFailure: () -> Unit) {
        tripsService.deleteTrip(token, tripId).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response?.isSuccessful == true) {
                    onSuccess()
                } else {
                    onFailure()
                }
            }

            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                onFailure()
            }

        })
    }


    fun postSuggestions(token: String, tripId: Long, tripPostSuggestionRequest: TripPostSuggestionRequest, onSuccess: () -> Unit, onFailure: (String) -> Unit) {
        tripsService.postSuggestions(token, tripId, tripPostSuggestionRequest)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess()
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Error posting suggestions, Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        if(ConnectivityReceiver.isConnected()) {
                            onFailure("Error posting suggestions, Please try again")
                        }
                    }

                })
    }

    fun getTripSuggestions(token: String, tripId: Long, onSuccess: (BaseListModel<TripSuggestion>) -> Unit, onFailure: (String) -> Unit) {
        getSuggestions(tripsService.getTripSuggestions(token, tripId), onSuccess, onFailure)
    }

    fun getMoreTripSuggestions(token: String, nextUrl: String, onSuccess: (BaseListModel<TripSuggestion>) -> Unit, onFailure: (String) -> Unit) {
        getSuggestions(tripsService.getMoreSuggestions(token, nextUrl), onSuccess, onFailure)
    }

    private fun getSuggestions(getSuggestionsCall: Call<BaseListModel<TripSuggestion>>, onSuccess: (BaseListModel<TripSuggestion>) -> Unit, onFailure: (String) -> Unit) {
        getSuggestionsCall
                .enqueue(object : Callback<BaseListModel<TripSuggestion>> {
                    override fun onResponse(call: Call<BaseListModel<TripSuggestion>>?, response: Response<BaseListModel<TripSuggestion>>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if(ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Error posting suggestions, Please try again"))

                            }}
                    }

                    override fun onFailure(call: Call<BaseListModel<TripSuggestion>>?, t: Throwable?) {
                        if(ConnectivityReceiver.isConnected()) {
                            onFailure("Error posting suggestions, Please try again")
                        }
                    }

                })
    }

    fun getSuggestionDetails(token: String, suggestionId: Long, onSuccess: (SuggestionDetails) -> Unit, onFailure: (String) -> Unit) {
        tripsService.getSuggestionDetails(token, suggestionId)
                .enqueue(object : Callback<SuggestionDetails> {
                    override fun onResponse(call: Call<SuggestionDetails>?, response: Response<SuggestionDetails>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            if (ConnectivityReceiver.isConnected()) {
                                onFailure(getFailureMessage(response?.errorBody(), "Error getting suggestions, Please try again"))
                            }
                        }
                    }

                    override fun onFailure(call: Call<SuggestionDetails>?, t: Throwable?) {
                        if(ConnectivityReceiver.isConnected()) {
                            onFailure("Error getting suggestions, Please try again")
                        }
                    }

                })
    }

    internal fun makeCreateTripRequestObject(tripInfo: TripInfo, suggestionId: Long? = null): CreateTripRequest {
        val selectedLocations = tripInfo.selectedLocations
        tripInfo.source?.let { selectedLocations.add(0, it) }
        tripInfo.destination?.let { selectedLocations.add(it) }

        selectedLocations.removeAll { location: Location -> location.status == LocationStatus.REMOVED }

        return CreateTripRequest(
                tripInfo.keyword,
                tripInfo.title,
                tripInfo.distance,
                tripInfo.duration,
                tripInfo.placeName,
                "%.4f".format(tripInfo.latLng.latitude).toDouble(),
                "%.4f".format(tripInfo.latLng.longitude).toDouble(),
                tripInfo.categories,
                selectedLocations,
                suggestionId
        )
    }

    fun getKeywords(onSuccess: (List<Keyword>) -> Unit, onFailure: (String) -> Unit) {
        tripsService
                .getKeywords()
                .enqueue(object : Callback<List<Keyword>> {
                    override fun onResponse(call: Call<List<Keyword>>?, response: Response<List<Keyword>>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                            onFailure(getFailureMessage(response?.errorBody(), "Cannot obtain categories. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<List<Keyword>>?, t: Throwable?) {
                        if(ConnectivityReceiver.isConnected()) {
                            onFailure("Cannot obtain categories. Please try again")
                        }
                    }
                })
    }

    fun getSavedTrips(): List<MyTrip> {
        var savedTrips: List<MyTrip> = ArrayList<MyTrip>()
        val savedMyTripsString = SharedPrefsUtils.getStringPreference(Constants.KEY_MY_TRIPS)
        if (savedMyTripsString != null) {
            savedTrips = Gson().fromJson(savedMyTripsString, object : TypeToken<List<MyTrip>>() {
            }.type)
        }
        return savedTrips
    }


   /* private fun isOnline(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = cm.activeNetworkInfo
        return if (ni != null && ni.isConnected) true else false

    }*/
}