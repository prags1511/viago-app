package com.viago.trips.interfaces

import com.google.android.gms.maps.model.LatLng
import com.viago.trips.Location

/**
 * Created by mayank on 9/9/17.
 */
interface TripInfo {
    val selectedLocations: ArrayList<Location>
    fun onSelectedPlacesUpdated()
    var source: Location?
    var destination: Location?
    var duration: String
    var distance: String
    //Place Details
    var latLng: LatLng
    var placeName: String
    var keyword: String

    var categories: List<Int>
    var title: String

    fun onSearchValuesChanged()
}