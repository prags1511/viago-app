package com.viago.trips.interfaces

import com.viago.trips.Location

/**
 * Created by mayank on 25/9/17.
 */
interface SuggestionsTripInfo {
    var tripId: Long
    val originalTripLocations: ArrayList<Location>
    var suggestionId: Long?
}