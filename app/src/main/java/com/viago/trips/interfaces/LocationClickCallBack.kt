package com.viago.trips.interfaces

import com.viago.trips.annotations.PlaceSearchProvider
import com.viago.trips.models.Place

/**
 * Created by Balwant on 10/02/18.
 */
interface LocationClickCallBack {

    fun clickLocation(position: Int)

}