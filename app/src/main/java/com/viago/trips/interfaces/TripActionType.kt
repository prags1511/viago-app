package com.viago.trips.interfaces

import androidx.annotation.IntDef
import androidx.annotation.LongDef

/**
 * Created by mayank on 12/9/17.
 */
interface TripActionType {

    @ActionType
    var actionType: Int

    companion object {
        const val CREATING = 0
        const val SUGGESTING = 1
        const val VIEWING_SUGGESTIONS = 2
        const val EDITING_TRIP = 3

        @Retention(AnnotationRetention.SOURCE)
        @LongDef(CREATING.toLong(), SUGGESTING.toLong(), VIEWING_SUGGESTIONS.toLong(), EDITING_TRIP.toLong())
        annotation class ActionType
    }
}