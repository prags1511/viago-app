package com.viago.trips.interfaces

import com.viago.trips.models.Place

/**
 * Created by thinksysuser on 5/9/18.
 */
interface PlaceInfo {

    abstract fun onSearchFailed()
    abstract fun onSearchSuccess(places: Place)
}