package com.viago.trips.interfaces

import com.viago.trips.annotations.PlaceSearchProvider
import com.viago.trips.models.Place

/**
 * Created by mayank on 8/8/17.
 */
interface PlacesSearchResultCallback {
    fun onPlacesSearchResultObtained(
            @PlaceSearchProvider provider: Long,
            places: List<Place>
    )
}