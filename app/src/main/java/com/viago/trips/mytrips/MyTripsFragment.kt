package com.viago.trips.mytrips

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.Toast
import bindView
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.baseclasses.NavItemClickEvent
import com.viago.extensions.dpToPx
import com.viago.extensions.hide
import com.viago.extensions.show
import com.viago.extensions.showToast
import com.viago.listeners.ContextualModeInteractor
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.profile.MediaDBUtil
import com.viago.trips.MyTrip
import com.viago.trips.MyTripCreatedEvent
import com.viago.trips.MyTripUpdatedEvent
import com.viago.trips.TripsUtil
import com.viago.trips.tripdetails.TripDetailsActivity
import com.viago.utils.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by mayank on 31/7/17.
 */
class MyTripsFragment : BaseFragment() {
    private var interactor: ContextualModeInteractor? = null
    private val recyclerView: RecyclerView by bindView(R.id.recycler_view)
    private val noTripsFoundContainer: View by bindView(R.id.no_trips_found_container)
    private val addTrip: View by bindView(R.id.add_trip)

    private val userToken by lazy { UserUtil.getUserToken() }
    private var nextUrl: String? = null
    private lateinit var myTripsAdapter: MyTripsAdapter
    private var loadedFromCached: Boolean = false

    val onSuccess = { baseListModel: BaseListModel<MyTrip> ->
        if (loadedFromCached) {
            myTripsAdapter.clear()
            loadedFromCached = false
        }
        myTripsAdapter.addItemsAtBottom(baseListModel.results)
        nextUrl = baseListModel.next
        if (nextUrl.isNullOrEmpty()) {
            saveTripsOffline(myTripsAdapter.itemsList)
        } else {
            loadMore()
        }
//        if (baseListModel.count == 0) {     commented by
//       if (baseListModel.count == 0) {
//            recyclerView.hide()
//            noTripsFoundContainer.show()
//            addTrip.setOnClickListener {
//                EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_create_trip))
//            }
//        }

        if (baseListModel.results == null || baseListModel.results.isEmpty()) {
            recyclerView.hide()
            noTripsFoundContainer.show()
            addTrip.setOnClickListener {
                EventBus.getDefault().postSticky(NavItemClickEvent(R.id.action_create_trip))
            }
        }
    }

    private fun loadMore() {
        nextUrl?.let { TripsUtil.getMoreMyTrips(userToken, it, onSuccess, onFailure) }
    }

    val onFailure: (String) -> Unit = {
        context!!.showToast(it)
    }

    private fun saveTripsOffline(itemsList: List<MyTrip>) {
        SharedPrefsUtils.setStringPreference(Constants.KEY_MY_TRIPS, Gson().toJson(itemsList, object : TypeToken<List<MyTrip>>() {
        }.type))
    }

    @LayoutRes
    override fun getLayoutRes(): Int = R.layout.fragment_my_trips

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        EventBus.getDefault().register(this)

        try {

            myTripsAdapter = MyTripsAdapter({ myTrip: MyTrip? ->
                myTrip?.let {
                    if (context != null) {

                        startActivity(TripDetailsActivity.getIntent(context!!, myTrip, false))

                    }
                }


            }, interactor!!)

            with(recyclerView) {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                addItemDecoration(SpacesItemDecoration(dpToPx(4)))
                adapter = myTripsAdapter
//            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
//                override fun onLoadMore(page: Int, totalItemsCount: Int) {
//                    nextUrl?.let { TripsUtil.getMoreMyTrips(userToken, it, onSuccess, onFailure) }
//                }
//            })
            }

            val savedTrips: List<MyTrip> = TripsUtil.getSavedTrips()
            if (!savedTrips.isEmpty()) {
                myTripsAdapter.addItemsAtBottom(savedTrips)
                loadedFromCached = true
            }
            TripsUtil.getMyTrips(userToken, onSuccess, onFailure)

        }catch (e :Exception){

            e.stackTrace
        }
    }


    override fun onDestroyView() {
        EventBus.getDefault().unregister(this)
        super.onDestroyView()
    }

    companion object {
        public fun newInstance(interactor: ContextualModeInteractor): MyTripsFragment {
            val myTripFragment: MyTripsFragment = MyTripsFragment()
            myTripFragment.interactor = interactor
            return myTripFragment
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onTripAdded(myTripCreatedEvent: MyTripCreatedEvent?) {
        myTripCreatedEvent?.run {
            myTripsAdapter.registerAdapterDataObserver(object : RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    super.onItemRangeInserted(positionStart, itemCount)
                    if (positionStart == 0) recyclerView.smoothScrollToPosition(0)
                    myTripsAdapter.unregisterAdapterDataObserver(this)
                }
            })
            myTripsAdapter.addItemOnTop(myTrip)

            noTripsFoundContainer.hide()

            recyclerView.show()

           // TripsUtil.getMyTrips(userToken, onSuccess, onFailure)

            saveTripsOffline(myTripsAdapter.itemsList)
            if (share) {
                ShareUtil.shareTrip(context!!, myTrip.title, myTrip.id)
            }
            if (start) {
                var toastMsg: String = ""
                if (GoogleMapsUtil().openGoogleMapsDirection(context!!, commaSeparatedLatLngList)) {
                    MediaDBUtil.saveStartTimeOf(myTrip.id)
                    toastMsg = "You can make this trip available offline!";
                } else {
                    toastMsg = "Something went wrong with the trip, try again!";
                }
                Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show()
            }
        }
        EventBus.getDefault().removeStickyEvent(myTripCreatedEvent)
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    fun onTripUpdated(myTripUpdatedEvent: MyTripUpdatedEvent?) {
        myTripUpdatedEvent?.createTripRequest?.let {
            val index = myTripsAdapter.itemsList.indexOfFirst { myTrip -> myTrip.id == it.id }
            if (index != -1) {
                TripsUtil.getTripDetails(userToken, it.id!!, {
                    myTripsAdapter.updateItem(index, it)
                    saveTripsOffline(myTripsAdapter.itemsList)
                }, {})
            }
        }
        EventBus.getDefault().removeStickyEvent(myTripUpdatedEvent)
    }

    fun contextualModeDisabled() {
        myTripsAdapter.contextualModeDisabled()
    }

    fun getSelectedTrips(): List<MyTrip> = myTripsAdapter.getSelectedItems()

    fun refresh() {
        myTripsAdapter.clear()
        TripsUtil.getMyTrips(userToken, onSuccess, onFailure)
    }

}