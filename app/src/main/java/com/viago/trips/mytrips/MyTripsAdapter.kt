package com.viago.trips.mytrips

import android.app.ProgressDialog
import android.content.Context
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import bindView
import com.google.android.gms.maps.model.LatLng
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.OnDrawableReadyListener
import com.viago.extensions.inflate
import com.viago.extensions.loadImage
import com.viago.extensions.showToast
import com.viago.listeners.ContextualModeInteractor
import com.viago.login.UserUtil
import com.viago.profile.MediaDBUtil
import com.viago.trips.Location
import com.viago.trips.MyTrip
import com.viago.trips.TripsUtil
import com.viago.trips.tripdetails.TripDetailsActivity
import com.viago.utils.GoogleMapsUtil
import com.viago.utils.ShareUtil
import com.viago.views.RoundCornerImageView

/**
 * Created by mayank on 18/8/17.
 */
class MyTripsAdapter(val onMyTripSelected: (myTrip: MyTrip) -> Unit, val interactor: ContextualModeInteractor) : EasyRecyclerViewAdapter<MyTrip>() {

    private val userToken by lazy { UserUtil.getUserToken() }
    private var selectedItemsCount: Int = 0

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyTripViewHolder(parent.inflate(R.layout.item_my_trips))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder?, myTrip: MyTrip, position: Int) {
        if (holder is MyTripViewHolder) holder.bind(myTrip)
    }

    inner class MyTripViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val image by bindView<RoundCornerImageView>(R.id.image)
        private val removeIndicator by bindView<View>(R.id.toBeRemoved)
        private val background by bindView<ImageView>(R.id.background)
        private val tripName by bindView<TextView>(R.id.trip_name)
        private val tripDuration by bindView<TextView>(R.id.trip_duration)
        private val tripType by bindView<TextView>(R.id.trip_type)
        private val suggestions by bindView<TextView>(R.id.suggestions)
        private val startTrip by bindView<TextView>(R.id.start_trip)
        private val share by bindView<View>(R.id.share)

        private val progressDialog: ProgressDialog by lazy {
            ProgressDialog(itemView.context).apply {
                setMessage("Starting Trip...")
                isIndeterminate = true
                setCancelable(false)
            }
        }

        init {
            itemView.setOnClickListener {
                if (interactor.isContextualMode()) {
                    toggleSelection(getItem(adapterPosition), adapterPosition)
                } else {
                    val myTrip = getItem(adapterPosition)
                    onMyTripSelected(myTrip)
                }
            }
            itemView.setOnLongClickListener {
                interactor.enableContextualMode()
                toggleSelection(getItem(adapterPosition), adapterPosition)
                return@setOnLongClickListener true
            }
            share.setOnClickListener {
                val myTrip = getItem(adapterPosition)
                ShareUtil.shareTrip(itemView.context, myTrip.title, myTrip.id)
            }
            startTrip.setOnClickListener { startTrip(itemView) }
        }

        private fun startTrip(itemView: View) {
            progressDialog.show()
            if (getItem(adapterPosition).locations != null && !getItem(adapterPosition).locations!!.isEmpty()) {
                progressDialog.dismiss()
                startTripWithLocations(itemView.context, getItem(adapterPosition).locations)
            } else {
                val tripId = getItem(adapterPosition).id
                TripsUtil.getTripDetails(userToken, tripId, {
                    MediaDBUtil.saveStartTimeOf(tripId)
                    progressDialog.dismiss()
                    startTripWithLocations(itemView.context, it.locations)
                }, {
                    progressDialog.dismiss()
                    itemView.context.showToast(it)
                })
            }
        }


        internal fun bind(myTrip: MyTrip) {
            removeIndicator.visibility = if (myTrip.selected) View.VISIBLE else View.GONE
            image.loadImage(myTrip.image, onDrawableReadyListener = object : OnDrawableReadyListener {
                override fun onDrawableReady(drawable: Drawable) {
                    background.setImageDrawable(drawable)
                }
            })
            tripName.text = "${myTrip.title} (${myTrip.locations!!.size})"
            tripDuration.text = "${myTrip.duration} Trip"
            tripType.text = myTrip.searched_location_type.replace("_", " ").capitalize()
            //TODO
            val noOfSuggestions = myTrip.suggestions_count.coerceAtLeast(0)
            suggestions.text = "$noOfSuggestions ${if (noOfSuggestions == 1L) "Person" else "People"} Suggested"
        }
    }

    private fun startTripWithLocations(context: Context, locations: List<Location>?) {
        var toastMsg: String = ""
        if (GoogleMapsUtil().openGoogleMapsDirection(context, locations!!.map { "${it.latitude},${it.longitude}" })) {
            toastMsg = "You can make this trip available offline!";
        } else {
            toastMsg = "Something went wrong with the trip, try again!";
        }
        Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show()
    }

    private fun toggleSelection(item: MyTrip?, pos: Int) {
        item!!.selected = !item.selected
        notifyItemChanged(pos)
        if (item.selected) {
            selectedItemsCount++
        } else {
            selectedItemsCount--
        }
        interactor.updateSelectedCount(selectedItemsCount)
    }

    fun contextualModeDisabled() {
        for (item in itemsList)
            item.selected = false
        selectedItemsCount = 0
        notifyDataSetChanged()
    }

    fun getSelectedItems(): List<MyTrip> {
        val selectedItems = ArrayList<MyTrip>()
        for (item in itemsList)
            if (item.selected)
                selectedItems.add(item)
        return selectedItems
    }
}