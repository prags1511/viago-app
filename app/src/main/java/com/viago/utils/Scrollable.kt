package com.viago.utils

/**
 * Created by mayank on 23/8/17.
 */
interface Scrollable {
    fun scrollTo(position: Int): Boolean

    fun setBackGroundItem(position: Int): Unit
}