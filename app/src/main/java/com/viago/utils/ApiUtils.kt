package com.viago.utils


import com.viago.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * object is used to create Singleton
 */

object ApiUtils {
     private val BASE_URL = "https://api1.viagoweb.com/spring/"
    //private val BASE_URL = "http://ec2-13-232-157-122.ap-south-1.compute.amazonaws.com:8080/spring/";
    //private val BASE_URL = "http://192.168.100.4:8080/spring/";
    private val addLoggingInterceptor = true


    val retrofitInstance: Retrofit by lazy {
        Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .apply {
                    if (BuildConfig.DEBUG)
                        client(OkHttpClient.
                                Builder()
                                .apply {
                                    if (addLoggingInterceptor)
                                        addInterceptor(HttpLoggingInterceptor(HttpLoggingInterceptor
                                                .Logger { Timber.tag("OkHttp").d(it) }).setLevel(HttpLoggingInterceptor.Level.BODY
                                        ))
                                }
                                .connectTimeout(30, TimeUnit.SECONDS)
                                .build()
                        )
                }
                .build()
    }
}
