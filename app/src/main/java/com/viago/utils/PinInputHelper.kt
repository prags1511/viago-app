package com.viago.utils

import android.app.Activity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import com.viago.extensions.openKeyboard

/**
 * Created by mayank on 2/8/16.
 */
class PinInputHelper(activity: Activity, private val otp: List<EditText>) : TextWatcher, View.OnFocusChangeListener {

    private var currentViewNum: Int = 0

    init {
        otp.forEach {
            it.addTextChangedListener(this)
            it.onFocusChangeListener = this
        }
        otp[0].requestFocus()
        activity.openKeyboard()
    }

    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) { }

    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
        val prev = currentViewNum - 1
        val current = currentViewNum
        val next = currentViewNum + 1

        if (charSequence.isEmpty()) {
            if (current > 0) {
                otp[current].isFocusableInTouchMode = false
                otp[prev].isFocusableInTouchMode = true
                otp[prev].requestFocus()
                otp[prev].setSelection(otp[prev].text.length)
            }
        } else if (charSequence.length == 2) {
            if (current < otp.size - 1) {
                if (TextUtils.isEmpty(otp[next].text)) {
                    otp[next].isFocusableInTouchMode = true
                    otp[current].isFocusableInTouchMode = false
                    otp[current].setText(charSequence[0].toString())
                    otp[next].setText(charSequence[1].toString())
                    otp[next].requestFocus()
                    otp[next].setSelection(otp[next].text.length)
                } else {
                    otp[current].setText(otp[current].text[0].toString())
                }
            }
        }
    }

    override fun afterTextChanged(editable: Editable) {

    }

    override fun onFocusChange(view: View, b: Boolean) {
        otp.indexOf(view).let {
            if (it != -1) currentViewNum = it
        }
    }
}
