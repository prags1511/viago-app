package com.viago.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;

//import com.crashlytics.android.Crashlytics;

import java.util.List;

/**
 * Created by mayank on 5/7/17.
 */

public class GoogleMapsUtil {

    public boolean openGoogleMapsDirection(@NonNull Context context, List<String> latLngStrings) {
        if (latLngStrings.size() > 2) {
            final String BASE_URI = "https://www.google.com/maps/dir/?api=1";
            String uri = BASE_URI + "&origin=" + latLngStrings.get(0);
            //String uri = BASE_URI + "&destination=" + latLngStrings.get(latLngStrings.size() - 1);

            if (latLngStrings.size() > 1) {
                for (int i = 1; i < latLngStrings.size() - 1; i++) {
                    if (i == 1)
                        uri += "&waypoints=" + latLngStrings.get(0);
                    else
                        uri = uri + "|" + latLngStrings.get(i);
                }
            }

            if (latLngStrings.size() > 2)
                uri += "&destination=" + latLngStrings.get(latLngStrings.size() - 1);

           // Crashlytics.log(uri);
            Log.e("CHECK", uri);

            Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (intent.resolveActivity(context.getPackageManager()) != null)
                context.startActivity(intent);
            return true;
        } else {
            return false;
        }
    }
}
