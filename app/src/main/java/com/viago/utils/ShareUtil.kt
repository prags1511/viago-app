package com.viago.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import com.viago.extensions.loadImage
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import io.branch.indexing.BranchUniversalObject
import io.branch.referral.BranchError
import io.branch.referral.util.LinkProperties


/**
 * Created by mayank on 20/9/17.
 */
object ShareUtil {

    // This is the function to handle sharing when a user clicks the share button
    fun shareTrip(context: Context, tripTitle: String, tripId: Long) {
        context.showToast("Sharing Trip...")

        var suggestions:String = "Hi, Can you give me some suggestions"
        UserUtil.getUser()?.let {

              if(it.name==null){

                  suggestions = "Hi, Can you give me some suggestions";

              }else {

                  suggestions = "Hi, "+it.name+" has shared trip with you. Feel free to give suggestions with Viago!"

              }
        }



        // Initialize a Branch Universal Object for the page the user is viewing
        val branchUniversalObject = BranchUniversalObject()
                .setCanonicalIdentifier("trip_id_$tripId")
                .setTitle(tripTitle)
                .setContentDescription(suggestions)
                .setContentImageUrl("https://cdn.branch.io/branch-assets/1570701290039-og_image.png")
                .addContentMetadata("trip_id", tripId.toString())
//                .addContentMetadata("\$desktop_url", "www.facebook.com")

        // Trigger a view on the content for analytics tracking
        branchUniversalObject.registerView()

        // List on Google App Indexing
        branchUniversalObject.listOnGoogleSearch(context)

        // Create your link properties
        // More link properties available at https://dev.branch.io/getting-started/configuring-links/guide/#link-control-parameters
        val linkProperties = LinkProperties()
                .setFeature("sharing")
                .addControlParameter("\$desktop_url", "https://www.viagoweb.com/suggestions/$tripId;params=suggestion%3Dyes")

        branchUniversalObject.generateShortUrl(context, linkProperties) { url: String?, error: BranchError? ->
            if (error == null) {
                GeneralUtils.openShareIntent(context, null, url)
                SharedPrefsUtils.setIntegerPreference(Constants.KEY_COUNT_SHARE, (SharedPrefsUtils.getIntegerPreference(Constants.KEY_COUNT_SHARE, 0) + 1))
            }
        }
/*
        // Customize the appearance of your share sheet
        val shareSheetStyle = ShareSheetStyle(context, "Check this out!", "Hey friend - I know you'll love this: ")
                .setCopyUrlStyle(ContextCompat.getDrawable(context, android.R.drawable.ic_menu_send), "Copy link", "Link added to clipboard!")
                .setMoreOptionStyle(ContextCompat.getDrawable(context, android.R.drawable.ic_menu_search), "Show more")
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.FACEBOOK)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.EMAIL)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.MESSAGE)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.TWITTER)
                .addPreferredSharingOption(SharingHelper.SHARE_WITH.WHATS_APP)

        // Show the share sheet for the content you want the user to share. A link will be automatically created and put in the message.
        branchUniversalObject.showShareSheet(activity, linkProperties, shareSheetStyle, object : Branch.BranchLinkShareListener {
            override fun onShareLinkDialogLaunched() {}
            override fun onShareLinkDialogDismissed() {}
            override fun onChannelSelected(channelName: String) {}
            override fun onLinkShareResponse(sharedLink: String, sharedChannel: String, error: BranchError) {
                // The link will be available in sharedLink
            }
        })*/
    }


    fun openFacebookPage(context: Context) {
        val url = "https://www.facebook.com/viagoweb1"
        var uri = Uri.parse(url)
        try {
            val applicationInfo = context.packageManager.getApplicationInfo("com.facebook.katana", 0)
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url)
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        context.startActivity(Intent(Intent.ACTION_VIEW, uri))
    }

    fun openTwitterPage(context: Context) {
        context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/Viagoweb")))
    }

    fun openInstagramPage(context: Context) {
        val url = "http://instagram.com/viagoweb"
        val intent = Intent(Intent.ACTION_VIEW)
        try {
            if (context.packageManager.getPackageInfo("com.instagram.android", 0) != null) {
                intent.data = Uri.parse(url)
                intent.`package` = "com.instagram.android"
            }
        } catch (ignored: PackageManager.NameNotFoundException) {
        }

        intent.data = Uri.parse(url)
        context.startActivity(intent)
    }
}