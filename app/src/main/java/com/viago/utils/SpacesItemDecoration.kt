package com.viago.utils

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * Created by mayank on 24/6/17.
 */
/**
 * https://github.com/lsjwzh/RecyclerViewPager/blob/master/app/src/main/java/com/lsjwzh/widget/recyclerviewpagerdeomo/SpacesItemDecoration.java
 */

class SpacesItemDecoration(private val mSpace: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.set(mSpace, mSpace, mSpace, mSpace)
    }
}
