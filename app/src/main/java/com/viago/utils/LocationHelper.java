package com.viago.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

public class LocationHelper implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = LocationHelper.class.getSimpleName();

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    public static final int REQUEST_LOCATION_SETTINGS = 1001;

    private Location lastLocation;

    // Google client to interact with Google API
    private GoogleApiClient googleApiClient;

    private Context context;

    private String latitude;

    private String longitude;
    private int retryCount = 3;
    private static boolean isLocationSaved = false;
    private OnLocationReceivedListener onLocationReceivedListener;


    public LocationHelper(Context context) {
        this.context = context;
    }

    public void build() {
        // First we need to check availability of play services
        try {
            final String appPackageName = "com.google.android.gms";
            int versionCode = context.getPackageManager().getPackageInfo(appPackageName, 0).versionCode;
//            if (versionCode < GoogleApiAvailability.GOOGLE_PLAY_SERVICES_VERSION_CODE){
            if (versionCode < 9452000) {
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        buildClient();
//        getLocation();
    }

    public void buildClient() {
        if (checkPlayServices()) {

            // Building the GoogleApi client
            buildGoogleApiClient();
        }
    }

    /**
     * Method to display the location on UI
     */
    private boolean getLocation() {
        Timber.d("getLocation");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return true;
        }
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastLocation == null) lastLocation = getLastKnownLocation();
        return saveLocation(lastLocation);
    }

    private boolean saveLocation(Location location) {
        Timber.d("saveLocation");
        lastLocation = location;
        if (lastLocation == null) {
            Timber.d("saveLocation: null");
            return false;
        } else {
            Timber.d("saveLocation: not null");
            latitude = String.valueOf(lastLocation.getLatitude());
            longitude = String.valueOf(lastLocation.getLongitude());
            SharedPrefsUtils.INSTANCE.setStringPreference(Constants.KEY_LATITUDE, latitude);
            SharedPrefsUtils.INSTANCE.setStringPreference(Constants.KEY_LONGITUDE, longitude);
//            Toast.makeText(context, "location saved\n" + latitude + " " + longitude, Toast.LENGTH_SHORT).show();
            if (onLocationReceivedListener != null)
                onLocationReceivedListener.onLocationReceivedSuccess(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()));
            isLocationSaved = true;
            return true;
        }
    }

    @Nullable
    public static Address getAddress(Context context, LatLng latLng) {
        Address address = null;
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0)
                address = addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }

    private Location getLastKnownLocation() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location l = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, "last known location, provider: " + provider + ", location: " + l);

            if (l == null) {
                continue;
            }
            if (bestLocation == null
                    || l.getAccuracy() < bestLocation.getAccuracy()) {
                Log.d(TAG, "found best last known location: " + l);
                bestLocation = l;
            }
        }
        if (bestLocation == null) {
            Log.d(TAG, "location not found");
            return null;
        } else {
            Log.d(TAG, "location found");
        }
        return bestLocation;
    }

    /**
     * Creating google api client object
     */
    private synchronized void buildGoogleApiClient() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        onStart();
//        onResume();
    }

    private void setupRequest() {

        Timber.d("setupRequest");

        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                Log.d(TAG, "status: " + status.getStatusCode());
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates(locationRequest);
//                        getLocation();
                        // All location settings are satisfied. The client can initialize location
                        // requests here.
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult((AppCompatActivity) context, REQUEST_LOCATION_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.
                        break;
                }
            }
        });

    }

    /**
     * Method to verify google play services on the device
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = googleApiAvailability
                .isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog((Activity) context, resultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(context.getApplicationContext(), "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }

    /**
     * Google api callback methods
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    @Override
    public void onConnected(Bundle arg0) {
        Timber.d("onConnected");
        if (!getLocation()) setupRequest();
        // Once connected with google api, get the location
    }

    protected void startLocationUpdates(LocationRequest locationRequest) {
        Timber.d("startLocationUpdates");
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (saveLocation(location)) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            retryCount = 0;
        } else if (retryCount == 0) {
            Toast.makeText(context, "Couldn't get the location.", Toast.LENGTH_SHORT).show();
            if (onLocationReceivedListener != null)
                onLocationReceivedListener.onLocationReceivedFailed();
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
        } else {
            retryCount--;
        }
    }

    @Override
    public void onConnectionSuspended(int arg0) {
    }

    private boolean areCallbacksRegistered = false;

    public void onPause() {
        Timber.d("onPause");
        if (googleApiClient != null) {
            unregisterCallbacks();
        }
    }

    private void unregisterCallbacks() {
        if (!areCallbacksRegistered) return;
        Timber.d("unregisterCallbacks");
        googleApiClient.unregisterConnectionCallbacks(this);
        googleApiClient.unregisterConnectionFailedListener(this);
        areCallbacksRegistered = false;
    }

    public void onResume() {
        Timber.d("onResume");
        if (googleApiClient != null) {
            unregisterCallbacks();
            registerCallbacks();
        }
    }

    private void registerCallbacks() {
        Timber.d("registerCallbacks");
        googleApiClient.registerConnectionCallbacks(this);
        googleApiClient.registerConnectionFailedListener(this);
        areCallbacksRegistered = true;
    }

    public void onStart() {
        Timber.d("onStart");
        if (googleApiClient != null) {
            googleApiClient.connect();
            Timber.d("connecting");
        }
    }

    public void onStop() {
        Timber.d("onStop");
        if (googleApiClient != null) {
            googleApiClient.disconnect();
            Timber.d("disconnecting");
        }
    }

    public void setOnLocationReceivedListener(OnLocationReceivedListener onLocationReceivedListener) {
        this.onLocationReceivedListener = onLocationReceivedListener;
    }

    public interface OnLocationReceivedListener {
        public void onLocationReceivedSuccess(LatLng latLng);
        public void onLocationReceivedFailed();
    }
}