package com.viago.utils

/**
 * object is used to create Singleton
 */

object Constants {
    const val KEY_IS_FIRST_GCM_TOKEN = "is first token"
    const val KEY_GCM_TOKEN = "gcm token"
    const val KEY_GCM_TOKEN_UPDATE_FAILED = "gcm token update failed"
    const val KEY_IS_NOTIFICATION_ENABLED = "notification enabled"
    const val USER_AUTH_KEY = "AUTH_KEY"
    const val KEY_NOTIFICATION_COUNT = "notificationMenuCountView"
    const val KEY_LOCATION = "location"
    const val KEY_LATITUDE = "latitude"
    const val KEY_LONGITUDE = "longitude"
    const val KEY_API_KEY = "api key"
    const val KEY_COUNT_SHARE = "sharecount"
    const val KEY_FIRST_OPTIMISE = "keyforstoptimise"
    const val KEY_TAP_TARGET_VIEW_SUGGESTION = "ttviewsuggestion"
    const val KEY_TAP_TARGET_ADD_LOCATION = "ttaddlocation"
    const val KEY_TAP_TARGET_SEARCH_LOCATION = "ttsearchlocation"
    const val KEY_TAP_TARGET_MULTI_SEARCH = "ttmultisearch"
    const val KEY_TAP_TARGET_SEARCH_NEW_LOCATION = "ttsearchnewlocation"
    const val KEY_SUGGESTION_READ = "suggestion_read"
    const val KEY_NOTIFICATION_READ = "notification_read"
    const val KEY_TEMP_LOCATIONS = "templocations"
    const val KEY_MY_TRIPS = "mytripskey"
    const val KEY_YELP_ACCESS_TOKEN = "yelp access token"
    const val KEY_YELP_EXPIRY = "yelp expiry"
    const val CREATETRIPACTIVITY_BACK = "createTripActivityback"

    //Append trip id to key when using
    const val KEY_SAVED_TRIP = "saved trip"
}
