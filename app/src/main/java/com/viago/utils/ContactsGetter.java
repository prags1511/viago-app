package com.viago.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;

import com.viago.models.Contact;

import java.util.ArrayList;

/**
 * Created by mayank on 21/8/16.
 */
public class ContactsGetter {

    public ContactsGetter(Context context, ContactsLoadListener contactsLoadListener) {
        loadContacts(context, contactsLoadListener);
    }

    private void loadContacts(Context context, ContactsLoadListener contactsLoadListener){
        ArrayList<Contact> contactsData = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED){
            return;
        }
        Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        if (cursor != null && !cursor.isClosed()) {
            cursor.getCount();
            while (cursor.moveToNext()){
//                String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                int hasPhoneNumber = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                if (hasPhoneNumber == 1) {
                    String phoneNum = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    contactsData.add(new Contact(name, phoneNum));
                }
            }
            cursor.close();
        }
        contactsLoadListener.onLoadFinished(contactsData);
    }

    public interface ContactsLoadListener {
        void onLoadFinished(ArrayList<Contact> contactsData);
    }
}
