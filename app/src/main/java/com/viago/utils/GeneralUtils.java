package com.viago.utils;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.appbar.AppBarLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by mayank on 11/7/16.
 */
public class GeneralUtils {

    private static String apiKey = null;
    private static int primaryKey;
    private static String lastFileName = "";
    private static int lastFileNameIndex;

    public static void removeToolbarFlags(Activity activity, Toolbar toolbar) {
        if (toolbar != null) {
            final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
            params.setScrollFlags(0);
            toolbar.setLayoutParams(params);
            ((AppCompatActivity) activity).setSupportActionBar(toolbar);
        }
    }

    public static void setToolbarFlags(Activity activity, Toolbar toolbar) {
        if (toolbar != null) {
            final AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
            params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                    | AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
            ((AppCompatActivity) activity).setSupportActionBar(toolbar);
        }
    }

    public static void openShareIntent(@NonNull Context context, @Nullable View itemView, String shareText){
        Intent intent = new Intent(Intent.ACTION_SEND);

        if (itemView != null) {
            try {
                Uri imageUri = getImageUri(context, itemView, "postBitmap.jpeg");
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } catch (IOException e) {
                intent.setType("text/plain");
                e.printStackTrace();
            }
        } else {
            intent.setType("text/plain");
        }

        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        context.startActivity(intent);
    }

    public static void performInviteAction(@NonNull Context context, String userName) {
       // GeneralUtils.openShareIntent(context, null, context.getString(R.string.invite_text)+" https://play.google.com/store/apps/details?id=com.viago&hl=en");


        /*try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Viago");
            String shareMessage= "\nHi, "+ userName +" invited you to join the Viago trip-planning app.\n\n";
            shareMessage = shareMessage + "https://tosto.re/viago" +"\n\n"; //@TODO Find a better way for invitation link
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "choose one"));

        } catch(Exception e) {
            //e.toString();
        }*/

        //@TODO Experimental code
        String invitationMessage = "Hi, "+ userName +" invited you to join the Viago trip-planning app.";

        BranchUniversalObject buo = new BranchUniversalObject()
                .setCanonicalIdentifier("viago_trips")
                .setTitle("Viago")
                .setContentDescription(invitationMessage)
                .setContentImageUrl("https://cdn.branch.io/branch-assets/1570701290039-og_image.png")
                .setContentIndexingMode(BranchUniversalObject.CONTENT_INDEX_MODE.PUBLIC);

        buo.listOnGoogleSearch(context);

        LinkProperties linkProperties = new LinkProperties()
                .setFeature("sharing");
        buo.generateShortUrl(context, linkProperties, new Branch.BranchLinkCreateListener() {
            @Override

            public void onLinkCreate(String url, BranchError error) {
                if (error == null) {
                    Log.i("BRANCH SDK", "got my Branch link to share: " + url);
                    try {
                        Intent shareIntent = new Intent(Intent.ACTION_SEND);
                        shareIntent.setType("text/plain");
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Viago");
                        //String shareMessage= "\nHi, "+ userName +" invited you to join the Viago trip-planning app.\n\n";
                        //shareMessage = shareMessage + "https://tosto.re/viago" +"\n\n";
                        shareIntent.putExtra(Intent.EXTRA_TEXT, url);
                        context.startActivity(Intent.createChooser(shareIntent, "choose one"));

                    } catch(Exception e) {
                        //e.toString();
                    }
                }
            }
        });


    }

    private static Uri getImageUri(Context context, View view, String fileName) throws IOException {
        Bitmap bitmap = loadBitmapFromView(view);
        File pictureFile = new File(context.getExternalCacheDir(), fileName);
        FileOutputStream fos = new FileOutputStream(pictureFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
        fos.close();
        return Uri.parse("file://" + pictureFile.getAbsolutePath());
    }

    private static Bitmap loadBitmapFromView(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            v.setDrawingCacheEnabled(true);
            return v.getDrawingCache();
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    public static void openPlayStoreIntent(@NonNull Context context){
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }else {
            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        }
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    public static void saveBitmap(Context context, @NonNull Bitmap bitmap, @NonNull String fileName) throws IOException {
        if (lastFileName.equals(fileName))
        {
            fileName = lastFileNameIndex++ + fileName;
        } else {
            lastFileName = fileName;
        }

        File pictureFile = new File(GeneralUtils.getRootDir(), fileName);
        saveBitmapToFile(bitmap, pictureFile);
        MediaScannerConnection.scanFile(context, new String[] { pictureFile.getAbsolutePath() }, new String[] { "image/jpeg" }, null);
    }

    public static void saveBitmapTemporary(Activity activity, Bitmap bitmap,String fileName) throws IOException {
        File pictureFile = new File(activity.getExternalFilesDir(null), fileName);
        saveBitmapToFile(bitmap, pictureFile);
        pictureFile.deleteOnExit();
    }

    public static void saveBitmapToFile(Bitmap bitmap, File pictureFile) throws IOException {
        FileOutputStream fos = new FileOutputStream(pictureFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);
        fos.close();
    }

    public static CharSequence timeDiff(String date) {


       /* DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        DateFormat localeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        localeFormat.setTimeZone(TimeZone.getDefault());

        try {
            Date timeStamp = utcFormat.parse(date);
            date = localeFormat.format(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
*/
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault());
        Date startDate = new Date();
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        try {
            startDate = simpleDateFormat.parse(date);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        return DateUtils.getRelativeTimeSpanString( startDate.getTime(), calendar.getTimeInMillis(), DateUtils.SECOND_IN_MILLIS);

//        long duration = startDate.getTime() - endDate.getTime();
//
//        long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);
//        long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
//        long diffInMins = TimeUnit.MILLISECONDS.toMinutes(duration);
//        long diffInSecs = TimeUnit.MILLISECONDS.toSeconds(duration);
//
//        if( diffInDays > 2 ) {
//            diff = "posted on " + startDate.getTime();
//        } else if( diffInDays == 1 ) {
//            diff = "posted yesterday";
//        } else if( diffInHours >= 1 ) {
//            diff = "posted " + diffInHours + "h ago";
//        } else if( diffInMins >= 1) {
//            diff = "posted " + diffInMins + "m ago";
//        } else if( diffInSecs >= 1) {
//            diff = "posted " + diffInSecs + "s ago";
//        } else {
//            diff = "posted right now";
//        }
//        return diff;
    }

    public static void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(500);
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                textview.setText(valueAnimator.getAnimatedValue().toString());

            }
        });
        valueAnimator.start();

    }

    public static File getRootDir(){
        File root=new File(Environment.getExternalStorageDirectory() +
                File.separator + "Chirpy");
        root.mkdirs();
        return root;
    }

    public static void enableBroadcastReceiver(Context context, Class mClass){
        Log.d("receiver", "enable");
        ComponentName receiver = new ComponentName(context, mClass);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    public static void disableBroadcastReceiver(Context context, Class mClass){
        Log.d("receiver", "disable");
        ComponentName receiver = new ComponentName(context, mClass);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }


    public static Fragment getCurrentFragment(AppCompatActivity activity){
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() == 0) {
            return null;
        }
        String tag = fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1).getName();
        return fragmentManager.findFragmentByTag(tag);
    }

}
