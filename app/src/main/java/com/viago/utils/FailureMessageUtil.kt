package com.viago.utils

import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by mayank on 8/9/17.
 */

public fun getFailureMessage(errorBody: ResponseBody?, defaultText: String): String {
    try {
        JSONObject(errorBody?.string()).run {
            return (get(keys().next()) as JSONArray).get(0) as String
        }
    } catch (e: Exception) {
        return defaultText
    }
}
