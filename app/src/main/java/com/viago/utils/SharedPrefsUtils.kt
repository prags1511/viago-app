package com.viago.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.text.TextUtils

/**
 * A pack of helpful getter and setter methods for reading/writing to [SharedPreferences].
 */
object SharedPrefsUtils {

    private lateinit var preferences: SharedPreferences

    public fun init(context: Context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context)
    }


    /**
     * Helper method to retrieve a String value from [SharedPreferences].

     * @param key
     * *
     * @return The value from shared preferences, or null if the value could not be read.
     */
    fun getStringPreference(key: String): String? = preferences.getString(key, null)

    /**
     * Helper method to write a String value to [SharedPreferences].

     * @param key
     * *
     * @param value
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun setStringPreference(key: String, value: String): Boolean = if (!TextUtils.isEmpty(key)) preferences.edit().putString(key, value).commit() else false

    /**
     * Helper method to retrieve a float value from [SharedPreferences].

     * @param key
     * *
     * @param defaultValue A default to return if the value could not be read.
     * *
     * @return The value from shared preferences, or the provided default.
     */
    fun getFloatPreference(key: String, defaultValue: Float): Float = preferences.getFloat(key, defaultValue)

    /**
     * Helper method to write a float value to [SharedPreferences].

     * @param key
     * *
     * @param value
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun setFloatPreference(key: String, value: Float): Boolean = preferences.edit().putFloat(key, value).commit()

    /**
     * Helper method to retrieve a long value from [SharedPreferences].

     * @param key
     * *
     * @param defaultValue A default to return if the value could not be read.
     * *
     * @return The value from shared preferences, or the provided default.
     */
    fun getLongPreference(key: String, defaultValue: Long): Long = preferences.getLong(key, defaultValue)

    /**
     * Helper method to write a long value to [SharedPreferences].

     * @param key
     * *
     * @param value
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun setLongPreference(key: String, value: Long): Boolean = preferences.edit().putLong(key, value).commit()

    /**
     * Helper method to retrieve an integer value from [SharedPreferences].

     * @param key
     * *
     * @param defaultValue A default to return if the value could not be read.
     * *
     * @return The value from shared preferences, or the provided default.
     */
    fun getIntegerPreference(key: String, defaultValue: Int): Int = preferences.getInt(key, defaultValue)

    /**
     * Helper method to write an integer value to [SharedPreferences].

     * @param key
     * *
     * @param value
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun setIntegerPreference(key: String, value: Int): Boolean = preferences.edit().putInt(key, value).commit()

    /**
     * Helper method to retrieve a boolean value from [SharedPreferences].

     * @param key
     * *
     * @param defaultValue A default to return if the value could not be read.
     * *
     * @return The value from shared preferences, or the provided default.
     */
    fun getBooleanPreference(key: String, defaultValue: Boolean): Boolean = preferences.getBoolean(key, defaultValue)

    /**
     * Helper method to write a boolean value to [SharedPreferences].

     * @param key
     * *
     * @param value
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun setBooleanPreference(key: String, value: Boolean): Boolean = preferences.edit().putBoolean(key, value).commit()

    /**
     * Helper method to remove a key from [SharedPreferences].

     * @param key
     * *
     * @return true if the new value was successfully written to persistent storage.
     */
    fun removePreference(key: String): Boolean = if (!TextUtils.isEmpty(key)) preferences.edit().remove(key).commit() else false

    fun contains(key: String): Boolean = preferences.contains(key)

    fun clear() {
        preferences.edit().clear().apply()
    }
}