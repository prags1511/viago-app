package com.viago.retrofitservice


import com.viago.models.Contact

import org.json.JSONObject

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

interface UserService {

    @POST("/users/contacts/")
    fun postContacts(@Header("Authorization") apiKey: String,
                     @Body contactsList: List<Contact>): Call<JSONObject>
}
