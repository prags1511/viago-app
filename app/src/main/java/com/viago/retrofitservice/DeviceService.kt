package com.viago.retrofitservice

import com.viago.models.Gcm
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by mayank on 01/12/16.
 */

interface DeviceService {

    @POST("/spring/device/gcm/")
    fun registerGcm(@Header("Authorization") apiKey: String,
                    @Body gcm: Gcm): Call<Gcm>

    @PUT("/spring/device/gcm/{registration_id}")
    fun updateGcm(@Header("Authorization") apiKey: String,
                  @Path("registration_id") registrationId: String,
                  @Body gcm: Gcm): Call<Gcm>
}
