package com.viago.bookvehicle

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.inflate
import com.viago.extensions.loadImage

/**
 * Created by mayank on 16/8/17.
 */
class BookVehicleAdapter : EasyRecyclerViewAdapter<BookVehicle>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BookVehicleViewHolder(parent.inflate(R.layout.item_book_vehicle))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder, bookVehicle: BookVehicle, position: Int) {
        if (holder is BookVehicleViewHolder) holder.bind(bookVehicle)
    }

    inner class BookVehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val image by bindView<ImageView>(R.id.image)
        private val name by bindView<TextView>(R.id.name)

        internal fun bind(bookVehicle: BookVehicle) {
            image.loadImage(bookVehicle.imageUrl)
            name.text = "Book a ${bookVehicle.name.capitalize()}"
        }
    }
}