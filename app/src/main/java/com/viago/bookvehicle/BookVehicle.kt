package com.viago.bookvehicle

/**
 * Created by mayank on 16/8/17.
 */
data class BookVehicle(
        val name: String,
        val imageUrl: String
)