package com.viago.bookvehicle

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.utils.SpacesItemDecoration

/**
 * Created by mayank on 16/8/17.
 */
class BookFragment : BaseFragment() {

    private val recyclerView by bindView<RecyclerView>(R.id.recycler_view)

    override fun getLayoutRes(): Int = R.layout.recycler_view

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        recyclerView.run {
            layoutManager = LinearLayoutManager(context)
            adapter = BookVehicleAdapter().apply {
                addItemsAtBottom(listOf(
                        BookVehicle("Car", "https://tinyurl.com/y7hd7hoq"),
                        BookVehicle("Train", "https://tinyurl.com/y7hd7hoq"),
                        BookVehicle("Cab", "https://tinyurl.com/y7hd7hoq"),
                        BookVehicle("Flight", "https://tinyurl.com/y7hd7hoq"),
                        BookVehicle("Bus", "https://tinyurl.com/y7hd7hoq")
                ))
            }
            setHasFixedSize(true)
            addItemDecoration(SpacesItemDecoration(dpToPx(4)))
        }
    }
}