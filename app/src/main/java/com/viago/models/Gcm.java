package com.viago.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mayank on 01/12/16.
 */

public class Gcm {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("registration_id")
    @Expose
    private String registrationId;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;

    private String cloud_message_type = "FCM";

    public Gcm(Boolean active, String deviceId, String registrationId) {
        this.active = active;
        this.deviceId = deviceId;
        this.registrationId = registrationId;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The registrationId
     */
    public String getRegistrationId() {
        return registrationId;
    }

    /**
     * @param registrationId The registration_id
     */
    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The device_id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The active
     */
    public Boolean getActive() {
        return active;
    }

    /**
     * @param active The active
     */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
     * @return The dateCreated
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated The date_created
     */
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

}