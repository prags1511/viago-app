package com.viago.notifications

import com.viago.models.BaseListModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Url

/**
 * Created by mayank on 7/9/17.
 */
internal interface NotificationsService {

    @GET("trips/notifications/")
    fun getNotifications(@Header("Authorization") token: String): Call<BaseListModel<Notification>>

    @GET()
    fun getMoreNotifications(@Header("Authorization") token: String, @Url nextUrl: String): Call<BaseListModel<Notification>>
}