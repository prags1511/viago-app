package com.viago.notifications

/**
 * Created by mayank on 29/7/17.
 */
data class Notification(val title: String, val image: String?, val trip: String, val detail: String, val created_on: String)