package com.viago.notifications

import android.util.Log
import com.viago.models.BaseListModel
import com.viago.utils.ApiUtils
import com.viago.utils.getFailureMessage
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by mayank on 8/9/17.
 */
object NotificationsUtil {

    private val notificationsService = ApiUtils.retrofitInstance.create(NotificationsService::class.java)

    fun getMyNotifications(token: String, onSuccess: (BaseListModel<Notification>) -> Unit, onFailure: (String) -> Unit) {
        getNotifications(notificationsService.getNotifications(token), onSuccess, onFailure);
    }

    fun getMoreNotifications(token: String, nextUrl: String, onSuccess: (BaseListModel<Notification>) -> Unit, onFailure: (String) -> Unit) {
        getNotifications(notificationsService.getMoreNotifications(token, nextUrl), onSuccess, onFailure)
    }

    private fun getNotifications(getNotificationsCall: Call<BaseListModel<Notification>>, onSuccess: (BaseListModel<Notification>) -> Unit, onFailure: (String) -> Unit) {
        getNotificationsCall
                .enqueue(object: Callback<BaseListModel<Notification>> {
                    override fun onResponse(call: Call<BaseListModel<Notification>>?, response: Response<BaseListModel<Notification>>?) {
                        if (response?.isSuccessful == true) {
                            onSuccess(response.body())
                        } else {
                         //   onFailure(getFailureMessage(response?.errorBody(), "Cannot get notifications. Please try again"))
                        }
                    }

                    override fun onFailure(call: Call<BaseListModel<Notification>>?, t: Throwable?) {
                        onFailure("Cannot get notifications. Please try again")
                    }
                })
    }
}