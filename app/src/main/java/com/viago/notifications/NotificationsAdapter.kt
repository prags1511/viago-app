package com.viago.notifications

import android.graphics.Color
import androidx.recyclerview.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import bindView
import com.viago.R
import com.viago.baseclasses.EasyRecyclerViewAdapter
import com.viago.extensions.loadImage
import com.viago.utils.Constants
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.TimeFormatHelper
import com.viago.views.CircularImageView

/**
 * Created by mayank on 29/7/17.
 */
class NotificationsAdapter(val onItemClicked: (tripId: String) -> Unit) : EasyRecyclerViewAdapter<Notification>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return NotificationsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_notification, parent, false))
    }

    override fun onBindItemView(holder: RecyclerView.ViewHolder?, item: Notification, position: Int) {
        if (holder is NotificationsViewHolder) {
            holder.bind(item)
        }
    }

    inner class NotificationsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image by bindView<CircularImageView>(R.id.image)
        val tripName by bindView<TextView>(R.id.trip_name)
        val time by bindView<TextView>(R.id.time)
        val status by bindView<TextView>(R.id.status)
      //  val statusline by bindView<View>(R.id.statusline)
        val grayedout by bindView<View>(R.id.grayedout)


        init {
            itemView.setOnClickListener {
                val notification = getItem(adapterPosition)
                SharedPrefsUtils.setBooleanPreference(Constants.KEY_NOTIFICATION_READ + notification.trip+notification.created_on, true)
                if (!TextUtils.isEmpty(getItem(adapterPosition).trip)) onItemClicked(notification.trip)

            }
        }

        fun bind(notification: Notification) {
            image.loadImage(notification.image)
            tripName.text = notification.title;
            status.text = notification.detail
            time.text = TimeFormatHelper.getRelativeTime(notification.created_on)

            if (SharedPrefsUtils.getBooleanPreference(
                            Constants.KEY_NOTIFICATION_READ +
                                    notification.trip+notification.created_on, false)){

                grayedout.visibility = View.VISIBLE

             //   statusline.setBackgroundColor(Color.parseColor("#f5f5f5"))
            }else {
                grayedout.visibility = View.GONE
               // statusline.se = tBackgroundColor(Color.parseColor("#F9666C"))
            }



//            status.setCompoundDrawablesWithIntrinsicBounds(if (SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + notification.trip+notification.created_on, false)) 0 else R.drawable.unread_indicater,
//                    0, 0, 0)
        }
    }
}