package com.viago.notifications

import android.app.ProgressDialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.appcompat.widget.Toolbar
import android.text.TextUtils
import android.view.Menu
import android.view.View
import bindView
import com.viago.R
import com.viago.baseclasses.BaseDrawerActivity
import com.viago.baseclasses.BaseFragment
import com.viago.extensions.dpToPx
import com.viago.extensions.hide
import com.viago.extensions.show
import com.viago.extensions.showToast
import com.viago.login.UserUtil
import com.viago.models.BaseListModel
import com.viago.profile.MediaDBUtil
import com.viago.trips.TripsUtil
import com.viago.trips.tripdetails.TripDetailsActivity
import com.viago.utils.Constants
import com.viago.utils.EndlessRecyclerViewScrollListener
import com.viago.utils.SharedPrefsUtils
import com.viago.utils.SpacesItemDecoration

/**
 * Created by mayank on 29/7/17.
 */
//SupportActivity: BaseDrawerActivity()
class NotificationsFragment : BaseDrawerActivity() {

    //private val toolbar by bindView<Toolbar>(R.id.toolbar)
    private val recyclerView: RecyclerView by bindView(R.id.recycler_view)
    private val noNotificationsFoundContainer by bindView<View>(R.id.no_notifications_found_container)

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this@NotificationsFragment).apply {
            setMessage("Loading Trip...")
            isIndeterminate = true
            setCancelable(false)
        }
    }

    private val notificationsAdapter: NotificationsAdapter by lazy {
        NotificationsAdapter { tripId: String ->
            setUnreadNotificationCount()
            progressDialog.show()
            TripsUtil.getTripDetails(userToken, tripId.toLong(), {
                progressDialog.dismiss()
                startActivity(TripDetailsActivity.getIntent(this@NotificationsFragment, it, true))
            }, {
                progressDialog.dismiss()
                showToast(it)
            })
        }
    }

    override fun onResume() {
        super.onResume()
        notificationsAdapter.notifyDataSetChanged();

    }

    private val userToken by lazy { UserUtil.getUserToken() }
    private var nextUrl: String? = null

    @LayoutRes
    override fun getLayoutRes(): Int = R.layout.fragment_notifications

    override fun onCreated(savedInstanceState: Bundle?)  {

       // setHasOptionsMenu(true)

        with(toolbar) {
            setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
            title = ""
         //   getAppCompatActivity().setSupportActionBar(this)
            setNavigationOnClickListener { onBackPressed() }
        }

        val onSuccess = { baseListModel: BaseListModel<Notification> ->
            notificationsAdapter.addItemsAtBottom(baseListModel.results)
            nextUrl = baseListModel.next
            setUnreadNotificationCount()

            if (nextUrl.isNullOrEmpty()) {
                recyclerView.clearOnScrollListeners()
            }
            if (baseListModel.count == 0) {
                recyclerView.hide()
                noNotificationsFoundContainer.show()
            }
        }

        val onFailure: (String) -> Unit = {
            showToast(it)
        }


        with(recyclerView) {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            addItemDecoration(SpacesItemDecoration(dpToPx(4)))
            adapter = notificationsAdapter
            addOnScrollListener(object : EndlessRecyclerViewScrollListener(layoutManager as LinearLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    nextUrl?.let { NotificationsUtil.getMoreNotifications(userToken, it, onSuccess, onFailure) }
                }
            })
        }

        NotificationsUtil.getMyNotifications(userToken, onSuccess, onFailure)
    }

    private fun setUnreadNotificationCount() {
       /* var unreadCount: Int = 0
        notificationsAdapter.itemsList.forEach {
            if (!SharedPrefsUtils.getBooleanPreference(Constants.KEY_NOTIFICATION_READ + it.trip+it.created_on, false))
                unreadCount++
        }
        SharedPrefsUtils.setIntegerPreference(Constants.KEY_NOTIFICATION_COUNT, unreadCount)*/
    }

//    override fun onPrepareOptionsMenu(menu: Menu?) {
//        menu?.clear()
//        super.onPrepareOptionsMenu(menu)
//    }
}